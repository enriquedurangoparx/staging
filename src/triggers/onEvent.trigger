/**
* Apex Trigger for Event.
*
* @author: <Egor Markuzov> (egor.markuzov@colliers.com)
*
* @history:
* version		    | author				                                | changes
* ====================================================================================
* 0.1 05.11.2019	| Egor Markuzov (egor.markuzov@colliers.com)	| initial version.
*/
trigger onEvent on Event (before insert, before update, before delete, after insert, after update, after delete) {
    new ActivityTrigger().execute();
}