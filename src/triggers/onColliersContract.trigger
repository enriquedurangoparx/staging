/**
 * Created by ille on 2019-05-29.
 */

trigger onColliersContract on ColliersContract__c (before insert, before update, before delete,
        after insert, after update, after delete)
{
    new ColliersContractTrigger().execute();
}