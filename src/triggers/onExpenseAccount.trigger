/**
 * Created by ille on 2019-05-10.
 */

trigger onExpenseAccount on ExpenseAccount__c (before insert, before update, before delete, after insert, after update, after delete)
{
    new ExpenseAccountTrigger().execute();
}