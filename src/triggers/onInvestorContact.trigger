/**
* Apex Trigger for ContactToInvestor__c.
*
* @author: <Egor Markuzov> (egor.markuzov@parx.com)
*
* @history:
* version		    | author				                                | changes
* ====================================================================================
* 0.1 31.12.2019	| Egor Markuzov (egor.markuzov@parx.com)	            | initial version.
*/
trigger onInvestorContact on ContactToInvestor__c (before insert, before update, before delete, after insert, after update, after delete) {
    new ContactToInvestorTrigger().execute();
}