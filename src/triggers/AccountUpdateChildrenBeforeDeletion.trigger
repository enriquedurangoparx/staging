trigger AccountUpdateChildrenBeforeDeletion on Account (before delete) {
    //catch all deleted to set
    Set<Id> delIds = new Set<Id>();
    for (Account delObj : Trigger.Old){
        delIds.add(delObj.Id);
    }
    
    //query children records
    List<Account> children = [SELECT id FROM Account WHERE ParentId IN : delIds];
    
    //change the updated value
    for(Account child : children){
        child.ParentId = null;
        child.HierarchyIsUpdated__c = false;
    }
    //bulk update children records
    update children;
}