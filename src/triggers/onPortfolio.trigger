/**
* Apex Trigger for Portfolio__c records.
*
* @author: <Maksim Fedorenko> (maksim.fedorenko@parx.com)
*
* @history:
* version		    | author				                                | changes
* ====================================================================================
* 0.1 31.01.2020	| Maksim Fedorenko (maksim.fedorenko@parx.com)          | initial version.
*/
trigger onPortfolio on Portfolio__c (before insert, before update, before delete, after insert, after update, after delete)
{
    new PortfolioTrigger().execute();
}