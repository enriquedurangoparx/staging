/**
 *  @descriptionTrigger handlings on KeyAccountManager__c object
 *  @author ikl
 *  @copyright Parx
*/
trigger KeyAccountManagerTrigger on KeyAccountManager__c (after insert, after update, after delete)
{
	new TriggerTemplate.TriggerManager(new KeyAccountManagerHandler()).runHandlers();
}