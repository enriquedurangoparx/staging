/**
 * Created by ille on 2019-05-08.
 */

trigger onOpportunity on Opportunity (before insert, before update, before delete, after insert, after update, after delete)
{
    OpportunityTrigger handler = new OpportunityTrigger();
    handler.disableForUserWithPermission(ATrigger.CAN_SKIP_TRIGGERS_PERMISSION_NAME);
    handler.execute();
}