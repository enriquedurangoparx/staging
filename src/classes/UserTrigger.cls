/**
 * Created by dyas on 2019-05-31.
 */

public with sharing class UserTrigger extends ATrigger
{

    public override void hookAfterUpdate(List<SObject> records, List<SObject> oldRecords)
    {

        List<User> users = (List<User>)records;
        List<User> oldUsers = (List<User>)oldRecords;

        Map<Id, User> usersWithDelegatedApproverMap = new Map<Id, User>();
        Map<Id, Id> userIdOldDelegatedApproverIdMap = new Map<Id, Id>();

        for (Integer i = 0; i < users.size(); i++)
        {

            User user = users[i];
            User oldUser = oldUsers[i];

            if (user.DelegatedApproverId != oldUser.DelegatedApproverId) 
            {

                if (user.DelegatedApproverId != null) 
                {
                    usersWithDelegatedApproverMap.put(user.Id, user);
                }
                else 
                {
                    userIdOldDelegatedApproverIdMap.put(user.Id, oldUser.DelegatedApproverId);
                }

            }

        }

        if (!usersWithDelegatedApproverMap.isEmpty()) 
        {
            OpportunityHandler.setOpportunityDelegatedOwner(usersWithDelegatedApproverMap);
        }

        if (!userIdOldDelegatedApproverIdMap.isEmpty()) 
        {
            OpportunityHandler.clearAndUpdateOpportunityDelegatedOwner(userIdOldDelegatedApproverIdMap);
        }

    }

}