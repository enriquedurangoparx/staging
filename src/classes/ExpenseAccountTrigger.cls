/**
 * Created by ille on 2019-05-10.
 */

public with sharing class ExpenseAccountTrigger extends ATrigger
{
    private static final Map<String, String> MONTH_STRING_TO_MONTH_DIGITS_MAPPING = new Map<String, String>{
        'January' => '01',
        'February' => '02',
        'March' => '03',
        'April' => '04',
        'May' => '05',
        'June' => '06',
        'July' => '07',
        'August' => '08',
        'September' => '09',
        'October' => '10',
        'November' => '11',
        'December' => '12'
    };

    public override void hookBeforeDelete(SObject record)
    {
        validateDeletion((ExpenseAccount__c) record);
    }

    public override void hookBeforeInsert(List<SObject> records)
    {
        populateName((List<ExpenseAccount__c>) records);
    }

    public override void hookBeforeUpdate(List<SObject> records, List<SObject> oldRecords)
    {
        populateName((List<ExpenseAccount__c>) records);
    }

    /**
     * Prevents deletion of the record as soon the status is not "Open"
     */
    private void validateDeletion(ExpenseAccount__c expenseAccount)
    {
        Exceptions.assert(expenseAccount.Status__c == 'Open',
            new Exceptions.GenericException(Label.NonOpenExpenseAccountDeleteError));
    }

    /**
     * Populates the Name of the Expense Account to Year + Month + Employee Name of the Expense Account.
     * Make sure the month is written with a trailing zero in case that it is single digit. Create Unit Tests. 
     */
    private void populateName(List<ExpenseAccount__c> expenseAccounts)
    {
        Map<Id, User> expenseKeeperMap = new Map<Id, User>(selectExpenseKeepers(expenseAccounts));
        for (ExpenseAccount__c expenseAccount : expenseAccounts)
        {
            User expenseKeeper = expenseKeeperMap.get(expenseAccount.ExpenseKeeper__c);
            populateName(expenseAccount, expenseKeeper);
        }
    }

    /**
     * Populates the Name of the Expense Account to Year + Month + Employee Name of the Expense Account.
     * Make sure the month is written with a trailing zero in case that it is single digit. Create Unit Tests. 
     */
    private void populateName(ExpenseAccount__c expenseAccount, User expenseKeeper)
    {
        expenseAccount.Name = '' + expenseAccount.Year__c + '-' + MONTH_STRING_TO_MONTH_DIGITS_MAPPING.get(expenseAccount.Month__c) +
            + ' ' + expenseKeeper.Name;
    }

    private List<User> selectExpenseKeepers(List<ExpenseAccount__c> expenseAccounts)
    {
        List<Id> expenseKeeperIDs = new List<Id>();
        for (ExpenseAccount__c expenseAccount : expenseAccounts)
        {
            expenseKeeperIDs.add(expenseAccount.ExpenseKeeper__c);
        }

        return [select Name from User where Id in :expenseKeeperIDs];
    }
}