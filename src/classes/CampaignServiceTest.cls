/**
*	Test for CampaignService.
*	@author npo
*	@copyright PARX
*/
@IsTest
private class CampaignServiceTest
{
    @IsTest
    static void testDeactivateCampaign()
    {
        Campaign c = new Campaign(IsActive = true, Name = 'Test');
        insert c;


        Test.startTest();
        CampaignService.deactivateCampaign(c.Id, true);
        Test.stopTest();

        System.assertEquals(false, [SELECT IsActive FROM Campaign WHERE Id = :c.Id].IsActive,
                                    'We expect campaign to be deactivated');
    }

    @IsTest
    static void testCreateCampaignMemberStatus()
    {
        Campaign c = new Campaign(IsActive = true, Name = 'Test');
        insert c;

        Test.startTest();
        CampaignService.createCampaignMemberStatus(c.Id, new List<String> {'Sent1', 'Open1'});
        System.assertEquals(2, [SELECT Id FROM CampaignMemberStatus WHERE Label IN ('Open1', 'Sent1')].size(),
                                'We expect statuses to exist.');

        Test.stopTest();
    }
}