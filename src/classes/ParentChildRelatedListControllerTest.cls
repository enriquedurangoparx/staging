/**
Test class for ParentChildRelatedListController.cls
*
@author: <Christian Schwabe> (Christian.Schwabe@colliers.com)
*
* @history:
* version		    | author				                                | changes
* ====================================================================================
* 0.1 03.03.2020	| Christian Schwabe (Christian.Schwabe@colliers.com)    | [SFINV/US 2204] Test getFlatHierarchy. Initial version.
* 0.2 17.03.2020	| Christian Schwabe (Christian.Schwabe@colliers.com)    | [SFINV/US 2407] Added Salutation for Contact.
*/
@IsTest
private class ParentChildRelatedListControllerTest
{
    @IsTest
    private static void testGetFlatHierarchy()
    {
        Account account = new Account();
        account.Name = 'Test GmbH';
        insert account;

        Contact contact = new Contact();
        contact.AccountId = account.Id;
        contact.FirstName = 'Max';
        contact.LastName = 'Müller';
        contact.Salutation = 'Mr.';
        contact.Email = 'email@info.de';
        contact.Phone = '+49 69 / 719192-5056';
        insert contact;

        Area__c area = new Area__c();
        area.Name = 'Areal Name';
        insert area;

        AccountToArea__c accountToArea = new AccountToArea__c();
        accountToArea.Account__c = account.Id;
        accountToArea.Area__c = area.Id;
        insert accountToArea;

        ContactToAccountToArea__c contactToAccountToArea = new ContactToAccountToArea__c();
        contactToAccountToArea.AccountToArea__c = accountToArea.Id;
        contactToAccountToArea.Contact__c = contact.Id;
        contactToAccountToArea.Role__c = 'Asset-Manager';
        insert contactToAccountToArea;

        ParentChildRelatedListController.CustomMap accountName = new ParentChildRelatedListController.CustomMap();
        accountName.key = 'Account__r.Name';
        accountName.value = 'accountName';
        ParentChildRelatedListController.CustomMap accountId = new ParentChildRelatedListController.CustomMap();
        accountId.key = 'Account__c';
        accountId.value = 'accountId';
        
        ParentChildRelatedListController.CustomMap contactId = new ParentChildRelatedListController.CustomMap();
        contactId.key = 'Contact__c';
        contactId.value = 'contactId';
        ParentChildRelatedListController.CustomMap contactFirstName = new ParentChildRelatedListController.CustomMap();
        contactFirstName.key = 'Contact__r.FirstName';
        contactFirstName.value = 'contactFirstName';
        ParentChildRelatedListController.CustomMap contactLastName = new ParentChildRelatedListController.CustomMap();
        contactLastName.key = 'Contact__r.LastName';
        contactLastName.value = 'contactLastName';
        ParentChildRelatedListController.CustomMap contactPhone = new ParentChildRelatedListController.CustomMap();
        contactPhone.key = 'Contact__r.Phone';
        contactPhone.value = 'contactPhone';
        ParentChildRelatedListController.CustomMap contactEmail = new ParentChildRelatedListController.CustomMap();
        contactEmail.key = 'Contact__r.Email';
        contactEmail.value = 'contactEmail';
        ParentChildRelatedListController.CustomMap contactRole = new ParentChildRelatedListController.CustomMap();
        contactRole.key = 'Role__c';
        contactRole.value = 'contactRole';

        List<ParentChildRelatedListController.ParentAndChild> listOfParentAndChild;
        Test.startTest();
            listOfParentAndChild = ParentChildRelatedListController.getFlatHierachy(
                area.Id,
                'Contacts_To_Account_Area__r',
                'Area__c',
                new List<String>{ 'Account__c, Account__r.Name' },
                new List<String>{ 'Contact__c', 'Contact__r.LastName', 'Contact__r.FirstName', 'Contact__r.Phone', 'Contact__r.Email', 'toLabel(Role__c)' },
                'AccountToArea__c',
                new List<ParentChildRelatedListController.CustomMap>{ accountName, accountId },
                new List<ParentChildRelatedListController.CustomMap>{ contactId, contactFirstName, contactLastName, contactPhone, contactEmail, contactRole }
            );
        Test.stopTest();

        System.assertEquals(1, listOfParentAndChild.size(), 'Only one record should be returned.');
        ParentChildRelatedListController.ParentAndChild parentAndChild = listOfParentAndChild.get(0);
        System.assert(String.isNotBlank(parentAndChild.accountId), 'Missing parameter in field accountId.');
        System.assert(String.isNotBlank(parentAndChild.accountName), 'Missing parameter in field accountName.');

        System.assert(String.isNotBlank(parentAndChild.contactId), 'Missing parameter in field contactId.');
        System.assert(String.isNotBlank(parentAndChild.contactEmail), 'Missing parameter in field contactEmail.');
        System.assert(String.isNotBlank(parentAndChild.contactFirstName), 'Missing parameter in field contactFirstName.');
        System.assert(String.isNotBlank(parentAndChild.contactLastName), 'Missing parameter in field contactLastName.');
        System.assert(String.isNotBlank(parentAndChild.contactPhone), 'Missing parameter in field contactPhone.');
        System.assert(String.isNotBlank(parentAndChild.contactRole), 'Missing parameter in field contactRole.');
        System.assert(String.isNotBlank(parentAndChild.key), 'Missing parameter in field key.');
    }
}