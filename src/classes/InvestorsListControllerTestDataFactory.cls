/**
 * @author jens.siffermann@youperience.com
 * @date 2019-10-29
 * @version 1.0.0
 *
 * @description InvestorsList test data factory
 * 
 * * 0.2 17.03.2020	| Christian Schwabe (Christian.Schwabe@colliers.com)    | [SFINV/US 2407] Added Saluation for Contact.
 */
@isTest
public with sharing class InvestorsListControllerTestDataFactory {
    /**
     * create simple account
     * @return   Account
     */
    public static Account createAccount() {
        Account a = new Account(Name = 'TestAccount');
        insert a;

        return a;
    }

    /**
     * create account with contacts
     * @param       numberOfContacts number of contacts to create
     * @return      return account with generated related contacts
     */
    public static Account createAccountWithContacts(Integer numberOfContacts) {
        Account a = createAccount();

        List<Contact> testContacts = new List<Contact>();
        for (Integer i = 0; i < numberOfContacts; i++) {
            testContacts.add(
                new Contact(
                    FirstName = 'John',
                    LastName = 'Doe-' + i,
                    Salutation = 'Mr.',
                    AccountId = a.Id
                )
            );
        }

        insert testContacts;

        return a;
    }

    /**
     * create account with contacts
     * @param       account
     * @param       numberOfContacts number of contacts to create
     * @return      List<Contact>
     */
    public static List<Contact> createContactsForAccount(Account account, Integer numberOfContacts) {

        List<Contact> testContacts = new List<Contact>();
        for (Integer i = 0; i < numberOfContacts; i++) {
            testContacts.add(
                new Contact(
                    FirstName = 'John',
                    LastName = 'Doe-' + i,
                    AccountId = account.Id
                )
            );
        }

        insert testContacts;

        return testContacts;
    }

    /**
     * create contact with related account
     * @return   return contact with account
     */
    public static Contact createContact() {
        Account a = createAccount();
        Contact c = new Contact(
            FirstName = 'John',
            LastName = 'Doe',
            Salutation = 'Mr.',
            AccountId = a.Id
        );

        insert c;

        return c;
    }

    /**
     * create Investor - AccountToOpportunity__c
     * @return   return AccountToOpportunity__c
     */
    public static AccountToOpportunity__c createInvestor() {
        // create Account
        Account account = createAccountWithContacts(10);

        // create opportunity
        Opportunity prebuildOpportunity = UnitTestDataTest.buildOpportunity('TestOpportunity');


        PropertyObject__c propertyObject = TestDataFactory.buildPropertyObject('property', true);
        Opportunity opportunity = TestDataFactory.createOpportunity(account.Id, propertyObject);
        insert opportunity;

        AccountToOpportunity__c investor = new AccountToOpportunity__c();
        investor.Opportunity__c = opportunity.Id;
        investor.Account__c = account.Id;

        insert investor;

        return investor;
    }

    /**
     * initialize Products for opportunity creation
     */
    public static void initializeProducts(){
        Product2 product = new Product2();
        product.Name = 'Sonstiges';
        product.IsActive = true;
        product.Family = 'Cost Transfer';
        product.ProductCode = 'Other';
        product.CanUseRevenueSchedule = true;
        product.RevenueScheduleType = 'Repeat';
        product.NumberOfRevenueInstallments = 1;
        product.RevenueInstallmentPeriod = 'Daily';
        insert product;

        Pricebook2 valuationPricebook = new Pricebook2();
        valuationPricebook.Name = 'Valuation';
        valuationPricebook.IsActive = true;
        insert valuationPricebook;

        PricebookEntry standardPricebookEntry = new PricebookEntry();
        standardPricebookEntry.IsActive = true;
        standardPricebookEntry.Pricebook2Id = Test.getStandardPricebookId();
        standardPricebookEntry.Product2Id = product.Id;
        standardPricebookEntry.UnitPrice = 100.00;
        insert standardPricebookEntry;

        PricebookEntry valuationPricebookEntry = new PricebookEntry();
        valuationPricebookEntry.IsActive = true;
        valuationPricebookEntry.Pricebook2Id = valuationPricebook.Id;
        valuationPricebookEntry.Product2Id = product.Id;
        valuationPricebookEntry.UnitPrice = 100.00;
        valuationPricebookEntry.UseStandardPrice = true;
        insert new List<PricebookEntry>{ valuationPricebookEntry };
    }

    public static Id createInvestorRelatedRecords() {
        AccountToOpportunity__c investor = createInvestor();

        Event event = new Event();
        event.Subject = 'Test Event';
        event.Type = 'Email';
        event.WhatId = investor.Id;
        event.StartDateTime = System.now();
        event.EndDateTime = System.now().addHours(1);
        insert event;

        Task task = new Task();
        task.Subject = 'Test Task';
        task.Status = 'New';
        task.Priority = 'Normal';
        task.WhatId = investor.Id;
        insert task;

        List<Contact> contacts = [SELECT Id FROM Contact WHERE AccountId = :investor.Account__c];

        ContactToInvestor__c cti = new ContactToInvestor__c();
        cti.Contact__c = contacts.get(0).Id;
        cti.Investor__c = investor.Id;
        insert cti;

        Status__c status = new Status__c();
        status.Investor__c = investor.Id;
        status.InvestorStatus__c = 'Offer';
        insert status;

        return investor.Id;
    }

    public static Task createTask(Id whatId, String status, String subject, Date customDate, Decimal customAmount, String customStatus)
    {
        Task newTask = new Task(
            Subject = subject,
            Status = status,
            Priority = 'Normal',
            WhatId = whatId,
            ActivityDate = customDate,
            Amount__c = customAmount,
            InvestorStatus__c = customStatus
        );

        return newTask;
    }
}