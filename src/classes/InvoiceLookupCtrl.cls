/**
 * Created by dyas on 2019-06-13.
 */
 
public without sharing class InvoiceLookupCtrl
{

    @AuraEnabled
    public static List<Map<String, String>> getInvoiceOptionsByNumber(String invoiceNumber)
    {
        
        List<Map<String, String>> optionsMapList = new List<Map<String, String>>();
        
        for (ONB2__Invoice__c invoice : [
                SELECT Id, Name, ONB2__Account__r.Name 
                FROM ONB2__Invoice__c
                WHERE Name LIKE : '%' + invoiceNumber + '%'
                LIMIT 50000
            ])
        {

            Map<String, String> optionsMap = new Map<String, String>();
            optionsMap.put('label', invoice.Name + ' - ' +invoice.ONB2__Account__r.Name);
            optionsMap.put('value', invoice.Id);
            optionsMapList.add(optionsMap);

        }

        return optionsMapList;

    }

    @AuraEnabled
    public static ONB2__Invoice__c setCustomerInvoiceId(String invoiceId, String customerInvoiceId)
    {
        
        ONB2__Invoice__c invoice = [
            SELECT Id, Name, ONB2__Account__r.Name, CustomerInvoice__c 
            FROM ONB2__Invoice__c
            WHERE Id =: invoiceId
        ];

        invoice.CustomerInvoice__c = customerInvoiceId;

        update invoice;
        system.debug('invoice==' + invoice);
        return [
            SELECT Id, Name, ONB2__Account__r.Name 
            FROM ONB2__Invoice__c
            WHERE Id =: customerInvoiceId
        ][0];

    }

}