/**
* Handle business logic for Task and Event executed by the trigger
*
* @author: <Egor Markuzov> (egor.markuzov@colliers.com)
*
* @history:
* version           | author                                                | changes
* ====================================================================================
* 0.1 05.11.2019	| Egor Markuzov (egor.markuzov@colliers.com)	| initial version.
* 0.2 13.11.2019    | Egor Markuzov (egor.markuzov@colliers.com)    | added validation for Event type
* 0.3 01.10.2019    | Egor Markuzov (egor.markuzov@colliers.com)    | adjustments for investor updation
* 0.4 21.01.2020    | Egor Markuzov (egor.markuzov@colliers.com)    | population of LastActivitiesField logic
* 0.5 22.01.2020    | Egor Markuzov (egor.markuzov@colliers.com)    | totally refactored hookAfterInsert and hookAfterDelete
*/
public inherited sharing class ActivityTrigger extends ATrigger
{
    public static final String TASK_ID = '00T';
    public static final Integer LAST_ACTIVITY_HISTORY_LIMIT = 4000;

    private static final String ACCOUNT_TO_OPPORTUNITY_KEY_PREFIX = Schema.SObjectType.AccountToOpportunity__c.getKeyPrefix();
    private static final String ACTIVITY_DATE_FIELD_API_NAME = 'ActivityDate';
    private static final String ACTIVITY_STATUS_FIELD_API_NAME = 'InvestorStatus__c';
    private static final String ACTIVITY_WHAT_ID_FIELD_API_NAME = 'WhatId';
    private static final String ACTIVITY_AMOUNT_API_NAME = 'Amount__c';
    private static final String ACTIVITY_SUBJECT_API_NAME = 'Subject';
    private static final String ACTIVITY_RECORD_TYPE_API_NAME = 'RecordTypeId';
    private static final String ACTIVITY_OWNER_ID_API_NAME = 'OwnerId';
    private static final String ACTIVITY_TASK_SUBTYPE_API_NAME = 'TaskSubtype';
    private static final String ACTIVITY_ACCOUNT_ID_API_NAME = 'AccountId';
    private static final Id ACTIVITY_OFFER_RECORD_TYPE_ID = Schema.SObjectType.Task.getRecordTypeInfosByDeveloperName().get('Offer').getRecordTypeId();
    private static final Schema.SObjectType ACTIVITY_EVENT_TYPE = Schema.Event.getSObjectType();

    private static final Set<String> ACTIVITY_OFFER_STATUS_SET = new Set<String>{'Offer', 'Confirmatory Offer', 'Binding Offer'};
    private static final String ACTIVITY_INSPECTION_STATUS = 'Inspection';

    private static final Integer MAX_LAST_ACTIVITIES_RECORDS = 5;

    // US 1613 : egor.markuzov@parx.com
    private static final Set<String> EXCLUDE_FROM_LAST_ACTIVITY_HISTORY_SET = new Set<String>{'Offer', 'Confirmatory Offer', 'Binding Offer', 'Inspection'};

    public override void hookAfterInsert(List<SObject> records)
    {
        System.debug('hookAfterInsert: ' + JSON.serialize(records));
        recordsAdjustmentsAfterInsertActivity(records);
    }

    public override void hookAfterDelete(List<SObject> records)
    {
        recordsAdjustmentsAfterDeleteActivity(records);
    }

    private void recordsAdjustmentsAfterInsertActivity(List<SObject> newList)
    {
        System.debug('recordsAdjustmentsAfterInsertActivity: ' + JSON.serialize(newList));
        Set<Id> accToOppIdSet = new Set<Id>();
        Map<Id, Task> tasksByInvestor = new Map<Id, Task>();

        for (SObject activity : newList)
        {
            if (activity.get(ACTIVITY_WHAT_ID_FIELD_API_NAME) != null && ( (String) activity.get(ACTIVITY_WHAT_ID_FIELD_API_NAME) ).startsWith(ACCOUNT_TO_OPPORTUNITY_KEY_PREFIX)
                && activity.get(ACTIVITY_DATE_FIELD_API_NAME) != null)
            {
                accToOppIdSet.add((Id) activity.get(ACTIVITY_WHAT_ID_FIELD_API_NAME));

                // Bug 2543 npo
                String activityId = activity.Id;
                if (activityId.startsWith(TASK_ID))
                {
                    tasksByInvestor.put((Id)activity.get(ACTIVITY_WHAT_ID_FIELD_API_NAME), (Task) activity);
                }
            }
        }

        if (!accToOppIdSet.isEmpty())
        {
            List<Status__c> newStatuses = new List<Status__c>();
             Map<Id, AccountToOpportunity__c> accToOppUpdateMap = new Map<Id, AccountToOpportunity__c>(
                                                                  [SELECT Id, LastActivityDate__c, LastBidDate__c, LastVisitDate__c, LastBidAmount__c, LastActivitiesHistory__c,
                                                                        (SELECT Id, Amount__c, ActivityDate, WhatId, Owner.Alias, Subject, InvestorStatus__c, Description
                                                                         FROM Tasks
                                                                         WHERE WhatId IN: accToOppIdSet
                                                                         AND ActivityDate != null
                                                                         ORDER BY ActivityDate DESC)
                                                                  FROM AccountToOpportunity__c
                                                                  WHERE Id IN :accToOppIdSet]);

            for (AccountToOpportunity__c investor : accToOppUpdateMap.values())
            {
                investor.LastActivitiesHistory__c = '';
                Set<String> newLastActivitiesHistorySet = new Set<String>();

                for (Task tsk : investor.tasks)
                {
                    if ( investor.LastActivityDate__c == null || investor.LastActivityDate__c <= tsk.ActivityDate )
                    {
                        investor.LastActivityDate__c = tsk.ActivityDate;
                    }

                    if ( ACTIVITY_OFFER_STATUS_SET.contains(tsk.InvestorStatus__c)
                         && (investor.LastBidDate__c == null || investor.LastBidDate__c <= tsk.ActivityDate)
                         && (investor.LastBidAmount__c == null || investor.LastBidAmount__c <= tsk.Amount__c) )
                    {
                        investor.LastBidDate__c = tsk.ActivityDate;
                        investor.LastBidAmount__c = tsk.Amount__c;
                    }

                    if ( ACTIVITY_INSPECTION_STATUS == tsk.InvestorStatus__c
                         && (investor.LastVisitDate__c == null || investor.LastVisitDate__c <= tsk.ActivityDate ) )
                    {
                        investor.LastVisitDate__c = tsk.ActivityDate;
                    }
                    System.debug(tsk);
                    if ((!EXCLUDE_FROM_LAST_ACTIVITY_HISTORY_SET.contains(tsk.InvestorStatus__c) || String.isBlank(tsk.InvestorStatus__c)) && newLastActivitiesHistorySet.size() < MAX_LAST_ACTIVITIES_RECORDS)
                    {
                        String newLastActivitiesHistory = prepareLastActivityHistoryRow(tsk);
                        if (!newLastActivitiesHistorySet.contains(newLastActivitiesHistory))
                        {
                            String result = investor.LastActivitiesHistory__c + newLastActivitiesHistory;
                            investor.LastActivitiesHistory__c = result.abbreviate(LAST_ACTIVITY_HISTORY_LIMIT);

                            newLastActivitiesHistorySet.add(newLastActivitiesHistory);
                        }
                    }
                }

                // Bug 2543 npo
                if (tasksByInvestor.containsKey(investor.Id))
                {
                    Task t = tasksByInvestor.get(investor.Id);
                    if(String.isNotEmpty(t.InvestorStatus__c))
                    {
                        investor.Status__c = t.InvestorStatus__c;
                        Status__c newStatus = new Status__c(InvestorStatus__c = investor.Status__c, Investor__c = investor.Id, StatusDate__c = t.ActivityDate);
                        newStatuses.add(newStatus);
                    }
                }
            }

            update accToOppUpdateMap.values();

            // Bug 2543 npo
            if (!newStatuses.isEmpty())
            {
                System.debug('...newStatuses: ' + newStatuses);
                insert newStatuses;
            }
        }
    }

    private void recordsAdjustmentsAfterDeleteActivity(List<SObject> newList)
    {
        System.debug('ActivityTrigger newList after delete: ' + JSON.serialize(newList));

        Set<Id> accToOppIdSet = new Set<Id>();

        for (SObject activity : newList)
        {
            if (activity.get(ACTIVITY_WHAT_ID_FIELD_API_NAME) != null && ( (String) activity.get(ACTIVITY_WHAT_ID_FIELD_API_NAME) ).startsWith(ACCOUNT_TO_OPPORTUNITY_KEY_PREFIX)
                && activity.get(ACTIVITY_DATE_FIELD_API_NAME) != null)
            {
                System.debug('...adding');
                // accountToOpportunityIdSet.add(activity.WhatId);
                accToOppIdSet.add((Id) activity.get(ACTIVITY_WHAT_ID_FIELD_API_NAME));
            }
            System.debug('...adding if end');
        }

        if (!accToOppIdSet.isEmpty())
        {
            Map<Id, AccountToOpportunity__c> accToOppUpdateMap = new Map<Id, AccountToOpportunity__c>(
                                                                  [SELECT Id, LastActivityDate__c, LastBidDate__c, LastVisitDate__c, LastBidAmount__c, LastActivitiesHistory__c,
                                                                    (SELECT Id, Amount__c, ActivityDate, WhatId, Owner.Alias, Subject, InvestorStatus__c, Description FROM Tasks WHERE WhatId IN: accToOppIdSet AND ActivityDate != null ORDER BY ActivityDate DESC)
                                                                  FROM AccountToOpportunity__c
                                                                  WHERE Id IN :accToOppIdSet]);

            for (AccountToOpportunity__c investor : accToOppUpdateMap.values())
            {
                System.debug('...investor.LastBidAmount__c at START: ' + investor.LastBidAmount__c);
                investor.LastBidDate__c = null;
                investor.LastBidAmount__c = null;
                investor.LastActivitiesHistory__c = '';

                Set<String> newLastActivitiesHistorySet = new Set<String>();

                for (Task tsk : investor.tasks)
                {
                    System.debug('...tasks: ' + investor.Tasks);
                    if (investor.LastBidDate__c == null && ACTIVITY_OFFER_STATUS_SET.contains(tsk.InvestorStatus__c))
                    {
                        investor.LastBidDate__c = tsk.ActivityDate;
                    }
                    if (investor.LastBidAmount__c == null && ACTIVITY_OFFER_STATUS_SET.contains(tsk.InvestorStatus__c))
                    {
                        investor.LastBidAmount__c = tsk.Amount__c;
                    }
                    if (!EXCLUDE_FROM_LAST_ACTIVITY_HISTORY_SET.contains(tsk.InvestorStatus__c) && newLastActivitiesHistorySet.size() < MAX_LAST_ACTIVITIES_RECORDS)
                    {
                        String newLastActivitiesHistory = prepareLastActivityHistoryRow(tsk);
                        if (!newLastActivitiesHistorySet.contains(newLastActivitiesHistory))
                        {
                            investor.LastActivitiesHistory__c += newLastActivitiesHistory;
                            newLastActivitiesHistorySet.add(newLastActivitiesHistory);
                        }
                    }
                    System.debug('...investor.LastBidAmount__c at the end: ' + investor.LastBidAmount__c);
                }
            }

            update accToOppUpdateMap.values();
        }
    }

    private static String formatNewDate(Date activityDate)
    {
        if (activityDate == null)
        {
            return 'Date was not populated';
        }

        return activityDate.day() + '.' + activityDate.month() + '.' + activityDate.year();
    }

    private static String prepareLastActivityHistoryRow(Task tsk)
    {
        System.Debug('tsk' + tsk);
        String row = formatNewDate(tsk.ActivityDate) + ' - ' + tsk.Owner.Alias + ' : ' + tsk.Subject;
        if(tsk.Description != null){
            row += ' - ' + tsk.Description;
        }
        row += ';';
        System.Debug('row' + row);
        return  row;
    }
}