/**
* Handles the trigger logic of Portfolio__c
*
* @author: <Maksim Fedorenko> (maksim.fedorenko@parx.com)
*
* @history:
* version		    | author				                                | changes
* ====================================================================================
* 0.1 31.01.2020	| Maksim Fedorenko (maksim.fedorenko@parx.com)          | initial version.
* 0.2 17.02.2020	| Christian Schwabe (Christian.Schwabe@colliers.com)    | [SFINV/US 1977] Added preventPartialChangingOnPortfolio() and setPartialChangingOnPortfolio.

*/
public with sharing class PortfolioTrigger extends ATrigger
{
    public override void hookBeforeUpdate(List<SObject> records, List<SObject> oldRecords)
    {
        PortfolioService.preventPartialChangingOnPortfolio(records, oldRecords);
        PortfolioService.setPartialChangingOnPortfolio(records, oldRecords);
    }

    public override void hookAfterUpdate(List<SObject> records, List<SObject> oldRecords)
    {
        PortfolioService.copyDataToOpportunity(records);
    }
}