/**
 * @author dme
 * @copyright PARX
 */

public without sharing class OpportunityService
{

	public static List<CustomPermission> osFeeShare
	{
		get
		{
			if (osFeeShare == null)
			{
				osFeeShare = [SELECT Id FROM CustomPermission WHERE DeveloperName = 'OsFeeShare'];
			}
			return osFeeShare;
		}
		set;
	}

	/**
     * @azure US 705 Übernahme der Feldwerte aus dem verlinkten Objekt / Portfolio in die Opportunity
     * @description Copies the fields, described in sourceTargetFieldMap from sourceRecords to respective opportunity records
     * @param sourceRecords List of source records, which will be use as a data source
     * @param sObjectIdOpportunityListMap Map of sourceRecord Id => Respective opportunity list
     * @param sourceTargetFieldMap Map of sourceRecord => Opportunity fields
     */
	public static void copyDataToOpportunity (List<SObject> sourceRecords, Map<Id, List<Opportunity>> sObjectIdOpportunityListMap,
			Map<SObjectField, SObjectField> sourceTargetFieldMap)
	{
		Map<Id, SObject> sObjectMap = new Map<Id, SObject>(sourceRecords);
		List<Opportunity> opportunitiesForUpdate = new List<Opportunity>();
		for (Id recordId : sObjectIdOpportunityListMap.keySet())
		{
			for (Opportunity opportunity : sObjectIdOpportunityListMap.get(recordId))
			{
				if (!opportunity.IsClosed)
				{
					opportunity = (Opportunity) Utils.copySObjectFieldsByFieldMap(sObjectMap.get(recordId), opportunity, sourceTargetFieldMap);
					opportunity.IsDataTransfer__c = true;
					opportunitiesForUpdate.add(opportunity);
				}
			}
		}
		update opportunitiesForUpdate;
	}
}