/**
 * Created by ille on 2019-05-27.
 */
/**
* Trigger Handler for OpportunityLineItemSchedule to delegate business logic. 
*
* @author: <Christian Schwabe> (Christian.Schwabe@colliers.com)
*
* @history:
* version           | author                                                | changes
* ====================================================================================
* 0.1 26.06.2019    | Parx Admin (salesforce.admin+colliers@parx.com)       | Initial version.
* 0.2 22.11.2019    | Christian Schwabe (Christian.Schwabe@colliers.com)    | Outsource logic for rollup-helper to RollupHelper.cls
*/
public with sharing class OpportunityLineItemScheduleTrigger extends ATrigger
{
    private static Type rollClass = System.Type.forName('rh2', 'ParentUtil');

    //private static final String objectApiName = 'OpportunityLineItemSchedule';
    private static Set<Id> AUTOMATICALLY_UPDATED_LINE_ITEM_SCHEDULES = new Set<Id>();

    public static void allowQuantityRevenueChange(Id lineItemScheduleId)
    {
        AUTOMATICALLY_UPDATED_LINE_ITEM_SCHEDULES.add(lineItemScheduleId);
    }

    private OpportunityLineItemScheduleTriggerWoSharing woSharing()
    {
        return new OpportunityLineItemScheduleTriggerWoSharing();
    }

    public override void hookAfterInsert(List<SObject> records)
    {
        woSharing().updateSubCommissions((List<OpportunityLineItemSchedule>) records, (List<OpportunityLineItemSchedule>) null);
        woSharing().linkTimecardsWithSchedule(records);
        executeRollupHelper(null, new Map<Id, SObject>(records));
    }

    public override void hookBeforeUpdate(List<SObject> records, List<SObject> oldRecords)
    {
        woSharing().preventUpdateIfInvoiceSet(records, new Map<Id, OpportunityLineItemSchedule>((List<OpportunityLineItemSchedule>) oldRecords));
        woSharing()
            //.validateManualRevenueQuantityChange((List<OpportunityLineItemSchedule>) records, (List<OpportunityLineItemSchedule>) oldRecords)
            .maintainPredefinedCenterSplit((List<OpportunityLineItemSchedule>) records, (List<OpportunityLineItemSchedule>) oldRecords);
    }

    public override void hookAfterUpdate(List<SObject> records, List<SObject> oldRecords)
    {
        woSharing().updateSubCommissions((List<OpportunityLineItemSchedule>) records, (List<OpportunityLineItemSchedule>) oldRecords);
        executeRollupHelper(new Map<Id, SObject>(oldRecords), new Map<Id, SObject>(records));
    }

    public override void hookBeforeDelete(List<SObject> records){
        woSharing().preventDeletionIfInvoiceSet((List<OpportunityLineItemSchedule>) records);
        executeRollupHelper(new Map<Id, SObject>(records), null);
    }

    public override void hookAfterDelete(List<SObject> records){
        executeRollupHelper(new Map<Id, SObject>(records), null);
    }

    public override void hookAfterUndelete(List<SObject> records){
        executeRollupHelper(new Map<Id, SObject>(records), null);
    }

    /**
    * Helpermethod to execute rollup-helper. 
    */
    private void executeRollUpHelper(Map<Id, SObject> oldMap, Map<Id, SObject> newMap){
        if(rollClass != null) {
            rh2.ParentUtil pu = (rh2.ParentUtil) rollClass.newInstance();
            pu.performTriggerRollups(oldMap, newMap, new String[]{'OpportunityLineItemSchedule'}, null);
        }
    }

    /*public void executeRollupHelper(List<SObject> oldRecords, List<SObject> newRecords){
        Type rollClass = System.Type.forName('rh2', 'ParentUtil');

        //System.debug('>>>rollClass: ' + rollClass);
        //System.debug('>>>objectApiName: ' + objectApiName);
        System.debug('>>>oldRecords: ' + oldRecords);
        System.debug('>>>newRecords: ' + newRecords);
        System.debug('>>>rollClass != null: ' + (rollClass != null));

        Map<Id, SObject> oldMap = new Map<Id, SObject>(oldRecords);
        Map<Id, SObject> newMap = new Map<Id, SObject>(newRecords);
        System.debug('>>>rollClass != null: ' + (rollClass != null));

        if(rollClass != null) {

            rh2.ParentUtil pu = (rh2.ParentUtil) rollClass.newInstance();
            System.debug('>>>pu: ' + pu);

            //if (trigger.isAfter) {
                pu.performTriggerRollups(oldMap, newMap, new String[]{'OpportunityLineItemSchedule'}, null);
            //}
        }
    }*/


    /**
     * A "without sharing" class which is used to bypass sharing settings.
     */
    private without sharing class OpportunityLineItemScheduleTriggerWoSharing
    {
        /**
     * New trigger on "OpportunityLineItemSchedule" (without sharing!)
        If is updated and OpportunityLineItemSchedule.ON_Invoice__c is changed from null to an ID
            Query all SubCommissions__c where SubCommissions__c.Invoice__c = NULL AND SubCommissions__c.Opportunity__c = OpportunityLineItemSchedule.OpportunityLineItem.OpportunityId
            Update retrieved SubCommissions__c records with 
                SubCommissions__c.Invoice__c = OpportunityLineItemSchedule.ON_Invoice__c
                SubCommissions__c.ProductScheduleId__c = OpportunityLineItemSchedule.Id
        If is updated and OpportunityLineItemSchedule.ON_Invoice__c is changed from ID to another ID
            Query all SubCommissions__c where SubCommissions__c.ProductScheduleId__c = OpportunityLineItemSchedule.Id
            Update retrieved SubCommissions__c records with 
                SubCommissions__c.Invoice__c = OpportunityLineItemSchedule.ON_Invoice__c
     *
     * @param lineItemSchedules
     */
        private OpportunityLineItemScheduleTriggerWoSharing updateSubCommissions(List<OpportunityLineItemSchedule> lineItemSchedules, List<OpportunityLineItemSchedule> oldLineItemSchedules)
        {
            List<OpportunityLineItemSchedule> updatedLineItemSchedules = new List<OpportunityLineItemSchedule>();
            for (Integer i = 0, n = lineItemSchedules.size(); i < n; i++)
            {
                OpportunityLineItemSchedule lineItemSchedule = lineItemSchedules.get(i);
                OpportunityLineItemSchedule oldLineItemSchedule = (oldLineItemSchedules != null ? oldLineItemSchedules.get(i) : null);

                if (lineItemSchedule.ON_Invoice__c != null
                    && (oldLineItemSchedule == null || lineItemSchedule.ON_Invoice__c != oldLineItemSchedule.ON_Invoice__c))
                {
                    updatedLineItemSchedules.add(lineItemSchedule);
                }
            }

            updateSubCommissions(updatedLineItemSchedules);

            return this;
        }

        /**
         * Updates related SubCommissions__c records with OpportunityLineItemSchedule.Invoice__c and OpportunityLineItemSchedule.ProductScheduleId__c.
         *
         * @param lineItemSchedules
         */
        private void updateSubCommissions(List<OpportunityLineItemSchedule> lineItemSchedules)
        {
            Map<Id, OpportunityLineItemSchedule> extendedLineItemSchedulesMap = new Map<Id, OpportunityLineItemSchedule>(selectExtendedLineItemSchedules(lineItemSchedules));

            List<Id> opportunityIDs = new List<Id>();
            for (OpportunityLineItemSchedule extendedLineItemSchedule : extendedLineItemSchedulesMap.values())
            {
                opportunityIDs.add(extendedLineItemSchedule.OpportunityLineItem.OpportunityId);
            }

            List<SubCommissions__c> modifiedSubCommissions = new List<SubCommissions__c>();
            for (SubCommissions__c subCommission : [select Invoice__c, ProductScheduleId__c, Opportunity__c from SubCommissions__c
            where (Invoice__c = null and Opportunity__c in :opportunityIDs) or (ProductScheduleId__c != null and ProductScheduleId__c in :extendedLineItemSchedulesMap.keySet())])
            {
                OpportunityLineItemSchedule lineItemSchedule = matchLineItemSchedule(subCommission, extendedLineItemSchedulesMap);
                if (lineItemSchedule != null)
                {
                    subCommission.Invoice__c = lineItemSchedule.ON_Invoice__c;
                    subCommission.ProductScheduleId__c = lineItemSchedule.Id;

                    modifiedSubCommissions.add(subCommission);
                }
            }

            update modifiedSubCommissions;
        }

        /**
         * Selects extended versions (which includes additional fields) of OpportunityLineItemSchedule records.
         *
         * @param lineItemSchedules
         */
        private List<OpportunityLineItemSchedule> selectExtendedLineItemSchedules(List<OpportunityLineItemSchedule> lineItemSchedules)
        {
            return [select Invoice__c, ON_Invoice__c, OpportunityLineItem.OpportunityId from OpportunityLineItemSchedule where Id in :lineItemSchedules];
        }

        /**
         * Matches SubCommission__c record against OpportunityLineItemSchedule records either by
         * SubCommission__c.ProductScheduleId__c = OpportunityLineItemSchedule.Id or by SubCommission__c.Opportunity__c = OpportunityLineItemSchedule.OpportunityLineItem.OpportunityId
         *
         * @return  either matched OpportunityLineItemSchedule or NULL
         */
        private OpportunityLineItemSchedule matchLineItemSchedule(SubCommissions__c subCommission, Map<Id, OpportunityLineItemSchedule> extendedLineItemSchedulesMap)
        {
            OpportunityLineItemSchedule lineItemSchedule = extendedLineItemSchedulesMap.get(subCommission.ProductScheduleId__c);
            List<OpportunityLineItemSchedule> extendedLineItemSchedules = extendedLineItemSchedulesMap.values();
            for (Integer i = 0, n = extendedLineItemSchedules.size(); lineItemSchedule == null && i < n; i++)
            {
                OpportunityLineItemSchedule extendedLineItemSchedule = extendedLineItemSchedules.get(i);
                if (extendedLineItemSchedule.OpportunityLineItem.OpportunityId == subCommission.Opportunity__c)
                {
                    lineItemSchedule = extendedLineItemSchedule;
                    break;
                }
            }

            return lineItemSchedule;
        }

        private OpportunityLineItemScheduleTriggerWoSharing maintainPredefinedCenterSplit(List<OpportunityLineItemSchedule> lineItemSchedules, List<OpportunityLineItemSchedule> oldLineItemSchedules)
        {
            Map<Id, OpportunityLineItem> opportunityLineItemMap = new Map<Id, OpportunityLineItem>(selectRelatedOpportunityLineItems(lineItemSchedules));
            List<OpportunityLineItemSchedule> modifiedItemSchedules = new List<OpportunityLineItemSchedule>();
            List<Id> opportunityIDs = new List<Id>();
            for (Integer i = 0, n = lineItemSchedules.size(); i < n; i++)
            {
                OpportunityLineItemSchedule lineItemSchedule = lineItemSchedules.get(i);
                OpportunityLineItemSchedule oldLineItemSchedule = oldLineItemSchedules.get(i);
                OpportunityLineItem relatedOpportunityLineItem = opportunityLineItemMap.get(lineItemSchedule.OpportunityLineItemId);

                if (lineItemSchedule.ReadyForInvoicing__c == true
                    && (lineItemSchedule.ReadyForInvoicing__c != oldLineItemSchedule.ReadyForInvoicing__c)
                    && relatedOpportunityLineItem.Product2.RelevantForFeeSharing__c == true)
                {
                    opportunityIDs.add(relatedOpportunityLineItem.OpportunityId);
                    modifiedItemSchedules.add(lineItemSchedule);
                }
            }

            List<FeeSharing__c> feeSharings = selectFeeSharings(opportunityIDs);

            //Build map to fill later FeeSharing__c.Center__c
            Map<OpportunityLineItemSchedule, List<FeeSharing__c>> olisToFeeSharingsMap = new Map<OpportunityLineItemSchedule, List<FeeSharing__c>>();

            for (OpportunityLineItemSchedule lineItemSchedule : modifiedItemSchedules)
            {
                OpportunityLineItem relatedOpportunityLineItem = opportunityLineItemMap.get(lineItemSchedule.OpportunityLineItemId);
                List<FeeSharing__c> matchedFeeSharings = matchFeeSharings(relatedOpportunityLineItem.OpportunityId, feeSharings);

                //OLD: populatePredefinedCenterSplit(lineItemSchedule, matchedFeeSharings);
                
                //NEW
                olisToFeeSharingsMap.put(lineItemSchedule, matchedFeeSharings);
            }

            
            //NEW: Fill FeeSharing__c.Center__c
            List<FeeSharing__c> feeSharingToUpdate = new List<FeeSharing__c>();
            for (List<FeeSharing__c> feeSharingList : olisToFeeSharingsMap.values())
            {
                ProjectFeeSharingController.prepareFeeSharing(feeSharingList);
                feeSharingToUpdate.addAll(feeSharingList);
            }
            update feeSharingToUpdate;

            for (OpportunityLineItemSchedule lineItemSchedule : olisToFeeSharingsMap.keySet())
            {
                populatePredefinedCenterSplit(lineItemSchedule, olisToFeeSharingsMap.get(lineItemSchedule));
            }//end NEW logic

            return this;
        }

        private void populatePredefinedCenterSplit(OpportunityLineItemSchedule lineItemSchedule, List<FeeSharing__c> feeSharings)
        {
            Map<String, Object> jsonMap = new Map<String, Object>();
            for (FeeSharing__c feeSharing : feeSharings)
            {
                if (!String.isEmpty(feeSharing.Center__c))
                {
                    jsonMap.put(feeSharing.Center__c, feeSharing.SharePercentage__c);
                }
            }

            String centerSplit = System.JSON.serialize(new List<Object>{new Map<String, Object>{
                'mode' => 'RELAXED',
                'type' => 'PERCENTAGE',
                'split' => jsonMap
            }});

            Integer centerSplitLength = centerSplit.length();
            if (centerSplitLength > 510)
            {
                lineItemSchedule.FeeSharingMapPt3__c = centerSplit.substring(510);
            }

            if (centerSplitLength > 255)
            {
                lineItemSchedule.FeeSharingMapPt2__c = centerSplit.substring(255, Math.min(510, centerSplitLength));
            }

            lineItemSchedule.FeeSharingMapPt1__c = centerSplit.substring(0, Math.min(255, centerSplitLength));
        }

        private List<FeeSharing__c> matchFeeSharings(Id opportunityID, List<FeeSharing__c> feeSharings)
        {
            List<FeeSharing__c> result = new List<FeeSharing__c>();
            for (FeeSharing__c feeSharing : feeSharings)
            {
                if (feeSharing.Opportunity__c == opportunityID)
                {
                    result.add(feeSharing);
                }
            }

            return result;
        }

        private List<OpportunityLineItem> selectRelatedOpportunityLineItems(List<OpportunityLineItemSchedule> lineItemSchedules)
        {
            List<Id> opportunityLineItemIds = new List<Id>();
            for (OpportunityLineItemSchedule lineItemSchedule : lineItemSchedules)
            {
                opportunityLineItemIds.add(lineItemSchedule.OpportunityLineItemId);
            }

            return [select Product2.RelevantForFeeSharing__c, OpportunityId from OpportunityLineItem where Id in :opportunityLineItemIds];
        }

        private List<FeeSharing__c> selectFeeSharings(List<Id> opportunityIDs)
        {
            List<FeeSharing__c> result = new List<FeeSharing__c>();
            List<FeeSharing__c> feeSharings = [select Center__c, SharePercentage__c, Opportunity__c, User__c 
                                               from FeeSharing__c where Opportunity__c in :opportunityIDs];
            //dme : Optimized SOQL queries
            for (FeeSharing__c feeSharing : feeSharings)
            {
                if (String.isBlank(feeSharing.Center__c))
                {
                    result.add(feeSharing);
                }
            }

            if (result.isEmpty())
            {
                result = feeSharings;
            }

            return result;
        }


        /**
         * @author dme
         * @description Prevent OpportunityLineItemSchedule records deletion if ON_Invoice__c is set
         */
        private void preventDeletionIfInvoiceSet(List<OpportunityLineItemSchedule> oldList)
        {
            for (OpportunityLineItemSchedule record : oldList)
            {
                if (record.ON_Invoice__c != null)
                {
                    record.addError(Label.OpportunityLineItemScheduleEditDeleteError);
                }
            }
        }

        /**
         * @author dme
         * @description Prevent OpportunityLineItemSchedule records update if ON_Invoice__c is set; only change of ON_Invoice__c is allowed
         */
        private void preventUpdateIfInvoiceSet(List<OpportunityLineItemSchedule> newList, Map<Id, OpportunityLineItemSchedule> oldMap)
        {
            for (OpportunityLineItemSchedule record : newList)
            {
                if (record.ON_Invoice__c != null && record.ON_Invoice__c == oldMap.get(record.Id).ON_Invoice__c)
                {
                    record.addError(Label.OpportunityLineItemScheduleEditDeleteError);
                }
            }
        }

        /**
         * @author dme
         * @description Associate timecards with newly created Schedules if needed
         */
        private void linkTimecardsWithSchedule(List<OpportunityLineItemSchedule> newList)
        {
            Set<String> opportunityProductIds = new Set<String>();
            List<TimeCard__c> timeCardsToUpdate = new List<TimeCard__c>();

            Map<String, Id> oldestSchedulesByLineItemIdMap = new Map<String, Id>();
            Map<String, List<OpportunityLineItemSchedule>> schedulesByLineItemIdMap = new Map<String, List<OpportunityLineItemSchedule>>();

            Set<String> opportunityLineItemIds = new Set<String>();
            for (OpportunityLineItemSchedule schedule : newList)
            {
                opportunityLineItemIds.add(schedule.OpportunityLineItemId);
            }

            List<OpportunityLineItemSchedule> schedules = [
                SELECT Id, ScheduleDate, OpportunityLineItemId FROM OpportunityLineItemSchedule
                WHERE OpportunityLineItemId IN :opportunityLineItemIds
                    AND ON_Invoice__c = NULL AND OpportunityLineItem.Opportunity.BillingType__c INCLUDES('Time and Material')
                ORDER BY ScheduleDate DESC
            ];

            for (OpportunityLineItemSchedule record : schedules)
            {
                opportunityProductIds.add(record.OpportunityLineItemId);
                oldestSchedulesByLineItemIdMap.put(record.OpportunityLineItemId, record.Id);
                if (!schedulesByLineItemIdMap.containsKey(record.OpportunityLineItemId))
                {
                    schedulesByLineItemIdMap.put(record.OpportunityLineItemId, new List<OpportunityLineItemSchedule>());
                }
                schedulesByLineItemIdMap.get(record.OpportunityLineItemId).add(record);
            }

            List<TimeCard__c> timeCards = [
                SELECT Id, Date__c, BillingType__c, Product__r.OpportunityProductId__c, OpportunityLineItemScheduleId__c FROM TimeCard__c
                WHERE Product__r.OpportunityProductId__c IN :opportunityProductIds AND Status__c = 'Open' AND ON_Invoice__c = NULL
            ];

            timeCardsToUpdate = TimecardService.setTimecardSchedules(timeCards, schedulesByLineItemIdMap, oldestSchedulesByLineItemIdMap);

            if (!timeCardsToUpdate.isEmpty())
            {
                update timeCardsToUpdate;
            }
        }

        /*
        private OpportunityLineItemScheduleTriggerWoSharing validateManualRevenueQuantityChange(List<OpportunityLineItemSchedule> lineItemSchedules, List<OpportunityLineItemSchedule> oldLineItemSchedules) {
            for (Integer i = 0, n = lineItemSchedules.size(); i < n; i++)
            {
                OpportunityLineItemSchedule lineItemSchedule = lineItemSchedules.get(i);
                OpportunityLineItemSchedule oldLineItemSchedule = oldLineItemSchedules.get(i);

                Exceptions.assert((lineItemSchedule.Quantity == oldLineItemSchedule.Quantity && lineItemSchedule.Revenue == oldLineItemSchedule.Revenue)
                        || AUTOMATICALLY_UPDATED_LINE_ITEM_SCHEDULES.contains(lineItemSchedule.Id), new Exceptions.GenericException(Label.LineItemScheduleRevenueQuantityManualUpdateError));
            }

            return this;
        }
        */
    }
}