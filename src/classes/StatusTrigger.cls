/**
* Handle business logic for StatusTrigger by the trigger
*
* @author: <Nadzeya Polautsava> (nadzeya.polautsava@parx.com)
*
* @history:
* version           | author                                                | changes
* ====================================================================================
* 0.1 07.04.2020	| Nadzeya Polautsava (nadzeya.polautsava@parx.com)	    | initial version - Bug1948.
*/
public inherited sharing class StatusTrigger extends ATrigger
{
    public override void hookAfterDelete(List<SObject> records)
    {
        StatusService.recalculateInvestorLastStatusDate(records);
    }
}