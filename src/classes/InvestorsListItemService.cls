/**
* InvestorsListItem SObject service class
*
* @author: <Maksim Fedorenko> (maksim.fedorenko@parx.com)
*
* @history:
* version		    | author				                                | changes
* ====================================================================================
* 0.1 14.02.2020	| Maksim Fedorenko (maksim.fedorenko@parx.com)          | initial version.
*/

public with sharing class InvestorsListItemService
{
    /**
     * Returns a map of accountId => InvestorsListItem__c records for a given account list
     * @param accountList List of Account records for search
     * @return Map of account Id => InvestorsListItem__c records
     */
    public static Map<Id, InvestorsListItem__c> getAccountIdInvestorsListItemMap(List<Account> accountList, Id investorsListId)
    {
        Map<Id, InvestorsListItem__c> accountIdInvestorsListItemMap = new Map<Id, InvestorsListItem__c>();
        List<InvestorsListItem__c> investorsListItems = [
                SELECT Id, Account__c, Name, NumberOfContacts__c
                FROM InvestorsListItem__c
                WHERE Account__c IN :accountList AND InvestorsList__c = :investorsListId
        ];

        for (InvestorsListItem__c investorsListItem : investorsListItems)
        {
            accountIdInvestorsListItemMap.put(investorsListItem.Account__c, investorsListItem);
        }
        return accountIdInvestorsListItemMap;
    }
}