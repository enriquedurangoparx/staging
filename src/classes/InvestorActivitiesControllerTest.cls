/**
* Test class for InvestorActivitiesController.cls
*
* @author: <Maksim Fedorenko> (maksim.fedorenko@parx.com)
*
* @history:
* version		    | author				                                | changes
* ====================================================================================
* 0.1 04.02.2020	| Maksim Fedorenko (maksim.fedorenko@parx.com)          | initial version.
*/
@IsTest
private class InvestorActivitiesControllerTest
{
    @TestSetup
    static void setup() {
        Embargo__c embargo = new Embargo__c();
        embargo.Name = 'Embargo';
        embargo.Countries__c = 'Egypt;Afghanistan;Belarus;Burundi;Iraq;Yemen';
        insert embargo;
    }

    @IsTest
    static void getActivityListTest()
    {
        Account account = new Account(Name = 'Test');
        insert account;

        PropertyObject__c propertyObject = TestDataFactory.buildPropertyObject('property', true);

        Opportunity opportunity = new Opportunity(
                Name = 'Test',
                RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('InvestmentInternal').getRecordTypeId(),
                AccountId = account.Id,
                StageName = 'Open',
                ProjectStart__c = Date.today(),
                CloseDate = Date.today().addDays(1),
                PrimaryProperty__c = propertyObject.Id
        );
        insert opportunity;

        AccountToOpportunity__c accountToOpportunity = new AccountToOpportunity__c(
                Opportunity__c = opportunity.Id,
                Account__c = account.Id
        );
        insert accountToOpportunity;

        Task activity = new Task(
                ActivityDate = Date.today(),
                OwnerId = UserInfo.getUserId(),
                Status = 'NDA sent',
                Subject = 'NDA sent',
                WhatId = accountToOpportunity.Id
        );
        insert activity;

        List<Task> opportunityActivities = InvestorActivitiesController.getActivityList(opportunity.Id,
                InvestorActivitiesController.OPPORTUNITY_OBJECT_API_NAME);
        List<Task> accountActivities = InvestorActivitiesController.getActivityList(account.Id,
                InvestorActivitiesController.ACCOUNT_OBJECT_API_NAME);

        System.assert(opportunityActivities.size() == 1, 'Test Opportunity should have a related Activity record');
        System.assert(accountActivities.size() == 1, 'Test Account should have a related Activity record');
    }
}