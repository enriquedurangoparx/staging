/**
*  @description Test Class for the BrandService.
*  @author ikl
*  @copyright PARX
*/
@isTest
public class BrandServiceTest
{
	private static final String TEST_BRAND_NAME = 'Test Brand 1';

	private static final String TEST_USER_USERNAME_1 = 'test1@username.com';
	private static final String TEST_USER_USERNAME_2 = 'test2@username.com';
	private static final String TEST_USER_USERNAME_3 = 'test3@username.com';

	private static final String TEST_USER_NAME = 'Test User 1';

	private static final String TEST_USER_FIST_NAME = 'FirstName';
	private static final String TEST_USER_LAST_NAME_A = 'ALastName';
	private static final String TEST_USER_LAST_NAME_B = 'BLastName';
	private static final String TEST_USER_LAST_NAME_C = 'CLastName';

	public static Brand__c createBrand(String theName, Boolean doCommitToDb)
	{
		Brand__c brand = new Brand__c(Name = theName);
		if (doCommitToDb)
		{
			insert brand;
		}
		return brand;
	}


	public static KeyAccountManager__c createKeyAccountManager(Id brandId, Id userId, Boolean doCommitToDb)
	{
		KeyAccountManager__c keyAccountManager = new KeyAccountManager__c(Brand__c = brandId, User__c = userId);
		if (doCommitToDb)
		{
			insert keyAccountManager;
		}
		return keyAccountManager;
	}


	public static User createUserWithParams(String userNameUnique, String firstName, String lastName, String userProfileId, Boolean doCommitToDb)
	{
		User user = new User (Alias = 'TestUser',
							  UserName = userNameUnique,
							  Email = 'newuser@test.org.com',
							  EmailEncodingKey = 'UTF-8',
							  FirstName = firstName,
							  LastName = lastName,
							  LanguageLocaleKey = 'en_US',
							  LocaleSidKey = 'en_US',
							  ProfileId = userProfileId,
							  TimeZoneSidKey = 'Europe/Berlin',
							  UserRoleId = null);
		if (doCommitToDb)
		{
			insert user;
		}
		return user;
	}


	public static Id systemAdministratorProfileId
	{
		get
		{
			if (systemAdministratorProfileId == null)
			{
				systemAdministratorProfileId = [
												SELECT 
													Id, Name
												FROM 
													Profile
												WHERE 
													UserType = 'Standard' 
												AND 
													PermissionsAuthorApex = true 
												LIMIT 
													1
												].Id;
			}
			return systemAdministratorProfileId;
		}
		set;
	}


	@isTest
	private static void testUpdateKeyAccountManagerFieldOnRelatedBrandRecord()
	{
		User userA = createUserWithParams(TEST_USER_USERNAME_2, TEST_USER_FIST_NAME, TEST_USER_LAST_NAME_A, systemAdministratorProfileId, true);
		User userB = createUserWithParams(TEST_USER_USERNAME_1, TEST_USER_FIST_NAME, TEST_USER_LAST_NAME_B, systemAdministratorProfileId, true);
		User userC = createUserWithParams(TEST_USER_USERNAME_3, TEST_USER_FIST_NAME, TEST_USER_LAST_NAME_C, systemAdministratorProfileId, true);

		Brand__c brand = createBrand(TEST_BRAND_NAME, true);
	 	createKeyAccountManager(brand.Id, userB.Id, true);

		Test.startTest();

		Brand__c updatedBrand = [SELECT Id, KeyAccountManager__c FROM Brand__c WHERE Id = :brand.Id];

		System.assertEquals(TEST_USER_FIST_NAME + BrandService.SPACE + TEST_USER_LAST_NAME_B, updatedBrand.KeyAccountManager__c);

		KeyAccountManager__c additionalKeyAccountManager = createKeyAccountManager(brand.Id, userA.Id, true);

		updatedBrand = [SELECT Id, KeyAccountManager__c FROM Brand__c WHERE Id = :brand.Id];

		System.assertEquals(TEST_USER_FIST_NAME + BrandService.SPACE + TEST_USER_LAST_NAME_A + BrandService.COMMA + BrandService.SPACE +
							TEST_USER_FIST_NAME + BrandService.SPACE + TEST_USER_LAST_NAME_B,
							updatedBrand.KeyAccountManager__c);

		additionalKeyAccountManager.User__c = userC.Id;
		update additionalKeyAccountManager;

		updatedBrand = [SELECT Id, KeyAccountManager__c FROM Brand__c WHERE Id = :brand.Id];

		System.assertEquals(TEST_USER_FIST_NAME + BrandService.SPACE + TEST_USER_LAST_NAME_B + BrandService.COMMA + BrandService.SPACE +
							TEST_USER_FIST_NAME + BrandService.SPACE + TEST_USER_LAST_NAME_C,
							updatedBrand.KeyAccountManager__c);

		delete additionalKeyAccountManager;
		Test.stopTest();

		updatedBrand = [SELECT Id, KeyAccountManager__c FROM Brand__c WHERE Id = :brand.Id];
		System.assertEquals(TEST_USER_FIST_NAME + BrandService.SPACE + TEST_USER_LAST_NAME_B, updatedBrand.KeyAccountManager__c);
	}
}