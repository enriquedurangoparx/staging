/**
* Testclass for LWCObjectMatchingController.cls
*
* @author: <Maksim Fedorenko> (maksim.fedorenko@parx.com)
*
* @history:
* version           | author                                                | changes
* ====================================================================================
* 0.1 13.12.2019    | Maksim Fedorenko (maksim@fedorneko@parx.com)          | initial version.
* 0.2 16.12.2019	| Egor Markuzov (egor.markuzov@parx.com)				| added getMatchingContactsTest()
* 0.3 17.03.2020	| Christian Schwabe (Christian.Schwabe@colliers.com)    | [SFINV/US 2407] Added Saluation for Contact.
* 0.4 06.04.2020	| Christian Schwabe (Christian.Schwabe@colliers.com)    | [SFINV/US 2886] Changed picklist values for matching.
*/

@IsTest
private class LWCObjectMatchingControllerTest
{
	@testSetup
    static void setup()
    {
        TestDataFactory.initializeProducts();

        Embargo__c embargo = new Embargo__c();
        embargo.Name = 'Embargo';
        embargo.Countries__c = 'Egypt;Afghanistan;Belarus;Burundi;Iraq;Yemen';
        insert embargo;
    }

	@isTest
	static void getMatchingContactsTest()
	{
		Account newAccount = TestDataFactory.createAccount('Test Account 1');
        insert newAccount;

        List<Contact> newContactList = TestDataFactory.createContacts(newAccount.Id, 5);
        insert newContactList;

        List<AcquisitionProfile__c> acqProfilesList = TestDataFactory.createAcquisitionProfiles(newAccount.Id, newContactList[0].Id, 5);
        for (AcquisitionProfile__c acqp : acqProfilesList)
		{
			acqp.WALTFrom__c = 6;
			acqp.LettingAreaFrom__c = 9;// acqp.TotalAreaFrom__c = 9;
			acqp.LettingAreaTo__c = 20;// acqp.TotalAreaTo__c = 20;
			acqp.InvestorRepresentative__c = newContactList[0].id;
			acqp.TypeOfUse__c = 'Office / Mixed-use';
			acqp.OccupancyRate__c = 5;
			acqp.ConversionProperty__c = true;
		}
        insert acqProfilesList;

		PropertyObject__c propertyObject = TestDataFactory.buildPropertyObject('Property', false);
		insert propertyObject;

		Opportunity initOpportunity = TestDataFactory.createOpportunity(null, propertyObject);
		initOpportunity.BillingType__c = 'Time and Material';
		initOpportunity.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('ValuationAcquisition').getRecordTypeId();
		initOpportunity.ProjectStart__c = Date.today();
		initOpportunity.CloseDate = Date.today().addYears(1);
		insert initOpportunity;

		String jsonSearchTerm = '[ { "accordionName": "investorenselektion", "filters": [ { "apiName": "TypeOfUse__c", "label": "Nutzungsart", "isActive": true, "values": [ "Office / Mixed-use" ], "minValue": "", "maxValue": "" }, { "apiName": "TypeOfRealEstate__c", "label": "Immobilienart", "isActive": false, "values": [], "minValue": "", "maxValue": "" }, { "apiName": "RiskCategory__c", "label": "Risikokategorie", "isActive": false, "values": [ "" ] }, { "apiName": "MacroLocation__c", "label": "Makrolage", "isActive": false, "values": [], "minValue": "", "maxValue": "" }, { "apiName": "Submarket__c", "label": "Teilmarkt", "isActive": false, "values": [ "" ] }, { "apiName": "PurchasingPriceOffer__c", "label": "Angebots-Kaufpreis", "isActive": false, "values": [ null ], "minValue": 1, "maxValue": "" }, { "apiName": "SqmInTotal__c", "label": "Gesamtfläche m²", "isActive": true, "values": null, "minValue": 1, "maxValue": 100 }, { "apiName": "OccupancyRate__c", "label": "Vermietungsstand", "isActive": true, "values": null, "minValue": 1, "maxValue": "" }, { "apiName": "WALT__c", "label": "Gewichtete Laufzeit (WALT)", "isActive": true, "values": null, "minValue": "1", "maxValue": "18" }, { "apiName": "Certification__c", "label": "Zertifizierung", "isActive": false, "values": [ "" ] }, { "apiName": "YearofConstruction__c", "label": "Baujahr", "isActive": false, "values": null, "minValue": 0, "maxValue": 0 } ], "isActive": true }, { "accordionName": "type-of-use-office", "filters": [ { "apiName": "SqmOffice__c", "label": "Bürofläche m²", "isActive": false, "values": null, "minValue": "", "maxValue": "" } ], "isActive": false }, { "accordionName": "type-of-use-land", "filters": [ { "apiName": "ConversionProperty__c", "label": "Umwandlungsobjekt", "isActive": true, "values": [ "true" ], "minValue": "", "maxValue": "" } ], "isActive": true } ]';

		String resp = LWCObjectMatchingController.getMatchingContacts(jsonSearchTerm, initOpportunity.Id, 25);
        Map<String, Object> result = (Map<String, Object>) JSON.deserializeUntyped(resp);
        List<Object> respList = (List<Object>) result.get('matchingRecords');
		System.assertEquals(1, respList.size(), 'Wrong number of matching contacts');
	}

	@IsTest
	static void getOpportunityTest()
	{
		Account newAccount = TestDataFactory.createAccount('Test Account 1');
        insert newAccount;

		PropertyObject__c propertyObject = TestDataFactory.buildPropertyObject('Property', false);
		propertyObject.SqmInTotal__c = 300;
		propertyObject.WALT__c = 5;
		insert propertyObject;

		Opportunity initOpportunity = TestDataFactory.createOpportunity(newAccount.Id, propertyObject);
		initOpportunity.BillingType__c = 'Time and Material';
		initOpportunity.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('ValuationAcquisition').getRecordTypeId();
		initOpportunity.ProjectStart__c = Date.today();
		initOpportunity.CloseDate = Date.today().addYears(1);
		insert initOpportunity;

		LWCObjectMatchingController.PropertyObjectWrapper propertyObjectWrapper = LWCObjectMatchingController.getPropertyObjectWrapper(initOpportunity.Id);
		System.assert(propertyObjectWrapper.opportunity.PrimaryProperty__r.SqmInTotal__c != null, 'SqmInTotal__c must be retrieved');
		System.assert(propertyObjectWrapper.opportunity.PrimaryProperty__r.WALT__c != null, 'WALT__c must be retrieved');
	}
}