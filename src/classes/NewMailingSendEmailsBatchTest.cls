/**
*	
*	@author npo
*	@copyright PARX
*/
@IsTest
private class NewMailingSendEmailsBatchTest
{
    @IsTest
    static void testExecute()
    {
        // prepare data
        TestDataFactory.initializeProducts();

        Embargo__c embargo = new Embargo__c();
        embargo.Name = 'Embargo';
        embargo.Countries__c = 'Egypt;Afghanistan;Belarus;Burundi;Iraq;Yemen';
        insert embargo;

        Account newAcc = TestDataFactory.createAccount('Test 1');
        insert newAcc;

        List<Contact> contacts = TestDataFactory.createContacts(newAcc.Id, 2);
        insert contacts;

        PropertyObject__c propertyObject = TestDataFactory.buildPropertyObject('property', true);
        Opportunity newOpportunity = TestDataFactory.createOpportunity(newAcc.Id, propertyObject);
        insert newOpportunity;

        AccountToOpportunity__c newAccToOpp = TestDataFactory.createAccountToOpportunity(newAcc.Id, newOpportunity.Id);
        insert newAccToOpp;

        List<ContactToInvestor__c> contactToInvestors = TestDataFactory.createContactToInvestor(newAccToOpp.Id, contacts);
        insert contactToInvestors;

        Campaign activeCampaign = new Campaign(IsActive = true, Name = 'Active', Opportunity__c = newOpportunity.Id, Sender__c = UserInfo.getUserId());

        List<EmailTemplate> emailTemplates = [SELECT Id, Subject, HtmlValue FROM EmailTemplate LIMIT 1];
        if (!emailTemplates.isEmpty())
        {
            activeCampaign.GermanEmailTemplateId__c = emailTemplates.get(0).Id;
            activeCampaign.EnglishEmailTemplateId__c = emailTemplates.get(0).Id;
        }

        insert new List<Campaign> {activeCampaign};

        InvestorMailingConfiguration__c mailingConfiguration = new InvestorMailingConfiguration__c(Campaign__c = activeCampaign.Id,
                Investor__c = newAccToOpp.Id);
        CampaignMember member1 =
                new CampaignMember(CampaignId = activeCampaign.Id,
                        InvestorContact__c = contactToInvestors.get(0).Id,
                        Language__c = 'English',
                        SendingType__c = 'To',
                        ContactId = contacts.get(0).Id);
        CampaignMember member2 =
                new CampaignMember(CampaignId = activeCampaign.Id,
                        InvestorContact__c = contactToInvestors.get(1).Id,
                        Language__c = 'German',
                        SendingType__c = 'To',
                        ContactId = contacts.get(1).Id);
        insert new List<CampaignMember>{member1, member2};

        insert mailingConfiguration;

        // start test
        Test.startTest();
        NewMailingSendEmailsBatch batch = new NewMailingSendEmailsBatch();
        batch.campaignId = activeCampaign.Id;
        Database.executeBatch(batch, 100);
        Test.stopTest();
    }
}