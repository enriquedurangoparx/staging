/**
* HttpCalloutMock for Google Geolocation  functionality unit testing
*
* @author: <Maksim Fedorenko> (maksim.fedorenko@parx.com)
* @history:
* version		    | author				                                | changes
* ====================================================================================
* 0.1 26.02.2020	| Maksim Fedorenko (maksim.fedorenko@parx.com)          | initial version.
*/

public with sharing class GeolocationCalloutMock implements HttpCalloutMock
{
    public static String GOOGLE_GEOCODE_RESPONSE = '{ "results" : [ { "address_components" : [ { "long_name" : "63", "short_name" : "63", "types" : [ "street_number" ] }, { "long_name" : "vulica Praŭdy", "short_name" : "vulica Praŭdy", "types" : [ "route" ] }, { "long_name" : "Kastryčnicki rajon", "short_name" : "Kastryčnicki rajon", "types" : [ "political", "sublocality", "sublocality_level_1" ] }, { "long_name" : "Viciebsk", "short_name" : "Viciebsk", "types" : [ "locality", "political" ] }, { "long_name" : "Viciebski rajon", "short_name" : "Viciebski rajon", "types" : [ "administrative_area_level_2", "political" ] }, { "long_name" : "Viciebskaja voblasć", "short_name" : "Viciebskaja voblasć", "types" : [ "administrative_area_level_1", "political" ] }, { "long_name" : "Belarus", "short_name" : "BY", "types" : [ "country", "political" ] } ], "formatted_address" : "vulica Praŭdy 63, Viciebsk, Belarus", "geometry" : { "bounds" : { "northeast" : { "lat" : 55.1797118, "lng" : 30.2424512 }, "southwest" : { "lat" : 55.17899449999999, "lng" : 30.2413353 } }, "location" : { "lat" : 55.1793713, "lng" : 30.241673 }, "location_type" : "ROOFTOP", "viewport" : { "northeast" : { "lat" : 55.1807021302915, "lng" : 30.2432422302915 }, "southwest" : { "lat" : 55.1780041697085, "lng" : 30.2405442697085 } } }, "place_id" : "ChIJzS61mkdxxUYRNAU2jbMm9-M", "types" : [ "premise" ] } ], "status" : "OK" }';

    public HttpResponse respond(HttpRequest request)
    {
        HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type', 'application/json');
        response.setBody(GOOGLE_GEOCODE_RESPONSE);
        response.setStatusCode(200);
        return response;
    }
}