/**
 * @description handler class for mobileSetObjectGeolocationAction.cmp
 * @author ema
 *
 * @copyright PARX
 */
public with sharing class SetObjectGeolocationController
{
    
    @AuraEnabled
    public static String getObjectData(Id recId) 
    {
        try 
        {
            PropertyObject__c record = [SELECT Id, Geolocation__latitude__s, Geolocation__longitude__s FROM PropertyObject__c WHERE Id = :recId LIMIT 1];
            return JSON.serialize(record); 
        } 
        catch (Exception e) 
        {
            // "Convert" the exception into an AuraHandledException
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled
    public static String setObjectCoordinates(Decimal latitude, Decimal longitude, Id recId)
    {
        PropertyObject__c recordToUpdate = new PropertyObject__c(
            Id = recId,
            Geolocation__latitude__s = latitude,
            Geolocation__longitude__s = longitude
        );

        try 
        {
            update recordToUpdate;
            return JSON.serialize(recordToUpdate);
        } 
        catch (Exception e) 
        {
            throw new AuraHandledException(e.getMessage());   
        }
    }
}