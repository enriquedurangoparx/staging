/**
* Testclass for InvestorSelectionHelper.cls
*
* @author: <Egor Markuzov> (egor.markuzov@parx.com)
*
* @history:
* version           | author                                                | changes
* ====================================================================================
* 0.1 11.11.2019    | Egor Markuzov (egor.markuzov@parx.com)                | initial version.
* 0.2 19.12.2019    | Maksim Fedorenko (maksim.fedorneko@parx.com)          | added PropertyObject__c to Opportunity on creation
* 0.3 17.03.2020	| Christian Schwabe (Christian.Schwabe@colliers.com)    | [SFINV/US 2407] Added Saluation for Contact.
*/
@isTest
private class InvestorSelectionHelperTest
{
    @testSetup
    static void setup()
    {
        TestDataFactory.initializeProducts();

        Embargo__c embargo = new Embargo__c();
        embargo.Name = 'Embargo';
        embargo.Countries__c = 'Egypt;Afghanistan;Belarus;Burundi;Iraq;Yemen';
        insert embargo;
    }

    /**
    * test InvestorSelectionHelper.findInvestors()
    */
    @isTest
    public static void testFindInvestors() {
        Account newAccount = TestDataFactory.createAccount('Test Account 1');
        insert newAccount;

        List<Contact> newContactList = TestDataFactory.createContacts(newAccount.Id, 5);
        insert newContactList;

        PropertyObject__c propertyObject = TestDataFactory.buildPropertyObject('property', true);
        Opportunity newOpportunity = TestDataFactory.createOpportunity(newAccount.Id, propertyObject);
        insert newOpportunity;

        AccountToOpportunity__c newAccToOpp = TestDataFactory.createAccountToOpportunity(newAccount.Id, newOpportunity.Id);
        insert newAccToOpp;

        List<ContactToInvestor__c> newContactToInvestorList = TestDataFactory.createContactToInvestor(newAccToOpp.Id, newContactList);
        insert newContactToInvestorList;

        List<InvestorSelectionHelper.sObjectWrapper> contactToInvestorListToCheck = InvestorSelectionHelper.findInvestorContacts(newOpportunity.Id, 15);
        System.assertEquals(newContactToInvestorList.size(), contactToInvestorListToCheck.size(), 'Unexpected number of ContactToInvestor__c records');
    }

    /**
    * test InvestorSelectionHelper.saveInvestors()
    */
    @isTest
    public static void testSaveInvestorContacts()
    {
        List<Account> newAccList = TestDataFactory.createAccount(2);
        insert newAccList;
        System.assert(newAccList.size() > 0, 'Accounts were not be inserted');

        List<Contact> newContactListFirstAcc = TestDataFactory.createContacts(newAccList[0].Id, 5);
        insert newContactListFirstAcc;

        List<Contact> newContactListSecondtAcc = TestDataFactory.createContacts(newAccList[1].Id, 5);
        insert newContactListSecondtAcc;

        PropertyObject__c propertyObject = TestDataFactory.buildPropertyObject('property', true);
        Opportunity newOpportunity = TestDataFactory.createOpportunity(newAccList[0].Id, propertyObject);
        insert newOpportunity;

        AccountToOpportunity__c newAccToOpp = TestDataFactory.createAccountToOpportunity(newAccList[0].Id, newOpportunity.Id);
        insert newAccToOpp;

        List<ContactToInvestor__c> newContactToInvestorList = TestDataFactory.createContactToInvestor(newAccToOpp.Id, newContactListFirstAcc);
        insert newContactToInvestorList;

        List<Contact> contactListForContactToInvestor = new List<Contact>();
        contactListForContactToInvestor.addAll(newContactListFirstAcc);
        contactListForContactToInvestor.addAll(newContactListSecondtAcc);

        String json = System.JSON.serialize(contactListForContactToInvestor);
        InvestorSelectionHelper.saveInvestors('Contact', json, newOpportunity.Id, 'List');
    }

    /**
    * test InvestorSelectionHelper.getInvestors()
    */
    @isTest
    public static void testGetInvestors()
    {
        InvestorsList__c newInvestorList = TestDataFactory.createInvestorList('Test List');
        insert newInvestorList;

        List<Account> accounts = TestDataFactory.createAccount(2);
        insert accounts;

        List<Contact> contacts = TestDataFactory.createContacts(accounts.get(0).Id, 5);
        contacts.addAll(TestDataFactory.createContacts(accounts.get(1).Id, 5));
        insert contacts;

        List<InvestorsListItem__c> investorsListItems = new List<InvestorsListItem__c>
        {
                TestDataFactory.createInvestorListItem(newInvestorList.Id, accounts.get(0).Id),
                TestDataFactory.createInvestorListItem(newInvestorList.Id, accounts.get(1).Id)
        };
        insert investorsListItems;

        InvestorsListItemContact__c investorsListItemContact = new InvestorsListItemContact__c(
                Contact__c = contacts.get(0).Id,
                InvestorsListItem__c = investorsListItems.get(0).Id
        );
        insert investorsListItemContact;

        String jsonResp = InvestorSelectionHelper.getInvestors(null);
        List<InvestorSelectionHelper.InvestorsListViewWrapper> investorsListViewWrap = (List<InvestorSelectionHelper.InvestorsListViewWrapper>) JSON.deserialize(jsonResp, List<InvestorSelectionHelper.InvestorsListViewWrapper>.class);

        System.assertEquals(true, investorsListViewWrap.size() > 0, 'Wrong number of Investor List');
    }

    /**
    * test InvestorSelectionHelper.loadInvestorsList()
    */
    @isTest
    public static void testLoadInvestorsList()
    {
        InvestorsList__c newInvestorList = TestDataFactory.createInvestorList('Test List');
        insert newInvestorList;

        String jsonResp = InvestorSelectionHelper.loadInvestorsList(100);
        InvestorSelectionHelper.ViewWrapperForInvestorsList investorsListViewWrap = (InvestorSelectionHelper.ViewWrapperForInvestorsList) JSON.deserialize(jsonResp, InvestorSelectionHelper.ViewWrapperForInvestorsList.class);

        System.assertEquals(true, investorsListViewWrap.investorsList.size() > 0, 'Wrong number of Investor List');
    }

    /**
    * test InvestorSelectionHelper.loadInvestorsListItems()
    */
    @isTest
    public static void testLoadInvestorsListItems()
    {
        InvestorsList__c newInvestorList = TestDataFactory.createInvestorList('Test List');
        insert newInvestorList;

        List<Account> accounts = TestDataFactory.createAccount(2);
        insert accounts;

        List<Contact> contacts = TestDataFactory.createContacts(accounts.get(0).Id, 5);
        contacts.addAll(TestDataFactory.createContacts(accounts.get(1).Id, 5));
        insert contacts;

        List<InvestorsListItem__c> investorsListItems = new List<InvestorsListItem__c>
        {
                TestDataFactory.createInvestorListItem(newInvestorList.Id, accounts.get(0).Id),
                TestDataFactory.createInvestorListItem(newInvestorList.Id, accounts.get(1).Id)
        };
        insert investorsListItems;

        InvestorsListItemContact__c investorsListItemContact = new InvestorsListItemContact__c(
                Contact__c = contacts.get(0).Id,
                InvestorsListItem__c = investorsListItems.get(0).Id
        );
        insert investorsListItemContact;

        PropertyObject__c propertyObject = TestDataFactory.buildPropertyObject('property', true);
        Opportunity newOpportunity = TestDataFactory.createOpportunity(accounts[0].Id, propertyObject);
        insert newOpportunity;

        String jsonResp = InvestorSelectionHelper.loadInvestorsListItems(newOpportunity.Id, newInvestorList.Id, 100);
        InvestorSelectionHelper.ViewWrapperForInvestorsListItems investorsListViewWrap = (InvestorSelectionHelper.ViewWrapperForInvestorsListItems) JSON.deserialize(jsonResp, InvestorSelectionHelper.ViewWrapperForInvestorsListItems.class);

        System.assertEquals(newInvestorList.Id, investorsListViewWrap.investorListId, 'Wrong Investor List Id');
        System.assertEquals(true, investorsListViewWrap.investorListItemsWithContacts.size() > 0, 'Wrong number of Investor List');
        System.assertEquals(true, investorsListViewWrap.investorListItemsWithoutContacts.size() > 0, 'Wrong number of Investor List');
    }

    /**
    * test InvestorSelectionHelper.loadMoreInvestorListItems()
    */
    @isTest
    public static void testLoadMoreInvestorListItems()
    {
        InvestorsList__c newInvestorList = TestDataFactory.createInvestorList('Test List');
        insert newInvestorList;

        List<Account> accounts = TestDataFactory.createAccount(2);
        insert accounts;

        List<Contact> contacts = TestDataFactory.createContacts(accounts.get(0).Id, 5);
        contacts.addAll(TestDataFactory.createContacts(accounts.get(1).Id, 5));
        insert contacts;

        List<InvestorsListItem__c> investorsListItems = new List<InvestorsListItem__c>
        {
                TestDataFactory.createInvestorListItem(newInvestorList.Id, accounts.get(0).Id),
                TestDataFactory.createInvestorListItem(newInvestorList.Id, accounts.get(1).Id)
        };
        insert investorsListItems;

        InvestorsListItemContact__c investorsListItemContact = new InvestorsListItemContact__c(
                Contact__c = contacts.get(0).Id,
                InvestorsListItem__c = investorsListItems.get(0).Id
        );
        insert investorsListItemContact;

        PropertyObject__c propertyObject = TestDataFactory.buildPropertyObject('property', true);
        Opportunity newOpportunity = TestDataFactory.createOpportunity(accounts[0].Id, propertyObject);
        insert newOpportunity;

        String jsonResp = InvestorSelectionHelper.loadMoreInvestorListItems(newOpportunity.Id, newInvestorList.Id, 100);
        InvestorSelectionHelper.ViewWrapperForInvestorsListItems investorsListViewWrap = (InvestorSelectionHelper.ViewWrapperForInvestorsListItems) JSON.deserialize(jsonResp, InvestorSelectionHelper.ViewWrapperForInvestorsListItems.class);

        System.assertEquals(newInvestorList.Id, investorsListViewWrap.investorListId, 'Wrong Investor List Id');
        System.assertEquals(true, investorsListViewWrap.investorListItemsWithContacts.size() == 0, 'Wrong number of Investor List Item Contacts');
        System.assertEquals(true, investorsListViewWrap.investorListItemsWithoutContacts.size() > 0, 'Wrong number of Investor List Items');
    }

    /**
    * test InvestorSelectionHelper.loadMoreInvestorListItemContacts()
    */
    @isTest
    public static void testLoadMoreInvestorListItemContacts()
    {
        InvestorsList__c newInvestorList = TestDataFactory.createInvestorList('Test List');
        insert newInvestorList;

        List<Account> accounts = TestDataFactory.createAccount(2);
        insert accounts;

        List<Contact> contacts = TestDataFactory.createContacts(accounts.get(0).Id, 5);
        contacts.addAll(TestDataFactory.createContacts(accounts.get(1).Id, 5));
        insert contacts;

        List<InvestorsListItem__c> investorsListItems = new List<InvestorsListItem__c>
        {
                TestDataFactory.createInvestorListItem(newInvestorList.Id, accounts.get(0).Id),
                TestDataFactory.createInvestorListItem(newInvestorList.Id, accounts.get(1).Id)
        };
        insert investorsListItems;

        InvestorsListItemContact__c investorsListItemContact = new InvestorsListItemContact__c(
                Contact__c = contacts.get(0).Id,
                InvestorsListItem__c = investorsListItems.get(0).Id
        );
        insert investorsListItemContact;

        PropertyObject__c propertyObject = TestDataFactory.buildPropertyObject('property', true);
        Opportunity newOpportunity = TestDataFactory.createOpportunity(accounts[0].Id, propertyObject);
        insert newOpportunity;

        String jsonResp = InvestorSelectionHelper.loadMoreInvestorListItemContacts(newOpportunity.Id, newInvestorList.Id, 100);
        InvestorSelectionHelper.ViewWrapperForInvestorsListItems investorsListViewWrap = (InvestorSelectionHelper.ViewWrapperForInvestorsListItems) JSON.deserialize(jsonResp, InvestorSelectionHelper.ViewWrapperForInvestorsListItems.class);

        System.assertEquals(newInvestorList.Id, investorsListViewWrap.investorListId, 'Wrong Investor List Id');
        System.assertEquals(true, investorsListViewWrap.investorListItemsWithContacts.size() > 0, 'Wrong number of Investor List Item Contacts');
        System.assertEquals(true, investorsListViewWrap.investorListItemsWithoutContacts.size() == 0, 'Wrong number of Investor List Items');
    }

    /**
    * test InvestorSelectionHelper.loadMoreSelectedInvestors()
    */
    @isTest
    public static void testLoadMoreSelectedInvestors() {
        Account newAccount = TestDataFactory.createAccount('Test Account 1');
        insert newAccount;

        List<Contact> newContactList = TestDataFactory.createContacts(newAccount.Id, 5);
        insert newContactList;

        PropertyObject__c propertyObject = TestDataFactory.buildPropertyObject('property', true);
        Opportunity newOpportunity = TestDataFactory.createOpportunity(newAccount.Id, propertyObject);
        insert newOpportunity;

        AccountToOpportunity__c newAccToOpp = TestDataFactory.createAccountToOpportunity(newAccount.Id, newOpportunity.Id);
        insert newAccToOpp;

        List<ContactToInvestor__c> newContactToInvestorList = TestDataFactory.createContactToInvestor(newAccToOpp.Id, newContactList);
        insert newContactToInvestorList;

        List<InvestorSelectionHelper.sObjectWrapper> contactToInvestorListToCheck = InvestorSelectionHelper.loadMoreSelectedInvestors(newOpportunity.Id, 100);
        System.assertEquals(0, contactToInvestorListToCheck.size(), 'Unexpected number of ContactToInvestor__c records');
    }

    /**
    * test InvestorSelectionHelper.loadMoreSelectedInvestorContacts()
    */
    @isTest
    public static void testLoadMoreSelectedInvestorContacts() {
        Account newAccount = TestDataFactory.createAccount('Test Account 1');
        insert newAccount;

        List<Contact> newContactList = TestDataFactory.createContacts(newAccount.Id, 5);
        insert newContactList;

        PropertyObject__c propertyObject = TestDataFactory.buildPropertyObject('property', true);
        Opportunity newOpportunity = TestDataFactory.createOpportunity(newAccount.Id, propertyObject);
        insert newOpportunity;

        AccountToOpportunity__c newAccToOpp = TestDataFactory.createAccountToOpportunity(newAccount.Id, newOpportunity.Id);
        insert newAccToOpp;

        List<ContactToInvestor__c> newContactToInvestorList = TestDataFactory.createContactToInvestor(newAccToOpp.Id, newContactList);
        insert newContactToInvestorList;

        List<InvestorSelectionHelper.sObjectWrapper> contactToInvestorListToCheck = InvestorSelectionHelper.loadMoreSelectedInvestorContacts(newOpportunity.Id, 100);
        System.assertEquals(newContactToInvestorList.size(), contactToInvestorListToCheck.size(), 'Unexpected number of ContactToInvestor__c records');
    }

    /**
    * test InvestorSelectionHelper.search()
    */
    @isTest
    public static void testSearch()
    {
        List<Account> newAccList = TestDataFactory.createAccount(2);
        insert newAccList;
        System.assert(newAccList.size() > 0, 'Accounts were not be inserted');

        List<Contact> newContactListFirstAcc = TestDataFactory.createContacts(newAccList[0].Id, 5);
        insert newContactListFirstAcc;

        List<Contact> newContactListSecondtAcc = TestDataFactory.createContacts(newAccList[1].Id, 5);
        insert newContactListSecondtAcc;

        PropertyObject__c propertyObject = TestDataFactory.buildPropertyObject('property', true);
        Opportunity newOpportunity = TestDataFactory.createOpportunity(newAccList[0].Id, propertyObject);
        newOpportunity.Name = 'Test';
        insert newOpportunity;

        Id [] fixedSearchResults= new Id[1];
        fixedSearchResults[0] = newContactListFirstAcc[0].Id;
        Test.setFixedSearchResults(fixedSearchResults);

        Test.startTest();
        List<InvestorSelectionHelper.sObjectWrapper> results = (List<InvestorSelectionHelper.sObjectWrapper>) InvestorSelectionHelper.search(newOpportunity.Id, 'Contact', 'Test', 15);
        Test.stopTest();

        System.assertEquals(results.size(), 1);
    }

    /**
    * test InvestorSelectionHelper.searchByObjectIds()
    */
    @isTest
    public static void testSearchByObjectIds()
    {
        Set<ID> setAccountIDs = new Set<ID>();
        List<Account> newAccList = TestDataFactory.createAccount(2);
        insert newAccList;
        System.assert(newAccList.size() > 0, 'Accounts were not be inserted');
        setAccountIDs.add(newAccList[0].Id);
        setAccountIDs.add(newAccList[1].Id);

        Set<ID> setContactIDs = new Set<ID>();
        List<Contact> newContactListFirstAcc = TestDataFactory.createContacts(newAccList[0].Id, 5);
        insert newContactListFirstAcc;
        setContactIDs.add(newContactListFirstAcc[0].Id);

        List<Contact> newContactListSecondtAcc = TestDataFactory.createContacts(newAccList[1].Id, 5);
        insert newContactListSecondtAcc;
        setContactIDs.add(newContactListSecondtAcc[0].Id);

        PropertyObject__c propertyObject = TestDataFactory.buildPropertyObject('property', true);
        Opportunity newOpportunity = TestDataFactory.createOpportunity(newAccList[0].Id, propertyObject);
        newOpportunity.Name = 'Test';
        insert newOpportunity;


        Test.startTest();
        List<InvestorSelectionHelper.sObjectWrapper> resultsForContacts = InvestorSelectionHelper.searchByObjectIds('Contact', System.JSON.serialize(setContactIDs), newOpportunity.Id, 25);
        List<InvestorSelectionHelper.sObjectWrapper> resultsForAccounts = InvestorSelectionHelper.searchByObjectIds('Account', System.JSON.serialize(setAccountIDs), newOpportunity.Id, 25);
        Test.stopTest();

        System.assertEquals(resultsForContacts.size(), 2);
        System.assertEquals(resultsForAccounts.size(), 10);
    }

    /**
    * test InvestorSelectionHelper.InvestorService()
    */
    @isTest
    public static void testInvestorServiceGetAccountIdInvestorMap()
    {
        List<Account> newAccList = TestDataFactory.createAccount(2);
        insert newAccList;

        PropertyObject__c propertyObject = TestDataFactory.buildPropertyObject('property', true);
        Opportunity newOpportunity = TestDataFactory.createOpportunity(newAccList[0].Id, propertyObject);
        newOpportunity.Name = 'Test';
        insert newOpportunity;

        AccountToOpportunity__c newAccToOpp = TestDataFactory.createAccountToOpportunity(newAccList[0].Id, newOpportunity.Id);
        insert newAccToOpp;

        Map<Id, AccountToOpportunity__c> resp = InvestorService.getAccountIdInvestorMap(newAccList, newOpportunity.Id);

        System.assertEquals(true, resp.containsKey(newAccList[0].Id));

    }

    /**
    * test InvestorSelectionHelper.InvestorService()
    */
    @isTest
    public static void testGetBidderInvestors()
    {
        List<Account> newAccList = TestDataFactory.createAccount(2);
        insert newAccList;

        PropertyObject__c propertyObject = TestDataFactory.buildPropertyObject('property', true);
        Opportunity newOpportunity = TestDataFactory.createOpportunity(newAccList[0].Id, propertyObject);
        newOpportunity.Name = 'Test';
        insert newOpportunity;
        
        AccountToOpportunity__c newAccToOpp = TestDataFactory.createAccountToOpportunity(newAccList[0].Id, newOpportunity.Id);
        newAccToOpp.LastBidDate__c = Date.today();
        newAccToOpp.Underbidder__c = 'Yes';
        insert newAccToOpp;

        Date todayDate = Date.today();
        String jsonResp = InvestorSelectionHelper.getBidderInvestors(newOpportunity.Id, todayDate, todayDate.addDays(1), 25);

        Test.startTest();
        List<InvestorSelectionHelper.BidderWrapper> resp = (List<InvestorSelectionHelper.BidderWrapper>) JSON.deserialize(jsonResp, List<InvestorSelectionHelper.BidderWrapper>.class);
        Test.stopTest();
        
        System.assertEquals(newAccToOpp.Id, resp[0].investorId);
    }

    /**
    * test InvestorSelectionHelper.InvestorService()
    */
    @isTest
    public static void testSaveBidderInvestors()
    {
        List<Account> newAccList = TestDataFactory.createAccount(2);
        insert newAccList;

        PropertyObject__c propertyObject = TestDataFactory.buildPropertyObject('property', true);
        Opportunity newOpportunity = TestDataFactory.createOpportunity(newAccList[0].Id, propertyObject);
        newOpportunity.Name = 'Test';
        insert newOpportunity;

        Opportunity newOpportunity2 = TestDataFactory.createOpportunity(newAccList[0].Id, propertyObject);
        newOpportunity.Name = 'Test2';
        insert newOpportunity2;
        
        AccountToOpportunity__c newAccToOpp = TestDataFactory.createAccountToOpportunity(newAccList[0].Id, newOpportunity.Id);
        newAccToOpp.LastBidDate__c = Date.today();
        newAccToOpp.Underbidder__c = 'Yes';
        insert newAccToOpp;

        Date todayDate = Date.today();
        String jsonResp = InvestorSelectionHelper.getBidderInvestors(newOpportunity.Id, todayDate, todayDate.addDays(1), 25);

        Test.startTest();
        InvestorSelectionHelper.saveInvestors(null, jsonResp, newOpportunity2.Id, 'bidder');
        Test.stopTest();
        
        System.assertEquals(1, [SELECT Id FROM AccountToOpportunity__c WHERE Opportunity__c = :newOpportunity2.Id].size());
    }
}