/**
*	
*	@author npo
*	@copyright PARX
*/
public without sharing class NewMailingService
{
    public static final String SENDING_TYPE_NONE = '-';
    public static final String SENDING_TYPE_TO = 'To';
    public static final String SENDING_TYPE_CC = 'Cc';
    public static final String LANGUAGE_ENGLISH = 'English';
    public static final String LANGUAGE_GERMAN = 'German';
    public static final String CAMPAIGN_SENDING_FINISHED = 'Sending finished';

    public static Messaging.SingleEmailMessage populateEmails(Id orgWideEmailAddressId, User sender, Id investorId,
            EmailTemplate emailTemplate, String salutation, List<String> toIds, List<String> ccIds,
            List<String> bccEmails, List<Id> attachmentsIds)
    {
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        System.debug('investorId: ' + investorId);
        System.debug('toIds: ' + toIds);
        System.debug('ccIds: ' + ccIds);
        System.debug('bccEmails: ' + bccEmails);

        String subject = emailTemplate.Subject;
        String htmlBody = emailTemplate.HtmlValue;
        if (!Test.isRunningTest())
        {
            htmlBody = htmlBody.replace('{{{Recipient.Salutation}}}', String.isNotBlank(salutation) ? salutation : '');
            htmlBody = htmlBody.replace('{{{Sender.Company}}}', String.isNotBlank(sender.CompanyName) ? sender.CompanyName : '');
            htmlBody = htmlBody.replace('{{{Sender.FirstName}}}', String.isNotBlank(sender.FirstName) ? sender.FirstName : '');
            htmlBody = htmlBody.replace('{{{Sender.LastName}}}', String.isNotBlank(sender.LastName) ? sender.LastName : '');
            htmlBody = htmlBody.replace('{{{Sender.Title}}}', String.isNotBlank(sender.Title) ? sender.Title : '');
            htmlBody = htmlBody.replace('{{{Sender.Country}}}', String.isNotBlank(sender.Country) ? sender.Country : '');
            htmlBody = htmlBody.replace('{{{Sender.Department}}}', String.isNotBlank(sender.Department) ? sender.Department : '');
            htmlBody = htmlBody.replace('{{{Sender.Email}}}', String.isNotBlank(sender.Email) ? sender.Email : '');
            htmlBody = htmlBody.replace('{{{Sender.Phone}}}', String.isNotBlank(sender.Phone) ? sender.Phone : '');
            htmlBody = htmlBody.replace('{{{Sender.Mobile}}}', String.isNotBlank(sender.MobilePhone) ? sender.MobilePhone : '');
        }

        email.setSubject(subject);
        // email.setPlainTextBody('test');
        email.setHtmlBody(htmlBody);
        email.setOrgWideEmailAddressId(orgWideEmailAddressId);
        email.setToAddresses(toIds);
        email.setEntityAttachments(attachmentsIds);
        if (!ccIds.isEmpty())
        {
            email.setCcAddresses(ccIds);
        }
        if (!bccEmails.isEmpty())
        {
            email.setBccAddresses(bccEmails);
        }

        // email.setTargetObjectId(toIds);
        email.setSaveAsActivity(true);
        email.setWhatId(investorId);
        // email.setTemplateId(templateId)
        // email.setTargetObjectId(contact.Id);


        return email;
    }

    public static List<Messaging.SingleEmailMessage> prepareMessages(
            List<InvestorMailingConfiguration__c> investorMailingConfigurations,
            Map<Id, CampaignMember> campaignMemberByInvestorContact,
            Id campaignId)
    {
        Map<Id, InvestorMailingConfiguration__c> configurationMapByInvestor = NewMailingController.prepareInvestorMailingConfigurationMap(campaignId);


        List<Messaging.SingleEmailMessage> emailsList = new List<Messaging.SingleEmailMessage>();
        Campaign c =
        [
                SELECT Id, Name, Bcc__c, Sender__c, InvestorStatus__c, GermanEmailTemplateId__c,
                        EnglishEmailTemplateId__c, Sender__r.Name, Opportunity__c, IsActive,
                        Sender__r.CompanyName, Sender__r.FirstName, Sender__r.LastName, Sender__r.Title,
                        Sender__r.Country, Sender__r.Department, Sender__r.Email, Sender__r.Phone, Sender__r.MobilePhone
                FROM Campaign
                WHERE Id = :campaignId
        ];

        User sender;
        Id orgWideEmailAddressId;
        if (c.Sender__c != null)
        {
            sender = [SELECT Id, Email, Name, CompanyName, FirstName, LastName, Title, Country, Department, Phone, MobilePhone FROM User WHERE Id = :c.Sender__c];
            List<OrgWideEmailAddress> owea =
            [
                    SELECT Id
                    FROM OrgWideEmailAddress
                    WHERE Address = :sender.Email
            ];
            if (!owea.isEmpty())
            {
                orgWideEmailAddressId = owea.get(0).Id;
            } else
            {
                setError(investorMailingConfigurations, 'Org wide address was not found.');
                return null;
            }
        } else
        {
            if (!Test.isRunningTest())
            {
                setError(investorMailingConfigurations, 'Sender is not specified.');
                return null;
            }
            else
            {
                // we need any id
                List<OrgWideEmailAddress> owea =
                [
                        SELECT Id
                        FROM OrgWideEmailAddress
                        LIMIT 1
                ];
                if (!owea.isEmpty())
                {
                    orgWideEmailAddressId = owea.get(0).Id;
                }
            }
        }

        List<Id> investorsIds = new List<Id>();
        for (InvestorMailingConfiguration__c configuration : investorMailingConfigurations)
        {
            investorsIds.add(configuration.Investor__c);
        }
        List<AccountToOpportunity__c> investors = NewMailingController.getInvestorsWithContacts(investorsIds);


        // get email templates data
        Set<Id> selectedEmailTemplateIdsSet = new Set<Id> {c.EnglishEmailTemplateId__c, c.GermanEmailTemplateId__c};
        selectedEmailTemplateIdsSet.remove(null);

        if (selectedEmailTemplateIdsSet.isEmpty())
        {
            // unexpected behavior we have to set email template
            setError(investorMailingConfigurations, 'Email Templates were not found.');
            return null;
        }

        Map<Id, EmailTemplate> emailTemplateMap = new Map<Id, EmailTemplate>
                ([
                        SELECT Id, Name, Subject, HtmlValue, Body, (SELECT Id, ContentDocumentId FROM AttachedContentDocuments)
                        FROM EmailTemplate
                        WHERE Id = :selectedEmailTemplateIdsSet
                ]);
        Map<Id, List<Id>> attachmentsIdsByEmail = prepareAttachmentsIds(emailTemplateMap);

        if (investors != null && !investors.isEmpty())
        {
            for (AccountToOpportunity__c investor : investors)
            {
                Set<String> toIdsEnSet = new Set<String>();
                Set<String> toIdsDeSet = new Set<String>();
                Set<String> ccIdsEnSet = new Set<String>();
                Set<String> ccIdsDeSet = new Set<String>();
                if (investor.InvestorContacts__r != null)
                {
                    for (ContactToInvestor__c investorContact : investor.InvestorContacts__r)
                    {
                        CampaignMember campaignMember = campaignMemberByInvestorContact.get(investorContact.Id);
                        if (campaignMember != null)
                        {
                            if (String.isNotBlank(campaignMember.SendingType__c) && campaignMember.SendingType__c != SENDING_TYPE_NONE)
                            {
                                // collect id for SingleEmailMessage.setCcAddresses and setTargetObjectId(targetObjectId)
                                if (campaignMember.SendingType__c == SENDING_TYPE_TO && campaignMember.Language__c == LANGUAGE_ENGLISH)
                                {
                                    toIdsEnSet.add(investorContact.Contact__c);
                                } else if (campaignMember.SendingType__c == SENDING_TYPE_TO && campaignMember.Language__c == LANGUAGE_GERMAN)
                                {
                                    toIdsDeSet.add(investorContact.Contact__c);
                                } else if (campaignMember.SendingType__c == SENDING_TYPE_CC && campaignMember.Language__c == LANGUAGE_ENGLISH)
                                {
                                    ccIdsEnSet.add(investorContact.Contact__c);
                                } else if (campaignMember.SendingType__c == SENDING_TYPE_CC && campaignMember.Language__c == LANGUAGE_GERMAN)
                                {
                                    ccIdsDeSet.add(investorContact.Contact__c);
                                }
                            }
                        }
                    }
                }

                toIdsEnSet.remove(null);
                toIdsDeSet.remove(null);
                ccIdsEnSet.remove(null);
                ccIdsDeSet.remove(null);
                InvestorMailingConfiguration__c configurationForInvestor = configurationMapByInvestor.get(investor.Id);
                List<String> bccEmails = c.Bcc__c != null ? c.Bcc__c.split(';') : new List<String>();
                if (!toIdsEnSet.isEmpty())
                {
                    List<String> toIdsEnList = new List<String>();
                    toIdsEnList.addAll(toIdsEnSet);
                    List<String> ccIdsEnList = new List<String>();
                    toIdsEnList.addAll(ccIdsEnSet);
                    Messaging.SingleEmailMessage mailEn = populateEmails(
                            orgWideEmailAddressId,
                            sender,
                            investor.Id,
                            emailTemplateMap.get(c.EnglishEmailTemplateId__c),
                            configurationForInvestor.EnglishSalutationSequence__c,
                            toIdsEnList,
                            ccIdsEnList,
                            bccEmails,
                            attachmentsIdsByEmail.get(emailTemplateMap.get(c.EnglishEmailTemplateId__c).Id)
                    );

                    emailsList.add(mailEn);
                }

                if (!toIdsDeSet.isEmpty())
                {
                    List<String> toIdsDeList = new List<String>();
                    toIdsDeList.addAll(toIdsDeSet);
                    List<String> ccIdsDeList = new List<String>();
                    ccIdsDeList.addAll(ccIdsDeSet);

                    Messaging.SingleEmailMessage mailDe = populateEmails(
                            orgWideEmailAddressId,
                            sender,
                            investor.Id,
                            emailTemplateMap.get(c.GermanEmailTemplateId__c),
                            configurationForInvestor.GermanSalutationSequence__c,
                            toIdsDeList,
                            ccIdsDeList,
                            bccEmails,
                            attachmentsIdsByEmail.get(emailTemplateMap.get(c.GermanEmailTemplateId__c).Id)
                    );
                    emailsList.add(mailDe);
                }
            }
        }
        return emailsList;
    }

    public static Map<Id, CampaignMember> prepareCampaignMemberMap(Id campaignId)
    {
        Map<Id, CampaignMember> campaignMemberByInvestorContact = new Map<Id, CampaignMember>();
        List<CampaignMember> campaignMembers;
        System.debug('...campaignId ' + campaignId);
        if (campaignId != null)
        {

            campaignMembers =
            [
                    SELECT Id, InvestorStatus__c, Language__c, SendingType__c, InvestorContact__c, Status
                    FROM CampaignMember
                    WHERE CampaignId = :campaignId
            ];
            for (CampaignMember cm : campaignMembers)
            {
                campaignMemberByInvestorContact.put(cm.InvestorContact__c, cm);
            }
        }

        return campaignMemberByInvestorContact;
    }

    public static void setError(List<InvestorMailingConfiguration__c> investorMailingConfigurations, String message)
    {
        for (InvestorMailingConfiguration__c configuration : investorMailingConfigurations)
        {
            configuration.SendingStatus__c = CAMPAIGN_SENDING_FINISHED;
            configuration.SendingErrorMessage__c = message;
        }
    }

    public static void setSuccess(List<InvestorMailingConfiguration__c> investorMailingConfigurations)
    {
        for (InvestorMailingConfiguration__c configuration : investorMailingConfigurations)
        {
            configuration.SendingStatus__c = CAMPAIGN_SENDING_FINISHED;
            configuration.SendingErrorMessage__c = '';
        }
    }

    public static List<Id> prepareEmailAttachments(EmailTemplate template)
    {
        List<Id> attachmentsIds = new List<Id>();
        if (template.AttachedContentDocuments != null && !template.AttachedContentDocuments.isEmpty())
        {
            for (AttachedContentDocument doc : template.AttachedContentDocuments)
            {
                attachmentsIds.add(doc.ContentDocumentId);
            }
        }
        return attachmentsIds;
    }

    public static Map<Id, List<Id>> prepareAttachmentsIds(Map<Id, EmailTemplate> emailTemplateMap)
    {
        Map<Id, List<Id>> attachmentsIdsByEmailTemplate = new Map<Id, List<Id>>();

        Map<Id, Id> emailByContentDocument = new Map<Id, Id>();
        for (EmailTemplate template : emailTemplateMap.values())
        {
            if (!template.AttachedContentDocuments.isEmpty())
            {
                for (AttachedContentDocument doc : template.AttachedContentDocuments)
                {
                    emailByContentDocument.put(doc.ContentDocumentId, template.Id);
                }
            }
        }

        List<ContentVersion> contentVersions =
        [
                SELECT Id, ContentDocumentId
                FROM ContentVersion
                WHERE ContentDocumentId IN :emailByContentDocument.keySet() AND IsLatest = TRUE
        ];

        for (ContentVersion cv : contentVersions)
        {
            Id emailId = emailByContentDocument.get(cv.ContentDocumentId);

            if (!attachmentsIdsByEmailTemplate.containsKey(emailId))
            {
                attachmentsIdsByEmailTemplate.put(emailId, new List<Id>());
            }
            attachmentsIdsByEmailTemplate.get(emailId).add(cv.Id);
        }

        return attachmentsIdsByEmailTemplate;
    }
}