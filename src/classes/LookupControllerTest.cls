/*
 * @author jens.siffermann@youperience.com
 * @date 2019-10-29
 * @version 1.0.0
 *
 * @description Unit test for LookupController
 */
@isTest
public with sharing class LookupControllerTest {
    /**
     * test LookupSearchResult object
     */
    @isTest
    public static void testLookupSearchResult() {
        Account testAccount = InvestorsListControllerTestDataFactory.createAccount();

        Test.startTest();
        LookupSearchResult result = new LookupSearchResult(
            testAccount.Id,
            'Account',
            'standard:icon',
            testAccount.Name,
            testAccount.Type
        );
        Test.stopTest();

        System.assertEquals(testAccount.Id, result.getId(), 'Unexpected Account Id');
        System.assertEquals('Account', result.getSObjectType(), 'Unexpected object type - should be Account');
        System.assertEquals('standard:icon', result.getIcon(), 'Unexpected icon');
        System.assertEquals(testAccount.Name, result.getTitle(), 'Unexpected Title');
        System.assertEquals(testAccount.Type, result.getSubtitle(), 'Unexpected Subtitle');
    }

    /**
     * test search()
     */
    @isTest
    public static void testLookupSearch() {
        Account a = InvestorsListControllerTestDataFactory.createAccountWithContacts(
            20
        );

        // set fixed search result for sosl search
        // https://developer.salesforce.com/docs/atlas.en-us.apexcode.meta/apexcode/apex_testing_SOSL.htm?search_text=setfixedsearch
        Id[] fixedSearchResults = new List<Id>{ a.Id };
        Test.setFixedSearchResults(fixedSearchResults);

        Test.startTest();
        List<LookupSearchResult> searchResult = LookupController.search(
            'Contact',
            'AccountId',
            'John',
            null
        );
        Test.stopTest();

        System.assertEquals(1, searchResult.size(), 'Unexpected search result');
    }

    /**
     * test search() with no records
     */
    @isTest
    public static void testLookupSearchWithNoRecords() {
 
        // set fixed search result for sosl search
        // https://developer.salesforce.com/docs/atlas.en-us.apexcode.meta/apexcode/apex_testing_SOSL.htm?search_text=setfixedsearch
        Id[] fixedSearchResults = new List<Id>{};
        Test.setFixedSearchResults(fixedSearchResults);

        Test.startTest();
        List<LookupSearchResult> searchResult = LookupController.search(
            'Contact',
            'AccountId',
            'John',
            null
        );
        Test.stopTest();

        System.assertEquals(0, searchResult.size(), 'Unexpected search result');
    }

    /**
     * test search() with object type limit
     */
    @isTest
    public static void testLookupSearchWithLimit() {
        Account a = InvestorsListControllerTestDataFactory.createAccountWithContacts(
            20
        );

        // set fixed search result for sosl search
        // https://developer.salesforce.com/docs/atlas.en-us.apexcode.meta/apexcode/apex_testing_SOSL.htm?search_text=setfixedsearch
        Id[] fixedSearchResults = new List<Id>{ a.Id };
        Test.setFixedSearchResults(fixedSearchResults);

        String[] limitTo = new List<String>{ 'Account' };

        Test.startTest();
        List<LookupSearchResult> searchResult = LookupController.search(
            'Contact',
            'AccountId',
            'John',
            limitTo
        );
        Test.stopTest();

        System.assertEquals(1, searchResult.size(), 'Unexpected search result');
        System.assertEquals(a.Name, searchResult.get(0).getTitle(), 'Unexpected title');
    }

    /**
     * test search() with object type limit
     */
    @isTest
    public static void testLookupSearchWithSelected() {
        Account a = InvestorsListControllerTestDataFactory.createAccountWithContacts(
            20
        );

        // set fixed search result for sosl search
        // https://developer.salesforce.com/docs/atlas.en-us.apexcode.meta/apexcode/apex_testing_SOSL.htm?search_text=setfixedsearch
        Id[] fixedSearchResults = new List<Id>{ a.Id };
        Test.setFixedSearchResults(fixedSearchResults);

        String[] limitTo = new List<String>{ 'Account' };
        List<String> selected = new List<String>();
        selected.add(a.Id);

        Test.startTest();
        List<LookupSearchResult> searchResult = LookupController.search(
            'Contact',
            'AccountId',
            'John',
            limitTo,
            selected
        );
        Test.stopTest();

        System.assertEquals(0, searchResult.size(), 'Unexpected search result');
    }

    /**
     * test search() with lookup filter
     */
    @isTest
    public static void testLookupSearchWithLookupFilter() {
        Account a = InvestorsListControllerTestDataFactory.createAccountWithContacts(
            20
        );

        // set fixed search result for sosl search
        // https://developer.salesforce.com/docs/atlas.en-us.apexcode.meta/apexcode/apex_testing_SOSL.htm?search_text=setfixedsearch
        Id[] fixedSearchResults = new List<Id>{ a.Id };
        Test.setFixedSearchResults(fixedSearchResults);

        List<LookupController.LookupFilter> lookupFilter = new List<LookupController.LookupFilter>();
        LookupController.LookupFilter filter = new LookupController.LookupFilter();
        filter.objectType = 'Account';
        filter.filter = 'Name = \'TestAccount\'';
        lookupFilter.add(filter);

        Test.startTest();
        List<LookupSearchResult> searchResult = LookupController.search(
            'Contact',
            'AccountId',
            'John',
            null,
            null,
            lookupFilter
        );
        Test.stopTest();

        System.assertEquals(1, searchResult.size(), 'Unexpected search result');
    }

    /**
     * test search() with lookup filter and selected
     */
    @isTest
    public static void testLookupSearchWithLookupFilterAndSelected() {
        Account a = InvestorsListControllerTestDataFactory.createAccountWithContacts(
            20
        );

        // set fixed search result for sosl search
        // https://developer.salesforce.com/docs/atlas.en-us.apexcode.meta/apexcode/apex_testing_SOSL.htm?search_text=setfixedsearch
        Id[] fixedSearchResults = new List<Id>{};
        Test.setFixedSearchResults(fixedSearchResults);

        List<LookupController.LookupFilter> lookupFilter = new List<LookupController.LookupFilter>();
        LookupController.LookupFilter filter = new LookupController.LookupFilter();
        filter.objectType = 'Account';
        filter.filter = 'Name = \'TestAccount\'';
        lookupFilter.add(filter);

        String[] limitTo = new List<String>{ 'Account' };
        List<String> selected = new List<String>();
        selected.add(a.Id);

        Test.startTest();
        List<LookupSearchResult> searchResult = LookupController.search(
            'Contact',
            'AccountId',
            'John',
            limitTo,
            selected,
            lookupFilter
        );
        Test.stopTest();

        System.assertEquals(0, searchResult.size(), 'Unexpected search result');
    }

    /**
     * test LookupController.lookupDetail
     */
    @isTest
    public static void lookupDetail() {
        Contact testContact = InvestorsListControllerTestDataFactory.createContact();

        Test.startTest();
        LookupController.LookupRecordResult result = LookupController.lookupDetail(
            'AccountId',
            testContact.AccountId
        );
        Test.stopTest();

        System.assertEquals(testContact.AccountId, result.record.Id, 'Unexpected Account Id');
    }

    /**
     * test LookupController.lookupDetail
     */
    @isTest
    public static void lookupDetailWithNullRecord() {
        Test.startTest();
        try {
            LookupController.LookupRecordResult result = LookupController.lookupDetail(
                'AccountId',
                null
            );
        } catch (AuraHandledException error) {
            System.assert(error != null, 'Unexpected error handling - error should not be null');
        }

        Test.stopTest();
    }
}