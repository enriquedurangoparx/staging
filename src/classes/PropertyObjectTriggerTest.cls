/**
* Test class for PropertyObjectTrigger.cls
*
* @author: <Maksim Fedorenko> (maksim.fedorenko@parx.com)
*
* @history:
* version		    | author				                                | changes
* ====================================================================================
* 0.1 31.01.2020	| Maksim Fedorenko (maksim.fedorenko@parx.com)          | initial version.
* 0.2 12.02.2020	| Christian Schwabe (Christian.Schwabe@colliers.com)    | [SFINV/US 1977] Added testmethods for aggregateObjectsToPortfolio().
* 0.3 06.04.2020	| Christian Schwabe (Christian.Schwabe@colliers.com)    | [SFINV/US 2886] Changed picklist values for matching.
*/
@IsTest
public with sharing class PropertyObjectTriggerTest
{
    @TestSetup
    static void setup()
    {
        Embargo__c embargo = new Embargo__c();
        embargo.Name = 'Embargo';
        embargo.Countries__c = 'Egypt;Afghanistan;Belarus;Burundi;Iraq;Yemen';
        insert embargo;
    }

    @IsTest
    static void copyDataToOpportunityTest()
    {
        PropertyObject__c property = new PropertyObject__c(
                WALT__c = 1, GrossYield__c = 2, NetYield__c = 3, FactorActual__c = 4, FactorTarget__c = 5, IRR__c = 6, CoC__c = 7, AmountOfSpaceLet__c = 8,
                SqmPlot__c = 9, SqmInTotal__c = 10, UnitMeasuring__c = 'BGF'
        );
        insert property;

        Opportunity opportunity = TestDataFactory.createOpportunity(null, property);
        insert opportunity;

        property.WALT__c = 2;
        property.NetYield__c = 4;
        property.FactorTarget__c = 6;
        property.CoC__c = 8;
        update property;

        Opportunity updatedOpportunity = [
                SELECT Id, WALTInYears__c, GrossYield__c, NetYield__c, FactorActual__c, FactorTarget__c, IRR__c, CoC__c, AmountOfSpaceLet__c, PlotSqm__c,
                        TotalSqm__c, UnitMeasurement__c
                FROM Opportunity
                WHERE Id = :opportunity.Id
        ];

        System.assertEquals(property.WALT__c, updatedOpportunity.WALTInYears__c, 'The values should be equal');
        System.assertEquals(property.GrossYield__c, updatedOpportunity.GrossYield__c, 'The values should be equal');
        System.assertEquals(property.NetYield__c, updatedOpportunity.NetYield__c, 'The values should be equal');
        System.assertEquals(property.FactorActual__c, updatedOpportunity.FactorActual__c, 'The values should be equal');
        System.assertEquals(property.FactorTarget__c, updatedOpportunity.FactorTarget__c, 'The values should be equal');
        System.assertEquals(property.IRR__c, updatedOpportunity.IRR__c, 'The values should be equal');
        System.assertEquals(property.CoC__c, updatedOpportunity.CoC__c, 'The values should be equal');
        System.assertEquals(property.AmountOfSpaceLet__c, updatedOpportunity.AmountOfSpaceLet__c, 'The values should be equal');
        System.assertEquals(property.SqmPlot__c, updatedOpportunity.PlotSqm__c, 'The values should be equal');
        System.assertEquals(property.SqmInTotal__c, updatedOpportunity.TotalSqm__c, 'The values should be equal');
        System.assertEquals(property.UnitMeasuring__c, updatedOpportunity.UnitMeasurement__c, 'The values should be equal');
    }

    @IsTest
    static void testAggregationForObjectsToPortfolioInsert(){
        Portfolio__c portfolio = new Portfolio__c();
        portfolio.Name = 'Test-Portfolio';
        insert portfolio;

        List<PropertyObject__c> listOfPropertiesToInsert = new List<PropertyObject__c>();
        PropertyObject__c propertyObject1 = new PropertyObject__c();
        propertyObject1.Portfolio__c = portfolio.Id;
        propertyObject1.Country__c = 'Germany';
        propertyObject1.Market__c = 'Frankfurt';
        propertyObject1.Submarket__c = 'CGN City';
        propertyObject1.FederalState__c = 'Hessen';
        propertyObject1.MacroLocation__c = 'B&C Cities';
        propertyObject1.MicroLocation__c = 'A-Location';
        listOfPropertiesToInsert.add(propertyObject1);

        PropertyObject__c propertyObject2 = new PropertyObject__c();
        propertyObject2.Portfolio__c = portfolio.Id;
        propertyObject2.Country__c = 'France';
        propertyObject2.Market__c = 'Hamburg';
        propertyObject2.Submarket__c = 'CGN Deutz';
        propertyObject2.FederalState__c = 'Bavaria';
        propertyObject2.MacroLocation__c = 'Top 7';
        propertyObject2.MicroLocation__c = 'Outermost Region';
        listOfPropertiesToInsert.add(propertyObject2);
        insert listOfPropertiesToInsert;

        portfolio = [SELECT Id, Countries__c, FederalState__c, MacroLocations__c, Markets__c, MicroLocations__c, Submarkets__c FROM Portfolio__c WHERE Id = :portfolio.Id];
        System.assert(portfolio.Countries__c.contains('Germany'), 'The picklistvalue "Germany" should be aggregated to Portfolio__c.');
        System.assert(portfolio.Countries__c.contains('France'), 'The picklistvalue "France" should be aggregated to Portfolio__c.');

        System.assert(portfolio.Markets__c.contains('Frankfurt'), 'The picklistvalue "Frankfurt" should be aggregated to Portfolio__c.');
        System.assert(portfolio.Markets__c.contains('Hamburg'), 'The picklistvalue "Hamburg" should be aggregated to Portfolio__c.');

        System.assert(portfolio.Submarkets__c.contains('CGN City'), 'The picklistvalue "CGN City" should be aggregated to Portfolio__c.');
        System.assert(portfolio.Submarkets__c.contains('CGN Deutz'), 'The picklistvalue "CGN Deutz" should be aggregated to Portfolio__c.');

        System.assert(portfolio.FederalState__c.contains('Hessen'), 'The picklistvalue "Hessen" should be aggregated to Portfolio__c.');
        System.assert(portfolio.FederalState__c.contains('Bavaria'), 'The picklistvalue "Bavaria" should be aggregated to Portfolio__c.');

        System.assert(portfolio.MacroLocations__c.contains('B&C Cities'), 'The picklistvalue "B&C Cities" should be aggregated to Portfolio__c.');
        System.assert(portfolio.MacroLocations__c.contains('Top 7'), 'The picklistvalue "Top 7" should be aggregated to Portfolio__c.');

        System.assert(portfolio.MicroLocations__c.contains('A-Location'), 'The picklistvalue "A-Location" should be aggregated to Portfolio__c.');
        System.assert(portfolio.MicroLocations__c.contains('Outermost Region'), 'The picklistvalue "Outermost Region" should be aggregated to Portfolio__c.');
    }

    @IsTest
    static void testAggregationForObjectsToPortfolioUpdate(){
        Portfolio__c portfolio = new Portfolio__c();
        portfolio.Name = 'Test-Portfolio';
        insert portfolio;

        List<PropertyObject__c> listOfPropertiesToInsert = new List<PropertyObject__c>();
        PropertyObject__c propertyObject1 = new PropertyObject__c();
        propertyObject1.Portfolio__c = portfolio.Id;
        propertyObject1.Country__c = 'Germany';
        propertyObject1.Market__c = 'Frankfurt';
        propertyObject1.Submarket__c = 'CGN Köln-West';
        propertyObject1.FederalState__c = 'Hessen';
        propertyObject1.MacroLocation__c = 'B&C Cities';
        propertyObject1.MicroLocation__c = 'A-Location';
        listOfPropertiesToInsert.add(propertyObject1);

        PropertyObject__c propertyObject2 = new PropertyObject__c();
        propertyObject2.Portfolio__c = portfolio.Id;
        propertyObject2.Country__c = 'France';
        propertyObject2.Market__c = 'Hamburg';
        propertyObject2.Submarket__c = 'CGN Köln-Ost';
        propertyObject2.FederalState__c = 'Bavaria';
        propertyObject2.MacroLocation__c = 'Top 7';
        propertyObject2.MicroLocation__c = 'Outermost Region';
        listOfPropertiesToInsert.add(propertyObject2);
        insert listOfPropertiesToInsert;

        propertyObject1.Portfolio__c = portfolio.Id;
        propertyObject1.Country__c = 'Belgium';
        propertyObject1.Market__c = 'Munich';
        propertyObject1.Submarket__c = 'CGN Sülz/Lindenthal/Klettenberg';
        propertyObject1.FederalState__c = 'Saarland';
        propertyObject1.MacroLocation__c = 'Top 7';
        propertyObject1.MicroLocation__c = 'Periphery';
        update propertyObject1;

        portfolio = [SELECT Id, Countries__c, FederalState__c, MacroLocations__c, Markets__c, MicroLocations__c, Submarkets__c FROM Portfolio__c WHERE Id = :portfolio.Id];
        System.assert(portfolio.Countries__c.contains('Belgium'), 'The picklistvalue "Belgium" should be aggregated to Portfolio__c.');
        System.assert(portfolio.Countries__c.contains('France'), 'The picklistvalue "France" should be aggregated to Portfolio__c.');

        System.assert(portfolio.Markets__c.contains('Munich'), 'The picklistvalue "Munich" should be aggregated to Portfolio__c.');
        System.assert(portfolio.Markets__c.contains('Hamburg'), 'The picklistvalue "Hamburg" should be aggregated to Portfolio__c.');

        System.assert(portfolio.Submarkets__c.contains('CGN Sülz/Lindenthal/Klettenberg'), 'The picklistvalue "CGN Sülz/Lindenthal/Klettenberg" should be aggregated to Portfolio__c.');
        System.assert(portfolio.Submarkets__c.contains('CGN Köln-Ost'), 'The picklistvalue "CGN Köln-Ost" should be aggregated to Portfolio__c.');

        System.assert(portfolio.FederalState__c.contains('Saarland'), 'The picklistvalue "Saarland" should be aggregated to Portfolio__c.');
        System.assert(portfolio.FederalState__c.contains('Bavaria'), 'The picklistvalue "Bavaria" should be aggregated to Portfolio__c.');

        System.assert(portfolio.MacroLocations__c.contains('Top 7'), 'The picklistvalue "Top 7" should be aggregated to Portfolio__c.');

        System.assert(portfolio.MicroLocations__c.contains('Periphery'), 'The picklistvalue "Periphery" should be aggregated to Portfolio__c.');
        System.assert(portfolio.MicroLocations__c.contains('Outermost Region'), 'The picklistvalue "Outermost Region" should be aggregated to Portfolio__c.');
    }

    @IsTest
    static void testAggregationForObjectsToPortfolioDelete(){
        Portfolio__c portfolio = new Portfolio__c();
        portfolio.Name = 'Test-Portfolio';
        insert portfolio;

        List<PropertyObject__c> listOfPropertiesToInsert = new List<PropertyObject__c>();
        PropertyObject__c propertyObject1 = new PropertyObject__c();
        propertyObject1.Portfolio__c = portfolio.Id;
        propertyObject1.Country__c = 'Germany';
        propertyObject1.Market__c = 'Frankfurt';
        propertyObject1.Submarket__c = 'BER Flughafen Schönefeld';
        propertyObject1.FederalState__c = 'Hessen';
        propertyObject1.MacroLocation__c = 'B&C Cities';
        propertyObject1.MicroLocation__c = 'A-Location';
        listOfPropertiesToInsert.add(propertyObject1);

        PropertyObject__c propertyObject2 = new PropertyObject__c();
        propertyObject2.Portfolio__c = portfolio.Id;
        propertyObject2.Country__c = 'France';
        propertyObject2.Market__c = 'Hamburg';
        propertyObject2.Submarket__c = 'BER Mediaspree';
        propertyObject2.FederalState__c = 'Bavaria';
        propertyObject2.MacroLocation__c = 'Top 7';
        propertyObject2.MicroLocation__c = 'Outermost Region';
        listOfPropertiesToInsert.add(propertyObject2);
        insert listOfPropertiesToInsert;

        delete propertyObject2;

        portfolio = [SELECT Id, Countries__c, FederalState__c, MacroLocations__c, Markets__c, MicroLocations__c, Submarkets__c FROM Portfolio__c WHERE Id = :portfolio.Id];
        System.assert(portfolio.Countries__c.contains('Germany'), 'The picklistvalue "Germany" should be aggregated to Portfolio__c.');
        System.assert(portfolio.Markets__c.contains('Frankfurt'), 'The picklistvalue "Frankfurt" should be aggregated to Portfolio__c.');
        System.assert(portfolio.Submarkets__c.contains('BER Flughafen Schönefeld'), 'The picklistvalue "BER Flughafen Schönefeld" should be aggregated to Portfolio__c.');
        System.assert(portfolio.FederalState__c.contains('Hessen'), 'The picklistvalue "Hessen" should be aggregated to Portfolio__c.');
        System.assert(portfolio.MacroLocations__c.contains('B&C Cities'), 'The picklistvalue "B&C Cities" should be aggregated to Portfolio__c.');
        System.assert(portfolio.MicroLocations__c.contains('A-Location'), 'The picklistvalue "A-Location" should be aggregated to Portfolio__c.');
    }

    /**
     * Test the usecase when an PropertyObject__c is not referenced anymore to an Portfolio__c.
     */
    @IsTest
    static void testAggregationForObjectsToPortfolioByDeReferencing(){
        Portfolio__c portfolio = new Portfolio__c();
        portfolio.Name = 'Test-Portfolio';
        insert portfolio;

        List<PropertyObject__c> listOfPropertiesToInsert = new List<PropertyObject__c>();
        PropertyObject__c propertyObject1 = new PropertyObject__c();
        propertyObject1.Portfolio__c = portfolio.Id;
        propertyObject1.Country__c = 'Germany';
        propertyObject1.Market__c = 'Frankfurt';
        propertyObject1.Submarket__c = 'BER CBD City Ost';
        propertyObject1.FederalState__c = 'Hessen';
        propertyObject1.MacroLocation__c = 'B&C Cities';
        propertyObject1.MicroLocation__c = 'A-Location';
        listOfPropertiesToInsert.add(propertyObject1);

        PropertyObject__c propertyObject2 = new PropertyObject__c();
        propertyObject2.Portfolio__c = portfolio.Id;
        propertyObject2.Country__c = 'France';
        propertyObject2.Market__c = 'Hamburg';
        propertyObject2.Submarket__c = 'BER Adlershof';
        propertyObject2.FederalState__c = 'Bavaria';
        propertyObject2.MacroLocation__c = 'Top 7';
        propertyObject2.MicroLocation__c = 'Outermost Region';
        listOfPropertiesToInsert.add(propertyObject2);
        insert listOfPropertiesToInsert;

        propertyObject2.Portfolio__c = null;
        update propertyObject2;

        portfolio = [SELECT Id, Countries__c, FederalState__c, MacroLocations__c, Markets__c, MicroLocations__c, Submarkets__c FROM Portfolio__c WHERE Id = :portfolio.Id];
        System.assert(portfolio.Countries__c.contains('Germany'), 'The picklistvalue "Germany" should be aggregated to Portfolio__c.');
        System.assert(portfolio.Markets__c.contains('Frankfurt'), 'The picklistvalue "Frankfurt" should be aggregated to Portfolio__c.');
        System.assert(portfolio.Submarkets__c.contains('BER Adlershof'), 'The picklistvalue "BER Adlershof" should be aggregated to Portfolio__c.');
        System.assert(portfolio.FederalState__c.contains('Hessen'), 'The picklistvalue "Hessen" should be aggregated to Portfolio__c.');
        System.assert(portfolio.MacroLocations__c.contains('B&C Cities'), 'The picklistvalue "B&C Cities" should be aggregated to Portfolio__c.');
        System.assert(portfolio.MicroLocations__c.contains('A-Location'), 'The picklistvalue "A-Location" should be aggregated to Portfolio__c.');
    }

    @IsTest
    static void updateGeolocationDataTest()
    {
        Test.setMock(HttpCalloutMock.class, new GeolocationCalloutMock());
        PropertyObject__c property = new PropertyObject__c(
                PostalCode__c = '1600',
                Street__c = 'Amphitheatre Parkway, Mountain View',
                City__c = 'CA',
                Country__c = 'United States'
        );

        insert property;

        Test.startTest();
        PropertyObjectService.updateGeolocationData(new List<SObject>{property}, null);
        Test.stopTest();

        property = [
                SELECT Id, Geolocation__Latitude__s, Geolocation__Longitude__s
                FROM PropertyObject__c
        ];

        System.assertEquals(55.1793713, property.Geolocation__Latitude__s, 'Geolocation data should be updated');
        System.assertEquals(30.241673, property.Geolocation__Longitude__s, 'Geolocation data should be updated');
    }
}