/**
* Testclass for SearchController.cls
*
* @author: <Egor Markuzov> (egor.markuzov@parx.com)
*
* @history:
* version		    | author				                                | changes
* ====================================================================================
* 0.1 11.11.2019	| Egor Markuzov (egor.markuzov@parx.com)    | initial version.
*/
@isTest
private class SearchControllerTest {
    /**
     * test search()
     */
    @isTest
    public static void testSearch()
    {
        List<Account> accList = new List<Account>();

        for (Integer i = 0; i < 9; i++)
        {
            accList.add(TestDataFactory.createAccount('Test ' + i));
        } 
        insert accList;

        SearchController.SearchCriteriaWrapper searchCriteria = new SearchController.SearchCriteriaWrapper();
        searchCriteria.objectApiName = 'Account';
        searchCriteria.fieldApiName = 'Name';
        searchCriteria.searchTerm = 'Test 1';
        searchCriteria.isLikeSearch = true;
        searchCriteria.orderBy = 'Name ASC';

        List<SearchController.SearchResult> resultList = SearchController.search(JSON.serialize(searchCriteria));
        System.assertEquals(1, resultList.size(), 'Wrong number of returned records');
    }

}