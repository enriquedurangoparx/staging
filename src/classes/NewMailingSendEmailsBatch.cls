/**
*	US2858
*	@author npo
*	@copyright PARX
*/
public class NewMailingSendEmailsBatch implements Database.Batchable<SObject>
{
    public static final String CAMPAIGN_SENDING_STARTED = 'Sending started';
    public static final String CAMPAIGN_MEMBER_STATUS_SENT = 'Sent';

    public Id campaignId { get; set; }

    public Database.queryLocator start(Database.BatchableContext bc)
    {
        return Database.getQueryLocator(
                'SELECT Id, Investor__c, GermanSalutationSequence__c, EnglishSalutationSequence__c FROM InvestorMailingConfiguration__c WHERE Campaign__c = :campaignId AND SendingStatus__c = \'' + CAMPAIGN_SENDING_STARTED + '\'');
    }

    public void execute(Database.BatchableContext bc, List<SObject> scope)
    {
        List<InvestorMailingConfiguration__c> investorMailingConfigurations = (List<InvestorMailingConfiguration__c>) scope;
        Map<Id, CampaignMember> campaignMemberByInvestorContact = NewMailingService.prepareCampaignMemberMap(campaignId);
        List<Messaging.SingleEmailMessage> emailMessages = NewMailingService.prepareMessages(investorMailingConfigurations, campaignMemberByInvestorContact, campaignId);


        if (emailMessages != null)
        {
            List<Messaging.SendEmailResult> results = Messaging.sendEmail(emailMessages, true);
            String error = '';
            for (Messaging.SendEmailResult result : results)
            {
                if (!result.isSuccess())
                {
                    error += result.getErrors();
                }
            }
            if (!String.isEmpty(error))
            {
                NewMailingService.setError(investorMailingConfigurations, error);
                System.debug('...Batch executed with errors: ' + error);
            }
            else
            {
                NewMailingService.setSuccess(investorMailingConfigurations);
                System.debug('...Batch executed successfully');
                for (CampaignMember member : campaignMemberByInvestorContact.values())
                {
                    member.Status = CAMPAIGN_MEMBER_STATUS_SENT;
                }

                update campaignMemberByInvestorContact.values();
            }

        }
        else
        {
            NewMailingService.setError(investorMailingConfigurations, 'prepareMessages failed');
            // update to save errors
        }
        update investorMailingConfigurations;
    }

    public void finish(Database.BatchableContext bc)
    {
    }

}