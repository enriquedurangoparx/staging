/**
* Account service class.
*
* @author: <Christian Schwabe> (Christian.Schwabe@colliers.com)
*
* @history:
* version		    | author				                                | changes
* ====================================================================================
* 0.1 18.02.2020	| Christian Schwabe (Christian.Schwabe@colliers.com)    | initial version.
* 0.2 19.02.2020	| Christian Schwabe (Christian.Schwabe@colliers.com)    | [SFINV/US 1386] Implement setContactToInactive().
*/
public with sharing class AccountService {

    public static void setAccountHierachy(List<Account> records)
    {
        //catch all deleted to set
        Set<Id> delIds = new Set<Id>();
        for (Account delObj : records){
            delIds.add(delObj.Id);
        }
        
        //query children records
        List<Account> children = [SELECT id FROM Account WHERE ParentId IN : delIds];
        
        //change the updated value
        for(Account child : children){
            child.ParentId = null;
            child.HierarchyIsUpdated__c = false;
        }
        //bulk update children records
        update children;
    }
    
    /**
     * [SFINV/US 1386]
     * As soon as an account has the status = inactive (Account.Status__c = Inactive),
     * the status of all directly connected contacts of the account should also get the status "Inactive" (Contact.Status__c = inactive).
     * 
     * Note: If the account is reactivated at a later time (status = active),
     * then all connected contacts should NOT be activated automatically.
     */
    public static void setContactToInactive(List<Account> newRecords, List<Account> oldRecords)
    {
        Map<Id, Account> mapOfOldAccountById = new Map<Id, Account>(oldRecords);
        Set<Id> setOfAccountId = new Set<Id>();// Contains Ids for inactive accounts.
        for(Account newAccount : newRecords){
            Id accountId = newAccount.Id;
            Account oldAccount = mapOfOldAccountById.get(accountId);

            // Evaluate only accounts which toggle from Status__c = Active to Status__c = Inactive.
            if(newAccount.Status__c == 'Inactive' && (oldAccount.Status__c == 'Active' || String.isBlank(oldAccount.Status__c))){
                setOfAccountId.add(accountId);
            }
        }
        
        List<Contact> listOfContact = new List<Contact>();
        for(Contact contact : [SELECT Id, Name, Status__c FROM Contact WHERE AccountId IN :setOfAccountId]){
            contact.Status__c = 'Inactive';

            listOfContact.add(contact);
        }
        
        update listOfContact;
    }
}