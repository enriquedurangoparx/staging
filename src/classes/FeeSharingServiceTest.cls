/**
*	
*	@author npo
*	@copyright PARX
*/
@IsTest
private class FeeSharingServiceTest
{

    @testSetup
    static void setup()
    {
        User currentUser = [SELECT Id, Office__c FROM User WHERE Id = :UserInfo.getUserId()];
        User u1 = TestUtil.createUser('FeeSharingServiceTest.testHandleOpportunitySharing1@test.com', 'Colliers Finance', 'TopManagement', false);
        User u2 = TestUtil.createUser('FeeSharingServiceTest.testHandleOpportunitySharing2@test.com', 'Colliers Finance', null, false);
        User u3 = TestUtil.createUser('FeeSharingServiceTest.testHandleOpportunitySharing3@test.com', 'Colliers Finance', null, false);

        u1.Office__c = 'Cologne'.equals(currentUser.Office__c) ? 'Berlin' :'Cologne';
        u2.Office__c = 'Cologne'.equals(currentUser.Office__c) ? 'Berlin' : 'Cologne';
        u3.Office__c = currentUser.Office__c;
        insert new List<User> {u1, u2, u3};
    }

    @IsTest
    static void testHandleOpportunitySharingDifferentOffices()
    {
        User currentUser = [SELECT Id, Office__c FROM User WHERE Id = :UserInfo.getUserId()];
        Embargo__c embargo = new Embargo__c();
        embargo.Name = 'Embargo';
        embargo.Countries__c = 'Egypt;Afghanistan;Belarus;Burundi;Iraq;Yemen';
        insert embargo;

        Account newAcc = TestDataFactory.createAccount('Test 1');
        insert newAcc;

        PropertyObject__c propertyObject = TestDataFactory.buildPropertyObject('property', true);
        Opportunity newOpportunity = TestDataFactory.createOpportunity(newAcc.Id, propertyObject);
        newOpportunity.Office__c = currentUser.Office__c;
        insert newOpportunity;

        List<User> users = [SELECT Id FROM User WHERE Username IN ('FeeSharingServiceTest.testHandleOpportunitySharing1@test.com', 'FeeSharingServiceTest.testHandleOpportunitySharing2@test.com') LIMIT 2];
        User u1 = users.get(0);
        User u2 = users.get(1);

        FeeSharing__c feeSharing = new FeeSharing__c(Opportunity__c = newOpportunity.Id, User__c = u1.Id);
        insert feeSharing;

        List<OpportunityShare> shares = [SELECT Id, UserOrGroupId, OpportunityId FROM OpportunityShare WHERE OpportunityId = :newOpportunity.Id AND UserOrGroupId = :u1.Id AND RowCause = 'Manual'];

        System.assertEquals(1, shares.size(), 'We expect OpportunityShare record to be created');

        try
        {
            insert new FeeSharing__c(Opportunity__c = newOpportunity.Id, User__c = u1.Id);
            System.assert(false, 'We expect an Exception to be thrown, as no duplicates are allowed.');
        }
        catch (Exception e)
        {
            System.assert(true, 'We expect an Exception to be thrown, as no duplicates are allowed.');
        }

        feeSharing.User__c = u2.Id;
        update feeSharing;

        shares = [SELECT Id, UserOrGroupId, OpportunityId FROM OpportunityShare WHERE OpportunityId = :newOpportunity.Id AND UserOrGroupId IN (:u1.Id, :u2.Id)  AND RowCause = 'Manual'];

        System.assertEquals(1, shares.size(), 'We expect only one OpportunityShare record');
        System.assertEquals(u2.Id, shares.get(0).UserOrGroupId, 'We expect OpportunityShare record to be created for User 2');

        delete feeSharing;

        shares = [SELECT Id, UserOrGroupId, OpportunityId FROM OpportunityShare WHERE OpportunityId = :newOpportunity.Id AND UserOrGroupId IN (:u1.Id, :u2.Id)  AND RowCause = 'Manual'];

        System.assertEquals(0, shares.size(), 'We expect OpportunityShare record to be deleted');
    }

    @IsTest
    static void testHandleOpportunitySharing()
    {
        Embargo__c embargo = new Embargo__c();
        embargo.Name = 'Embargo';
        embargo.Countries__c = 'Egypt;Afghanistan;Belarus;Burundi;Iraq;Yemen';
        insert embargo;

        Account newAcc = TestDataFactory.createAccount('Test 1');
        insert newAcc;

        PropertyObject__c propertyObject = TestDataFactory.buildPropertyObject('property', true);
        Opportunity newOpportunity = TestDataFactory.createOpportunity(newAcc.Id, propertyObject);
        newOpportunity.Office__c = 'Berlin';
        insert newOpportunity;

        List<User> users = [SELECT Id FROM User WHERE Username IN ('FeeSharingServiceTest.testHandleOpportunitySharing3@test.com') LIMIT 1];
        User u1 = users.get(0);

        FeeSharing__c feeSharing = new FeeSharing__c(Opportunity__c = newOpportunity.Id, User__c = u1.Id);
        insert feeSharing;

        List<OpportunityShare> shares = [SELECT Id, UserOrGroupId, OpportunityId FROM OpportunityShare WHERE OpportunityId = :newOpportunity.Id AND UserOrGroupId = :u1.Id AND RowCause = 'Manual'];


        System.assertEquals(0, shares.size(), 'We expect OpportunityShare record not to be created (the same Office)');
    }
}