/**
 * Created by ille on 2019-04-02.
 */

@istest
private class ExceptionsTest
{

    @istest
    private static void shouldAssert()
    {
        Exceptions.assert(true, new Exceptions.GenericException('Positive assertion!'));
        try
        {
            Exceptions.assert(false, new Exceptions.GenericException('Negative assertion!'));
            System.assert(false, 'Should not reach here!');
        }
        catch (Exceptions.GenericException e)
        {
            System.debug('Expected exception caught: ' + e.getMessage());
            System.assert(true);
        }
    }
}