/**
*	
*	@author npo
*	@copyright PARX
*/
public with sharing class MailPermissionService
{
	public static void handleUniquenessByUser(List<SObject> newValues, List<SObject> oldRecords)
	{
		Map<Id, SObject> oldMap = oldRecords != null ? new Map<Id, SObject>(oldRecords) : null;

		List<MailPermission__c> modifiedPermissions = (List<MailPermission__c>) TriggerUtil.getModifiedObjectsList(new List<String>
		{
				'DelegatedUser__c', 'User__c'
		},
				newValues, oldMap);
		Set<Id> usersToCheck = new Set<Id>();
		for (MailPermission__c permission : modifiedPermissions)
		{
			usersToCheck.add(permission.User__c);
			if (oldMap != null)
			{
				usersToCheck.add(((MailPermission__c)oldMap.get(permission.Id)).User__c);
			}
		}
		List<MailPermission__c> existingPermissions =
		[
				SELECT Id, User__c, DelegatedUser__c
				FROM MailPermission__c
				WHERE User__c IN :usersToCheck
				AND Id NOT IN :modifiedPermissions
		];
		System.debug('...existingPermissions:' + existingPermissions);
		Map<Id, List<MailPermission__c>> permissionsByUser = new Map<Id, List<MailPermission__c>>();

		List<MailPermission__c> all = new List<MailPermission__c>(existingPermissions);
		all.addAll(modifiedPermissions);
		System.debug('...all:' + all);
		for (MailPermission__c permission : all)
		{
			List<MailPermission__c> permissionsForCurrentUser = permissionsByUser.containsKey(permission.User__c) ?
					permissionsByUser.get(permission.User__c) : new List<MailPermission__c>();
			permissionsForCurrentUser.add(permission);
			permissionsByUser.put(permission.User__c, permissionsForCurrentUser);
		}
		System.debug('...permissionsByUser:' + permissionsByUser);

		for (Id userId : permissionsByUser.keySet())
		{
			List<MailPermission__c> permissionsForCurrentUser = permissionsByUser.get(userId);
			Set<Id> uniqueDelegatedUsers = new Set<Id>();

			for (MailPermission__c permission : permissionsForCurrentUser)
			{
				if (permission.DelegatedUser__c != null)
				{
					if (uniqueDelegatedUsers.contains(permission.DelegatedUser__c))
					{
						MailPermission__c forError = findPermissionForError(modifiedPermissions, permission);
						if (forError != null)
						{
							forError.addError(Label.MailingOnMyBehalfUnique);
						}
					} else
					{
						uniqueDelegatedUsers.add(permission.DelegatedUser__c);
					}
				}
			}
		}
	}

	private static MailPermission__c findPermissionForError(List<MailPermission__c> permissions, MailPermission__c notUniquePermission)
	{
		if (notUniquePermission.User__c != null && notUniquePermission.DelegatedUser__c != null)
		{
			for (MailPermission__c p : permissions)
			{
				if (p.User__c == notUniquePermission.User__c && p.DelegatedUser__c == p.DelegatedUser__c)
				{
					return p;
				}
			}
		}
		return null;
	}

	@AuraEnabled(cacheable=false)
	public static List<MailPermission__c> findPermissionsForCurrentUser()
	{
		System.debug('...findPermissionsForCurrentUser' + UserInfo.getUserId());
		return [SELECT Id, User__c, DelegatedUser__c FROM MailPermission__c WHERE DelegatedUser__c = :UserInfo.getUserId()];
	}
}