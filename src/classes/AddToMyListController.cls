/**
* Is used to
* -save accounts to "Investor List Item" (apiname: InvestorsListItem__c)
* -save contacts to "Investor List Item Contact" (apiname: InvestorsListItemContact__c)
*
* @author: <Egor Markuzov> (egor.markuzov@parx.com)
*
* @history:
* version		    | author				                                | changes
* ====================================================================================
* 0.1 14.11.2019	| Egor Markuzov (egor.markuzov@parx.com)                | initial version.
*/
public with sharing class AddToMyListController 
{
    private static final String ACCOUNT_API_NAME = 'Account';
    private static final String CONTACT_API_NAME = 'Contact';
    /**
    * Method for creating InvestorsListItemContact__c record from Contact layout
    *
    * @param  Id       recordId              id of Contact / Account record.
    * @param  Id       investorListId        id of Investor List record.
    * @param  String   objApiName            Api name of object.
    * @return Map<String, String> where key is error message param and value is a param value
    */
    @AuraEnabled
    public static String createInvestorListItemRecord(Id recordId, Id investorListId, String objApiName)
    {
        if (objApiName == CONTACT_API_NAME)
        {
            List<InvestorsListItemContact__c> investorsListItemContactList = [SELECT Id, InvestorsListItem__c, Contact__c 
                                                                       FROM InvestorsListItemContact__c 
                                                                       WHERE Contact__c = :recordId 
                                                                       AND InvestorsListItem__r.InvestorsList__c = :investorListId
                                                                       LIMIT 1];
            // Check if we already have InvestorsListItemContact__c junction
            if (!investorsListItemContactList.isEmpty())
            {
                return JSON.serialize(new ResponseMessage(false, Label.GeneralLabelWarning, Label.AddToMyListContactIncludedNotification, 'info', null));
            }
            
            Contact selectedCont = [SELECT Id, Name, AccountId, Account.Name 
                                    FROM Contact 
                                    WHERE Id = :recordId
                                    LIMIT 1];
            
            List<InvestorsListItem__c> investorListItemList = [SELECT Id, Account__c, InvestorsList__c 
                                                            FROM InvestorsListItem__c 
                                                            WHERE Account__c = :selectedCont.AccountId
                                                            AND InvestorsList__c = :investorListId
                                                            LIMIT 1];
            // Check if we already have InvestorsListItem__c junction
            if (!investorListItemList.isEmpty())
            {
                // if we have, we just need to create InvestorsListItemContact__c
                InvestorsListItemContact__c newInvestorsListItemContact = new InvestorsListItemContact__c(
                    Contact__c = recordId, 
                    InvestorsListItem__c = investorListItemList[0].Id
                );

                try 
                {
                    insert newInvestorsListItemContact;
                    return JSON.serialize(new ResponseMessage(true, Label.GeneralLabelSuccess, Label.AddToMyListRecordCreated, 'success', newInvestorsListItemContact.Id));
                } 
                catch (Exception e)
                {
                    return JSON.serialize(new ResponseMessage(false, Label.GeneralLabelError, e.getMessage(), 'error', null));
                }
            }
            else
            {
                // if we have not, we need to create InvestorsListItem__c first, and InvestorsListItemContact__c then
                InvestorsListItem__c newInvestorsListItem = new InvestorsListItem__c(
                    Account__c = selectedCont.AccountId,
                    InvestorsList__c = investorListId
                );
                
                try
                {
                    System.savePoint sp = Database.setSavePoint();

                    insert newInvestorsListItem;

                    InvestorsListItemContact__c newInvestorsListItemContact = new InvestorsListItemContact__c(
                        Contact__c = recordId, 
                        InvestorsListItem__c = newInvestorsListItem.Id
                    );

                    try
                    {
                        insert newInvestorsListItemContact;
                        return JSON.serialize(new ResponseMessage(true, Label.GeneralLabelSuccess, Label.AddToMyListRecordCreated, 'success', newInvestorsListItemContact.Id));
                    }
                    catch (Exception e)
                    {
                        database.rollback(sp);
                        return JSON.serialize(new ResponseMessage(false, Label.GeneralLabelError, e.getMessage(), 'error', null));
                    }
                }
                catch (Exception e)
                {
                    return JSON.serialize(new ResponseMessage(false, Label.GeneralLabelError, e.getMessage(), 'error', null));
                }
            }
        }
        else if (objApiName == ACCOUNT_API_NAME)
        {
            List<InvestorsListItem__c> investorListItemList = [SELECT Id, Account__c, InvestorsList__c 
                                                                FROM InvestorsListItem__c 
                                                                WHERE Account__c = :recordId
                                                                AND InvestorsList__c = :investorListId
                                                                LIMIT 1];
            
            // Check if we already have InvestorsListItem__c junction
            if (!investorListItemList.isEmpty())
            {
                return JSON.serialize(new ResponseMessage(false, Label.GeneralLabelWarning, Label.AddToMyListAccountIncludedNotification, 'info', null));
            }

            // if we have not, we need to create InvestorsListItem__c
            InvestorsListItem__c newInvestorsListItem = new InvestorsListItem__c(
                Account__c = recordId,
                InvestorsList__c = investorListId
            );
            
            try
            {
                insert newInvestorsListItem;
                return JSON.serialize(new ResponseMessage(true, Label.GeneralLabelSuccess, Label.AddToMyListRecordCreated, 'success', newInvestorsListItem.Id));
            }
            catch (Exception e)
            {
                return JSON.serialize(new ResponseMessage(false, Label.GeneralLabelError, e.getMessage(), 'error', null));
            }
        }
        else
        {
            return JSON.serialize(new ResponseMessage(false, Label.GeneralLabelError, 'Unsupported object. This component is just for Account / Contact objects', 'error', null));
        }
    }

    public class ResponseMessage
    {
        public Boolean isSuccess;
        public String title;
        public String message;
        /**
         *      possible values for variant: info (default), success, warning, and error.
         */
        public String variant;
        public Id recordId; 

        public ResponseMessage(Boolean isSuccess, String title, String message, String variant, Id recordId)
        {
            this.isSuccess = isSuccess;
            this.title = title;
            this.message = message;
            this.recordId = recordId;
            this.variant = variant;
        }
    }
}