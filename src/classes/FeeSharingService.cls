/**
*	
*	@author npo
*	@copyright PARX
*/
public without sharing class FeeSharingService
{
    public static final String FIELD_INTEGRITY_EXCEPTION = 'FIELD_INTEGRITY_EXCEPTION';
    public static final String USER_ID_START = '005';
    public static final String CHATTER_PROFILE = 'Chatter Free User';
    public static final String ROW_CAUSE_MANUAL = 'Manual';

    public static final Map<String, String> groupNameByOffice = new Map<String, String>
    {
            'Berlin' => 'RegionalManagerBerlin',
            'Cologne' => 'RegionalManagerKoeln',
            'Düsseldorf' => 'RegionalManagerDuesseldorf',
            'Frankfurt' => 'RegionalManagerFrankfurt',
            'Leipzig' => 'RegionalManagerLeipzig',
            'Munich' => 'RegionalManagerMuenchen',
            'Nuremberg' => 'RegionalManagerNuernberg',
            'Stuttgart' => 'RegionalManagerStuttgart',
            'Hamburg' => 'RegionalManagerHamburg'

    };

    public static void handleOpportunitySharing(List<SObject> records, List<SObject> oldRecords)
    {
        System.debug('...handleOpportunitySharing start');
        Map<Id, SObject> oldRecordsMap = oldRecords != null ? new Map<Id, SObject> (oldRecords) : null;
        List<FeeSharing__c> modifiedRecords = (List<FeeSharing__c>)TriggerUtil.getModifiedObjectsList(new List<String> {'Opportunity__c', 'User__c'}, records, oldRecordsMap);

        List<OpportunityShare> sharingRecordsToInsert = new List<OpportunityShare>();

        List<OpportunityShare> existingRecords;
        Set<String> uniquenessKeys;
        Map<String, OpportunityShare> sharingsByKey = new Map<String, OpportunityShare>();
        if (!modifiedRecords.isEmpty())
        {
            Map<String, Id> groupsByName = initGroupMap();
            Set<Id> groupsIds = new Set<Id>(groupsByName.values());
            System.debug('...groupsByName: ' + groupsByName);
            Set<Id> opportunityIds = new Set<Id>();
            Set<Id> usersIds = new Set<Id>();

            // deleting OpportunityShare for modified records
            if (oldRecords != null && !oldRecords.isEmpty())
            {
                System.debug('...if (!oldRecords.isEmpty())');
                List<FeeSharing__c> toProcess = new List<FeeSharing__c>();
                for (FeeSharing__c record : modifiedRecords)
                {
                    toProcess.add((FeeSharing__c)oldRecordsMap.get(record.Id));
                }
                System.debug('...before deleteOpportunitySharing(toProcess) ' + toProcess);
                deleteOpportunitySharing(toProcess);
            }

            for(FeeSharing__c feeSharing : modifiedRecords)
            {
                opportunityIds.add(feeSharing.Opportunity__c);
                usersIds.add(feeSharing.User__c);
            }

            Map<Id, User> users = new Map<Id, User>([SELECT Id, Office__c, Profile.Name FROM User WHERE Id IN :usersIds]);
            Map<Id, Opportunity> opportunities = new Map<Id, Opportunity>([SELECT Id, Office__c FROM Opportunity WHERE Id IN :opportunityIds]);

            List<Id> usersAndGroupsIds = new List<Id>(usersIds);
            usersAndGroupsIds.addAll(groupsIds);
            existingRecords = [
                    SELECT Id, UserOrGroupId, OpportunityId, RowCause
                    FROM OpportunityShare
                    WHERE OpportunityId IN :opportunityIds
                    AND UserOrGroupId IN :usersAndGroupsIds
                    AND OpportunityAccessLevel != 'read'
            ];

            // OpportunityId + '#' + UserId/GroupId
            uniquenessKeys = new Set<String>();
            for (OpportunityShare opportunityShare : existingRecords)
            {
                String key = opportunityShare.OpportunityId + '#' + opportunityShare.UserOrGroupId;
                uniquenessKeys.add(key);
                sharingsByKey.put(key, opportunityShare);
            }

            for(FeeSharing__c feeSharing : modifiedRecords)
            {
                System.debug('...handleOpportunitySharing for(FeeSharing__c feeSharing : modifiedRecords)');
                // we want to avoid duplicates
                if (!uniquenessKeys.contains(feeSharing.Opportunity__c + '#' + feeSharing.User__c))
                {
                    if (users.get(feeSharing.User__c).Profile.Name != CHATTER_PROFILE && opportunities.get(feeSharing.Opportunity__c).Office__c != users.get(feeSharing.User__c).Office__c)
                    {
                        OpportunityShare record = new OpportunityShare
                                (
                                        OpportunityId = feeSharing.Opportunity__c,
                                        RowCause = Schema.OpportunityShare.RowCause.Manual,
                                        UserOrGroupId = feeSharing.User__c,
                                        OpportunityAccessLevel = 'Edit'
                                );
                        sharingRecordsToInsert.add(record);
                        uniquenessKeys.add(feeSharing.Opportunity__c + '#' + feeSharing.User__c);
                        String groupName = groupNameByOffice.get(users.get(feeSharing.User__c).Office__c);
                        Id groupId = groupsByName.get(groupName);
                        System.debug('...check if group not null: ' + groupId + ' office is: ' + users.get(feeSharing.User__c).Office__c);
                        if (groupId != null)
                        {
                            // check if such sharing already exists
                            System.debug('...check if such group sharing already exists');
                            if (!uniquenessKeys.contains(feeSharing.Opportunity__c + '#' + groupId))
                            {
                                System.debug('...creating group sharing');
                                // add sharing to group
                                OpportunityShare recordForGroup = new OpportunityShare
                                        (
                                                OpportunityId = feeSharing.Opportunity__c,
                                                RowCause = Schema.OpportunityShare.RowCause.Manual,
                                                UserOrGroupId = groupId,
                                                OpportunityAccessLevel = 'Edit'
                                        );
                                sharingRecordsToInsert.add(recordForGroup);
                                uniquenessKeys.add(feeSharing.Opportunity__c + '#' + groupId);
                            }
                        }
                    }
                }
                else
                {
                    for (OpportunityShare shareRecord : existingRecords)
                    {
                        if (shareRecord.UserOrGroupId == feeSharing.User__c
                                && shareRecord.OpportunityId == feeSharing.Opportunity__c
                                && ROW_CAUSE_MANUAL.equals(shareRecord.RowCause))
                        {
                            // a duplicate value
                            feeSharing.addError(Label.FeeSahringInvalidUser);
                        }
                    }

                }
            }
        }

        if (!sharingRecordsToInsert.isEmpty())
        {
            try
            {
                System.debug('...inserting: ' + sharingRecordsToInsert);
                insert sharingRecordsToInsert;
            }
            catch (System.DmlException e)
            {
                if (e.getMessage().contains(FIELD_INTEGRITY_EXCEPTION))
                {
                    for (SObject record : records)
                    {
                        record.addError(Label.FeeSahringIsNotCreated);
                    }
                }
            }
        }
    }

    public static void deleteOpportunitySharing(List<SObject> records)
    {
        List<FeeSharing__c> modifiedRecords = (List<FeeSharing__c>) records;
        deleteOpportunitySharingsRecords(modifiedRecords);
    }

    private static void deleteOpportunitySharingsRecords(List<FeeSharing__c>feeSharings)
    {
        System.debug('...deleteOpportunitySharings(List<FeeSharing__c>feeSharings) start');
        Set<OpportunityShare> sharingRecordsToDelete = new Set<OpportunityShare>();
        Map<String, Id> groupsByName = initGroupMap();

        List<OpportunityShare> existingRecords;
        Set<String> uniquenessKeys;
        Map<String, OpportunityShare> sharingsByKey = new Map<String, OpportunityShare>();
        if (!feeSharings.isEmpty())
        {
            Set<Id> opportunityIds = new Set<Id>();
            Set<Id> usersIds = new Set<Id>();

            for(FeeSharing__c feeSharing : feeSharings)
            {
                opportunityIds.add(feeSharing.Opportunity__c);
                usersIds.add(feeSharing.User__c);
            }


            Map<Id, Opportunity> opportunities = new Map<Id, Opportunity>([SELECT Id, Office__c FROM Opportunity WHERE Id IN :opportunityIds]);

            existingRecords = [
                    SELECT Id, UserOrGroupId, OpportunityId, RowCause
                    FROM OpportunityShare
                    WHERE OpportunityId IN :opportunityIds
//                    AND UserOrGroupId IN :usersIds
                    AND RowCause = :Schema.OpportunityShare.RowCause.Manual
            ];
            System.debug('...on delete existingRecords:' + existingRecords);

            Set<Id> existingRecordsUsers = new Set<Id>();
            for (OpportunityShare share : existingRecords)
            {
                existingRecordsUsers.add(share.UserOrGroupId);
            }

            Set<Id> allIds = new Set<Id>(usersIds);
            allIds.addAll(existingRecordsUsers);
            Map<Id, User> users = new Map<Id, User>([SELECT Id, Office__c, Profile.Name FROM User WHERE Id IN :allIds]);
            Map<Id, OpportunityShare> existingRecordsByUserOrGroupId = new Map<Id, OpportunityShare>();
            for(OpportunityShare share : existingRecords)
            {
                existingRecordsByUserOrGroupId.put(share.UserOrGroupId, share);
            }
            Map<Id, List<OpportunityShare>> existingRecordsByOpportunities = new Map<Id,  List<OpportunityShare>>();
            for(OpportunityShare share : existingRecords)
            {
                if (!existingRecordsByOpportunities.containsKey(share.OpportunityId))
                {
                    existingRecordsByOpportunities.put(share.OpportunityId, new List<OpportunityShare>());
                }
                existingRecordsByOpportunities.get(share.OpportunityId).add(share);
            }

            System.debug('...existingRecords: ' + existingRecords);

            // OpportunityId + '#' + UserId
            uniquenessKeys = new Set<String>();
            for (OpportunityShare opportunityShare : existingRecords)
            {
                String key = opportunityShare.OpportunityId + '#' + opportunityShare.UserOrGroupId;
                uniquenessKeys.add(key);
                sharingsByKey.put(key, opportunityShare);
            }

            for (FeeSharing__c oldFeeSharing : feeSharings)
            {
                String key = oldFeeSharing.Opportunity__c + '#' + oldFeeSharing.User__c;
                if (sharingsByKey.containsKey(key))
                {
                    String currentUserOffice = users.get(oldFeeSharing.User__c).Office__c;
                    String groupName = groupNameByOffice.get(currentUserOffice);
                    Id groupId = groupsByName.get(groupName);
                    System.debug('...groupId:' + groupId);

                    if (opportunities.get(oldFeeSharing.Opportunity__c).Office__c != currentUserOffice)
                    {
                        sharingRecordsToDelete.add(sharingsByKey.get(key));

                        // check if group record should be deleted
                        List<OpportunityShare> sharingsForCurrentOpportunity = existingRecordsByOpportunities.get(oldFeeSharing.Opportunity__c);
                        Boolean groupSharingWillBeDeleted = true;

                        OpportunityShare groupShare;
                        // finding another match by user
                        System.debug('...sharingsForCurrentOpportunity:' + sharingsForCurrentOpportunity);
                        for (OpportunityShare opportunityShare : sharingsForCurrentOpportunity)
                        {
                            User u = users.get(opportunityShare.UserOrGroupId);
                             if (u != null && u.Id != oldFeeSharing.User__c && u.Office__c == currentUserOffice)
                             {
                                 groupSharingWillBeDeleted = false;
                                 System.debug('...groupSharingWillBeDeleted:' + groupSharingWillBeDeleted);
                                 System.debug('...because of user:' + u);
//                                 break;
                             }
                            if (opportunityShare.UserOrGroupId == groupId)
                            {
                                groupShare = opportunityShare;
                                System.debug('...groupShare is:' + groupShare);
                            }
                        }

                        if (groupSharingWillBeDeleted && groupShare != null)
                        {
                            sharingRecordsToDelete.add(groupShare);
                        }
                    }
                }
            }
        }

        if (!sharingRecordsToDelete.isEmpty())
        {
            System.debug('...deleting: ' + sharingRecordsToDelete);
            delete new List<OpportunityShare>(sharingRecordsToDelete);
        }

        System.debug('...deleteOpportunitySharings(List<FeeSharing__c>feeSharings) end');
    }

    private static Map<String, Id> initGroupMap()
    {
        List<Group> groups = [SELECT Id, Name, DeveloperName FROM Group WHERE DeveloperName IN :groupNameByOffice.values()];
        Map<String, Id> groupsByName = new Map<String, Id>();

        for (Group g : groups)
        {
            groupsByName.put(g.DeveloperName, g.Id);
        }

        return groupsByName;
    }
}