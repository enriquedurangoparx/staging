/*
*   UnitTestData functionality.
*
*   @author Tejashree
*   @copyright PARX
*/

@isTest
public class UnitTestDataTest{
    public static final String PHONE_DEFAULT = '+375 29 000 00 00';
    public static final String POSTAL_CODE_DEFAULT = '210033';
    public static final String COUNTRY_DEFAULT = 'Germany';
    public static final String CITY_DEFAULT = 'City';
    public static final String STREET_DEFAULT = 'Street';

    public static final String ACCOUNT_NAME_DEFAULT = 'Default Account';
   
   public static void createTestRecords(){
     //BillOn Settings
        ONB2__GlobalSettings__c billOnSetting = new ONB2__GlobalSettings__c();
        billOnSetting.Name = 'Default';
        billOnSetting.ONB2__BaseURL__c = 'https://INSTANCE.force.com/';
        billOnSetting.ONB2__DecimalPlacesForQuantity__c = 0;
        billOnSetting.ONB2__DecimalPlacesForUnitPrice__c = 2;
        billOnSetting.ONB2__TrackEMail__c = true;
        billOnSetting.ONB2__AllowInvoiceChanges__c = true;
        insert billOnSetting;
        
        ONB2__InvoiceCounter__c counters = new ONB2__InvoiceCounter__c();
        counters.Name = 'Default';
       // counters.ONB2__Counter__c = 0;
        counters.ONB2__Reset__c = 'YEARLY';
        counters.ONB2__Template__c = '[Year]{00000}';
        counters.ONB2__UseAccountCounters__c = false;
        insert counters;
        
        ONB2__InvoiceCounter__c countersSub = new ONB2__InvoiceCounter__c();
        countersSub.Name = 'Subscription';
        //countersSub.ONB2__Counter__c = 0;
        countersSub.ONB2__Reset__c = '';
        countersSub.ONB2__Template__c = '[AccountAccountName]';
        countersSub.ONB2__UseAccountCounters__c = false;
        insert countersSub;
        
        insert new List<ONB2__TriggerSettings__c>{
           new ONB2__TriggerSettings__c(
               Name = 'InvoiceBeforeInsert'
           ),
           new ONB2__TriggerSettings__c(
               Name = 'InvoiceBeforeUpdate'
           ),
           new ONB2__TriggerSettings__c(
               Name = 'InvoiceLineItemBeforeInsert'
           ),
           new ONB2__TriggerSettings__c(
               Name = 'InvoiceLineItemBeforeUpdate'
           ),
           new ONB2__TriggerSettings__c(
               Name = 'InvoiceLineItemAfterInsert'
           ),
           new ONB2__TriggerSettings__c(
               Name = 'InvoiceAfterInsert'
           )
           
        };
        
        
       
   }
   public static MAP<String,Account> createTestAccountCustomer(){ 
        ONB2__Template__c template = new ONB2__Template__c(Name='default');
        insert template;
        
        //create the necessary information Product/translations
        Product2 stdProd = new Product2 (Name='StdProd');
        insert stdProd;
        
        stdProd = [Select Id, Name FROM Product2 WHERE Id=:stdProd.Id];
        //create account of different applciant types
        List<Account> accountList = new List<Account>();
        Account custAccount = new Account(Name='CustomerTest',AccountType__c='Customer');
        accountList.add(custAccount);
        Account collierAccount = new Account(Name='ColleirTest',AccountType__c='Intercompany Colliers');
        accountList.add(collierAccount);
        Account shareHolderAccount = new Account(Name='ShareTest',AccountType__c='Intercompany Shareholder');
        accountList.add(shareHolderAccount);
        insert accountList;
        MAP<String,Account> accMap = new MAP<String,Account>();
        for(Account acc:accountList){
            if(acc.AccountType__c=='Customer')
            {
                accMap.put('Customer',acc);
            }else if(acc.AccountType__c=='Intercompany Colliers')
            {
                accMap.put('Intercompany Colliers',acc);
            }else if(acc.AccountType__c=='Intercompany Shareholder')
            {
                accMap.put('Intercompany Shareholder',acc);
            }
        }
        return accMap;
   }
   public static void createTestCounter(){ 
        List<ColliersCounter__c> counterList = new List<ColliersCounter__c>();
       // Counters__c counterCust = new Counters__c(Name=Utils.ACCOUNT_TYPE_CUSTOMER, Counter__c=60000);
        //Counters__c counterCollier = new Counters__c(Name=Utils.ACCOUNT_TYPE_COLLIERS, Counter__c=60000);
        //Counters__c counterShare = new Counters__c(Name=Utils.ACCOUNT_TYPE_SHAREHOLDER, Counter__c=60000);
        counterList.add(new ColliersCounter__c(Name=Utils.ACCOUNT_TYPE_CUSTOMER, Counter__c=1000000));
        counterList.add(new ColliersCounter__c(Name=Utils.ACCOUNT_TYPE_COLLIERS, Counter__c=6000000));
        counterList.add(new ColliersCounter__c(Name=Utils.ACCOUNT_TYPE_SHAREHOLDER, Counter__c=2000000));
        insert counterList;
   }

    /**
     * Builds a stub Opportunity record
     */
    public static Opportunity buildOpportunity(String name)
    {
        return new Opportunity(
            Name = name,
            FrameworkContractType__c = 'Preferred in Panel',
            Region__c = 'Global',
            StageName = SObjectType.Opportunity.fields.StageName.getPicklistValues().get(0).getValue(),
            CloseDate = System.today().addDays(5)
        );
    }

    /**
     * Builds a stub Opportunity record
     */
    public static Opportunity buildOpportunity(String name, PropertyObject__c propertyObject)
    {
        Opportunity opportunity = buildOpportunity(name);
        if (propertyObject != null)
        {
            opportunity.PrimaryProperty__c = propertyObject.Id;
        }
        return opportunity;
    }

    /**
     * Builds a stub Contact record
     */
    public static Contact buildContact(String lastName)
    {
        return new Contact(
            LastName = lastName
        );
    }

    /**
     * Builds a User for System.runAs based on a current user
     *
     * @return
     */
    public static User buildUser()
    {
        User testUser = new User();
        for (User each : [select Username, Email, ProfileId, LastName, Alias, TimeZoneSidKey, LocaleSidKey, EmailEncodingKey, LanguageLocaleKey from User where Id = :UserInfo.getUserId()])
        {
            testUser = each.clone(false, true);

            testUser.Alias += 'ut';
            testUser.Email += '.ut';
            testUser.Username += '.ut';
        }

        return testUser;
    }

    /**
     * Builds a stub ExpenseAccount__c record
     */
    public static ExpenseAccount__c buildExpenseAccount()
    {
        Datetime dateRefund = System.now().addDays(10);
        return new ExpenseAccount__c(
            DateRefund__c = dateRefund.date(),
            ExpenseKeeper__c = UserInfo.getUserId(),
            Month__c = dateRefund.format('MMMM'),
            Year__c = dateRefund.year(),
            Status__c = 'Open'
        );
    }

    /**
     * Builds a stub Account record
     *
     * @param name  Account record Name
     *
     * @return
     */
    public static Account buildAccount(String name)
    {
        return new Account(Name = name);
    }

    /**
     * @description Inserts a Default Account record for unit testing
     * @return Account record
     */
    public static Account buildDefaultAccount()
    {
        Account account = new Account(
                Name = ACCOUNT_NAME_DEFAULT,
                Phone = PHONE_DEFAULT,
                BillingCountry = COUNTRY_DEFAULT,
                BillingCity = CITY_DEFAULT,
                BillingPostalCode = POSTAL_CODE_DEFAULT,
                BillingStreet = STREET_DEFAULT
        );

        insert account;
        return account;
    }

    /**
     * @description Allows to create a PropertyObject__c record for unit testing
     * @return PropertyObject__c record
     */
    public static PropertyObject__c buildPropertyObject(String name, Boolean withInsertion)
    {
        PropertyObject__c propertyObject = new PropertyObject__c(
                Name = name
        );

        if (withInsertion)
        {
            insert propertyObject;
        }

        return propertyObject;
    }
}