/*
* @author andreas.reichert@youperience.com
* @date 2019-09-09
* @version 1.0.0
*
* @description Controller class for investors list components
*/
public with sharing class InvestorsListController
{
    /**
     * get Investors list for a given Opportunity-Id to display the investors list
     * @param Opportunity Id
     * @param searchKey String
     *
     * @return List of Investors
     */
    @AuraEnabled(Cacheable = true)
    public static List<AccountToOpportunity__c> findInvestors(Id recordId, String searchKey, Integer limitSize)
    {
        return
        [
                SELECT Id, Account__c, Account__r.Name, LastStatus__c, LastStatusDate__c, LastActivityDate__c, Approved__c,LastVisitDate__c, ListOfContacts__c,Advisor__c,Advisor__r.Name,
                        LastBidDate__c, LastBidAmount__c, toLabel(ReasonForRejection__c), Opportunity__r.Pinned_Investors__c, Status__c,Priority__c,
                        LastActivitiesHistory__c, toLabel(Underbidder__c), (SELECT Id, Contact__r.FirstName, Contact__r.LastName, Contact__r.Account.Name FROM InvestorContacts__r WHERE Contact__r.AccountId != NULL)
                FROM AccountToOpportunity__c
                WHERE Opportunity__c = :recordId
                ORDER BY Account__r.Name ASC
                LIMIT :limitSize
        ];
    }

    /**
     * get Investors list for a given Opportunity-Id to display the investors list
     * @param Opportunity Id
     * @param searchKey String
     * @param keyToDisableCaching Workaround to bypass salesforce caching
     */
    @AuraEnabled(Cacheable = true)
    public static List<AccountToOpportunity__c> findInvestors(Id recordId, String searchKey, Long keyToDisableCaching, Integer limitSize)
    {
        return findInvestors(recordId, searchKey, limitSize);
    }

    @AuraEnabled(Cacheable = true)
    public static String getUnderbidderPicklistEntries()
    {
        Schema.DescribeFieldResult fieldResult = AccountToOpportunity__c.Underbidder__c.getDescribe();
        return JSON.serialize(fieldResult.getPicklistValues());
    }

    @AuraEnabled
    public static void saveUnderbidders(String jsonUnderbidders)
    {
        List<AccountToOpportunity__c> underbidders = (List<AccountToOpportunity__c>) JSON.deserialize(jsonUnderbidders, List<AccountToOpportunity__c>.class);
        update underbidders;
    }

    @AuraEnabled(cacheable=false)
    public static Map<String, List<SObject>> getRelatedData(Id recordId)
    {
        Map<String, List<SObject>> returnMap = new Map<String, List<SObject>>();
        // get Activity Data
        // Bug2507 npo: added TaskRelations to query
        List<Task> tasks = new List<Task>([SELECT Id, Subject, ActivityDate, Who.Name, (SELECT Id, RelationId, Relation.Type, Relation.Name FROM TaskRelations) FROM Task WHERE WhatId = :recordId ORDER BY ActivityDate DESC]);
        returnMap.put('Task', tasks);
        List<Event> events = new List<Event>([SELECT Id, Subject, ActivityDate, Date__c, Who.Name, (SELECT Id, RelationId, Relation.Type, Relation.Name FROM EventRelations) FROM Event WHERE WhatId = :recordId AND ActivityDate < TODAY ORDER BY Date__c DESC]);
        returnMap.put('Event', events);
        // get Contacts Data
        List<ContactToInvestor__c> contacts = new List<ContactToInvestor__c>([SELECT Id, Contact__c, Contact__r.FirstName, Contact__r.LastName, Contact__r.Name, Contact__r.Email, Contact__r.Account.Name, Investor__c, Name FROM ContactToInvestor__c WHERE Investor__c = :recordId AND Contact__r.AccountId != null ORDER By Contact__r.FirstName ASC]);
        returnMap.put('Contact', contacts);
        // get Status History Data
        List<Status__c> status = new List<Status__c>([SELECT Id, Investor__c, Name, toLabel(InvestorStatus__c), CreatedDate, StatusDate__c, LastModifiedBy.Name FROM Status__c WHERE Investor__c = :recordId ORDER BY StatusDate__c DESC LIMIT 5]);
        returnMap.put('Status__c', status);

        return returnMap;
    }

    @AuraEnabled(cacheable=false)
    public static Boolean setPinnedOnOpportunity(Id opportunityId, Id accountId, Boolean pin)
    {
        Opportunity opportunity = [SELECT Id, Pinned_Investors__c FROM Opportunity WHERE Id = :opportunityId];
        List<String> pinnedIds;
        if (opportunity.Pinned_Investors__c != null)
        {
            pinnedIds = opportunity.Pinned_Investors__c.split(';');
        } else
        {
            pinnedIds = new List<String>();
        }

        Boolean hasChanged = false;
        String cid = String.valueOf(accountId);
        if (pinnedIds.contains(cid) && pin == false)
        {
            pinnedids.remove(pinnedIds.indexOf(cid));
            hasChanged = true;
        } else if (!pinnedIds.contains(cid) && pin == true)
        {
            pinnedIds.add(cid);
            hasChanged = true;
        }
        if (hasChanged == true)
        {
            opportunity.Pinned_Investors__c = String.join(pinnedIds, ';');
            update opportunity;
            return true;
        }
        return false;
    }


    // US 1605 : ema : adjusted 02.20.2020
    @AuraEnabled(cacheable=false)
    public static String deleteInvestors(List<Id> investorIds)
    {
        if (investorIds != null && investorIds.size() > 0)
        {

            List<AccountToOpportunity__c> atos = new List<AccountToOpportunity__c>();

            for (Integer i = 0; i < investorIds.size(); i++)
            {
                AccountToOpportunity__c ato = new AccountToOpportunity__c(
                        Id = investorIds.get(i)
                );

                atos.add(ato);
            }

            try
            {
                delete atos;
                return JSON.serialize(new ResponseMessage(true, System.Label.GeneralLabelSuccess, System.Label.InvestorsListDeleteModalToastTitle, 'success', null));
            }
            catch (Exception e)
            {
                String errorMessage = e.getMessage();
                Id recordCausedTheIssue = null;
                if (errorMessage.contains(System.Label.GeneralLabelErrorBecauseHasActivityOrStatus))
                {
                    // Add {0} on the end, because we use the label on many places without variable, but here we need a variable
                    errorMessage = System.Label.GeneralLabelErrorBecauseHasActivityOrStatus + ' {0}';
                    recordCausedTheIssue = e.getDmlId(0);
                }
                return JSON.serialize(new ResponseMessage(false, System.Label.GeneralLabelError, errorMessage, 'error', recordCausedTheIssue));
            }
        }

        return JSON.serialize(new ResponseMessage(false, System.Label.GeneralLabelError, System.Label.GeneralLabelUnknownError, 'error', null));
    }

    @AuraEnabled(cacheable=false)
    public static Boolean updateInvestors(List<AccountToOpportunity__c> investors)
    {
        List<Status__c> status;
        if (investors != null && investors.size() > 0)
        {
            for (Integer i = 0; i < investors.size(); i++)
            {
                if (investors.get(i).isSet('Status__c'))
                {
                    if (status == null)
                    {
                        status = new List<Status__c>();
                    }
                    Status__c newStatus = new Status__c(InvestorStatus__c = investors.get(i).Status__c, Investor__c = investors.get(i).Id);
                    status.add(newStatus);
                }
            }
            if (status != null && status.size() > 0)
            {
                insert status;
            }
            update investors;
            return true;
        } else
        {
            return false;
        }
    }

    /**
     * add the Status field as datepicker field on the modal window for the status update
     * there is no restriction on the date selection (past, present or future)
     * this field is not required, as default display and save the current date
     * create a new date field on the "Status" object to store the date of the creation of the status (instead of using the default creation date)
     * the field should be called "Status date"
     * update the process that updates the Investor's "Last Status Date" field
     */
    @AuraEnabled(cacheable=false)
    public static Boolean updateInvestors(List<AccountToOpportunity__c> investors, Date statusDate)
    {
        return updateInvestors(investors, statusDate, true);
    }

    @AuraEnabled(cacheable=false)
    public static Boolean updateInvestors(List<AccountToOpportunity__c> investors, Date statusDate, Boolean createNewStatus)
    {
        List<Status__c> statusList = new List<Status__c>();
        if (investors != null && !investors.isEmpty())
        {
            for (AccountToOpportunity__c inv : investors)
            {
                // we don't need process anymore. We can update Investor's "Last Status Date" here.
                inv.LastStatusDate__c = statusDate;

                if (String.isNotBlank(inv.Status__c))
                {
                    statusList.add(new Status__c(
                            InvestorStatus__c = inv.Status__c,
                            Investor__c = inv.Id,
                            StatusDate__c = statusDate
                    ));
                }
            }

            if (createNewStatus && !statusList.isEmpty())
            {
                System.debug('statusList: ' + JSON.serialize(statusList));
                insert statusList;
            }
            System.debug('before update: ' + JSON.serialize(investors));
            update investors;
            System.debug('after update: ' + JSON.serialize(investors));
            return true;
        } else
        {
            return false;
        }
    }


    /**
    * Update Priority in investors list
    */
    @AuraEnabled(cacheable=false)
    public static Boolean updateInvestorsPriority(List<AccountToOpportunity__c> investors)
    {
        if (investors != null && !investors.isEmpty())
        {
            update investors;
            return true;
        } else
        {
            return false;
        }
    }

    @AuraEnabled
    public static Task createTask(String investorStatus, Task data, List<String> taskWhoIds)
    {
        Id offerRecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByDeveloperName().get('Offer').getRecordTypeId();
        Set<String> offerStatusSet = new Set<String> {'Indicative Offer', 'Confirmatory Offer', 'Binding Offer'};

        if (String.isNotBlank(investorStatus) && offerStatusSet.contains(investorStatus))
        {
            data.recordTypeId = offerRecordTypeId;
        }
        insert data;

        List<TaskRelation> taskRelations = new List<TaskRelation>();
        for (String taskWhoId : taskWhoIds)
        {
            taskRelations.add(new TaskRelation(TaskId = data.Id, RelationId = taskWhoId));
        }
        insert taskRelations;

        return data;
    }


    @AuraEnabled(cacheable=false)
    public static Task createTask(Task data, String type)
    {
        if (type == 'offer')
        {
            Id recordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByDeveloperName().get('Offer').getRecordTypeId();
            data.RecordTypeId = recordTypeId;
        } if (type == 'call')
    {
        data.Type = 'Call';
    }
        insert data;
        return data;
    }

    @AuraEnabled
    public static void deleteTask(Id taskId){
        delete [SELECT ID FROM Task WHERE ID = :taskId];
    }

    @AuraEnabled(cacheable=false)
    public static Event createEvent(Event data)
    {
        insert data;
        return data;
    }

    @AuraEnabled(cacheable=true)
    public static List<Contact> searchContacts(String searchKey, List<Id> excludeContacts, Id accountId, Boolean globalSearch,
            Integer contactSearchLimit)
    {
        List<Contact> contacts = new List<Contact>();

        if (String.isNotEmpty(searchKey))
        {
            String sosl = 'FIND :searchKey IN ALL FIELDS RETURNING Contact (Id, Name, FirstName, LastName, Email, Account.Name WHERE Id NOT IN :excludeContacts AND AccountId != null ';

            if (accountId != null && globalSearch == false)
            {
                sosl += ' AND AccountId = :accountId ';
            }

            sosl += ') ';


            if (globalSearch)
            {
                sosl += ', Account(Id, Name) ';
            }
            sosl += ' LIMIT :contactSearchLimit';
            System.debug('...sosl: ' + sosl);
            List<List<SObject>> searchResult = Search.query(sosl);

            if (searchResult.size() > 0)
            {
                contacts = searchResult.get(0);

                if (globalSearch)
                {
                    List<Account> accounts = searchResult.get(1);

                    if (accounts.size() > 0 && Contact.sObjectType.getDescribe().isAccessible())
                    {
                        List<Id> accountIds = new List<Id>();
                        List<Id> contactIds = new List<Id>();

                        for (Account account : accounts)
                        {
                            accountIds.add(account.Id);
                        }

                        for (Contact contact : contacts)
                        {
                            contactIds.add(contact.Id);
                        }
                        contactIds.addAll(excludeContacts);

                        if (contacts.size() < contactSearchLimit)
                        {
                            Integer newLimit = contactSearchLimit - contacts.size();
                            List<Contact> accountContacts = [SELECT Id, Name, FirstName, LastName, Email, Account.Name FROM Contact WHERE AccountId IN :accountIds AND Id NOT IN :contactIds AND AccountId != null LIMIT :newLimit];
                            contacts.addAll(accountContacts);
                        }
                    }
                }
            }
        }

        return contacts;
    }

    @AuraEnabled(cacheable=false)
    public static List<Task> getBidsByRecordId(Id recordId)
    {
        Set<String> ACTIVITY_OFFER_STATUS_SET = new Set<String> {'Offer'};
        return [SELECT Id, ActivityDate, InvestorStatus__c, Amount__c FROM Task WHERE WhatId = :recordId AND InvestorStatus__c IN :ACTIVITY_OFFER_STATUS_SET ORDER BY ActivityDate DESC, Amount__c DESC LIMIT 10];
    }

    /**
     * US1910
     * @author npo
     *
     * @param recordIds List of investors Ids
     *
     * @return List of investors' contacts
     */
    @AuraEnabled(cacheable=false)
    public static List<ContactToInvestor__c> getInvestorContacts(List<Id> recordsIds)
    {
        List<ContactToInvestor__c> investorContacts = [SELECT Id, Investor__c, Investor__r.Name FROM ContactToInvestor__c WHERE Investor__c IN :recordsIds];
        return investorContacts;
    }

    // Wrapper for error handling
    public class ResponseMessage
    {
        public Boolean isSuccess;
        public String title;
        public String message;
        /**
         *      possible values for variant: info (default), success, warning, and error.
        */
        public String variant;
        public Id recordId;

        public ResponseMessage(Boolean isSuccess, String title, String message, String variant, Id recordId)
        {
            this.isSuccess = isSuccess;
            this.title = title;
            this.message = message;
            this.recordId = recordId;
            this.variant = variant;
        }
    }
}