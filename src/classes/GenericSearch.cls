/**
* Is a generic search to select multiple records.
*
* @author: <Christian Schwabe> (Christian.Schwabe@colliers.com)
*
* @history:
* version		    | author				                                | changes
* ====================================================================================
* 0.1   07.10.2019	| Christian Schwabe (Christian.Schwabe@colliers.com)    | Initial version.
* 0.2   14.11.2019	| Christian Schwabe (Christian.Schwabe@colliers.com)    | Added the possibility to order the result.
* 0.3   07.01.2020	| Christian Schwabe (Christian.Schwabe@colliers.com)    | Added description for new method parameter string_filter and changed comparision != null to String.isNotBlank(...).
*/
public with sharing class GenericSearch {

    /**
     * Creates a dynamic SOQL query at runtime.
     * NOTICE: Currently - Does not support IN-Operator or query for integer values.
     *
     * @param  <List>String     list_fieldApiName       Contains api-names for fields.
     * @param  String           string_objectApiName    Contains api-name for object.
     * @param  String           string_searchTerm       Defines which search term is searched for
     * @param  String           string_whereApiName     Defines the api-name to search in.
     * @param  String           string_filter           Define additional filter criteria.
     * @param  String           string_soqlOperator     Currently supported: '=', 'LIKE'
     * @param  String           string_orderBy          i.e.: Id ASC or Id DESC
     * @param  Integer          integer_limit           Use LIMIT to specify the maximum number of rows to return.
     * @param  Integer          integer_offset          Use OFFSET to specify the starting row offset into the result set returned by your query.
     * @return List<SObject>
     */
    @AuraEnabled(cacheable = false)
    public static List<SObject> search(
        List<String> list_fieldApiName,
        String string_objectApiName,
        String string_searchTerm,
        String string_whereApiName,
        String string_filter,
        String string_soqlOperator,
        String string_orderBy,
        Integer integer_limit,
        Integer integer_offset
    ){
        //string_soqlOperator = String.isBlank(string_soqlOperator) ? '' : string_soqlOperator;//Avoid null pointer exception
        Boolean isLikeSearch = (string_soqlOperator.equalsIgnoreCase('LIKE') ? true : false);
        String key = (isLikeSearch ? '%' : '') + string_searchTerm + (isLikeSearch ? '%' : '');
        String string_fields = String.join(list_fieldApiName, ',').removeEnd(',');

        String query = 'SELECT ' + string_fields + ' FROM '+ string_objectApiName +' WHERE '+ string_whereApiName + ' ' + string_soqlOperator;

        if(string_soqlOperator == 'IN'){
            List<String> setIDs = string_searchTerm.split(',');
            query += ' :setIDs ';
        }else{
            query += ' :key ';
        }

        if(String.isNotBlank(string_filter)){
            query += ' AND ' + string_filter;
        }

        query += ' ORDER BY ' + string_orderBy + ' LIMIT :integer_limit OFFSET :integer_offset';

        List<SObject> sObjectList = Database.query(query);

        return sObjectList;
    }
}