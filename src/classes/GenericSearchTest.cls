/**
* Testclass for GenericSearch.cls
*
* @author: <Christian Schwabe> (Christian.Schwabe@colliers.com)
*
* @history:
* version		    | author				                                | changes
* ====================================================================================
* 0.1 11.11.2019	| Christian Schwabe (Christian.Schwabe@colliers.com)    | initial version.
* 0.2 07.01.2020	| Christian Schwabe (Christian.Schwabe@colliers.com)    | adapt changed method parameter.
*/
@isTest
private class GenericSearchTest {
    private static final String ACCOUNT_NAME = 'Test GmbH';

    private static void initialize(){
        Account account = new Account();
        account.Name = ACCOUNT_NAME;
        account.BillingCity = 'Frankfurt am Main';

        insert account;
    }

    @isTest
    private static void testWithLikeOperator(){
        initialize();

        List<SObject> listOfSObject =  GenericSearch.search(
            new List<String>{'Name'},
            'Account',
            'Test',
            'Name',
            null,
            'LIKE',
            'Name DESC',
            1,
            0
        );
        Integer actual = listOfSObject.size();
        Integer expected = 1;

        System.assertEquals(expected, actual);
    }

    @isTest
    private static void testWithEqualOperator(){
        initialize();

        List<SObject> listOfSObject =  GenericSearch.search(
            new List<String>{'Name'},
            'Account',
            ACCOUNT_NAME,
            'Name',
            null,
            '=',
            'Name DESC',
            1,
            0
        );
        Integer actual = listOfSObject.size();
        Integer expected = 1;

        System.assertEquals(expected, actual);
    }
}