/**
*   US2427.
*
*   @author npo
*   @copyright PARX
*/
public with sharing class EmailTemplateHelper
{
	@AuraEnabled(Cacheable=false)
	public static List<EmailTemplate> selectEmailTemplates()
	{
		String enLike = '%\\_EN';
		String deLike = '%\\_DE';
		return [SELECT Id, Name FROM EmailTemplate WHERE Name LIKE :deLike OR Name LIKE :enLike];
	}
}