public with sharing class MobilePropertyExplorerController {
    public List<PropertyObject__c> propertiesList { get; set; }
    public String currentLocationCoordinates { get; set; }
    public MobilePropertyExplorerController()
    {
        Map<String, String> params = ApexPages.currentPage().getParameters();
        System.debug('params: ' + JSON.serialize(params));
        MobilePropertySearchWrapper newMobilePropertySearchWrapper = new MobilePropertySearchWrapper();
        LocationCoordinatesWrapper newLocationCoordinatesWrapper = new LocationCoordinatesWrapper();
        if (params.containsKey('latitude') && params.containsKey('longitude')) 
        {
            newMobilePropertySearchWrapper.latitude = Decimal.valueOf(params.get('latitude'));
            newMobilePropertySearchWrapper.longitude =  Decimal.valueOf(params.get('longitude'));
            newLocationCoordinatesWrapper.latitude = params.get('latitude');
            newLocationCoordinatesWrapper.longitude = params.get('longitude');
            
            // this.currentLocationCoordinates = '{latitude:52.086409621077514, longitude:23.67908924512974}';
            // this.currentLocationCoordinates = '{latitude:52.094633099999996, longitude:23.691689399999998}';
            this.currentLocationCoordinates = JSON.serialize(newLocationCoordinatesWrapper);
        }
        if (params.containsKey('distanceMin'))
        {
            newMobilePropertySearchWrapper.distanceMin =  Decimal.valueOf(params.get('distanceMin'));
        }
        if (params.containsKey('distanceMax'))
        {
            newMobilePropertySearchWrapper.distanceMax =  Decimal.valueOf(params.get('distanceMax'));
        }
        if (params.containsKey('rentedMin'))
        {
            newMobilePropertySearchWrapper.rentedMin =  Decimal.valueOf(params.get('rentedMin'));
        }
        if (params.containsKey('rentedMax'))
        {
            newMobilePropertySearchWrapper.rentedMax =  Decimal.valueOf(params.get('rentedMax'));
        }
        if (params.containsKey('typeOfUse'))
        {
            newMobilePropertySearchWrapper.typeOfUse =  params.get('typeOfUse');
        }
        System.debug('newMobilePropertySearchWrapper: ' + JSON.serialize(newMobilePropertySearchWrapper));
        System.debug('newLocationCoordinatesWrapper: ' + JSON.serialize(newLocationCoordinatesWrapper));
        this.propertiesList = (List<PropertyObject__c>) getProperties(newMobilePropertySearchWrapper.latitude, newMobilePropertySearchWrapper.longitude, newMobilePropertySearchWrapper.distanceMin, newMobilePropertySearchWrapper.distanceMax, newMobilePropertySearchWrapper.rentedMin, newMobilePropertySearchWrapper.rentedMax, newMobilePropertySearchWrapper.typeOfUse);
        System.debug('propertiesList: ' + JSON.serialize(propertiesList));
    }

    @AuraEnabled
    public static List<sObject> getProperties(Decimal latitude, Decimal longitude, Decimal distanceMin, Decimal distanceMax, Decimal rentedMin, Decimal rentedMax, String typeOfUse){

        String queryString = 'SELECT ID, Name, PropertyOwner__r.Name, City__c, Street__c, PostalCode__c, AmountOfSpaceLet__c, Thumbnail__c, Geolocation__latitude__s, Geolocation__longitude__s FROM PropertyObject__c ' +
                             'WHERE DISTANCE(Geolocation__c, GEOLOCATION(' + latitude + ',' + longitude + '), \'km\') > ' + distanceMin +
                             ' AND DISTANCE(Geolocation__c, GEOLOCATION(' + latitude + ',' + longitude + '), \'km\') < ' + distanceMax +
                             ' AND ((AmountOfSpaceLet__c > ' + rentedMin + ' AND AmountOfSpaceLet__c < ' + rentedMax + ') OR AmountOfSpaceLet__c = null)';

        if(typeOfUse != ''){
            queryString += ' AND TypeOfUse__c INCLUDES (' + typeOfUse + ')';
        }

        queryString += ' ORDER BY DISTANCE(Geolocation__c, GEOLOCATION('+ latitude +','+ longitude +'), \'km\') LIMIT 50';
        System.debug('queryString: ' + queryString);
        return Database.query(queryString);
    }


    /**
    * Wrapper class to store search params
    */
    public class MobilePropertySearchWrapper
    {
        Decimal latitude { get; set; }
        Decimal longitude { get; set; }
        Decimal distanceMin { get; set; }
        Decimal distanceMax { get; set; }
        Decimal rentedMin { get; set; }
        Decimal rentedMax { get; set; }
        String typeOfUse { get; set; }

        public MobilePropertySearchWrapper()
        {
            this.latitude = 0;
            this.longitude = 0;
            this.distanceMin = 0;
            this.distanceMax = 0;
            this.rentedMin = 0;
            this.rentedMax = 0;
            this.typeOfUse = '';
        }
    }

     /**
    * Wrapper class to store coordinates
    */
    public class LocationCoordinatesWrapper
    {
        String latitude { get; set; }
        String longitude { get; set; }

        public LocationCoordinatesWrapper()
        {
            this.latitude = '';
            this.longitude = '';
        }
    }
}