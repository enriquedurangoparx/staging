/**
* Handle test logic for ContactTrigger
*
* @author: <Nadzeya Polautsava> (nadzeya.polautsava@parx.com)
*
* @history:
* version           | author                                                | changes
* ====================================================================================
* 0.1 01.04.2020	| Nadzeya Polautsava (nadzeya.polautsava@parx.com)	    | initial version - Bug1948.
*/
@IsTest
public class ContactTriggerTest
{
    @testSetup
    static void setup()
    {
        TestDataFactory.initializeProducts();

        Embargo__c embargo = new Embargo__c();
        embargo.Name = 'Embargo';
        embargo.Countries__c = 'Egypt;Afghanistan;Belarus;Burundi;Iraq;Yemen';
        insert embargo;
    }

    @IsTest
    static void testPopulateListOfContactsAfterUpdate()
    {
        Account newAccount = TestDataFactory.createAccount('Test Account 1');
        insert newAccount;

        List<Contact> newContactList = TestDataFactory.createContacts(newAccount.Id, 1);
        insert newContactList;

        PropertyObject__c propertyObject = TestDataFactory.buildPropertyObject('property', true);
        Opportunity newOpportunity = TestDataFactory.createOpportunity(newAccount.Id, propertyObject);
        insert newOpportunity;

        AccountToOpportunity__c newAccToOpp = TestDataFactory.createAccountToOpportunity(newAccount.Id, newOpportunity.Id);
        insert newAccToOpp;

        AccountToOpportunity__c accToOppToCheck = [SELECT Id, ListOfContacts__c FROM AccountToOpportunity__c WHERE Id = :newAccToOpp.Id LIMIT 1];

        List<ContactToInvestor__c> newContactToInvestorList = TestDataFactory.createContactToInvestor(newAccToOpp.Id, newContactList);
        insert newContactToInvestorList;

        accToOppToCheck = [SELECT Id, ListOfContacts__c FROM AccountToOpportunity__c WHERE Id = :newAccToOpp.Id LIMIT 1];
        System.assertEquals(true, String.isNotBlank(accToOppToCheck.ListOfContacts__c), 'List of contacts should not be blank');

        newContactList.get(0).FirstName = 'New value';
        update newContactList;

        accToOppToCheck = [SELECT Id, ListOfContacts__c FROM AccountToOpportunity__c WHERE Id = :newAccToOpp.Id LIMIT 1];
        System.assert(accToOppToCheck.ListOfContacts__c.contains('New value'),
                'We expect ListOfContacts__c to be updated');

    }

}