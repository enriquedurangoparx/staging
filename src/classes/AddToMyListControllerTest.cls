/**
* Test class for AddToMyListController.cls
*
* @author: <Egor Markuzov> (egor.markuzov@parx.com)
*
* @history:
* version		    | author				                                | changes
* ====================================================================================
* 0.1 16.11.2019	| Egor Markuzov (egor.markuzov@parx.com)                | initial version.
* 0.2 17.03.2020	| Christian Schwabe (Christian.Schwabe@colliers.com)    | [SFINV/US 2407] Added Saluation for Contact.
*/
@isTest
private class AddToMyListControllerTest
{
    /**
     * test AddToMyListController.createInvestorListItemRecord(Id recordId, Id investorListId, String objApiName)
     * for case if we already have Investor List Item for Account
     */
    @isTest
    public static void testCreateInvestorListExistingAccount()
    {
        Account newAcc = TestDataFactory.createAccount('Test 1');
        insert newAcc;

        InvestorsList__c newInvestorList = TestDataFactory.createInvestorList('Test List');
        insert newInvestorList;

        InvestorsListItem__c newInvestorListItem = TestDataFactory.createInvestorListItem(newInvestorList.Id, newAcc.Id);
        insert newInvestorListItem;

        String jsonResp = AddToMyListController.createInvestorListItemRecord(newAcc.Id, newInvestorList.Id, 'Account');

        AddToMyListController.ResponseMessage resp = (AddToMyListController.ResponseMessage) JSON.deserialize(jsonResp, AddToMyListController.ResponseMessage.class);
        System.assertEquals(false, resp.isSuccess, 'Investor List Item record was created, but should not.');
    }

    /**
     * test AddToMyListController.createInvestorListItemRecord(Id recordId, Id investorListId, String objApiName)
     * for case if we have no Investor List Item for Account
     */
    @isTest
    public static void testCreateInvestorListNewAccount()
    {
        Account newAcc = TestDataFactory.createAccount('Test 1');
        insert newAcc;

        InvestorsList__c newInvestorList = TestDataFactory.createInvestorList('Test List');
        insert newInvestorList;

        String jsonResp = AddToMyListController.createInvestorListItemRecord(newAcc.Id, newInvestorList.Id, 'Account');

        AddToMyListController.ResponseMessage resp = (AddToMyListController.ResponseMessage) JSON.deserialize(jsonResp, AddToMyListController.ResponseMessage.class);
        System.assertEquals(true, resp.isSuccess, 'Investor List Item record was not created, but should.');
    }

    /**
     * test AddToMyListController.createInvestorListItemRecord(Id recordId, Id investorListId, String objApiName)
     * for case if we already have Investor List Item for Contact
     */
    @isTest
    public static void testCreateInvestorListExistingListItemForContact()
    {
        Account newAcc = TestDataFactory.createAccount('Test 1');
        insert newAcc;

        List<Contact> contList = TestDataFactory.createContacts(newAcc.Id, 1);
        insert contList;

        InvestorsList__c newInvestorList = TestDataFactory.createInvestorList('Test List');
        insert newInvestorList;

        InvestorsListItem__c newInvestorListItem = TestDataFactory.createInvestorListItem(newInvestorList.Id, newAcc.Id);
        insert newInvestorListItem;

        String jsonResp = AddToMyListController.createInvestorListItemRecord(contList[0].Id, newInvestorList.Id, 'Contact');

        AddToMyListController.ResponseMessage resp = (AddToMyListController.ResponseMessage) JSON.deserialize(jsonResp, AddToMyListController.ResponseMessage.class);
        System.assertEquals(true, resp.isSuccess, 'Investor List Item Contact record was not created, but should.');
    }

    /**
     * test AddToMyListController.createInvestorListItemRecord(Id recordId, Id investorListId, String objApiName)
     * for case if we already have Investor List Item and Investor List Item Contact for Contact
     */
    @isTest
    public static void testCreateInvestorListExistingListItemContact()
    {
        Account newAcc = TestDataFactory.createAccount('Test 1');
        insert newAcc;

        List<Contact> contList = TestDataFactory.createContacts(newAcc.Id, 1);
        insert contList;

        InvestorsList__c newInvestorList = TestDataFactory.createInvestorList('Test List');
        insert newInvestorList;

        InvestorsListItem__c newInvestorListItem = TestDataFactory.createInvestorListItem(newInvestorList.Id, newAcc.Id);
        insert newInvestorListItem;

        InvestorsListItemContact__c newInvestorListItemContact = TestDataFactory.createInvestorListItemContact(newInvestorListItem.Id, contList[0].Id);
        insert newInvestorListItemContact;

        String jsonResp = AddToMyListController.createInvestorListItemRecord(contList[0].Id, newInvestorList.Id, 'Contact');

        AddToMyListController.ResponseMessage resp = (AddToMyListController.ResponseMessage) JSON.deserialize(jsonResp, AddToMyListController.ResponseMessage.class);
        System.assertEquals(false, resp.isSuccess, 'Investor List Item Contact record was created, but should not.');
    }

    /**
     * test AddToMyListController.createInvestorListItemRecord(Id recordId, Id investorListId, String objApiName)
     * for case if we have no Investor List Item Contact for Contact
     */
    @isTest
    public static void testCreateInvestorListNewContact()
    {
        Account newAcc = TestDataFactory.createAccount('Test 1');
        insert newAcc;

        List<Contact> contList = TestDataFactory.createContacts(newAcc.Id, 1);
        insert contList;

        InvestorsList__c newInvestorList = TestDataFactory.createInvestorList('Test List');
        insert newInvestorList;

        String jsonResp = AddToMyListController.createInvestorListItemRecord(contList[0].Id, newInvestorList.Id, 'Contact');

        AddToMyListController.ResponseMessage resp = (AddToMyListController.ResponseMessage) JSON.deserialize(jsonResp, AddToMyListController.ResponseMessage.class);
        System.assertEquals(true, resp.isSuccess, 'Investor List Item Contact record was not created, but should.');
    }
}