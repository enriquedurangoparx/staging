/**
*   Test class for Utils class
*
*   @author Tejashree
*   @copyright PARX
*/

@isTest
private class UtilsTest 
{
    @isTest
    static void testGetCounterRecords() 
    {
        Test.startTest();
        insert new ColliersCounter__c(Name = 'Customer', Counter__c = 1000000);
        Map<String, ColliersCounter__c> counterMap= Utils.getCounterRecords();
        System.assert(counterMap != null, 'Counter Records Found');
        Test.stopTest();
    }

    @IsTest
    public static void copySObjectFieldsByFieldMapTest()
    {
        Contact sourceContact = new Contact(
                FirstName = 'John',
                LastName = 'Doe',
                Phone = '+ 375 29 000 00 00',
                MailingCity = 'Bremen'
        );

        Account targetAccount = new Account();
        targetAccount = (Account) Utils.copySObjectFieldsByFieldMap(sourceContact, targetAccount, new Map<SObjectField, SObjectField>{
                Contact.LastName => Account.Name,
                Contact.FirstName => Account.Description,
                Contact.Phone => Account.Phone,
                Contact.MailingCity => Account.BillingCity
        });

        System.assertEquals(sourceContact.LastName, targetAccount.Name, 'The fields should have an equal value');
        System.assertEquals(sourceContact.FirstName, targetAccount.Description, 'The fields should have an equal value');
        System.assertEquals(sourceContact.Phone, targetAccount.Phone, 'The fields should have an equal value');
        System.assertEquals(sourceContact.MailingCity, targetAccount.BillingCity, 'The fields should not have an equal value');
    }
}