public class AddSelectedToInvestorsListController {
    private static final String KEY_OF_RET_URL = 'vfRetURLInSFX';
    public String selectedIds {get; set;}
    public String retUrl {get; set;}

    public AddSelectedToInvestorsListController(ApexPages.StandardSetController controller) {
        Set<Id> selectedIdsSet = new Set<Id>();
        System.debug(ApexPages.currentPage().getParameters().get('vfRetURLInSFX'));

        for (SObject c : controller.getSelected())
        {
        	selectedIdsSet.add(c.Id);    
        }
        this.selectedIds = JSON.serialize(selectedIdsSet);

        this.retUrl = '/';
        if (ApexPages.currentPage().getParameters().containsKey(KEY_OF_RET_URL))
        {
            this.retUrl = JSON.serialize(ApexPages.currentPage().getParameters().get(KEY_OF_RET_URL));
        }
    }
}