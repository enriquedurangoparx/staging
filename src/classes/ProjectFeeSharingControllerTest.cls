/**
* This is a test class for ProjectFeeSharingController apex class.
*
 * @author ile
 * @copyright PARX
 */

@istest
private class ProjectFeeSharingControllerTest
{
    public static FeeSharing__c buildFeeSharing()
    {
        return new FeeSharing__c();
    }

    private static Map<String, SObject> prepareData()
    {
        OpportunityHandlerTest.initializeProducts();// Needed to setup product, pricebook and pricebookentry.

        Embargo__c embargo = new Embargo__c();
        embargo.Name = 'Embargo';
        embargo.Countries__c = 'Egypt;Afghanistan;Belarus;Burundi;Iraq;Yemen';
        insert embargo;

        Product2 productA = new Product2(Name = 'A', Family = 'ABC', RelevantForFeeSharing__c = TRUE, IsActive = TRUE);
        Product2 productB = new Product2(Name = 'B', Family = 'ABC', RelevantForFeeSharing__c = TRUE, IsActive = TRUE);
        insert new List<Product2>{productA, productB};

        PricebookEntry standardPricebookEntry = new PricebookEntry();
        standardPricebookEntry.IsActive = true;
        standardPricebookEntry.Pricebook2Id = Test.getStandardPricebookId();
        standardPricebookEntry.Product2Id = productA.Id;
        standardPricebookEntry.UnitPrice = 100;
        insert standardPricebookEntry;

        Pricebook2 valuationPricebook = [SELECT Id FROM Pricebook2 WHERE Name = :OpportunityHandler.VALUATION_PRICEBOOKNAME];

        PricebookEntry pricebookEntryA = standardPricebookEntry;//new PricebookEntry(Pricebook2Id = Test.getStandardPricebookId(), Product2Id = productA.Id, UnitPrice = 1, IsActive = true);
        //PricebookEntry pricebookEntryB = new PricebookEntry(Pricebook2Id = Test.getStandardPricebookId(), Product2Id = productB.Id, UnitPrice = 1, IsActive = true);
        PricebookEntry pricebookEntryB = [SELECT Id, Product2Id FROM PriceBookEntry WHERE Pricebook2Id = :valuationPricebook.Id];//new PricebookEntry(Pricebook2Id = valuationPricebook.Id, Product2Id = productB.Id, UnitPrice = 1, IsActive = true);


        /* PricebookEntry pricebookEntryA = new PricebookEntry(Pricebook2Id = Test.getStandardPricebookId(), Product2Id = productA.Id, UnitPrice = 1, IsActive = true);
        PricebookEntry pricebookEntryB = new PricebookEntry(Pricebook2Id = Test.getStandardPricebookId(), Product2Id = productB.Id, UnitPrice = 1, IsActive = true);
        insert new List<PricebookEntry>{pricebookEntryA, pricebookEntryB}; */

        PropertyObject__c propertyObject = TestDataFactory.buildPropertyObject('property', true);
        Opportunity testOpportunity = UnitTestDataTest.buildOpportunity('Test Opportunity', propertyObject);
        testOpportunity.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('ValuationAcquisition').getRecordTypeId();
        testOpportunity.ProjectStart__c = Date.today();
        testOpportunity.CloseDate = Date.today().addDays(3);
        insert testOpportunity;
        
        return new Map<String, SObject>{
            'Product A' => productA,
            'Product B' => productB,
            'Price Book Entry A' => pricebookEntryA,
            'Price Book Entry B' => pricebookEntryB,
            'Opportunity' => testOpportunity
        };
    }

    @istest
    static void shouldSelectOpportunity()
    {
        Map<String, SObject> dataMap = prepareData();
        Opportunity opportunityRecord = ProjectFeeSharingController.selectOpportunity(dataMap.get('Opportunity').Id);
        System.assertEquals(dataMap.get('Opportunity').Id, opportunityRecord.Id);
//        ONB2__AssignmentRulesCenter__c assignmentRulesCenterSettings = ProjectFeeSharingController.getAssignmentRulesCenterSettings(relatedUser.Name);
    }

    @istest
    static void shouldCommitAndSelectFeeSharing()
    {
        Map<String, SObject> dataMap = prepareData();
        
        User u = UnitTestDataTest.buildUser();
        u.Tenant__c = 'Deutschland';
        u.FirstName = 'colliersTestMethod';
        u.LastName = 'systemTestCenter';
        insert u;
        
        System.assert(ProjectFeeSharingController.selectFeeSharing(null).isEmpty());
        Opportunity opportunityRecord = (Opportunity) dataMap.get('Opportunity');

        FeeSharing__c feeSharing = buildFeeSharing();
        feeSharing.User__c = u.Id;
        feeSharing.Opportunity__c = opportunityRecord.Id;
        feeSharing.Tenant__c = 'Deutschland';

        feeSharing = ProjectFeeSharingController.commitFeeSharing(System.JSON.serialize(new List<FeeSharing__c>{feeSharing}), opportunityRecord.Id).get(0);
        System.assertEquals(feeSharing.Id, ProjectFeeSharingController.selectFeeSharing(opportunityRecord.Id).get(0).Id);

        PricebookEntry pbEntryRecord = (PricebookEntry)dataMap.get('Price Book Entry B');
        OpportunityLineItem opportunityItemRecord = new OpportunityLineItem (
                                                                                PricebookEntryId = pbEntryRecord.Id,
                                                                                Product2Id = pbEntryRecord.Product2Id,
                                                                                Quantity = 1,
                                                                                UnitPrice = 10.0,
                                                                                OpportunityId = opportunityRecord.Id
                                                                            );
        insert opportunityItemRecord;

        //OpportunityLineItem oli = [SELECT Id, Product2Id, Quantity FROM OpportunityLineItem WHERE OpportunityId = : opportunityRecord.Id LIMIT 1];
        //system.debug('#### oli: ' + oli);
        
        
        ONB2__AssignmentRulesCenter__c assignmentRulesCenterSettings = new ONB2__AssignmentRulesCenter__c(
            Name = 'A',
            ONB2__BillingType__c = u.FirstName + ' ' + u.LastName,
            ONB2__Tenant__c = 'Deutschland',
            ONB2__Center__c = 'DEF',
            ONB2__ProductGroup__c = 'ABC'
        );

        insert assignmentRulesCenterSettings;

//        feeSharing = feeSharing.clone(false, true);
//        feeSharing = ProjectFeeSharingController.commitFeeSharing(System.JSON.serialize(new List<FeeSharing__c>{feeSharing})).get(0);

//        FeeSharing__c updatedFeeSharing = [select Center__c, Tenant__c from FeeSharing__c where Id = :feeSharing.Id limit 1];
//        system.debug('#### updatedFee Sharing record: ' + updatedFeeSharing);
        //System.assertEquals(assignmentRulesCenterSettings.ONB2__Center__c, updatedFeeSharing.Center__c);
        assignmentRulesCenterSettings = ProjectFeeSharingController.getAssignmentRulesCenterSettings(u.FirstName + ' ' + u.LastName);

    }

    @istest
    public static void shouldSelectFeeDistributionSetting()
    {
        System.assertEquals(null, ProjectFeeSharingController.selectFeeDistributionSetting());
        FeeDistributionSettings__c feeDistributionSetting = new FeeDistributionSettings__c(
                DistributionForReporting__c = 0,
                DistributionForOrderAcquisition__c = 0,
                DistributionForExecution__c = 0,
                DistributionForIdentification__c = 0,
                MaxAmountOfWorkers__c = 1
        );

        insert feeDistributionSetting;

        System.assertEquals(feeDistributionSetting.DistributionForIdentification__c,
                ProjectFeeSharingController.selectFeeDistributionSetting().DistributionForIdentification__c);
    }

    @istest
    public static void shouldGetSObjectDescription()
    {
        ProjectFeeSharingController.SObjectDescription sObjectDescription = ProjectFeeSharingController.getSObjectDescription('Account');
        System.assertEquals(sObjectDescription.label, Account.class.getName());
    }

}