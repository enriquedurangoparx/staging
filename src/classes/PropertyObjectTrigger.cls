/**
* Handles the trigger logic of PropertyObject__c
*
* @author: <Maksim Fedorenko> (maksim.fedorenko@parx.com)
*
* @history:
* version		    | author				                                | changes
* ====================================================================================
* 0.1 31.01.2020	| Maksim Fedorenko (maksim.fedorenko@parx.com)          | initial version.
* 0.2 13.02.2020	| Christian Schwabe (Christian.Schwabe@colliers.com)    | [SFINV/US 1977] Added aggregateObjectsToPortfolio(...).
* 0.3 26.02.2020	| Maksim Fedorenko (maksim.fedorenko@parx.com)          | added hookBeforeUpdate() + invocation of updateGeolocationData() methods.
*/
public with sharing class PropertyObjectTrigger extends ATrigger
{
    private static Type rollClass = System.Type.forName('rh2', 'ParentUtil');

    public override void hookAfterInsert(List<SObject> records)
    {
        PropertyObjectService.aggregateObjectsToPortfolio(records, null);
        executeRollupHelper(null, new Map<Id, SObject>(records));
        if (!Test.isRunningTest())
        {
            PropertyObjectService.updateGeolocationData(records, null);
        }
    }

    public override void hookBeforeUpdate(List<SObject> records, List<SObject> oldRecords)
    {
        if (!Test.isRunningTest())
        {
            PropertyObjectService.updateGeolocationData(records, oldRecords);
        }
    }

    public override void hookAfterUpdate(List<SObject> records, List<SObject> oldRecords)
    {
        PropertyObjectService.copyDataToOpportunity(records);
        PropertyObjectService.aggregateObjectsToPortfolio(records, oldRecords);
        executeRollupHelper(new Map<Id, SObject>(oldRecords), new Map<Id, SObject>(records));
    }

    public override void hookAfterDelete(List<SObject> records)
    {
        PropertyObjectService.aggregateObjectsToPortfolio(records, null);
        executeRollupHelper(new Map<Id, SObject>(records), null);
    }

    public override void hookBeforeDelete(List<SObject> records){
        executeRollupHelper(new Map<Id, SObject>(records), null);
    }

    public override void hookAfterUndelete(List<SObject> records){
        executeRollupHelper(new Map<Id, SObject>(records), null);
    }

    /**
    * Helpermethod to execute rollup-helper. 
    */
    private void executeRollUpHelper(Map<Id, SObject> oldMap, Map<Id, SObject> newMap){
        if(rollClass != null) {
            rh2.ParentUtil pu = (rh2.ParentUtil) rollClass.newInstance();
            pu.performTriggerRollups(oldMap, newMap, new String[]{'PropertyObject__c'}, null);
        }
    }
}