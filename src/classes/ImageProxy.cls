@RestResource(urlMapping='/ContentDoc/*')
global without sharing class ImageProxy {
    public static Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe();
    
    public static String findObjectNameFromRecordIdPrefix(String recordIdOrPrefix){
        String objectName = '';
        try{
            //Get prefix from record ID
            //This assumes that you have passed at least 3 characters
            String myIdPrefix = String.valueOf(recordIdOrPrefix).substring(0,3);
            
            //Loop through all the sObject types returned by Schema
            for(Schema.SObjectType stype : gd.values()){
                Schema.DescribeSObjectResult r = stype.getDescribe();
                String prefix = r.getKeyPrefix();
                System.debug('Prefix is ' + prefix);
                
                //Check if the prefix matches with requested prefix
                if(prefix!=null && prefix.equals(myIdPrefix)){
                    objectName = r.getName();
                    System.debug('Object Name! ' + objectName);
                    break;
                }
            }
        }catch(Exception e){
            System.debug(e);
        }
        return objectName;
    }
    
    global static String imageProxySiteUrl  = [SELECT DeveloperName, StringValue__c FROM Preferences__mdt WHERE DeveloperName = 'imageProxySiteUrl'].StringValue__c;
    
    @HttpGet
    global static void getBlob() {
        RestResponse res = RestContext.response;
        RestRequest req = RestContext.request;
        String url = null;
        
        
        String b = req.requestURI;
        String s = EncodingUtil.base64Decode(req.requestURI.split('/ContentDoc/')[1]).toString().replaceAll('&amp;','&');
        
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setMethod('GET');
        
        if (s.indexOf('sfc/servlet.shepherd/version') >= 0)
        {
            //content Version
            String cid = s.split('/version/download/')[1].split('\\?')[0];
            url = 'callout:Self/services/data/v44.0/sobjects/ContentVersion/' + cid + '/VersionData';
        }
        else if (s.indexOf('/servlet/rtaImage') >= 0)
        {
            Map<String,String> paramMap = new Map<String,String>();
            //content Version
            for (String cid : s.split('/servlet/rtaImage\\?')[1].split('&'))
            {
                System.debug(cid);
                paramMap.put(cid.split('=')[0], cid.split('=')[1]);
            }
            
            System.debug(paramMap);
            String recId = paramMap.get('eid');
            String fieldID = paramMap.get('feoid');
            String refId = paramMap.get('refid');
            String objectType = findObjectNameFromRecordIdPrefix(recId);
            
            if (Test.isRunningTest())
            {
                return;
            }
            FieldDefinition fd = [
                SELECT Id, DeveloperName, DurableId, QualifiedApiName 
                FROM FieldDefinition 
                WHERE DurableId = : objectType +'.' + fieldID];
            url = 'callout:Self/services/data/v45.0/sobjects/' + objectType + '/' + recId + '/richTextImageFields/' + fd.QualifiedApiName + '/' + refId + '';
                    }        
        
        request.setEndpoint(url);
        
        if (Test.isRunningTest())
        {
            return;
        }
        HttpResponse response = http.send(request);
        
        if (response.getStatusCode() == 200) {
            Blob a = response.getBodyAsBlob();
            if (response.getHeader('Content-Type') == 'application/octetstream')
            {
                res.addHeader('Content-Type','image/png');    
            }
            else
            {
                res.addHeader('Content-Type',response.getHeader('Content-Type'));
            }
            String afterblob = EncodingUtil.base64Encode(a);
            res.responseBody =EncodingUtil.base64Decode(afterblob); 
            
            
            /*
            String s2 = '';
            for (String k : response.getHeaderKeys())
            {
                s2 += k + ' ' + response.getHeader(k) + '\n';
            }
            res.addHeader('Content-Type','text/plain');
            res.responseBody = Blob.valueOf(s2);
            */
        }
        else
        {
            res.addHeader('Content-Type','text/plain');
            res.responseBody = Blob.valueOf(response.getBody());
        }
        
        
    } 
    
    global class ExtractFieldName {
        @InvocableVariable(label='Record Id' required=true)
        global Id recordId;
        @InvocableVariable(label='Rich Text' required=false)
        global String richText;
        @InvocableVariable(label='URL Field API Name' required=true)
        global String urlFieldName;
    }    
    
    @InvocableMethod(label='Extract Image Url')
    global static void extractFieldNameActionsBatch(List<ExtractFieldName> requests) {
        for(ExtractFieldName request: requests){
            extractFieldNameAction(request);
        }
    }
    public static void extractFieldNameAction(ExtractFieldName request) {
        System.debug(request.recordId);

        Id recordId = request.recordId;
        String sObjectType = recordId.getSObjectType().getDescribe().getName();
        Sobject o1 = Schema.getGlobalDescribe().get(sObjectType).newSObject(recordId);


        String url = null;
        
        if (String.isNotBlank(request.richText))
        {
            for (String token : request.richText.split('"'))
            {
                System.debug(token);
                if (token.indexOf('rtaImage') >= 0 || token.indexOf('servlet.shepherd') >= 0 )
                {
                    //url = 'https://parxdevkam-colliersde--parxdevkam.cs101.force.com/imageproxy/services/apexrest/ContentDoc/' + EncodingUtil.base64Encode(Blob.valueOf(token));
                    url = imageProxySiteUrl + '/services/apexrest/ContentDoc/' + EncodingUtil.base64Encode(Blob.valueOf(token));
                }
            }         
        }



        o1.put(request.urlFieldName, url);
        System.debug(sobjectType);
        System.debug(o1);
        update o1;
     }

}