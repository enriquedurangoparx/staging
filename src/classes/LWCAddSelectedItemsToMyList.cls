/**
* Controller for AddSelectedItemsToMyList lightning web component
* Is used to
* -save accounts to "Investor List Item" (apiname: InvestorsListItem__c)
* -save contacts to "Investor List Item Contact" (apiname: InvestorsListItemContact__c)
*
* @author: <Egor Markuzov> (egor.markuzov@parx.com)
*
* @history:
* version		    | author				                                | changes
* ====================================================================================
* 0.1 18.11.2019	| Egor Markuzov (egor.markuzov@parx.com)                | initial version.
* 0.1 26.11.2019	| Egor Markuzov (egor.markuzov@parx.com)                | adjusted collecting data of existinginvestorlistitem for createInvestorListItemContactsRecords
*/
public class LWCAddSelectedItemsToMyList {
    /**
    * Method for creating InvestorsListItems records from Account listview
    *
    * @param  List<Id>       recordId        List of AccountIds record.
    * @param  Id             investorListId        id of Investor List record.
    * @return String of InvestorListViewWrapper in JSON format
    */
    @AuraEnabled
    public static String createInvestorListItemRecords(List<Id> recordIds, Id investorListId)
    {
        System.debug('recordIds: ' + recordIds);
        System.debug('investorListId: ' + investorListId);
        InvestorListViewWrapper investorListViewWrap = new InvestorListViewWrapper();
        InvestorsList__c investorListRec = [SELECT Id, Name FROM InvestorsList__c WHERE Id = :investorListId LIMIT 1];
        investorListViewWrap.investorListName = investorListRec.Name;
        investorListViewWrap.investorListId = investorListId;
        // System.savePoint sp = Database.setSavePoint();
        // List<InvestorsListItem__c> investorListItemList = ;
        Map<Id, InvestorsListItem__c> accountIdToInvestorListItemMap = new Map<Id, InvestorsListItem__c>();
        // Split existing InvestorListItems and new InvestorListItems
        for (InvestorsListItem__c investorListItem : [SELECT Id, Name, Account__c, Account__r.Name, InvestorsList__c, InvestorsList__r.Name 
                                                        FROM InvestorsListItem__c 
                                                        WHERE Account__c = :recordIds
                                                        AND InvestorsList__c = :investorListId])
        {
            accountIdToInvestorListItemMap.put(investorListItem.Account__c, investorListItem);
        }

        List<InvestorsListItem__c> investorsListItemToCreateList = new List<InvestorsListItem__c>();
        for (Id accId : recordIds)
        {
            if (accountIdToInvestorListItemMap.containsKey(accId))
            {
                investorListViewWrap.existingInvestorsListItems.add(accountIdToInvestorListItemMap.get(accId));
            }
            else
            {
                InvestorsListItem__c investorListItem = new InvestorsListItem__c(
                    Account__c = accId,
                    InvestorsList__c = investorListId
                );
                investorsListItemToCreateList.add(investorListItem);
            }
        }

        if (!investorsListItemToCreateList.isEmpty())
        {
            insert investorsListItemToCreateList;

            investorListViewWrap.newInvestorsListItems = [SELECT Id, Name, Account__c, Account__r.Name, InvestorsList__c, InvestorsList__r.Name
                                                          FROM InvestorsListItem__c
                                                          WHERE Id IN :investorsListItemToCreateList];
            // database.rollback(sp);
        }
            
        return JSON.serialize(investorListViewWrap);
    }

    /**
    * Method for creating InvestorsListItemContacts records from Contact listview
    *
    * @param  List<Id>       recordId               List of Contact.Id record.
    * @param  Id             investorListId         id of Investor List record.
    * @return String of InvestorListViewWrapper in JSON format
    */
    @AuraEnabled
    public static String createInvestorListItemContactsRecords(List<Id> recordIds, Id investorListId)
    {
        InvestorListViewWrapper investorListViewWrap = new InvestorListViewWrapper();
        InvestorsList__c investorListRec = [SELECT Id, Name FROM InvestorsList__c WHERE Id = :investorListId LIMIT 1];
        investorListViewWrap.investorListName = investorListRec.Name;
        investorListViewWrap.investorListId = investorListId;

        Set<Id> accIdSet = new Set<Id>();
        Map<Id, Contact> contactIdToContactMap = new Map<Id, Contact>();

        // System.savePoint sp = Database.setSavePoint();
        // Collect contact's data with AccountId
        for (Contact cont : [SELECT Id, AccountId FROM Contact WHERE Id IN :recordIds AND AccountId != NULL])
        {
            accIdSet.add(cont.AccountId);
            contactIdToContactMap.put(cont.Id, cont);
        }

        // Collect existing InvestorsListItem__c
        Map<Id, InvestorsListItem__c> accountIdToInvestorListItemMap = new Map<Id, InvestorsListItem__c>();
        for (InvestorsListItem__c investorsListItem : [SELECT Id, Name, Account__c, Account__r.Name, InvestorsList__c, InvestorsList__r.Name 
                                                        FROM InvestorsListItem__c 
                                                        WHERE Account__c = :accIdSet
                                                        AND InvestorsList__c = :investorListId])
        {
            accountIdToInvestorListItemMap.put(investorsListItem.Account__c, investorsListItem);
        }

        // Detect missing InvestorsListItem__c records
        List<InvestorsListItem__c> investorsListItemToCreateList = new List<InvestorsListItem__c>();
        Set<Id> accIdForNewInvestorListItemsSet = new Set<Id>();
        Set<InvestorsListItem__c> existingInvestorsListItemsSet = new Set<InvestorsListItem__c>();
        for (Contact cont : contactIdToContactMap.values())
        {
            if (!accountIdToInvestorListItemMap.containsKey(cont.AccountId))
            {
                InvestorsListItem__c newInvestorListItem = new InvestorsListItem__c(
                    Account__c = cont.AccountId,
                    InvestorsList__c = investorListId
                );
                accountIdToInvestorListItemMap.put(cont.AccountId, newInvestorListItem);
                investorsListItemToCreateList.add(newInvestorListItem);
                accIdForNewInvestorListItemsSet.add(cont.AccountId);
            }
            else 
            {
                if (!accIdForNewInvestorListItemsSet.contains(cont.AccountId))
                {
                    existingInvestorsListItemsSet.add(accountIdToInvestorListItemMap.get(cont.AccountId));
                }
            }
        }

        if (!existingInvestorsListItemsSet.isEmpty())
        {
            investorListViewWrap.existingInvestorsListItems.addAll(existingInvestorsListItemsSet);
        }

        // Create missing InvestorsListItem__c records
        if (!investorsListItemToCreateList.isEmpty())
        {
            insert investorsListItemToCreateList;

            // Collect data for LWC result view
            investorListViewWrap.newInvestorsListItems = [SELECT Id, Name, Account__c, Account__r.Name, InvestorsList__c, InvestorsList__r.Name
                                                          FROM InvestorsListItem__c
                                                          WHERE Id IN :investorsListItemToCreateList];
        }

        // Collect existing InvestorsListItemContact__c records
        Map<Id, InvestorsListItemContact__c> contIdToInvestorListItemContactMap = new Map<Id, InvestorsListItemContact__c>();
        for (InvestorsListItemContact__c investorListItemContact : [SELECT Id, Name, InvestorsListItem__c, Contact__c, Contact__r.FirstName, Contact__r.LastName 
                                                                    FROM InvestorsListItemContact__c 
                                                                    WHERE Contact__c IN :contactIdToContactMap.keySet() 
                                                                    AND InvestorsListItem__r.InvestorsList__c = :investorListId])
        {
            contIdToInvestorListItemContactMap.put(investorListItemContact.Contact__c, investorListItemContact);
        }

        // Detect missing InvestorsListItemContact__c records
        List<InvestorsListItemContact__c> investorsListItemContactToCreateList = new List<InvestorsListItemContact__c>();
        for (Contact cont : contactIdToContactMap.values())
        {
            if (!contIdToInvestorListItemContactMap.containsKey(cont.Id))
            {
                InvestorsListItemContact__c newInvestorListItemContact = new InvestorsListItemContact__c(
                    Contact__c = cont.Id,
                    InvestorsListItem__c = accountIdToInvestorListItemMap.get(cont.AccountId).Id
                );
                contIdToInvestorListItemContactMap.put(cont.Id, newInvestorListItemContact);
                investorsListItemContactToCreateList.add(newInvestorListItemContact);
            }
            else 
            {
                investorListViewWrap.existingInvestorsListItemsContact.add(contIdToInvestorListItemContactMap.get(cont.Id));
            }
        }

        // Create missing InvestorsListItemContact__c records
        if (!investorsListItemContactToCreateList.isEmpty())
        {
            insert investorsListItemContactToCreateList;

            // Collect data for LWC result view
            investorListViewWrap.newInvestorsListItemsContact = [SELECT Id, Name, InvestorsListItem__c, Contact__c, Contact__r.FirstName, Contact__r.LastName
                                                                 FROM InvestorsListItemContact__c 
                                                                 WHERE Id IN :investorsListItemContactToCreateList];
        }
        // database.rollback(sp);

        return JSON.serialize(investorListViewWrap);
    }

    public class InvestorListViewWrapper
    {
        public List<InvestorsListItem__c> existingInvestorsListItems { get; set; }
        public List<InvestorsListItem__c> newInvestorsListItems { get; set; }
        public List<InvestorsListItemContact__c> existingInvestorsListItemsContact { get; set; }
        public List<InvestorsListItemContact__c> newInvestorsListItemsContact { get; set; }
        public String investorListName { get; set; }
        public Id investorListId { get; set; }

        public InvestorListViewWrapper()
        {
            this.existingInvestorsListItems = new List<InvestorsListItem__c>();
            this.newInvestorsListItems = new List<InvestorsListItem__c>();
            this.existingInvestorsListItemsContact = new List<InvestorsListItemContact__c>();
            this.newInvestorsListItemsContact = new List<InvestorsListItemContact__c>();
        }
    }
}