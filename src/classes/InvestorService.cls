/**
* Investor (AccountToOpportunity__c) SObject service class
*
* @author: <Maksim Fedorenko> (maksim.fedorenko@parx.com)
*
* @history:
* version		    | author				                                | changes
* ====================================================================================
* 0.1 14.02.2020	| Maksim Fedorenko (maksim.fedorenko@parx.com)          | initial version.
*/

public inherited sharing class InvestorService
{
    /**
     * Returns a map of accountId => AccountToOpportunity__c records for a given account list
     * @param accountList List of Account records for search
     * @param opportunityId Id of the opportunity related to Investor object
     * @return Map of account Id => AccountToOpportunity__c records
     */
    public static Map<Id, AccountToOpportunity__c> getAccountIdInvestorMap(List<Account> accountList, Id opportunityId)
    {
        Map<Id, AccountToOpportunity__c> accountIdInvestorMap = new Map<Id, AccountToOpportunity__c>();

        List<AccountToOpportunity__c> investors = [
                SELECT Account__c, Account__r.Name, Account__r.BillingStreet, Account__r.BillingCity, Account__r.Brand__c,
                        Account__r.Brand__r.Name, (SELECT Id FROM InvestorContacts__r)
                FROM AccountToOpportunity__c
                WHERE Account__c IN :accountList AND Opportunity__c = :opportunityId
        ];

        for (AccountToOpportunity__c investor : investors)
        {
            accountIdInvestorMap.put(investor.Account__c, investor);
        }

        return accountIdInvestorMap;
    }
}