/**
* Testclass for MailingCreateTask.
*
* @author: <Christian Schwabe> (Christian.Schwabe@colliers.com)
*
* @history:
* version		    | author				                                | changes
* ====================================================================================
* 0.1 13.02.2020	| Christian Schwabe (Christian.Schwabe@colliers.com)    | [SFINV/US 2407] initial version.
*/
@isTest
private class MailingCreateTaskTest {
    @testSetup
    static void setup()
    {
        TestDataFactory.initializeProducts();

        Embargo__c embargo = new Embargo__c();
        embargo.Name = 'Embargo';
        embargo.Countries__c = 'Egypt;Afghanistan;Belarus;Burundi;Iraq;Yemen';
        insert embargo;
    }

    @isTest
    public static void testCreateTask()
    {

        Portfolio__c portfolio = new Portfolio__c();
        portfolio.Name = 'Test Portfolio';
        insert portfolio;

        Id investmentInternalRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('InvestmentInternal').getRecordTypeId();
        Opportunity opportunity = new Opportunity();
        opportunity.Name = 'Test Opportunity';
        opportunity.StageName = '10%';
        opportunity.CloseDate = System.today() + 21;
        opportunity.PortfolioLookup__c = portfolio.Id;
        opportunity.RecordTypeId = investmentInternalRecordTypeId;
        insert opportunity;

        Task task = new Task();
        task.WhatId = opportunity.Id;
        task.Subject = 'Subject';
        task.ActivityDate = System.today();
        task.Description = 'Description';
        task.OwnerId = UserInfo.getUserId();

        Id taskId = null;
        Test.startTest();
            taskId = MailingCreateTask.create(task);
        Test.stopTest();

        System.assert(String.isNotBlank(taskId), 'taskId is missing. Insertion does not work correctly.');
    }
}