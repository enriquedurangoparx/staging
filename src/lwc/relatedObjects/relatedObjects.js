import { LightningElement, api, track, wire } from 'lwc';
import { getObjectInfo, getPicklistValues  } from 'lightning/uiObjectInfoApi';
import PROPERTY_OBJECT from '@salesforce/schema/PropertyObject__c';
import AREA_OBJECT from '@salesforce/schema/Area__c';
import PORTFOLIO_OBJECT from '@salesforce/schema/Portfolio__c';
import STATUS_FIELD from '@salesforce/schema/PropertyObject__c.Status__c';
import { getRecord } from 'lightning/uiRecordApi';
import { registerListener, unregisterAllListeners, fireEvent } from 'c/pubsub';


import searchRelatedObjectsBySosl from '@salesforce/apex/LWCObjectHelper.searchRelatedObjectsBySosl';
import getInitialState from '@salesforce/apex/LWCObjectHelper.getInitialState';

import addToArea from '@salesforce/label/c.AddToArea';
import addToPortfolio from '@salesforce/label/c.PortfolioAddToPortfolio';
import areaSearchPlaceholder from '@salesforce/label/c.ArealSearch';
import portfolioSearchPlaceholder from '@salesforce/label/c.PortfolioSearch';

import portfolioAreaRelatedObjectName from '@salesforce/label/c.PortfolioAreaRelatedObjectName';
import portfolioAreaRelatedObjectStreet from '@salesforce/label/c.PortfolioAreaRelatedObjectStreet';
import portfolioAreaRelatedObjectPostalcode from '@salesforce/label/c.PortfolioAreaRelatedObjectPostalcode';
import portfolioAreaRelatedObjectCity from '@salesforce/label/c.PortfolioAreaRelatedObjectCity';
import portfolioAreaRelatedObjectStatus from '@salesforce/label/c.PortfolioAreaRelatedObjectStatus';
import portfolioAreaRelatedObjectPortfolio from '@salesforce/label/c.PortfolioAreaRelatedObjectPortfolio';
import portfolioAreaRelatedObjectArea from '@salesforce/label/c.PortfolioAreaRelatedObjectArea';
import searchDescription from '@salesforce/label/c.InvestorsSelectionSearchDescription';
import { subscribe, unsubscribe, onError, setDebugFlag, isEmpEnabled } from 'lightning/empApi';


export default class RelatedObjects extends LightningElement
{
    @api recordId;
    @api objectApiName;
    @track isLoading = false;
    @track isDataReady = false;

    @wire(getRecord, { recordId: '$recordId', fields: '$currentRecordFields' })
    currentRecord;

    @wire(getObjectInfo, { objectApiName: AREA_OBJECT })
    areaObjectInfo;

    @wire(getObjectInfo, { objectApiName: PORTFOLIO_OBJECT })
    portfolioObjectInfo;
    
    @track objectInfo;

    data = [];
    @track tableData = [];
    @track searchTerm = '';
    @track statusPicklist;
    @track selectedRows = [];

    @track columns = [];

    areaColumns = [
        {
            label: '', 
            type: 'checkboxContainer',
            fieldName: 'Id', 
            fixedWidth: 36,
            typeAttributes: {
                disabledandchecked: {fieldName : 'disabledandchecked'}
            }, 
            sortable: true,
        },
        { label: portfolioAreaRelatedObjectName, type: 'url', fieldName: 'link', typeAttributes: { label: {fieldName: 'Name'}, target:'_blank'}},
        { label: portfolioAreaRelatedObjectStreet, fieldName: 'Street__c' },
        { label: portfolioAreaRelatedObjectPostalcode, fieldName: 'PostalCode__c' },
        { label: portfolioAreaRelatedObjectCity, fieldName: 'City__c' },
        { label: portfolioAreaRelatedObjectStatus, fieldName: 'Status__c' },
        {
            label: portfolioAreaRelatedObjectArea, 
            fieldName: 'relateToLink',  
            type: 'url', 
            typeAttributes: { label: {fieldName: 'relateToName'}, target:'_blank'}, 
            sortable: true,
        },
        {
            label: '',
            type: 'deleteRelationRow',
            fieldName: 'Id',
            fixedWidth: 36,
            typeAttributes: {
                relateToId: { fieldName: 'relateToId' }
            },
            // cellAttributes: { class: { fieldName: 'bgClass' }}
        },
    ];

    portfolioColumns = [
        {
            label: '', 
            type: 'checkboxContainer',
            fieldName: 'Id', 
            fixedWidth: 36,
            typeAttributes: {
                disabledandchecked: {fieldName : 'disabledandchecked'}
            }, 
        },
        { label: portfolioAreaRelatedObjectName, type: 'url', fieldName: 'link', typeAttributes: { label: {fieldName: 'Name'}, target:'_blank'}},
        { label: portfolioAreaRelatedObjectStreet, fieldName: 'Street__c' },
        { label: portfolioAreaRelatedObjectPostalcode, fieldName: 'PostalCode__c' },
        { label: portfolioAreaRelatedObjectCity, fieldName: 'City__c' },
        { label: portfolioAreaRelatedObjectStatus, fieldName: 'Status__c' },
        {
            label: portfolioAreaRelatedObjectPortfolio, 
            fieldName: 'relateToLink',  
            type: 'url', 
            typeAttributes: { label: {fieldName: 'relateToName'}, target:'_blank'}, 
            sortable: true,
        },
        {
            label: '',
            type: 'deleteRelationRow',
            fieldName: 'Id',
            fixedWidth: 36,
            typeAttributes: {
                relateToId: { fieldName: 'relateToId' }
            },
            // cellAttributes: { class: { fieldName: 'bgClass' }}
        },
    ];

    portfolioApiName = 'Portfolio__c';
    areaApiName = 'Area__c';

    label = {
        addToArea,
        addToPortfolio,
        areaSearchPlaceholder,
        portfolioSearchPlaceholder,
        searchDescription
    };

    dataMap;
    selectedRows = new Map();
    @track isDisabled = true;

    get searchPlaceholderLabel()
    {
        return this.objectApiName === this.areaApiName ? this.label.areaSearchPlaceholder : this.label.portfolioSearchPlaceholder;
    }

    get addRelationLabel()
    {
        return this.objectApiName === this.areaApiName ? this.label.addToArea : this.label.addToPortfolio;
    }

    get currentRecordFields()
    {
        return this.objectApiName === this.areaApiName ? ['Area__c.Name'] : ['Portfolio__c.Name'];
    }

    get currentRecordName() {
        return this.currentRecord.data.fields.Name.value;
    }

    get currentObjLabel()
    {
        return this.objectApiName === this.portfolioApiName ? this.portfolioObjectInfo.data.label : this.areaObjectInfo.data.label;
    }

    get renderComponent()
    {
        return this.objectInfo && this.statusPicklist ? true : false;
    }
    
    @wire(getObjectInfo, { objectApiName: PROPERTY_OBJECT })
    wiredObjectInfo({ error, data }) {
        if (data) {
            this.objectInfo = data;
            this.error = undefined;
        } else if (error) {
            this.error = error;
        }
    }

    @wire(getPicklistValues, { recordTypeId: '0120O000000dAY1QAM', fieldApiName: STATUS_FIELD })
    wiredPicklistValues({ error, data }) {

        if(data) {
            console.log('wiredPicklistValues data', JSON.stringify(data));
            this.statusPicklist = data;
            this.setDatableColumns();
            this.error = undefined;
        } else if(error) {
            console.log('wiredPicklistValues error', JSON.stringify(error));
            this.error = error;
        }
    }
    
    queryFieldsAPIName = ['Name', 'Street__c', 'PostalCode__c', 'City__c', 'Status__c', 'Area__c', 'Area__r.Name', 'Portfolio__c', 'Portfolio__r.Name'];

   

    get filteredFieldAPIName()
    {
        return this.objectApiName === this.portfolioApiName ? 'Portfolio__c' : this.objectApiName === this.areaApiName ? 'Area__c' : '';
    } 

    subscription = {};

    connectedCallback()
    {
        let self = this;
		const messageCallback = function(response) {
			console.log('...New message received : ', JSON.stringify(response));
			self.reloadView();
		};
        subscribe('/data/PropertyObject__ChangeEvent', -1, messageCallback).then(response => {
            console.log('...Successfully subscribed to : ', JSON.stringify(response));
            this.propertySubscription = response;
				console.log('...handlePropertyObjectChangeEvent');
        });
        this.columns = this.objectApiName === this.portfolioApiName ? this.portfolioColumns : this.areaColumns;
        this.refreshInitialState();

    }

    callbackFunc(event) {
        console.log('...New message received : ');
    }

    handlePropertyObjectChangeEvent()
    {
        console.log('...handlePropertyObjectChangeEvent');
    }
    
    refreshInitialState()
    {
        console.log('...refreshInitialState: ');
        this.isLoading = true;
        this.isDisabled = true;

        let params = {
            queryFields: this.queryFieldsAPIName,
            filteredField: this.filteredFieldAPIName,
            recordId: this.recordId
        }
       
        getInitialState(params)
        .then(results => {
            this.data = results;
            this.isDataReady = true;
            if (this.searchTerm && this.searchTerm.length > 0)
            {
                this.template.querySelector('[data-id="searchinput"]').value = '';
                this.searchTerm = '';
            }
            this.prepareData();
            this.isLoading = false;
        })
    }

    /*
    * Set the columns for datatable, based on the object metadata to prevent custom labels. 
    */
    setDatableColumns(){
        console.log('setDatableColumns starts: ');
        if(this.objectInfo && this.objectInfo.fields){
            let tempColumns = [];

            this.columns.forEach(column => {
                try {
                    column.label = this.objectInfo.fields[column.fieldName].label;
                    column.label = column.type === 'checkboxContainer' || column.type === 'deleteRelationRow' ? '' : column.label;
                } catch (e) {
                    console.warn('Label for ', column.fieldName, 'not found', e);
                } finally {
                    tempColumns.push(column);
                }
            });
            this.columns = tempColumns;
            console.log('setDatableColumns: ', JSON.stringify(tempColumns));
        }
        console.log('setDatableColumns ends: ');
    }

    /**
     * This is triggered when the user hits a key and is filtered by enter key.
     */
    keyUpHandler(event){
        this.searchTerm = this.template.querySelector('lightning-input').value;
        const isEnterKey = event.keyCode === 13;

        if (isEnterKey) {
            this.loadData();
        }
    }

    rowSelectionHandler(event){
        console.log('rowSelectionHandler');
        if (event.detail.checked)
        {
            console.log('event.detail.rowId: ', event.detail.rowId);
            console.log('this.dataMap.get(event.detail.rowId): ', this.dataMap.get(event.detail.rowId));
            this.selectedRows.set(event.detail.rowId, this.dataMap.get(event.detail.rowId));
        }
        else
        {
            this.selectedRows.delete(event.detail.rowId);
        }

        // const selectedRows = event.detail.rowId;
        // this.selectedRows = [];

        // selectedRows.forEach(row => {
        //     this.selectedRows.push(row.Id);
        // })
        this.isDisabled = this.selectedRows && this.selectedRows.size > 0 ? false : true;
    }

    /*
    * Load records, based on the searchTermn with a sosl.
    */
    loadData(){
        console.log('...loadData:');
        this.isLoading = true;
        searchRelatedObjectsBySosl({searchTerm: this.searchTerm})
            .then(result => {
                this.data = JSON.parse(JSON.stringify(result[0]));
                console.log('load data JSON.parse(JSON.stringify(result[0]): ' + JSON.parse(JSON.stringify(result[0])));

                this.prepareData();
                this.isLoading = false;
            })
            .catch(error => {
                this.error = JSON.stringify(error);
                this.isLoading = false;
            });
    }

    /**
     * Prepare links for datatable
     * Handle translation for picklist value.
     */
    prepareData(){
        let tempData = [];
        // Convert picklist values
        let data = JSON.parse(JSON.stringify(this.data));
        let tempDataMap = new Map();
        data.forEach(record => {

            if(typeof record.Status__c !== undefined && this.statusPicklist){
                const picklistApiName = record.Status__c;

                this.statusPicklist.values.forEach(picklist => {
                    if(picklistApiName === picklist.value){
                        record.Status__c = picklist.label;
                    }
                })
            }

            record.disabledandchecked = false;
            if (this.objectApiName === this.areaApiName && record.Area__c)
            {
                record.relateToLink = '/' + record.Area__c;
                record.relateToName = record.Area__r.Name;
                record.relateToId = record.Area__c;
                record.disabledandchecked = true;
            }
            else if (this.objectApiName === this.portfolioApiName && record.Portfolio__c)
            {
                record.relateToLink = '/' + record.Portfolio__c;
                record.relateToName = record.Portfolio__r.Name;
                record.relateToId = record.Portfolio__c;
                record.disabledandchecked = true;
            }
            record.link = '/' + record.Id;
            
            tempDataMap.set(record.Id, record);
            tempData.push(record);
        });

        this.dataMap = tempDataMap;
        this.tableData = tempData;
        console.log('tempDataMap: ', tempDataMap);
        console.log('this.tableData: ', JSON.stringify(this.tableData));
        this.isDataReady = true;
    }

    /**
    * This is triggered when the value in the input field changes
    */
   handleInputChange(event)
   {
        if (event.target.value === '')
        {
            this.refreshInitialState();
        }
    }

    handleDeleteRelation(event)
    {
        const child = this.template.querySelector('c-related-objects-confirm-modal');
        let tempData = this.dataMap.get(event.detail.objId);
        child.openModal({
            'isDeleteMode' : true,
            'data' : tempData,
            'recordToDeleteRelation' : event.detail.objId,
            'currentRecordName' : this.currentRecordName,
            'objectApiName' : this.objectApiName,
            'objectLabel' : this.currentObjLabel
        });
    }

    openModalWindow()
    {
        const child = this.template.querySelector('c-related-objects-confirm-modal');
        console.log('openModalWindow: ', JSON.stringify(child));
        console.log('this.selectedRows: ', this.selectedRows);
        console.log('recordId: ', JSON.stringify(this.recordId));
        console.log('this.currentRecordName: ', JSON.stringify(this.currentRecordName));
        console.log('this.objectApiName: ', JSON.stringify(this.objectApiName));
        console.log('this.currentObjLabel: ', JSON.stringify(this.currentObjLabel));

        child.openModal({
            'isDeleteMode' : false,
            'data' : this.selectedRows,
            'recordId' : this.recordId,
            'currentRecordName' : this.currentRecordName,
            'objectApiName' : this.objectApiName,
            'objectLabel' : this.currentObjLabel
        });
    }

    handleRecordsUpdate(event)
    {
        console.log('...handleRecordsUpdate');
        if (event.detail.isDeleteMode)
        {
            this.reloadView();
        }
        else
        {
            this.selectedRows = new Map();
            this.refreshInitialState();
        }
    }

    handleSortData(event) {
        // calling sortdata function to sort the data based on direction and selected field
        console.log('=== handleSortData starts ===');
        console.log(event.detail.fieldName);
        console.log(event.detail.sortDirection);
        this.sortData(event.detail.fieldName, event.detail.sortDirection);
        console.log('=== handleSortData ends ===');
    }

    /**
     *
     * @param {*} fieldname Name of the field to be sorted
     * @param {*} direction "asc" or "desc"
     *
     * @description function to sort the array
     */
    sortData(fieldname, direction) {
        // field name
        this.sortBy = fieldname;

        // sort direction
        this.sortDirection = direction;
        // we need to remap fieldname because of that columns data is a hyperlink
        if (fieldname === 'accountLink') {
            fieldname = 'Name';
        }
        //window.console.log("sortData", fieldname, direction);

        // serialize the data before calling sort function
        let parseData = JSON.parse(JSON.stringify(this.tableData));

        // Return the value stored in the field
        let keyValue = (a, b) => {
            // add a prefix depending on Sort Direction to keep pinned records on top
            if (a && a.Id && a.Id > -1) {
                return b === 1
                    ? '0000000000' + a[fieldname]
                    : 'XXXXXXXXXX' + a[fieldname];
            }
            return a[fieldname];
        };

        // cheking reverse direction
        let isReverse = direction === 'asc' ? 1 : -1;

        // sorting data
        parseData.sort((x, y) => {
            x = keyValue(x, isReverse) ? keyValue(x, isReverse) : ''; // handling null values
            y = keyValue(y, isReverse) ? keyValue(y, isReverse) : '';

            // sorting values based on direction
            return isReverse * ((x > y) - (y > x));
        });

        // set the sorted data to data table data
        this.tableData = parseData;
    }

	disconnectedCallback() {
		unsubscribe(this.propertySubscription, response => {
			console.log('unsubscribe() response: ', JSON.stringify(response));
			// Response is true for successful unsubscribe
		});
	}

	reloadView()
	{
		let searchInputValue = this.template.querySelector('[data-id="searchinput"]').value;
		if (searchInputValue === '')
		{
			this.refreshInitialState();
		}
		else
		{
			this.loadData();
		}
 	}
}