import LightningDatatable from 'lightning/datatable';
import deleteButtonRow from './investorSelectionDeleteRow.html';
import actionButtonRow from "./investorsListActionButtonRow.html";

export default class investorSelectionSelectedContactsTable extends LightningDatatable {
    static customTypes = {
        deleteButtonRow: {
            template: deleteButtonRow
        },
        actionButtonRow: {
            template: actionButtonRow,
            typeAttributes: ['rowStatus']
        },
    };
}