import { LightningElement, wire, api } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { getRecord, getFieldValue, getRecordUi, generateRecordInputForUpdate ,updateRecord } from 'lightning/uiRecordApi';

import LANG from '@salesforce/i18n/lang';

import Id from '@salesforce/user/Id';
import USER_ALIAS_FIELD from '@salesforce/schema/User.Alias';

// Import custom labels
import saveGenericComment from '@salesforce/label/c.SaveGenericComment';
import cancelGenericComment from '@salesforce/label/c.CancelGenericComment';
import genericCommentTooManyCharacters from '@salesforce/label/c.GenericCommentTooManyCharacters';
import genericCommentError from '@salesforce/label/c.GenericCommentError';
import errorTitle from '@salesforce/label/c.ErrorTitleGenericComment';

const BLANK_SPACE = ' ';
const LINE_BREAK = '\n';

export default class GenericComment extends LightningElement {
    @api recordId;
    @api objectApiName;
    userId = Id;
    userLanguage = LANG;// Determines the user's userLanguage

    // Text formatter.
    blankspace = BLANK_SPACE;
    lineBreak = LINE_BREAK;

    oldTextAreaFieldValue;// Hold old value for textArea.
    isChangedTheFirstTime = false;// Hold the information if textarea is changed after loading.

    // Following variables are set by lightning app builder.
    @api fieldApiName;

    @api englishFieldLabel;
    @api germanFieldLabel;
    @api englishPlaceholderText;
    @api germanPlaceholderText;

    //@api englishSuccessToastMessageTitle;
    //@api germanSuccessToastMessageTitle;
    @api englishSuccessToastMessage;
    @api germanSuccessToastMessage;

    customLabel = {
        save: saveGenericComment,
        cancel: cancelGenericComment,
        tooManyCharacters: genericCommentTooManyCharacters,
        error: genericCommentError,
        errorTitle: errorTitle
    }

    // https://developer.salesforce.com/docs/component-library/documentation/lwc/lwc.reference_generate_record_input_update
    @wire(getRecordUi, {
        recordIds: '$recordId',
        layoutTypes: 'Full',
        modes: 'Edit'
    })
    recordUi;

    @wire(getRecord, { recordId: '$userId', fields: [USER_ALIAS_FIELD] })
    user;

    _save(){
        const objectInfo = this.recordUi.data.objectInfos[this.objectApiName];
        const maximumLengthOfCommentField = objectInfo.fields[this.fieldApiName].length;

        const input = this.template.querySelector('[data-name=input]');
        const inputValue = (input.value ? input.value.trim() : '');

        const textAreaField = this.template.querySelector('lightning-input-field');
        const textAreaFieldValue = (textAreaField.value ? textAreaField.value : '');//Initialize textAreaFieldValue if no text is available - in case if it is the first comment.

        let newGenericComment = '';
        //New comment typed.
        if(inputValue.length > 0){
            const today = new Date();
            const date = today.getDate() + '.' + (today.getMonth()+1) + '.' + today.getFullYear();
            let newComment = date + this.blankspace + this.getAlias() + this.blankspace + inputValue;

            newGenericComment += newComment + this.lineBreak + textAreaFieldValue;
        } else {
            newGenericComment = textAreaFieldValue;
        }      
        
        const currentLengthOfCommentField = newGenericComment.length;
        if (currentLengthOfCommentField <= maximumLengthOfCommentField) {
            let recordInput = this.getRecordInputForUpdate();
            recordInput.fields[this.fieldApiName] = newGenericComment;

            updateRecord(recordInput)
                .then(() => {
                    this.template.querySelector('[data-name=input]').value = '';// Reset first input.

                    this.showSuccessMessage();

                    // Display fresh data in the form
                    this.template.querySelector('lightning-input-field').value = newGenericComment;
                    // Update oldTextAreaFieldValue to cover the case that the cancel button is clicked after successfully saving.
                    this.oldTextAreaFieldValue = newGenericComment;
                })
                .catch(error => {
                    this.dispatchEvent(
                        new ShowToastEvent({
                            title: this.customLabel.errorTitle,
                            message: error.body.message,
                            variant: 'error',
                            mode: 'sticky'
                        })
                    );
                });
        }
        else {
            let errorMessage = this.customLabel.tooManyCharacters;
            errorMessage = errorMessage.replace('{!currentLengthOfCommentField}', currentLengthOfCommentField);
            errorMessage = errorMessage.replace('{!maximumLengthOfCommentField}', maximumLengthOfCommentField);

            // Reached maximum length of field.
            this.dispatchEvent(
                new ShowToastEvent({
                    title: this.customLabel.error,
                    message: errorMessage,
                    variant: 'error',
                    mode: 'sticky'
                })
            );
        }
    }

    /**
     * Is trigger by a click on the button "Save".
     */
    handleSave(){
        this._save();
    }

    // https://developer.salesforce.com/docs/component-library/documentation/lwc/lwc.reference_generate_record_input_update
    getRecordInputForUpdate(){
        const objectInfo = this.recordUi.data.objectInfos[this.objectApiName];

        const record = this.recordUi.data.records[this.recordId];
        const recordInput = generateRecordInputForUpdate(
            record,
            objectInfo
        );

        return recordInput;
    }

    handleCancel(){
        const recordInput = this.getRecordInputForUpdate();
        this.oldTextAreaFieldValue = recordInput.fields[this.fieldApiName];

        // Reset first input
        this.template.querySelector('[data-name=input]').value = '';
        this.template.querySelector('lightning-input-field').value = this.oldTextAreaFieldValue;
    }

    /**
     * Is triggered if something changed in the textarea.
     * Used to save the origin value of the textarea to have the possibility to reset this it to the initial value.
     */
    textAreaChangeHandler(){
        if(this.isChangedTheFirstTime === false){
            const recordInput = this.getRecordInputForUpdate();

            this.oldTextAreaFieldValue = recordInput.fields[this.fieldApiName];
            this.isChangedTheFirstTime = true;
        }
    }

    /**
     * This is triggered when the user hits a key and is filtered by enter key.
     */
    handleKeyUp(event){
        const isEnterKey = event.keyCode === 13;
        
        if (isEnterKey) {
            this._save();
        }
    }

    getAlias() {
        return getFieldValue(this.user.data, USER_ALIAS_FIELD);
    }

    showSuccessMessage(){
        //let title = (this._isGermanUserLanguage() ? this.germanSuccessToastMessageTitle : this.englishSuccessToastMessageTitle);
        let title = (this._isGermanUserLanguage() ? this.germanSuccessToastMessage : this.englishSuccessToastMessage);

        this.dispatchEvent(
            new ShowToastEvent({
                title: title,
                //message: message,
                variant: 'success'
            })
        );
    }

    _isGermanUserLanguage(){
        return (this.userLanguage === 'de' ? true : false);
    }

    get label(){
        let label = (this._isGermanUserLanguage() ? this.germanFieldLabel : this.englishFieldLabel);

        return label;
    }

    get placeholder(){
        let placeholder = (this._isGermanUserLanguage() ? this.germanPlaceholderText : this.englishPlaceholderText);

        return placeholder;
    }
}