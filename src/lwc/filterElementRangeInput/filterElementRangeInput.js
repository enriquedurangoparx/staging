import { LightningElement, api, track, wire } from 'lwc';
import rangeFrom from '@salesforce/label/c.rangeFrom';
import rangeTo from '@salesforce/label/c.rangeTo';
import { CurrentPageReference } from 'lightning/navigation';
import { fireEvent } from 'c/pubsub';

export default class FilterElementRangeInput extends LightningElement {
    @api sectionLabel;
    @api staticLabel;
    @api min;
    @api max;
    @track elementDisabled = false;
    @wire(CurrentPageReference) pageRef;
    hasRendered = false;
    currentMinValue;
    currentMaxValue;
    timeoutId;

    label = {
        "rangeFrom": rangeFrom,
        "rangeTo": rangeTo
    };

    get isSectionEnabled() {
        return Boolean(this.min && this.max);
    }

    renderedCallback() {
        if (!this.hasRendered) {
            this.hasRendered = true;
            this.elementDisabled = !this.isSectionEnabled;

            if (this.isSectionEnabled) {
                this.currentMinValue = this.min;
                this.currentMaxValue = this.max;
            }

            // remove lightning-input spinners
            const style = document.createElement('style');
            style.innerText = 'input::-webkit-outer-spin-button {display:none;}input::-webkit-inner-spin-button {display:none;}';
            this.template.querySelector('lightning-input').appendChild(style);
        }
    }

    handleCheckboxStateChange(event) {
        this.elementDisabled = !event.target.checked;

        this.fireValueChanged();
    }

    handleValueChange(event) {
        this.currentMinValue = event.target.name === 'inputMin' ? event.target.value : this.currentMinValue;
        this.currentMaxValue = event.target.name === 'inputMax' ? event.target.value : this.currentMaxValue;

        clearTimeout(this.timeoutId);
        this.timeoutId = setTimeout(() => {
            this.fireValueChanged();
        }, 500);
    }

    fireValueChanged() {
        fireEvent(this.pageRef, 'filterValueChanged', {
            label: this.staticLabel,
            isActive: !this.elementDisabled,
            values: '',
            minValue: this.currentMinValue,
            maxValue: this.currentMaxValue
        });
    }
}