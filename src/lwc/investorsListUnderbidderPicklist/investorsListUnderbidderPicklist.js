import { LightningElement, api, wire, track } from 'lwc';
import { CurrentPageReference } from 'lightning/navigation';
import { registerListener, unregisterAllListeners, fireEvent } from 'c/pubsub';
import { showToast, updateSObject } from 'c/utilsSObject';

import errorLabel from '@salesforce/label/c.GeneralLabelError';
import underbidderMarkingErrorLabel from '@salesforce/label/c.InvestorsListUnableMarkUnderbidder';
import updateSuccessLabel from '@salesforce/label/c.GeneralLabelSuccessUpdateMessage';


export default class InvestorsListUnderbidderPicklist extends LightningElement {
    @api record;
    @api underbidderValues;
    @track _underbidderValues;
    @track emptyValueSelected = false;
    @track rendered = true;
    noLabelTranslated = '';

    @wire(CurrentPageReference) pageRef;

    connectedCallback() {
        registerListener('investorsListUnderbiddersModal_recordsUpdated', this.updateUnderbidders, this);
        
        this.emptyValueSelected = !!this.record.Underbidder__c;

        let result = [];
        for (const underbidderValue of this.underbidderValues) {
            let obj = {... underbidderValue};
            obj.isSelected = obj.label === this.record.Underbidder__c;
            result.push(obj);
        }
        this._underbidderValues = result;
        this.noLabelTranslated = this.underbidderValues.find((row) => {
            return row.value === 'No';
        }).label;
    }

    disconnectedCallback() {
        unregisterAllListeners(this);
    }

    handleStatusChange(event) {
        const value = event.target.value;

        if (this.record.LastBidAmount__c && this.record.LastBidDate__c || value === this.noLabelTranslated || value === '') {
            let record = JSON.parse(JSON.stringify(this.record));
            record.Underbidder__c = event.target.value;
            updateSObject(record, updateSuccessLabel, () => {
                // record updated callback function
                let _underbidderValue = this._underbidderValues.find((row) => {
                    return row.label === record.Underbidder__c || row.value === record.Underbidder__c;
                });
                if (_underbidderValue) {
                    _underbidderValue.isSelected = true;
                }
                fireEvent(this.pageRef, 'investorsList_underbidderUpdated', record);
            });
        } else {
            for (let i = 0; i < this._underbidderValues.length; i++) {
                if (this._underbidderValues[i].value === 'Yes') {
                    this._underbidderValues[i].isSelected = false;
                }
            }

            this.rerender();
            showToast(errorLabel, underbidderMarkingErrorLabel, 'error');
        }
    }

    updateUnderbidders(records) {
        const recordIdList = records.underbidders.map((row) => { return row.Id });
        if (recordIdList.includes(this.record.Id)) {
            const currentRecord = records.underbidders.find(row => { return row['Id'] === this.record.Id });
            for (let i = 0; i < this._underbidderValues.length; i++) {
                this._underbidderValues[i].isSelected = currentRecord.Underbidder__c === this._underbidderValues[i].value;
            }
        }

        this.rerender();
    }

    rerender() {
        this.rendered = false;
        setTimeout(() => {
            this.rendered = true;
        }, 10);
    }
}