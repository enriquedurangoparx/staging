import { LightningElement, track, api, wire } from 'lwc';
import { fireEvent } from 'c/pubsub';
import { getRecord, getFieldValue } from 'lightning/uiRecordApi';
import Id from '@salesforce/user/Id';
import { CurrentPageReference } from 'lightning/navigation';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { deleteRecord } from 'lightning/uiRecordApi';
import { NavigationMixin } from 'lightning/navigation';
import cancel from '@salesforce/label/c.InvestorsListCancelDeleteContact';
import deleteButton from '@salesforce/label/c.InvestorsListDeleteContact';
import headline from '@salesforce/label/c.FileListAllRecordsRowActionsHeadline';
import confirmation from '@salesforce/label/c.FileListAllRecordsRowActionsConfirmation';
import OWNER_FIELD from '@salesforce/schema/ContentDocument.OwnerId';

const fields = [OWNER_FIELD];

export default class FileListAllRecordsRowActions extends NavigationMixin(LightningElement) {
    @wire(CurrentPageReference) pageRef;
    @wire(getRecord, { recordId: '$fileId', fields })
    fileRecord;

    @api fileId;
    @api action;
    @track showConfirmDelete = false;

    label = {
        cancel,
        deleteButton,
        headline,
        confirmation
    }

    get fileOwner() {
        return getFieldValue(this.fileRecord.data, OWNER_FIELD);
    }

    get details() {
        return this.action === 'details';
    }

    get deleteFile() {
        return this.fileOwner == Id && this.action === 'deleteFile';
    }

    handleShowDetails(){
        this[NavigationMixin.Navigate]({
            type: 'standard__namedPage',
            attributes: {
                pageName: 'filePreview'
            },
            state : {
                selectedRecordId : this.fileId
            }
          });
    }

    handleDeleteFile(){
        this.showConfirmDelete = true;
    }

    onDeleteConfirm() {
        deleteRecord(this.fileId)
        .then(() => {
            fireEvent(this.pageRef, 'FileListAllRecords_refreshList', this);
        })
        .catch(error => {
            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'Error Label',
                    message: error.body.message,
                    variant: 'error'
                })
            );
        })
        .finally(() => {
            this.closeModal();
        });

    }

    closeModal() {
        this.showConfirmDelete = false;
    }
}