import { LightningElement, api } from 'lwc';

export default class addSelectedItemsToMyListResults extends LightningElement 
{
    @api item;
    @api objectApiName;

    get junctionName()
    {
        return this.item && this.item.Name ? this.item.Name : '';
    }

    get junctionLink()
    {
        return this.item && this.item.Id ? '/' + this.item.Id : '';
    }

    get itemName()
    {
        if (this.objectApiName === 'Account')
        {
            return this.item && this.item.Account__c ? this.item.Account__r.Name : '';
        }
        else if (this.objectApiName === 'Contact')
        {
            return this.item && this.item.Contact__c ? this.item.Contact__r.FirstName ? this.item.Contact__r.FirstName + ' ' + this.item.Contact__r.LastName : this.item.Contact__r.LastName : '';
        }
        return '';
    }

    get itemLink()
    {
        if (this.objectApiName === 'Account')
        {
            return this.item && this.item.Account__c ? '/' + this.item.Account__c : '';
        }
        else if (this.objectApiName === 'Contact')
        {
            return this.item && this.item.Contact__c ? '/' + this.item.Contact__c : '';
        }
        return '';
    }
}