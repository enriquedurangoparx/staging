import { LightningElement, api, track, wire } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { updateRecord } from 'lightning/uiRecordApi';

//import { getRecord } from 'lightning/uiRecordApi';
import ID_FIELD from '@salesforce/schema/Opportunity.Id';
import OFFICE_AREA_FIELD from '@salesforce/schema/Opportunity.OfficeArea__c';
import OFFICE_RENT_PER_SQUARE_METER_FIELD from '@salesforce/schema/Opportunity.OfficeRentPerSquareMeter__c';
import OFFICE_MARKET_PER_SQUARE_METER from '@salesforce/schema/Opportunity.OfficeMarketPerSquareMeter__c';

import {// https://developer.salesforce.com/docs/component-library/documentation/lwc/lwc.reference_generate_record_input_update
    getRecordUi,
    generateRecordInputForUpdate
} from 'lightning/uiRecordApi';
import OPPORTUNITY_OBJECT from '@salesforce/schema/Opportunity';

const DECIMAL_PLACES = 2;
const CONVERSION_FACTOR = 1.13;

export default class CmCalculation extends LightningElement {
    @api recordId;
    
    opportunity = null;
    monthsInAYear = 12;
    minimumFractionDigits = 2;

    //Calculation field
    @track calculationOverUnderrent = 0;
    @track calculationERV = 0;
    @track calculationMultiplier = 0;
    @track calculationPurchasePrice = 0;
    @track calculationPerSquareMeter = 0;

    //Yield fields
    @track yieldGrossYield = 0;
    @track yieldNetYield = 0;
    @track yieldGrossMultiplier = 0;
    @track yieldNetMultiplier = 0;

    //Office fields
    @track officeArea = 0;
    @track officeRentPerSquareMeter = 0;
    @track officeRentPerAnno = 0;
    @track officeMarketPerSquareMeter = 0;
    @track officeMarketPerAnno = 0;

    //Aggregated sum for office
    @track sumArea = 0;
    @track sumRentPerSquareMeter = 0;
    @track sumRentPerAnno = 0;
    @track sumMarketPerSquareMeter = 0;
    @track sumMarketPerAnno = 0;

    @wire(getRecordUi, { //https://developer.salesforce.com/docs/component-library/documentation/lwc/lwc.reference_generate_record_input_update
        recordIds: '$recordId',
        layoutTypes: 'Full',
        modes: 'Edit'
    })
    wireRecordUi({error, data}){
        console.log('>>>wireRecordUi called.');

        if (data) {
            //console.log('>>>data: ' + JSON.stringify(data, null, '\t'));
            this.officeArea = data.records[this.recordId].fields.OfficeArea__c.value;
            this.officeRentPerSquareMeter = data.records[this.recordId].fields.OfficeRentPerSquareMeter__c.value;
            this.officeMarketPerSquareMeter = data.records[this.recordId].fields.OfficeMarketPerSquareMeter__c.value;
            console.log('>>>this.opportunity: ' + JSON.stringify(this.opportunity, null, '\t'));

            const opportunityObjectInfo = data.objectInfos[
                OPPORTUNITY_OBJECT.objectApiName
            ];
            const record = data.records[this.recordId];
            this.opportunity = generateRecordInputForUpdate(
                record,
                opportunityObjectInfo
            );

            this.error = undefined;
        } else if (error) {
            console.error('>>>error: ' + JSON.stringify(error, null, '\t'));

            this.error = error;
            this.opportunity = undefined;
        }
    }
    recordUi;



    // Load opportunity record with specific field.
    /*@wire(getRecord, { recordId: '$recordId', fields: [OFFICE_AREA_FIELD, OFFICE_RENT_PER_SQUARE_METER_FIELD, OFFICE_RENT_PER_ANNO_FIELD] })
    wireRecord({error, data}){
        console.log('>>>wireRecord called.');

        if (data) {
            //console.log('>>>data: ' + JSON.stringify(data, null, '\t'));
            this.opportunity = data;
            this.officeArea = this.opportunity.fields.OfficeArea__c.value;

            this.error = undefined;
        } else if (error) {
            console.error('>>>error: ' + JSON.stringify(error, null, '\t'));

            this.error = error;
            this.opportunity = undefined;
        }
    }*/

    /*get officeArea(){
        console.log('>>>changeOfficeHandler called.');
        //const officeArea = this.opportunity.data.fields.OfficeArea__c.value;
        console.log('>>>this.opportunity: ' + this.opportunity);
        console.log('>>>JSON.stringify(this.opportunity): ' + this.opportunity, null, '\t');
        console.log('>>>this.officeArea: ' + this.officeArea);
        
        return '';//this.opportunity.data.fields.OfficeArea__c.value;// ? this.opportunity.data.fields.OfficeArea__c.value : '';
    }*/

    /**
     * Handle change events for row "office" .
     */
    changeOfficeHandler(event){
        console.log('>>>changeOfficeHandler called.');

        const field = event.target.name;
        const value = event.target.value;

        console.log('>>>value: ' + value);
        console.log('>>>field: ' + field);

        switch(field){
            case 'officeArea':
                this.officeArea = value;
                console.log('>>>this.officeArea: ' + this.officeArea);
                break;
            case 'officeRentPerSquareMeter':
                this.officeRentPerSquareMeter = value;
                console.log('>>>this.officeRentPerSquareMeter: ' + this.officeArea);
                break;
            case 'officeMarketPerSquareMeter':
                this.officeMarketPerSquareMeter = value;
                console.log('>>>this.officeMarketPerSquareMeter: ' + this.officeMarketPerSquareMeter);
                break;
            default:
        }

        this._reCalculateOffice();
        this._reCalculateSum();
    }

    /**
     * Handle change events for calculation fields.
     */
    changeCalculationMultiplierHandler(event){
        console.log('>>>changeCalculationMultiplierHandler called.');

        const field = event.target.name;
        const value = event.target.value;

        console.log('>>>value: ' + value);
        console.log('>>>field: ' + field);

        switch(field){
            case 'calculationMultiplier':
                this.calculationMultiplier = value;
                this._reCalculateOnChangeCalculationMultiplier();
                break;
            default:

        }
    }

    _reCalculateOnChangeCalculationMultiplier(){
        this.calculationOverUnderrent = (this.sumRentPerAnno / this.sumMarketPerAnno) - 1;

        /**
         * Hier muss noch die Berechnung implementiert werden.
         */
    }

    /**
     * Handle change events for yield fields.
     */
    changeYieldHandler(event){
        console.log('>>>changeYieldHandler called.');

        const field = event.target.name;
        const value = event.target.value;

        console.log('>>>value: ' + value);
        console.log('>>>field: ' + field);

        switch(field){
            case 'yieldGrossYield':
                this.yieldGrossYield = value;
                this._reCalculateOnChangeYieldGrossYield();
                break;
            case 'yieldNetYield':
                this.yieldNetYield = value;
                this._reCalculateOnChangeYieldNetYield();
                break;
            case 'yieldGrossMultiplier':
                this.yieldGrossMultiplier = value;
                this._reCalculateOnChangeYieldGrossMultiplier();
                break;
            case 'yieldNetMultiplier':
                this.yieldNetMultiplier = value;
                this._reCalculateOnChangeYieldNetMultiplier();
                break;
            default:
        }
    }

    _reCalculateOnChangeYieldGrossYield(){
        this.yieldNetYield = this._round(this.yieldGrossYield / CONVERSION_FACTOR);
        this.yieldGrossMultiplier = this._round(1 / this.yieldGrossYield);
        this.yieldNetMultiplier = this._round(1 / this.yieldNetYield);
    }

    _reCalculateOnChangeYieldNetYield(){
        this.yieldGrossYield = this._round(this.yieldNetYield / CONVERSION_FACTOR);
        this.yieldNetMultiplier = this._round(1 / this.yieldGrossYield);
        this.yieldNetMultiplier = this._round(1 / this.yieldNetYield);
    }

    _reCalculateOnChangeYieldGrossMultiplier(){
        this.yieldGrossYield = this._round(1 / this.yieldGrossMultiplier);
        this.yieldNetMultiplier = this._round(this.yieldGrossYield * CONVERSION_FACTOR);
        this.yieldNetYield = this._round(1 / this.yieldNetMultiplier);
    }

    _reCalculateOnChangeYieldNetMultiplier(){
        this.yieldGrossMultiplier = this._round(this.yieldNetMultiplier / CONVERSION_FACTOR);
        this.yieldGrossYield = this._round(1 / this.yieldGrossMultiplier);
        this.yieldNetYield = this._round(1 / this.yieldNetMultiplier);
    }

    /**
     * Recalculate office kpis.
     */
    _reCalculateOffice(){
        console.log('>>>_reCalculateOffice called.');
        const fixedOfficeRentPerAnno = this.officeArea * this.officeRentPerSquareMeter * this.monthsInAYear;
        const fixedOfficeMarketPerAnno = this.officeArea * this.officeMarketPerSquareMeter * this.monthsInAYear;
        
        this.officeRentPerAnno = this._toFixed(fixedOfficeRentPerAnno);
        this.officeMarketPerAnno = this._toFixed(fixedOfficeMarketPerAnno)
    }

    /**
     * Aggregate last row (sum).
     * This kpis are not stored in sfdc and are calculated on the fly.
     */
    _reCalculateSum(){
        console.log('>>>_reCalculateSum called.');

        this.sumArea = this.officeArea; //this._replacePointWithComma(this.officeArea) + ' m²';
        this.sumRentPerSquareMeter = this.officeRentPerSquareMeter;//this._replacePointWithComma(this.officeRentPerSquareMeter) + ' €/m²';
        this.sumRentPerAnno = this.officeRentPerAnno;//this._replacePointWithComma(this.officeRentPerAnno) + ' €';
        this.sumMarketPerSquareMeter = this.officeMarketPerSquareMeter;//this._replacePointWithComma(this.officeMarketPerSquareMeter) + ' €/m²';
        this.sumMarketPerAnno = this.officeMarketPerAnno; // this._replacePointWithComma(this.officeMarketPerAnno) + ' €';
    }



    //https://developer.salesforce.com/docs/component-library/documentation/lwc/reference_update_record
    handleSave(){
        console.log('>>>handleSave called.');

        // Check validation.
        /*const allValid = [...this.template.querySelectorAll('lightning-input')]
            .reduce((validSoFar, inputFields) => {
                inputFields.reportValidity();
                return validSoFar && inputFields.checkValidity();
            }, true);*/

            const allValid = true;// Only for test purpose!!!
            if (allValid) {
                // Create the recordInput object
                const fields = {};
                fields[ID_FIELD.fieldApiName] = this.recordId;
    
                //https://www.sfdcblogs.com/post/how-to-fetch-inputs-in-lightning-web-components
                const input = this.template.querySelectorAll("lightning-input");
                input.forEach(function(element){
                    if(element.name=="officeArea"){
                        fields[OFFICE_AREA_FIELD.fieldApiName] = element.value;
                    } else if(element.name=="officeRentPerSquareMeter"){
                        fields[OFFICE_RENT_PER_SQUARE_METER_FIELD.fieldApiName] = element.value;
                    } else if(element.name=="officeMarketPerSquareMeter"){
                        fields[OFFICE_MARKET_PER_SQUARE_METER.fieldApiName] = element.value;
                    }
                },this);

                const recordInput = { fields };
    
                updateRecord(recordInput)
                    .then(() => {
                        this.dispatchEvent(
                            new ShowToastEvent({
                                title: 'Success',
                                message: 'Calculation updated.',
                                variant: 'success'
                            })
                        );
                        // Display fresh data in the form
                        //return refreshApex(this.opportunity);
                    })
                    .catch(error => {
                        this.dispatchEvent(
                            new ShowToastEvent({
                                title: 'Error save calculation!',
                                message: error.body.message,
                                variant: 'error'
                            })
                        );
                    });
                } else {
                    // The form is not valid
                    this.dispatchEvent(
                        new ShowToastEvent({
                            title: 'Something is wrong!',
                            message: 'Check your input and try again.',
                            variant: 'error'
                        })
                    );
                }
    }

    /**
     * The toFixed() function in JavaScript is used to format a number using fixed-point notation.
     * It can be used to format a number with a specific number of digits to the right of the decimal.
     * 
     * This function accepts a single parameter value.
     * It signifies the number of digits to appear after the decimal point.
     * 
     * It returns a number in the string representation.
     * The number has the exact number of digits after the decimal place as mentioned in the toFixed() function.
     */
    _toFixed(number){
        return number.toFixed(DECIMAL_PLACES);
    }
    
    /**
     * The Math.round() method is used to round a number its nearest integer value.
     * It takes one parameter which is the number to be rounded.
     */
    _round(number){
        return Math.round(number * 100) / 100;
    }

    /**
     * https://stackoverflow.com/questions/5731193/how-do-i-format-numbers-using-javascript
     * In en-US, logs '100,000.00'
     * In de-DE, logs '100.000,00'
     * In hi-IN, logs '1,00,000.00'
     */
    /*_formatNumberToText(number){
        return number.toLocaleString(
            undefined, // leave undefined to use the browser's locale,
                       // or use a string like 'en-US' to override it.
            { minimumFractionDigits: this.minimumFractionDigits }
          );
    }*/

    _replacePointWithComma(number){
        return number.toString().replace('.', ',');
    }
    /**
     * The renderedCallback() is unique to Lightning Web Components. Use it to perform logic after a component has finished the rendering phase.
     * Trigger calculation with retrieved values. 
     */
    renderedCallback(){
        console.log('>>>renderedCallback called.');

        this._reCalculateOffice();
        this._reCalculateSum();

    }
}