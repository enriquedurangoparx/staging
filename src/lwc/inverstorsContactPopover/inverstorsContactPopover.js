import { LightningElement, api } from 'lwc';

export default class InverstorsContactPopover extends LightningElement {
    @api contact;
    @api top = 100;
    @api left = 100;
    @api popover = false;

    label = {
        close: 'Schließen',
        headline: 'Kontakt Details'
    };

    renderedCallback() {
        const popover = this.template.querySelector('.slds-box');

        if (popover) {
            popover.style.top = this.top + 'px';
            popover.style.left = this.left + 'px';
        }
    }

    closeModal() {
        this.dispatchEvent(new CustomEvent('closemodal'));
    }
}