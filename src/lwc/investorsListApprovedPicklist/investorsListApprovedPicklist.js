import { LightningElement, track, api, wire } from 'lwc';
import { registerListener, fireEvent } from 'c/pubsub';
import APPROVED_FIELD from '@salesforce/schema/AccountToOpportunity__c.Approved__c';
import { getPicklistValues } from 'lightning/uiObjectInfoApi';
import { CurrentPageReference } from 'lightning/navigation';

export default class InvestorsListApprovedPicklist extends LightningElement {
    @wire(CurrentPageReference) pageRef;
    @api rowId;
    @api approved;
    @track options;

    connectedCallback(){
        registerListener('investorsListApprovedPicklist_refresh', this.refreshSelectedValue, this);
    }

    @wire(getPicklistValues, { recordTypeId: '012000000000000AAA', fieldApiName: APPROVED_FIELD })
    setPicklistOptions({error, data}) {
        if (data) {
            this.options = [];
            for(let i = 0; i < data.values.length; i++ ){
                let option = {};
                option.label = data.values[i].label;
                option.value = data.values[i].value;

                if(option.value == this.approved){
                    option.selected = 'selected';
                }
                this.options.push(option);
            }
        } else{
            console.log('error' + error);
        }
    }

    handleApprovedChange(event){
        let data = {}
        data.id = this.rowId;
        data.approved = event.target.value;
        fireEvent(this.pageRef, 'investorsListApprovedPicklist_approvedChange', data);
    }

    refreshSelectedValue(data){
        for(var i = 0; i < data.length; i++){
            if(this.rowId === data[i].Id){
                for(var j = 0; j < this.options.length; j++){
                    // First reset
                    this.options[j].selected = '';

                    // Set the selected value
                    if(this.options[j].value == data[i].Approved__c){
                        this.options[j].selected = 'selected';
                    }
                }
            }
        }
    }
}