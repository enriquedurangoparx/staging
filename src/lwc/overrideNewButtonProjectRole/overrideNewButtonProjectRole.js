import { LightningElement, api } from 'lwc';

export default class OverrideNewButtonProjectRole extends LightningElement {
    @api recordId;

    objectApiName = 'ProjectRole__c';
    layoutType = "Full";
}