import LightningDatatable from 'lightning/datatable';
import pinButtonRow from './investorsListPinButtonRow.html';
import actionButtonRow from './investorsListActionButtonRow.html';
import contactListAction from './investorsContactListAction.html';
import statusPicklist from './investorsStatusPicklist.html';
import priorityPicklist from './investorsPriorityPicklist.html';
import approvedPicklist from './investorsApprovedPicklist.html';
import underbidderPicklist from './investorsUnderbidderPicklist.html';
import contactsList from './investorsListContacts.html';
import lastActivitiesList from './investorsListLastActivities.html';

import { wire } from 'lwc';
import { CurrentPageReference } from 'lightning/navigation';
import { fireEvent } from 'c/pubsub';

import lastBid from '@salesforce/label/c.InvestorsListLastBidLabel';
import lastBidAmount from '@salesforce/label/c.InvestorsListLastBidAmount';

export default class investorsListDataTable extends LightningDatatable {
    static customTypes = {
        pinButtonRow: {
            template: pinButtonRow,
            typeAttributes: ['isPinned']
        },
        actionButtonRow: {
            template: actionButtonRow,
            typeAttributes: ['rowStatus']
        },
        contactListAction: {
            template: contactListAction,
            typeAttributes: ['action']
        },
        priorityPicklist: {
            template: priorityPicklist,
            typeAttributes: ['priority']
        },
        statusPicklist: {
            template: statusPicklist,
            typeAttributes: ['status']
        },
        approvedPicklist: {
            template: approvedPicklist,
            typeAttributes: ['approved']
        },
        underbidderPicklist: {
            template: underbidderPicklist,
            typeAttributes: ['underbidderValues']
        },
        contactsList: {
            template: contactsList,
            typeAttributes: ['listOfContacts']
        },
        lastActivitiesList: {
            template: lastActivitiesList,
            typeAttributes: ['listOfLastActivities']
        },
    };

    @wire(CurrentPageReference)
    pageRef;

    renderedCallback() {
        super.renderedCallback();
        var self = this;
        let lastBidNodes = this.template.querySelectorAll(`lightning-primitive-cell-factory[data-label=\"${lastBid}\"]`);
        let lastBidAmountNodes = this.template.querySelectorAll(`lightning-primitive-cell-factory[data-label=\"${lastBidAmount}\"]`);
        if (lastBidNodes.length > 0) {
            this.addMouseOverAndLeaveListeners(lastBidNodes, self, 'AccountToOpportunity__c');
        }
        if (lastBidAmountNodes.length > 0) {
            this.addMouseOverAndLeaveListeners(lastBidAmountNodes, self, 'AccountToOpportunity__c');
        }

    }

    addMouseOverAndLeaveListeners(nodes, self, objectType) {
        if ( nodes )
        {
            nodes.forEach( node => {
                node.addEventListener("mouseover", function(event) {
                    self.handleMouseOverEvent(event, self, objectType);
                });
                node.addEventListener("mouseleave", function (event) {
                    self.handleMouseLeaveEvent(event, self, objectType);
                });
            });
        }
    }

    disconnectedCallback() {}

    handleMouseOverEvent(event, self, objectType) {
        self.throwPayloadEvent(event, true, self, objectType);
    }

    handleMouseLeaveEvent(event, self, objectType) {
        self.throwPayloadEvent(event, false, self, objectType);
    }

    throwPayloadEvent(event, open, self, objectType) {
        if (event && event.target && event.target.parentNode) {
            // var requestId;
            // var mdmAccountId;
            var recordId = event.target.parentNode.parentNode.getAttribute('data-row-key-value');
            // if (recordId) {
            //     requestId = objectType === 'MDM_Request__c' && recordId.startsWith('a8c') ? recordId : undefined;
            //     mdmAccountId = objectType === 'MDM_Account__c' && recordId.startsWith('a8b') ? recordId : undefined;
            // }

            //Standard dispatchEvent method doesn't work in this context, pubsub is used for communication
            fireEvent(self.pageRef, 'payloadrender', {
                    recordId: recordId,
                    open: open,
                    left: event.clientX,
                    top: event.clientY
                });
        }
    }
}