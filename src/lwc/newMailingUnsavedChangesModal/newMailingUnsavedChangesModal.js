/**
*
*	@author npo
*	@copyright PARX
*/
import { LightningElement, api, track } from 'lwc';
import cancelLabel from '@salesforce/label/c.investorsListCancelLabel';
import newMailingSaveChangesLabel from '@salesforce/label/c.NewMailingSaveChanges';
import saveLabel from '@salesforce/label/c.NewMailingSave';
import newMailingContinueEditingLabel from '@salesforce/label/c.NewMailingContinueEditing';
import newMailingDiscardChangesLabel from '@salesforce/label/c.NewMailingDiscardChanges';



export default class NewMailingUnsavedChangesModal extends LightningElement {
	label = {
		cancelLabel,
		saveLabel,
		newMailingSaveChangesLabel,
		newMailingContinueEditingLabel,
		newMailingDiscardChangesLabel
	};

	@api
	navigateTo;

	@track showModal = false;
	@api show() {
	    this.showModal = true;
	}

	@api hide() {
		this.showModal = false;
	}

	handleCancelNewMailingModal()
	{
		this.hide();
	}

	handleSaveChanges()
	{
	    this.hide();
	    const event = new CustomEvent('savechanges', {
			detail: {
				navigateTo: this.navigateTo
			}
		});
	    this.dispatchEvent(event);
	}

	handleDiscardChanges()
	{
		this.hide();
		const event = new CustomEvent('discardchanges', {
			detail: {
				navigateTo: this.navigateTo
			}
		});
		this.dispatchEvent(event);
	}
}