import { LightningElement, api, track, wire } from 'lwc';
import { refreshApex } from '@salesforce/apex';
import { getObjectInfo} from 'lightning/uiObjectInfoApi';
import ACQUISITION_OBJECT from '@salesforce/schema/AcquisitionProfile__c';

export default class InvestorSelectionAcquisitionProfilePopover extends LightningElement{
    @api recordId;
    @api record;
    @api top;
    @api left;
    @track request;

    connectedCallback(){

    }

    handleRefreshBidActivity()
    {
        return refreshApex(this.wiredResult);
    }

    get positionStyle() {
        return `top: ${this.top}px; left:${this.left}px`;
    }

    @wire(getObjectInfo, { objectApiName: ACQUISITION_OBJECT }) acquisitionProfileObjectInfo;



    get typeOfUseLabel() {
        return (typeof this.acquisitionProfileObjectInfo.data !== 'undefined' ? this.acquisitionProfileObjectInfo.data.fields.TypeOfUse__c.label : '');
    }
    get typeOfUseValue() {
        return (typeof this.record.TypeOfUse__c !== 'undefined' ? this.record.TypeOfUse__c : '-');
    }

    get investmentVolumeFromLabel() {
        return (typeof this.acquisitionProfileObjectInfo.data !== 'undefined' ? this.acquisitionProfileObjectInfo.data.fields.InvestmentVolumeFrom__c.label : '');
    }
    get investmentVolumeFromValue() {
        return (typeof this.record.InvestmentVolumeFrom__c !== 'undefined' ? this.record.InvestmentVolumeFrom__c : '-');
    }

    get investmentVolumeToLabel() {
        return (typeof this.acquisitionProfileObjectInfo.data !== 'undefined' ? this.acquisitionProfileObjectInfo.data.fields.InvestmentVolumeTo__c.label : '');
    }
    get investmentVolumeToValue() {
        return (typeof this.record.InvestmentVolumeTo__c !== 'undefined' ? this.record.InvestmentVolumeTo__c : '-');
    }


    get occupancyRateLabel() {
        return (typeof this.acquisitionProfileObjectInfo.data !== 'undefined' ? this.acquisitionProfileObjectInfo.data.fields.OccupancyRate__c.label : '');
    }
    get occupancyRateValue() {
        return (typeof this.record.OccupancyRate__c !== 'undefined' ? this.record.OccupancyRate__c : '-');
    }


    get macroLocationLabel() {
        return (typeof this.acquisitionProfileObjectInfo.data !== 'undefined' ? this.acquisitionProfileObjectInfo.data.fields.MacroLocation__c.label : '');
    }
    get macroLocationValue() {
        return (typeof this.record.MacroLocation__c !== 'undefined' ? this.record.MacroLocation__c : '-');
    }


    get microLocationLabel() {
        return (typeof this.acquisitionProfileObjectInfo.data !== 'undefined' ? this.acquisitionProfileObjectInfo.data.fields.MicroLocation__c.label : '');
    }
    get microLocationValue() {
        return (typeof this.record.MicroLocation__c !== 'undefined' ? this.record.MicroLocation__c : '-');
    }

    get riskCategoryLabel() {
        return (typeof this.acquisitionProfileObjectInfo.data !== 'undefined' ? this.acquisitionProfileObjectInfo.data.fields.RiskCategory__c.label : '');
    }
    get riskCategoryValue() {
        return (typeof this.record.RiskCategory__c !== 'undefined' ? this.record.RiskCategory__c : '-');
    }

    get waltFromLabel() {
        return (typeof this.acquisitionProfileObjectInfo.data !== 'undefined' ? this.acquisitionProfileObjectInfo.data.fields.WALTFrom__c.label : '');
    }
    get waltFromValue() {
        return (typeof this.record.WALTFrom__c !== 'undefined' ? this.record.WALTFrom__c : '-');
    }

    get irrFromLabel() {
        return (typeof this.acquisitionProfileObjectInfo.data !== 'undefined' ? this.acquisitionProfileObjectInfo.data.fields.IRRFrom__c.label : '');
    }
    get irrFromValue() {
        return (typeof this.record.IRRFrom__c !== 'undefined' ? this.record.IRRFrom__c : '-');
   }

    get cocFromLabel() {
        return (typeof this.acquisitionProfileObjectInfo.data !== 'undefined' ? this.acquisitionProfileObjectInfo.data.fields.CoCFrom__c.label : '');
    }
    get cocFromValue() {
        return (typeof this.record.CoCFrom__c !== 'undefined' ? this.record.CoCFrom__c : '-');
   }

    get factorLabel() {
        return (typeof this.acquisitionProfileObjectInfo.data !== 'undefined' ? this.acquisitionProfileObjectInfo.data.fields.Factor__c.label : '');
    }
    get factorValue() {
        return (typeof this.record.Factor__c !== 'undefined' ? this.record.Factor__c : '-');
   }

    get commentsLabel() {
        return (typeof this.acquisitionProfileObjectInfo.data !== 'undefined' ? this.acquisitionProfileObjectInfo.data.fields.Comments__c.label : '');
    }
    get commentsValue() {
        return (typeof this.record.Comments__c !== 'undefined' ? this.record.Comments__c : '-');
   }
}