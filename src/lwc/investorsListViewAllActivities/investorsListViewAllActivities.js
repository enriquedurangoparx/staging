import { LightningElement, track, wire } from 'lwc';
import search from '@salesforce/apex/GenericSearch.search';
import deleteTask from '@salesforce/apex/InvestorsListController.deleteTask';
import { CurrentPageReference} from'lightning/navigation';
import rowLabelSubject from '@salesforce/label/c.InvestorsListTableRowSubject';
import rowLabelDate from '@salesforce/label/c.InvestorsListTableRowDate';
import rowLabelContact from '@salesforce/label/c.InvestorsListTableRowContact';
import rowLabelStatus from '@salesforce/label/c.InvestorsListStatusLabel';
import rowLabelAssignedTo from '@salesforce/label/c.InvestorsListActionFormAssignedTo';
import rowLabelComment from '@salesforce/label/c.InvestorsListActionFormComment';
import activities from '@salesforce/label/c.InvestorsListActivity';

import cancelDelete from '@salesforce/label/c.investorsListCancelLabel';
import deleteActivity from '@salesforce/label/c.InvestorsListDeleteContact';
import deleteActivityHeading from '@salesforce/label/c.InvestorsListDeleteActivityHeadline';
import deleteActivityConfirm from '@salesforce/label/c.InvestorsListDeleteActivityConfirm';



export default class InvestorsListViewAllActivities extends LightningElement {
    recordId;
    @track activityData;
    @track showConfirmDelete = false;
    @track isLoading = false;
    activityToDeleteId;

    activityColumns = [{label: rowLabelSubject, fieldName: 'activityid',  type: 'url', typeAttributes: { label: {fieldName: 'Subject'}, target:'_blank'}},
                        {label: rowLabelComment, fieldName: 'Description', type: 'text', sortable: 'false', wrapText: true},
                        {label: rowLabelContact, fieldName: 'contactId',  type: 'url', typeAttributes: { label: {fieldName: 'contactName'}, target:'_blank'}},
                        {label: rowLabelDate, fieldName: 'ActivityDate', type: 'date', sortable: 'false' },
                        {label: rowLabelStatus, fieldName: 'Status', type: 'text', sortable: 'false' },
                        {label: rowLabelAssignedTo, fieldName: 'ownerId',  type: 'url', typeAttributes: { label: {fieldName: 'ownerName'}, target:'_blank'}},
                        {label: '', type: 'activityListAction', fieldName: 'Id', fixedWidth: 36, typeAttributes: { action: 'deleteActivity' }}
                        ];

    @wire(CurrentPageReference) pageRef;

    label = {
        activities,
        deleteActivity,
        cancelDelete,
        deleteActivityHeading,
        deleteActivityConfirm
    }

    connectedCallback() {
        this.recordId = this.pageRef.state.c__recordId;
        this.getActivities(this.recordId);
    }

    getActivities(investorId){
        if(investorId != null){
            search({
                list_fieldApiName: ['Id', 'Subject','Description', 'Who.Name', 'toLabel(Status)', 'ActivityDate', 'Date__c', 'Owner.Id', 'Owner.Name', '(SELECT Id, RelationId, Relation.Type, Relation.Name FROM TaskRelations)'],
                string_objectApiName: 'Task',
                string_searchTerm: investorId,
                string_whereApiName: 'WhatId',
                string_filter : '',
                string_soqlOperator: '=',
                string_orderBy: 'ActivityDate DESC',
                integer_limit: 1000,
                integer_offset: 0
            })
            .then(result => {
                const tempResult = JSON.parse(JSON.stringify(result));
                let _activityData = this._preprocessActivityData(tempResult);
                this.activityData = JSON.parse(JSON.stringify(_activityData));

            }).catch(error => {
                console.error(JSON.stringify(error, null, '\t'));
            })
        }
    }

    _preprocessActivityData(result){
        let preprocessData = [];
        result.forEach(element => {
            console.log(JSON.stringify(element, null, '\t'));
            element.activityid = '/' + element.Id;

            if(typeof element.WhoId !== 'undefined'){
                element.contactId = '/' + element.WhoId;
                element.contactName = element.Who.Name;
            }
            element.ownerId = '/' + element.OwnerId;
            element.ownerName = element.Owner.Name;

            preprocessData.push(element);
        });

       return preprocessData;
    }

    handleDeleteActivity(event) {
        this.activityToDeleteId = event.detail.activityId;
        this.showConfirmDelete = true;
    }

    closeModal() {
        this.showConfirmDelete = false;
    }

    async deleteActivity() {
        this.isLoading = true;
        deleteTask({
            taskId: this.activityToDeleteId
        })
        .then(() => {
            this.getActivities(this.recordId);
            this.isLoading = false;
        })
        .catch((error) => {
            console.log('Error');
            this.isLoading = false;
        });
        this.closeModal();
    }
}