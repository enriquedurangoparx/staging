import { LightningElement, track, api, wire } from 'lwc';

import updateInvestorsCall from '@salesforce/apex/InvestorsListController.updateInvestors';

import { getPicklistValues } from 'lightning/uiObjectInfoApi';
import REASON_FIELD from '@salesforce/schema/AccountToOpportunity__c.ReasonForRejection__c';

// import custom labels
import close from '@salesforce/label/c.InvestorsListCloseLabel';
import cancel from '@salesforce/label/c.investorsListCancelLabel';
import header from '@salesforce/label/c.InvestorsListDeclinationModalReason';
import update from '@salesforce/label/c.investorsListUpdateLabel';
import text from '@salesforce/label/c.investorsListDeclinationModalText';
import reason from '@salesforce/label/c.InvestorsListDeclinationModalReason';
import placeholder from '@salesforce/label/c.InvestorsListDeclinationModalPlaceholder';
import toastTitle from '@salesforce/label/c.InvestorsListDeclinationModalToastTitle';

const DEFAULT_RECORDTYPE = '012000000000000AAA';

export default class InvestorsListDeclinationModal extends LightningElement {
    @track showModal = false;
    @api recordsToUpdate;
    @track value = [];

    selectedItems;

    label = {
        close,
        cancel,
        header,
        update,
        text,
        reason,
        placeholder,
        toastTitle,
        toastMessage: ""
    }

    @track picklistValues;

    @wire(getPicklistValues, { recordTypeId: DEFAULT_RECORDTYPE, fieldApiName: REASON_FIELD })
        getpicklistValues(result){
            if(result && result.data){
                this.picklistValues=result;
            }
        }

    /********************
     * EXPOSED FUNCTIONS
     ********************/
    /**
     * open objective selection modal
     */
    @api
    openModal(recordsToUpdate) {

        // Map Picklist value Label to Value
        let mapPicklistValues = new Map();
        for (let i = 0; i < this.picklistValues.data.values.length; i++) {
            mapPicklistValues[this.picklistValues.data.values[i].label] = this.picklistValues.data.values[i].value;
        }
        this.recordsToUpdate = recordsToUpdate;
        let tempValues = [];
        for (let i = 0; i < this.recordsToUpdate.length; i++) {
            if(this.recordsToUpdate[i].declination !== '-'){
                let declinations = this.recordsToUpdate[i].declination.toString().split(';');

                for(let k = 0; k < declinations.length; k++){
                    let declinationValue = mapPicklistValues[declinations[k]];

                    if(!tempValues.includes(declinationValue)){
                        tempValues.push(declinationValue);
                    }
                }
            }
        }

        this.value = tempValues;
        this.showModal = true;
    }

    /**
     * close objective selection modal
     */
    closeModal() {
        this.showModal = false;
    }

    /**
     * deletes Investors by fetching Id's from Objects in recordsToUpdate Array
     */
    async updateInvestors(){
        let investors = [];
        let decValue = this.selectedItems ? this.selectedItems.join(';') : '';
        for(let i=0; i<this.recordsToUpdate.length; i++){
            investors.push({Id: this.recordsToUpdate[i].Id, ReasonForRejection__c: decValue});
        }
        // call update function
        const updateInvestorsResult = await updateInvestorsCall({
            investors : investors
        });

        const event = CustomEvent('investorsupdated', {
            composed: true,
            bubbles: true,
            cancelable: true,
            detail: {
                toastTitle: this.label.toastTitle,
                toastMessage: this.label.toastMessage,
                toastVariant: 'success'
            },
        });
        this.dispatchEvent(event);
        this.closeModal();

        this.value = [];
    }

    handleChange(event){
        if(event.detail.value){
            this.selectedItems = [];
            for (let i in event.detail.value) {
                this.selectedItems.push(event.detail.value[i]);
            }
        }
    }
}