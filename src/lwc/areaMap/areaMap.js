import { LightningElement, wire, api, track } from 'lwc';
import { createMarker } from 'c/mapObject';

// controller methods
import getPropertyObjects from '@salesforce/apex/LWCAreaMapController.getPropertyObjects';

// import custom labels:
import noRecordsFoundLabel from '@salesforce/label/c.GeneralLabelNoRecordsFound';

export default class AreaMap extends LightningElement
{
    @api recordId;

    @track error;   //this holds errors
    @track showMap = true;

    @track markers = [];
    @track zoomLevel = 4;

    label = {
        noRecordsFoundLabel
    }
    
    /* Load address information based on accountNameParam from Controller */
    @wire(getPropertyObjects, { recordId: '$recordId' })
    getPropertyObjects(result) {
        if (result.data) {
            if (result.data.length === 0) {
                this.showMap = false;
                this.error = this.label.noRecordsFoundLabel;
            }
            else
            {
                let tempMarkers = [];

                result.data.forEach(record => {
                    if(typeof record !== undefined){
                        let marker = createMarker(
                            record.Id, 
                            record.Geolocation__Latitude__s,
                            record.Geolocation__Longitude__s,
                            typeof record.Name !== undefined ? record.Name : '', 
                            typeof record.PropertyDescriptionDe__c !== undefined ? record.PropertyDescriptionDe__c : '', 
                            '', 
                            record.Street__c, 
                            record.PostalCode__c, 
                            record.Country__c);

                        tempMarkers.push(marker); 
                    }
                });
                this.markers = tempMarkers;
                this.showMap = true;
            }
        } 
        else if (result.error) {
            this.error = result.error;
            this.markers = [];
        }
    }
}