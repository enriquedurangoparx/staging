import { LightningElement, api, track } from 'lwc';

import startEmailsSendingProcess from '@salesforce/apex/NewMailingController.startEmailsSendingProcess';

import generalLabelClose from '@salesforce/label/c.GeneralLabelClose';
import generalLabelSend from '@salesforce/label/c.GeneralLabelSend';
import generalLabelCancel from '@salesforce/label/c.GeneralLabelCancel';
import mailingSendConfirmationText from '@salesforce/label/c.MailingSendConfirmationText';
import mailingSendConfirmationTitle from '@salesforce/label/c.MailingSendConfirmationTitle';

export default class NewMailingSendModal extends LightningElement 
{
    label = {
        generalLabelClose,
        generalLabelSend,
        mailingSendConfirmationText,
        mailingSendConfirmationTitle,
        generalLabelCancel
    }

    @track
    showModal = false;

    @track
    isLoading = false;

    @track
    jsonObj;

    @api
    openModal(obj) 
    {
        this.showModal = true;
        this.jsonObj = obj;
    }

    closeModal() 
    {
        this.showModal = false;
    }

    handleConfirm()
    {
        this.isLoading = true;

        startEmailsSendingProcess({jsonString: this.jsonObj})
        .then(resultJson => {
            this.isLoading = false;
            this.showModal = false;
            const evnt = new CustomEvent(
                'sendingresponse',
                {
                    detail: resultJson
                }
            );
            this.dispatchEvent(evnt);
        })
        .catch(error => {
            this.isLoading = false;
            console.log('...error: ' + JSON.stringify(error));
        });	
    }

   
}