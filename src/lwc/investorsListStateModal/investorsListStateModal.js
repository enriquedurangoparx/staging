import { LightningElement, api, track, wire  } from 'lwc';
import updateInvestorsCall from '@salesforce/apex/InvestorsListController.updateInvestors';

import { getPicklistValues } from 'lightning/uiObjectInfoApi';
import STAGE_FIELD from '@salesforce/schema/AccountToOpportunity__c.Status__c';
import {fireEvent} from 'c/pubsub';
import { CurrentPageReference} from 'lightning/navigation';

// import custom labels
import close from '@salesforce/label/c.InvestorsListCloseLabel';
import cancel from '@salesforce/label/c.investorsListCancelLabel';
import update from '@salesforce/label/c.investorsListUpdateLabel';
import header from '@salesforce/label/c.InvestorsListStatusModalTitle';
import status from '@salesforce/label/c.InvestorListInvestorStatus';
import text from '@salesforce/label/c.InvestorsListStatusModalText';
import toastTitle from '@salesforce/label/c.InvestorsListStatusModalToastTitle';
import statusDateLabel from '@salesforce/label/c.InvestorsListStatusDate';
import statusDateValidityLabel from '@salesforce/label/c.InvestorsListStatusDateValidity';
import declination from '@salesforce/label/c.InvestorsListDeclinationLabel';
import amount from '@salesforce/label/c.InvestorsListActionFormAmount';
import AreaRelatePropertyObjects from 'c/areaRelateObjects';

const DEFAULT_RECORDTYPE = '012000000000000AAA';

export default class InvestorsListStateModal extends LightningElement {
    @wire(CurrentPageReference) pageRef;
    @track showModal = false;
    @api recordsToUpdate;
    @track state;
    @track declinationSelected;
    @track _selectedDeclinations = [];
    @track stateLabel;
    @track saveButtonDisabled = true;
    @track dateValue;
    @track amountValue;
    @track defaultRecordTypeId = DEFAULT_RECORDTYPE;
    showAmount;

    label = {
        close,
        cancel,
        header,
        update,
        text,
        toastTitle,
        statusDateLabel,
        statusDateValidityLabel,
        toastMessage: "",
        declination,
        amount,
        status
    }

    renderedCallback(){
        // pre-populating the date field with the current date
        var localDate = new Date();
        this.dateValue = localDate.toISOString().substring(0,10);
    }

    @wire(getPicklistValues, { recordTypeId: DEFAULT_RECORDTYPE, fieldApiName: STAGE_FIELD })
        picklistValues;

    /********************
     * EXPOSED FUNCTIONS
     ********************/
    /**
     * open objective selection modal
     */
    @api
    openModal(recordsToUpdate, state, stateLabel) {
        this.recordsToUpdate = recordsToUpdate;
        this.state = state;
        this.stateLabel = stateLabel;
        this.showModal = true;
    }

    /**
     * close objective selection modal
     */
    closeModal() {
        this.showModal = false;
    }

    /**
     * deletes Investors by fetching Id's from Objects in recordsToUpdate Array
     */
    async updateInvestors(){
        if(!this.state)
            return;

        let investors = [];

        let amount;
        if(typeof this.template.querySelector('[data-id="amountInput"]') !== 'undefined' && this.template.querySelector('[data-id="amountInput"]') != null){
            amount = this.template.querySelector('[data-id="amountInput"]').value;
        }


        let decValue = this._selectedDeclinations ? this._selectedDeclinations.join(';') : '';
        for(let i=0; i<this.recordsToUpdate.length; i++){
            let investor = {Id: this.recordsToUpdate[i].Id, Status__c: this.state, ReasonForRejection__c : decValue};
            if(this.showAmount){
                investor.LastBidAmount__c = amount;
            }

            investors.push(investor);
        }

        let dateCmp = this.template.querySelector('[data-id="dateInput"]');
        let localDate = new Date();
        let hasErrors = false;
        if (dateCmp.value > localDate.toISOString().substring(0,10)){
            dateCmp.setCustomValidity(this.label.statusDateValidityLabel);
            hasErrors = true;
        }
        else
        {
            dateCmp.setCustomValidity("");
        }
        dateCmp.reportValidity();

        if (!hasErrors)
        {
            // call update function
            const updateInvestorsResult = await updateInvestorsCall({
                investors : investors,
                statusDate : dateCmp.value
            });

            fireEvent(this.pageRef, 'investorsListStatusPicklist_refresh', investors);
            const event = CustomEvent('investorsupdated', {
                composed: true,
                bubbles: true,
                cancelable: true,
                detail: {
                    toastTitle: this.label.toastTitle,
                    toastMessage: this.label.toastMessage,
                    toastVariant: 'success'
                },
            });
            this.dispatchEvent(event);
            this.closeModal();
        }
    }


    /**
     *
     * @param {*} event event dispatched by combobox on changing selection
     */
    handleComboboxChange(event){
        // handling enabling/disabling the save button
        this.saveButtonDisabled = (event.detail.value === null);
        this.state = event.detail.value;
        this.declinationSelected = this.state == 'Declination' ? true : false;
        this.showAmount = this.state == 'Offer' ? true : false;

        if(this.declinationSelected && this._selectedDeclinations.length == 0){
            this.saveButtonDisabled = true;
        }

        if(this.showAmount){
            this.saveButtonDisabled = true;
        }
    }

    handleDeclinationReasonChange(event) {
        this._selectedDeclinations = event.detail;

        if(this.declinationSelected && this._selectedDeclinations.length == 0){
            this.saveButtonDisabled = true;
        }else{
            this.saveButtonDisabled = false;
        }
    }


    handleAmountChange(event){
        let amount = this.template.querySelector('[data-id="amountInput"]').value;
        if(typeof amount !== 'undefined' && amount != '' && amount != 0){
            this.saveButtonDisabled = false;
        }else{
            this.saveButtonDisabled = true;
        }
    }
}