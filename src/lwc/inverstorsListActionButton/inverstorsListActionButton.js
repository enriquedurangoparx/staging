import { LightningElement, api } from 'lwc';
// Accessibility module
import { baseNavigation } from 'lightning/datatableKeyboardMixins';
// For the render() method
import template from './inverstorsListActionButton.html';

import activity from '@salesforce/label/c.InvestorsListActivityButtonLabel';

// export default class DatatableDeleteRowBtn extends baseNavigation(LightningElement) {
export default class inverstorsListActionButton extends LightningElement {
    @api rowId;

    label = {
        activity
    }

    // Required for mixins
    render() {
        return template;
    }

    fireActionButton() {
      window.console.log(baseNavigation);
        const event = CustomEvent('actionbutton', {
            composed: true,
            bubbles: true,
            cancelable: true,
            detail: {
                rowId: this.rowId,
            },
        });
        this.dispatchEvent(event);
    }
}