import { LightningElement, api } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';

export default class NavigateToButton extends NavigationMixin(
    LightningElement
) {
    @api label;
    @api navType;
    @api recordId;
    @api objectApiName;
    @api tabName;
    @api variant;
    @api showAsCard;

    navigate() {
        switch (this.navType) {
            case 'standard__navItemPage':
                this[NavigationMixin.Navigate]({
                    type: 'standard__navItemPage',
                    attributes: {
                        apiName: this.tabName
                    },
                    state:{
                        c__recordId: this.recordId
                    }
                });
                break;
            case 'standard__recordPage':
                if (this.recordId) {
                    this[NavigationMixin.Navigate]({
                        type: 'standard__recordPage',
                        attributes: {
                            recordId: this.recordId,
                            actionName: 'view'
                        }
                    });
                }
                break;
            case 'standard__objectPage':
                if (this.objectApiName) {
                    this[NavigationMixin.Navigate]({
                        type: 'standard__objectPage',
                        attributes: {
                            objectApiName: this.objectApiName,
                            actionName: 'home'
                        }
                    });
                }
                break;

            default:
                break;
        }
    }
}