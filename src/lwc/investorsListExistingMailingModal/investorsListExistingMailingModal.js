/**
*
*	@author npo
*	@copyright PARX
*/
import { LightningElement, api, track } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import loadCampaigns from '@salesforce/apex/NewMailingController.loadCampaigns';
import loadInvestorsForCampaign from '@salesforce/apex/NewMailingController.loadInvestorsForCampaign';
import campaignNameLabel from '@salesforce/label/c.NewMailingCampaignName';
import getInvestorContacts from '@salesforce/apex/InvestorsListController.getInvestorContacts';


import creatorLabel from '@salesforce/label/c.NewMailingCreator';
import createdDateLabel from '@salesforce/label/c.NewMailingCreatedDate';
import cancelLabel from '@salesforce/label/c.investorsListCancelLabel';
import newMailingShowDetailsLabel from '@salesforce/label/c.NewMailingShowDetails';
import addInvestorsLabel from '@salesforce/label/c.AddInvestors';
import copyLabel from '@salesforce/label/c.Copy';
import campaignIsActiveLabel from '@salesforce/label/c.CampaignIsActive';
import existingMailing from '@salesforce/label/c.InvestorsListExistingMailing';
import close from '@salesforce/label/c.InvestorsListCloseLabel';


const actions = [
	{ label: newMailingShowDetailsLabel, name: 'show_details' },
	{ label: addInvestorsLabel, name: 'add_investors' },
	{ label: copyLabel, name: 'copy'}
];

const DEFAULT_SORT_FIELD = 'Name';
const DEFAULT_SORT_DIRECTION = 'asc';


export default class  extends NavigationMixin(LightningElement) {
	@track showModal = false;

	@api
	opportunityId;

	@api
	selectedInvestors;

	@api
	records;

	@track
	investorsWithContacts;

	@track campaigns;

	label = {
		campaignNameLabel,
		creatorLabel,
		createdDateLabel,
		cancelLabel,
		campaignIsActiveLabel,
		existingMailing
	};

	@track campaignColumns = [
		{label: this.label.campaignNameLabel, type: 'url', fieldName: 'campaignUrl', sortable: true, typeAttributes: { label: { fieldName: 'name' }, target: '_blank' }},
		{label: this.label.createdDateLabel, type: 'date', fieldName: 'createdDate', sortable: true},
		{label: this.label.creatorLabel, type: 'text', fieldName: 'creator', sortable: true},
		{label: this.label.campaignIsActiveLabel, type: 'boolean', fieldName: 'isActive', sortable: true},
		{
			 type: 'action',
			 typeAttributes: { rowActions: actions },
		}
	 ];

	 // Table sorting
	 @track
	 sortedBy = DEFAULT_SORT_FIELD;
	 @track
	 sortedDirection = DEFAULT_SORT_DIRECTION;

	connectedCallback()
	{
	}


	@api show() {
	    this.showModal = true;

	    let loadActiveCampaignsOnly = this.selectedInvestors !== undefined && this.selectedInvestors.length > 0;
		loadCampaigns({opportunityId: this.opportunityId, loadOnlyActive: loadActiveCampaignsOnly})
			.then(result => {
				console.log('...loadCampaigns: ' + JSON.stringify(result));
				let campaigns = [];
				result.forEach(function(campaign) {
					campaigns.push({
					    id: campaign.Id,
						name: campaign.Name,
						campaignUrl:  '/lightning/n/NewMailing?c__campaignId=' + campaign.Id,
						createdDate: campaign.CreatedDate,
						creator: campaign.CreatedBy.Alias,
						isActive: campaign.IsActive
					})
				})

				this.campaigns = campaigns;
				console.log('...loadCampaigns: this.campaigns' + JSON.stringify(this.campaigns));
			})
			.catch(error => {
				console.log('...error' + JSON.stringify(error));
			});
		if (this.selectedInvestors && this.selectedInvestors.length > 0)
		{
			this.verifySelectedInvestors();
		}
	}

	@api hide() {
		this.showModal = false;
	}

	handleCancelNewMailingModal()
	{
		this.hide();
	}

	handleCloseDialog()
	{
	    this.showModal = true;
	}

    handleRowAction(event) {
        console.log('...handleRowAction');
        const actionName = event.detail.action.name;
        const row = event.detail.row;
        switch (actionName) {
            case 'show_details':
//                this.openMailingForCampaign(row);
				this.navigateToMailing(null, row.id)
                break;
			case 'add_investors':
//				this.openMailingForCampaign(row);
				this.openNewMailingModal(row.id);
				break;
			case 'copy':
				this.openNewMailingEditCriteriaModal(row.id);
				break;
            default:
        }
    }

    openNewMailingModal(campaignId)
    {
		this.hide();
		const modal = this.template.querySelector('c-investors-list-new-mailing-modal');
		modal.selectedInvestors = this.selectedInvestors;
		modal.records = this.records;
		modal.campaignId = campaignId;
		modal.show();
    }

	openNewMailingEditCriteriaModal(campaignId)
	{
		this.hide();
		this[NavigationMixin.Navigate]({
			type: 'standard__navItemPage',
			attributes: {
				apiName: 'NewMailing'
			},
			state: {
			    c__copyCampaignId: campaignId
			}
		});
	}

    openMailingForCampaign(row)
    {
        console.log('...openMailingForCampaign' + JSON.stringify(row));
        let campaignId = row.id;
		let investors = [];
		if (this.investorsWithContacts && this.investorsWithContacts.length > 0)
		{
			investors.push(this.investorsWithContacts);
			console.log('...push this.investorsWithContacts: ' + this.investorsWithContacts);
		}
		console.log('...push this.investorsWithContacts: over this.selectedInvestors: ' + this.selectedInvestors);
//		investors.push(this.selectedInvestors);
		let self = this;
		loadInvestorsForCampaign({campaignId: campaignId})
			.then(result => {
				result.forEach(function(record){
					investors.push(record.Investor__c);
				});
				self.navigateToMailing(investors, campaignId);
			})
			.catch(error => {
				console.log('...error' + JSON.stringify(error));
			});
    }

    verifySelectedInvestors()
    {
        getInvestorContacts({ recordsIds: this.selectedInvestors })
		.then(result => {
			let investorsContacts = result;
			let investorsWithAndWithoutContacts = this.calculateInvestorsWithoutContacts(this.selectedInvestors, investorsContacts);
			this.investorsWithContacts = investorsWithAndWithoutContacts.investorsWithContacts;
		})
		.catch(error => {
		});
    }

    navigateToMailing(investors, campaignId)
    {
        let state =  {
        };
        if (investors != undefined)
        {
            state.c__investors = investors;
        }
		if (campaignId != undefined)
		{
			state.c__campaignId = campaignId;
		}
		this[NavigationMixin.Navigate]({
			type: 'standard__navItemPage',
			attributes: {
				apiName: 'NewMailing'
			},
			state: state
		});
    }

	calculateInvestorsWithoutContacts(investors, investorsContacts)
	{
		let investorsContactsMap = new Map();
		investorsContacts.forEach(function(cont){
			let listOfContacts = investorsContactsMap.has(cont.Investor__c) ? investorsContactsMap.get(cont.Investor__c) : [];
			listOfContacts.push(cont);
			investorsContactsMap.set(cont.Investor__c, listOfContacts);
		});

		let investorsWithNoContacts = [];
		let investorsWithContacts = [];
		investors.forEach(function(investorId){
			if (!investorsContactsMap.has(investorId) || investorsContactsMap.get(investorId).length < 1)
			{
				investorsWithNoContacts.push(investorId);
			}
			else
			{
				investorsWithContacts.push(investorId);
			}
		});
		return {investorsWithNoContacts : investorsWithNoContacts, investorsWithContacts : investorsWithContacts};
	}

	updateColumnSorting(event) {
	    console.log('...updateColumnSorting(event) ');
		this.sortedBy = event.detail.fieldName;
		this.sortedDirection = event.detail.sortDirection;

		this.sortData(this.sortedBy, this.sortedDirection);
   }

   sortData(sortedBy, sortDirection)
   {
		var data = JSON.parse(JSON.stringify(this.campaigns));
		//function to return the value stored in the field
		var key =(a) => a[sortedBy] + '';
		var reverse = sortDirection === 'asc' ? 1: -1;
		data.sort((a,b) => {
			let valueA = key(a) ? key(a).toLowerCase() : '';
			let valueB = key(b) ? key(b).toLowerCase() : '';
			return reverse * ((valueA > valueB) - (valueB > valueA));
		});

		//set sorted data to opportunities attribute
		this.campaigns = data;
   }
}