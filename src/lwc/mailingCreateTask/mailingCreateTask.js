import { LightningElement, api, track, wire } from 'lwc';
import { getRecord, getFieldValue } from 'lightning/uiRecordApi';
import NAME_FIELD from '@salesforce/schema/User.Name';

import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { reduceErrors } from 'c/lwcUtil';
import currentUserId from '@salesforce/user/Id';

import lookupSearch from '@salesforce/apex/LookupController.search';
import createTask from '@salesforce/apex/MailingCreateTask.create';

// import custom labels
import close from '@salesforce/label/c.GeneralLabelClose';
import save from '@salesforce/label/c.GeneralLabelSave';
import cancel from '@salesforce/label/c.GeneralLabelCancel';
import errorTitle from '@salesforce/label/c.GeneralLabelError';
import successTitle from '@salesforce/label/c.GeneralLabelSuccess';
import successMessage from '@salesforce/label/c.GeneralLabelSuccessInsertMessage';

import header from '@salesforce/label/c.MailingCreateTaskHeader';
import comment from '@salesforce/label/c.MailingTaskComment';
import subject from '@salesforce/label/c.MailingTaskSubject';
import date from '@salesforce/label/c.MailingTaskDueDate';
import assignedTo from '@salesforce/label/c.MailingTaskAssignedTo';
import ownerErrorMessage from '@salesforce/label/c.MailingCreateTaskOwnerErrorMessage';


export default class MailingCreateTask extends LightningElement {
    /********************
     * PUBLIC PROPERTIES
     ********************/
    @api opportunityRecordId;
    @api correspondenceName;

    
    // Use this wire adapter to get user data.
    @wire(getRecord, { recordId: currentUserId, fields: [NAME_FIELD] })
    user;

    /********************
     * PRIVATE PROPERTIES
     ********************/
    dateValue;// Contains current date.
    selectedOwnerId;// Holds the information who is selected for Task.OwnerId.
    initialized = false;

    @track
    defaultTopicValue;// Contains correspondence name.

    label = {
        header,
        close,
        save,
        cancel,
        subject,
        date,
        assignedTo,
        comment,
        ownerErrorMessage,
        errorTitle,
        successTitle,
        successMessage
    }

    // configuration-objects for lookup components
    ownerField = {objectApiName: "Task", apiName: "OwnerId", isMultiEntry: false};

    connectedCallback()
    {
        let localDate = new Date();
        this.dateValue = localDate.toISOString().substring(0,10);
        this.defaultTopicValue = this.correspondenceName;
    }

    renderedCallback()
    {
        if(this.initialized === false)
        {
            this.initialized = ((this.template.querySelector('[data-id="ownerSearchInput"]') !== null && this.user.data !== undefined) ? true : false);
            this.selectedOwnerId = currentUserId;

            let selection = [{
                id: currentUserId,
                sObjectType: 'User',
                icon: 'utility:user',
                title: getFieldValue(this.user.data, NAME_FIELD),
            }];

            /**
             * Is needed to prevent a null point
             * Error message: [Cannot read property 'setSelection' of null]
             */
            if(this.initialized)
            {
                this.template
                    .querySelector('[data-id="ownerSearchInput"]')
                    .setSelection(selection);
            }
        }
    }

    /********************
     * EVENT HANDLER
     ********************/
    /**
     * Is triggered when you type in a searchterm in lookup field.
     * @param {Event} event 
     */
    handleOwnerLookupSearch(event)
    {
        let searchParam = {
            parentObjectApiName: this.ownerField.objectApiName,
            lookupApiName: this.ownerField.apiName,
            searchTerm: event.detail.searchTerm
        };

        lookupSearch(searchParam)
            .then(results => {
                this.template
                    .querySelector('[data-id="ownerSearchInput"]')
                    .setSearchResults(results);
            })
            .catch(error => {
                this.notifyUser(
                    'Lookup Error',
                    'An error occured while searching with the lookup field.\n' +
                        reduceErrors(error).join(', '),
                    'error'
                );
            });
    }

    handleOwnerSelectionChange(event)
    {
        if(event.target.selection && event.target.selection.length === 1) {
            this.selectedOwnerId = event.target.selection[0].id;
        }
    }

    createActivity(event)
    {
        let allValid = this.validateForm();
        if(!allValid)
            return;

        let subject = this.template.querySelector('[data-id="topicInput"]').value;
        let dueDate = this.template.querySelector('[data-id="dateInput"]').value;
        let description = this.template.querySelector('[data-id="commentInput"]').value;

        let task = {
            WhatId: this.opportunityRecordId,
            Subject: subject,
            ActivityDate: dueDate,
            Description: description,
            OwnerId: this.selectedOwnerId
        };

        createTask({task: task})
            .then(result => {
                let taskId = result;
                if(typeof taskId !== undefined){// Successfully created Task.
                    this.notifyParentOnClose();

                    this.notifyUser(
                        this.label.successTitle,
                        this.label.successMessage,
                        'success'
                    );
                } else {
                    this.notifyUser(
                        this.label.error,
                        error,
                        'error'
                    );
                }
            })
            .catch(error => {                
                this.notifyUser(
                    this.label.error,
                    error,
                    'error'
                );
            });
    }

    validateForm()
    {
        const allValidInput = [...this.template.querySelectorAll('lightning-input')]
            .reduce((validSoFar, inputCmp) => {
                        inputCmp.reportValidity();
                        return validSoFar && inputCmp.checkValidity();
            }, true);

            const allValidTextArea = [...this.template.querySelectorAll('lightning-textarea')]
            .reduce((validSoFar, inputCmp) => {
                        inputCmp.reportValidity();
                        return validSoFar && inputCmp.checkValidity();
            }, true);

            const allValidLookup = [...this.template.querySelectorAll('c-lookup')]
            .reduce((validSoFar, inputCmp) => {
                        inputCmp.reportValidity();
                        return validSoFar && inputCmp.checkValidity();
            }, true);

        let allValid = (allValidInput && allValidTextArea && allValidLookup);

        return allValid;
    }

    /********************
     * EVENT HANDLER
     ********************/
    /**
     * close objective selection modal
     */
    closeModal(event) {
        this.notifyParentOnClose();
    }

    /********************
     * HELPER FUNCTIONS
     ********************/
    notifyUser(title, message, variant)
    {
        const toastEvent = new ShowToastEvent({
            title,
            message,
            variant
        });

        this.dispatchEvent(toastEvent);
    }

    notifyParentOnClose()
    {
        let closeEvent = CustomEvent('close');

        this.dispatchEvent(closeEvent);
    }
}