/**
 * azure 1179 Dynamisches Anzeigen der Matching-Parameter
 */
export class AcquisitionProfileService {
    constructor(acquisitionProfiles) {
        this.profiles = acquisitionProfiles;
    }

    hasEmptyOrSameEntryValues(fieldName) {
        if (this.profiles.length === 0) {
            return true;
        }

        let hasSameValues = true, isValuesEmpty = true;

        let prevValue = this.profiles[0];
        for (let i = 0; i < this.profiles.length; i++) {
            let currentValue = this.profiles[i][fieldName];
            if (currentValue && isValuesEmpty) {
                isValuesEmpty = false;
            }

            if (currentValue !== prevValue) {
                hasSameValues = false;
                break;
            }

            if (currentValue) {
                prevValue = currentValue;
            }
        }
        return hasSameValues || isValuesEmpty;
    }
}