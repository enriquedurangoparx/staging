import { LightningElement, api, wire, track } from 'lwc';
import { getPicklistValues, getObjectInfo } from 'lightning/uiObjectInfoApi';
import { registerListener, unregisterAllListeners, fireEvent } from 'c/pubsub';
import { CurrentPageReference } from 'lightning/navigation';
import { AcquisitionProfileService } from "./acquisitionProfileService";

import getMatchingContacts from '@salesforce/apex/LWCObjectMatchingController.getMatchingContacts';

import PROPERTY_OBJECT from '@salesforce/schema/PropertyObject__c';
import TYPE_OF_USE_FIELD from '@salesforce/schema/PropertyObject__c.TypeOfUse__c';
import RISK_CATEGORY_FIELD from '@salesforce/schema/PropertyObject__c.RiskCategory__c';
import MACRO_LOCATION_FIELD from '@salesforce/schema/PropertyObject__c.MacroLocation__c';
import SUBMARKET_FIELD from '@salesforce/schema/PropertyObject__c.Submarket__c';
import TYPE_OF_REAL_ESTATE_FIELD from '@salesforce/schema/PropertyObject__c.TypeOfRealEstate__c';
import CERTIFICATION_FIELD from '@salesforce/schema/PropertyObject__c.Certification__c';
import CATEGORY_FIELD from '@salesforce/schema/PropertyObject__c.Category__c';
import LOCATION_TYPE_FIELD from '@salesforce/schema/PropertyObject__c.LocationType__c';
import TYPE_OF_CONTRACT_FIELD from '@salesforce/schema/PropertyObject__c.TypeOfContract__c';
import BRAND_HOTELS_FIELD from '@salesforce/schema/PropertyObject__c.BrandHotels__c';
import PLANNING_REGULATIONS_FIELD from '@salesforce/schema/PropertyObject__c.PlanningRegulations__c';
import PLOT_WITH_BUILDING_PERMISSION_FIELD from '@salesforce/schema/PropertyObject__c.PlotWithBuildingPermission__c';
import KIND_OF_DEVELOPMENT_FIELD from '@salesforce/schema/PropertyObject__c.KindOfDevelopment__c';
import STATUS_FIELD from '@salesforce/schema/PropertyObject__c.Status__c';

// import custom labels:
import brandLabel from '@salesforce/label/c.InvestorsSelectionBrand';
import representativeLabel from '@salesforce/label/c.InvestorsSelectionRepresentative';
import streetLabel from '@salesforce/label/c.InvestorsSelectionStreet';
import cityLabel from '@salesforce/label/c.InvestorsSelectionCity';
import accountNameLabel from '@salesforce/label/c.InvestorsSelectionAccountName';
import firstnameLabel from '@salesforce/label/c.InvestorsSelectionFirstname';
import lastnameLabel from '@salesforce/label/c.InvestorsSelectionLastname';
import emailLabel from '@salesforce/label/c.InvestorsSelectionEmail';
import acquisitionProfilesContact from '@salesforce/label/c.InvestorsSelectionAcquisitionProfilesContact';
import activeLabel from '@salesforce/label/c.InvestorsSelectionActive';
import investmentVolume from '@salesforce/label/c.investorSelectionMatchingInvestmentVolume';



export default class InvestorSelectionObjectMatching extends LightningElement {
    @api propertyObject;
    @api propertyToAcquisitionProfileMatchingMap;
    @track isInitialFilterPrepared = false;
    @track initialFilterObject = {};
    @track isTableLoaded = false;
    @wire(getObjectInfo, { objectApiName: PROPERTY_OBJECT }) propertyObjectInfo;
    @wire(CurrentPageReference) pageRef;

    @wire(getPicklistValues, { recordTypeId: '$propertyObjectInfo.data.defaultRecordTypeId', fieldApiName: TYPE_OF_USE_FIELD}) typeOfUsePicklistValues;
    @wire(getPicklistValues, { recordTypeId: '$propertyObjectInfo.data.defaultRecordTypeId', fieldApiName: RISK_CATEGORY_FIELD}) riskCategoryPicklistValues;
    @wire(getPicklistValues, { recordTypeId: '$propertyObjectInfo.data.defaultRecordTypeId', fieldApiName: MACRO_LOCATION_FIELD}) macroLocationPicklistValues;
    @wire(getPicklistValues, { recordTypeId: '$propertyObjectInfo.data.defaultRecordTypeId', fieldApiName: TYPE_OF_REAL_ESTATE_FIELD}) typeOfRealEstatePicklistValues;
    @wire(getPicklistValues, { recordTypeId: '$propertyObjectInfo.data.defaultRecordTypeId', fieldApiName: SUBMARKET_FIELD}) submarketPicklistValues;
    @wire(getPicklistValues, { recordTypeId: '$propertyObjectInfo.data.defaultRecordTypeId', fieldApiName: CERTIFICATION_FIELD}) certificationPicklistValues;
    @wire(getPicklistValues, { recordTypeId: '$propertyObjectInfo.data.defaultRecordTypeId', fieldApiName: CATEGORY_FIELD}) categoryPicklistValues;
    @wire(getPicklistValues, { recordTypeId: '$propertyObjectInfo.data.defaultRecordTypeId', fieldApiName: LOCATION_TYPE_FIELD}) locationTypePicklistValues;
    @wire(getPicklistValues, { recordTypeId: '$propertyObjectInfo.data.defaultRecordTypeId', fieldApiName: TYPE_OF_CONTRACT_FIELD}) typeOfContractPicklistValues;
    @wire(getPicklistValues, { recordTypeId: '$propertyObjectInfo.data.defaultRecordTypeId', fieldApiName: BRAND_HOTELS_FIELD}) brandHotelsPicklistValues;
    @wire(getPicklistValues, { recordTypeId: '$propertyObjectInfo.data.defaultRecordTypeId', fieldApiName: PLANNING_REGULATIONS_FIELD}) planningRegPicklistValues;
    @wire(getPicklistValues, { recordTypeId: '$propertyObjectInfo.data.defaultRecordTypeId', fieldApiName: PLOT_WITH_BUILDING_PERMISSION_FIELD}) plotBuildingPicklistValues;
    @wire(getPicklistValues, { recordTypeId: '$propertyObjectInfo.data.defaultRecordTypeId', fieldApiName: KIND_OF_DEVELOPMENT_FIELD}) kindOfDevelopmentPicklistValues;
    @wire(getPicklistValues, { recordTypeId: '$propertyObjectInfo.data.defaultRecordTypeId', fieldApiName: STATUS_FIELD}) statusPicklistValues;

    accordionElementList = null;
    checkboxFilterElementList = null;
    filterList = [];
    hasRendered = false;
    lastChangedFilterApiName = '';

    keyField = 'Id';
    @track columns = [
        { label: ' ', fieldName: 'Id', type: 'checkboxContainer', cellAttributes: { class: { fieldName: 'bgClass' }},typeAttributes: {disabledandchecked: {fieldName : 'disabledandchecked'}}, fixedWidth: 45, sortable: true },
        { label: accountNameLabel, fieldName: '_InvestorId',  type: 'url',  cellAttributes: { class: { fieldName: 'bgClass' }},typeAttributes: { label: {fieldName: 'InvestorName'}, target:'_blank'}, sortable: true },
        { label: firstnameLabel, fieldName: '_ContactIdForName',  type: 'url',  cellAttributes: { class: { fieldName: 'bgClass' }},typeAttributes: { label: {fieldName: 'FirstName'}, target:'_blank'}, sortable: true },
        { label: lastnameLabel, fieldName: '_ContactIdForSurname',  type: 'url', cellAttributes: { class: { fieldName: 'bgClass' }}, typeAttributes: { label: {fieldName: 'LastName'}, target:'_blank'}, sortable: true },
        { label: representativeLabel, fieldName: 'isRepresentative',  cellAttributes: { class: { fieldName: 'bgClass' }},sortable: true },
        { label: 'Rolle', fieldName: 'investorRole',  cellAttributes: { class: { fieldName: 'bgClass' }},sortable: true },
        { label: streetLabel, fieldName: 'InvestorBillingStreet',  cellAttributes: { class: { fieldName: 'bgClass' }},sortable: true },
        { label: cityLabel, fieldName: 'InvestorBillingCity',  cellAttributes: { class: { fieldName: 'bgClass' }},sortable: true },
        { label: emailLabel, fieldName: 'Email',  cellAttributes: { class: { fieldName: 'bgClass' }},sortable: true },
        { label: brandLabel, fieldName: '_InvestorBrandId',  type: 'url',  cellAttributes: { class: { fieldName: 'bgClass' }}, typeAttributes: { label: {fieldName: 'InvestorBrandName'}, target:'_blank'}, sortable: true },
        { label: activeLabel, fieldName: 'isContactActive', type: 'boolean',  cellAttributes: { class: { fieldName: 'bgClass' }},sortable: true },
        { label: acquisitionProfilesContact, fieldName: 'AcquisitionProfiles', sortable: true, cellAttributes: { class: { fieldName: 'bgClass' }} },
        { label: '', type: 'actionButtonRow', fieldName: 'Id', fixedWidth: 36, typeAttributes: {rowStatus: { fieldName: 'Stage'}}, cellAttributes: { class: { fieldName: 'bgClass' }}}

    ];

    @track filterVisibility = {
        hotelAssetClass: "filtering-element slds-hide",
        officeAssetClass: "filtering-element slds-hide",
        landAssetClass: "filtering-element slds-hide",
        industryAssetClass: "filtering-element slds-hide",
        retailAssetClass: "filtering-element slds-hide",
        residentialAssetClass: "filtering-element slds-hide",
        otherAssetClass: "filtering-element slds-hide"
    };

    @track conversionProperty = {
        data: {
            values: [
                {attributes: null, label: "conversionProperty", validFor: [], value: "true"}
            ]
        }
    };

    accountFormFields = ['Name', 'Brand__c', 'BillingStreet', 'BillingCity'];
    contactFormFields = ['AccountId', 'Title', 'FirstName', 'LastName', 'Position__c', 'Email'];

    connectedCallback() {
        registerListener('filterValueChanged', this.handleFilterValueChanged, this);
        registerListener('searchMatchingContactsCompleted', this.handleDataTableChange, this);
        this.prepareInitialFilter(this.propertyObject);
    }

    renderedCallback() {
        if (!this.hasRendered) {
            this.hasRendered = true;

            this.accordionElementList = this.template.querySelectorAll('lightning-accordion.section');
            this.checkboxFilterElementList = this.template.querySelectorAll('c-filter-element-checkbox-list');

            this.filterVisibility = {
                hotelAssetClass: this.isHotelAsset ? "filtering-element" : "filtering-element slds-hide",
                officeAssetClass: this.isOfficeAsset ? "filtering-element" : "filtering-element slds-hide",
                landAssetClass: this.isLandAsset ? "filtering-element" : "filtering-element slds-hide",
                industryAssetClass: this.isIndustryAsset ? "filtering-element" : "filtering-element slds-hide",
                retailAssetClass: this.isRetailAsset ? "filtering-element" : "filtering-element slds-hide",
                residentialAssetClass: this.isResidentialAsset ? "filtering-element" : "filtering-element slds-hide",
                otherAssetClass: this.isOtherAsset ? "filtering-element" : "filtering-element slds-hide"
            };

            for (const accordion of this.accordionElementList) {
                let tmpAccordionFilterObject = {accordionName: accordion.querySelector('lightning-accordion-section').name};
                const filters = accordion.querySelectorAll('.filter');
                let tmpFilterList = [];
                let isFilterActive = false;
                for (const filter of filters) {
                    const values = filter['activeValue'] && filter['activeValue'].length > 0 ? filter['activeValue'].split(';') : [];
                    const min = filter['min'];
                    const max = filter['max'];
                    tmpFilterList.push({
                        apiName: filter.getAttribute('data-api-name'),
                        label: filter['staticLabel'],
                        isActive: Boolean(values[0] || min || max),
                        values: values,
                        minValue: min,
                        maxValue: max
                    });
                    if (Boolean(values[0] || min || max)) {
                        if (tmpAccordionFilterObject.accordionName !== "investorenselektion") {
                            switch(tmpAccordionFilterObject.accordionName) {
                                case 'type-of-use-hotel':
                                    isFilterActive = this.isHotelAsset;
                                    break;
                                case 'type-of-use-office':
                                    isFilterActive = this.isOfficeAsset;
                                    break;
                                case 'type-of-use-land':
                                    isFilterActive = this.isLandAsset;
                                    break;
                                case 'type-of-use-industrie':
                                    isFilterActive = this.isIndustryAsset;
                                    break;
                                case 'type-of-use-retail':
                                    isFilterActive = this.isRetailAsset;
                                    break;
                                case 'type-of-use-residential':
                                    isFilterActive = this.isResidentialAsset;
                                    break;
                                case 'weitere':
                                    isFilterActive = this.isOtherAsset;
                                    break;
                            }
                        } else {
                            isFilterActive = true;
                        }
                    }
                }
                tmpAccordionFilterObject['filters'] = tmpFilterList;
                tmpAccordionFilterObject['isActive'] = isFilterActive;
                this.filterList.push(tmpAccordionFilterObject);
            }
            this.updateAccordionLabelState();
            this.handleRefreshTable();

            console.log('renderedCallback', this.filterList);
            console.log('>>>> renderedCallback ends <<<');
        }
    }

    disconnectedCallback() {
        unregisterAllListeners(this);
    }

    get isHotelAsset() {
        return this.propertyObject && this.propertyObject['TypeOfUse__c'] ? ['Hotel', 'Service Appartments', 'Student Housing'].includes(this.propertyObject['TypeOfUse__c']) : false;
    }

    get isOfficeAsset() {
        return this.propertyObject && this.propertyObject['TypeOfUse__c'] ? ['Office'].includes(this.propertyObject['TypeOfUse__c']) : false;
    }

    get isLandAsset() {
        return this.propertyObject && this.propertyObject['TypeOfUse__c'] ? ['Residential'].includes(this.propertyObject['TypeOfUse__c']) : false;
    }

    get isIndustryAsset() {
        return this.propertyObject && this.propertyObject['TypeOfUse__c'] ? ['Business Park', 'Logistics', 'Production'].includes(this.propertyObject['TypeOfUse__c']) : false;
    }

    get isRetailAsset() {
        return this.propertyObject && this.propertyObject['TypeOfUse__c'] ? ['RET: Discounter', 'RET: Warehouse', 'RET: Commercials', 'RET: Shopping-Center', 'RET: Supermarket'].includes(this.propertyObject['TypeOfUse__c']) : false;
    }

    get isOtherAsset() {
        return this.propertyObject && this.propertyObject['TypeOfUse__c'] ? ['Mixed-Use', 'Other', 'Nursing Home', 'Datacenter'].includes(this.propertyObject['TypeOfUse__c']) : false;
    }

    get isResidentialAsset() {
        return this.propertyObject && this.propertyObject['TypeOfUse__c'] ? ['Residential'].includes(this.propertyObject['TypeOfUse__c']) : false;
    }

    get labels() {
        const fieldList = [
            'TypeOfUse__c', 'OccupancyRate__c', 'AmountOfSpaceLet__c', 'TypeOfRealEstate__c', 'RiskCategory__c', 'MacroLocation__c', 'Submarket__c', 'PurchasingPriceOffer__c',
            'SqmInTotal__c', 'WALT__c', 'Certification__c', /* 'YearofConstruction__c', */'NumberOfRooms__c', 'Category__c', 'LocationType__c', 'TypeOfContract__c',
            'BrandHotels__c', 'SqmOffice__c', 'RealizableBGF__c', 'SqmPlot__c', 'PlanningRegulations__c', 'PlotWithBuildingPermission__c', 'SqmLogistics__c',
            'SqmRetail__c', 'AccomodationUnit__c', 'IRR__c', 'CoC__c', 'EquityMultiplier__c', 'FactorActual__c', 'KindOfDevelopment__c', 'ConversionProperty__c',
            'Status__c'
        ];

        let result = {};
        if (this.propertyObjectInfo && this.propertyObjectInfo["data"]) {
            // retrieves field labels from propertyObjectInfo right after it's loading
            for (const field of fieldList) {
                result[field] = this.propertyObjectInfo["data"]["fields"][field]["label"];
            }
        } else {
            // otherwise sets empty values for labels
            for (const field of fieldList) {
                result[field] = "";
            }
        }

        return result;
    }
    label = {
        investmentVolume
    };

    prepareInitialFilter(propertyObject) {
        console.log('>>>> prepareInitialFilter starts <<<');
        propertyObject = propertyObject ? propertyObject : {};
        this.initialFilterObject['TypeOfUse'] = propertyObject['TypeOfUse__c'] ? propertyObject['TypeOfUse__c'] : '';
        this.initialFilterObject['TypeOfRealEstate'] = propertyObject['TypeOfRealEstate__c'] ? propertyObject['TypeOfRealEstate__c'] : '';
        this.initialFilterObject['RiskCategory'] = propertyObject['RiskCategory__c'] ? propertyObject['RiskCategory__c'] : '';
        this.initialFilterObject['MacroLocation'] = propertyObject['MacroLocation__c'] ? propertyObject['MacroLocation__c'] : '';
        this.initialFilterObject['Submarket'] = propertyObject['Submarket__c'] ? propertyObject['Submarket__c'] : '';
        this.initialFilterObject['PurchasingPriceOffer'] = this.calculateRange(propertyObject['PurchasingPriceOffer__c'], 10);
        this.initialFilterObject['SqmInTotal'] = this.calculateRange(propertyObject['SqmInTotal__c'], 10);
        this.initialFilterObject['OccupancyRate'] = this.calculateDiapasonAndRange(propertyObject['OccupancyRate__c'], 15, 0, 100);
        this.initialFilterObject['AmountOfSpaceLet'] = this.calculateDiapasonAndRange(propertyObject['AmountOfSpaceLet__c'], 15, 0, 100);
        this.initialFilterObject['WALT'] = this.calculateDiapasonAndRange(propertyObject['WALT__c'], 15, 1, 20);
        this.initialFilterObject['Certification'] = propertyObject['Certification__c'] ? propertyObject['Certification__c'] : '';
        // Deprecated (US 2413)
        // this.initialFilterObject['YearofConstruction'] = this.calculateDiapasonAndRange(propertyObject['YearofConstruction__c'], 1, 1900, 2030);
        this.initialFilterObject['NumberOfRooms'] = this.calculateRange(propertyObject['NumberOfRooms__c'], 10);
        this.initialFilterObject['Category'] = propertyObject['Category__c'] ? propertyObject['Category__c'] : '';
        this.initialFilterObject['LocationType'] = propertyObject['LocationType__c'] ? propertyObject['LocationType__c'] : '';
        this.initialFilterObject['TypeOfContract'] = propertyObject['TypeOfContract__c'] ? propertyObject['TypeOfContract__c'] : '';
        this.initialFilterObject['BrandHotels'] = propertyObject['BrandHotels__c'] ? propertyObject['BrandHotels__c'] : '';
        this.initialFilterObject['SqmOffice'] = this.calculateRange(propertyObject['SqmOffice__c'], 10);
        this.initialFilterObject['RealizableBGF'] = this.calculateRange(propertyObject['RealizableBGF__c'], 10);
        this.initialFilterObject['SqmPlot'] = this.calculateRange(propertyObject['SqmPlot__c'], 10);
        this.initialFilterObject['PlanningRegulations'] = propertyObject['PlanningRegulations__c'] ? propertyObject['PlanningRegulations__c'] : '';
        this.initialFilterObject['PlotWithBuildingPermission'] = propertyObject['PlotWithBuildingPermission__c'] ? propertyObject['PlotWithBuildingPermission__c'] : '';
        this.initialFilterObject['SqmLogistics'] = this.calculateRange(propertyObject['SqmLogistics__c'], 10);
        this.initialFilterObject['SqmRetail'] = this.calculateRange(propertyObject['SqmRetail__c'], 10);
        this.initialFilterObject['AccomodationUnit'] = this.calculateRange(propertyObject['AccomodationUnit__c'], 10);
        this.initialFilterObject['IRR'] = this.calculateDiapasonAndRange(propertyObject['IRR__c'], 15, 0, 100);
        this.initialFilterObject['CoC'] = this.calculateDiapasonAndRange(propertyObject['CoC__c'], 15, 0, 100);
        this.initialFilterObject['EquityMultiplier'] = this.calculateDiapasonAndRange(propertyObject['EquityMultiplier__c'], 15, 0, 100);
        this.initialFilterObject['FactorActual'] = this.calculateDiapasonAndRange(propertyObject['FactorActual__c'], 15, 0, 100);
        this.initialFilterObject['KindOfDevelopment'] = propertyObject['KindOfDevelopment__c'] ? propertyObject['KindOfDevelopment__c'] : '';
        this.initialFilterObject['ConversionProperty'] = propertyObject['ConversionProperty__c'] ? 'true' : '';
        this.initialFilterObject['Status'] = propertyObject['Status__c'] ? propertyObject['Status__c'] : '';
        this.isInitialFilterPrepared = true;
        console.log('>>>> prepareInitialFilter ends <<<');
    }

    calculateRange(value, percentage) {
        if (value) {
            value = parseInt(value);
            let diapason = Math.round((percentage * value) / 100);
            return { min: value - diapason > 0 ? value - diapason : 0, max: value + diapason };
        }
        return {min: '', max: ''};
    }

    calculateDiapasonAndRange(value, percentage, start, end) {
        let result = {};
        if (value) {
            result = this.calculateRange(value, percentage);
            result['min'] = result['min'] > start ? result['min'] : start;
            result['max'] = result['max'] < end ? result['max'] : end;
        } else {
            result = {min: "", max: ""};
        }
        result['start'] = start;
        result['end'] = end;
        return result;
    }

    handleFilterValueChanged(payload) {
        console.log('InvestorSelectionObjectMatching:handleFilterValueChanged', payload);

        for (let i = 0; i < this.filterList.length; i++) {
            for (let j = 0; j < this.filterList[i]['filters'].length; j++) {
                if (this.filterList[i]['filters'][j]['label'] === payload['label']){
                    console.log('YYY', payload['label'], JSON.stringify(payload['values']), payload['min'], payload['max'], payload['isActive']);
                    this.lastChangedFilterApiName = this.filterList[i]['filters'][j]['apiName'];
                    this.filterList[i]['filters'][j]["isActive"] = JSON.parse(payload['isActive']);
                    try {
                        this.filterList[i]['filters'][j]["values"] = JSON.parse(payload['values']);
                    } catch (e) {
                        this.filterList[i]['filters'][j]["values"] = null;
                    }
                    this.filterList[i]['filters'][j]["minValue"] = payload['minValue'];
                    this.filterList[i]['filters'][j]["maxValue"] = payload['maxValue'];
                    if (payload['isActive']) {
                        this.filterList[i]["isActive"] = true;
                    } else {
                        this.filterList[i]["isActive"] = this.isFilterActive(this.filterList[i]['filters']);
                    }
                }
            }
        }
        console.log('handleFilterValueChanged', this.filterList);
        this.updateAccordionLabelState();
        this.handleRefreshTable();
    }

    /**
     * @azure US 1710 Dynamic search blocks
     */
    updateTypeOfUseAccordionsVisibility(values) {
        if (values && Array.isArray(values)) {
            this.updateAccordionVisibility(this.getAccordionByName("type-of-use-hotel"), "hotelAssetClass",
                values.includes('Hotel') || values.includes('Service Appartments') || values.includes('Student Housing'));
            this.updateAccordionVisibility(this.getAccordionByName("type-of-use-office"), "officeAssetClass",
                values.includes('Office'));
            this.updateAccordionVisibility(this.getAccordionByName("type-of-use-land"), "landAssetClass",
                values.includes('Residential'));
            this.updateAccordionVisibility(this.getAccordionByName("type-of-use-industrie"), "industryAssetClass",
                values.includes('Business Park') || values.includes('Logistics') || values.includes('Production'));
            this.updateAccordionVisibility(this.getAccordionByName("type-of-use-retail"), "retailAssetClass",
                values.includes('RET: Discounter') || values.includes('RET: Warehouse') || values.includes('RET: Commercials') || values.includes('RET: Shopping-Center') || values.includes('RET: Supermarket'));
            this.updateAccordionVisibility(this.getAccordionByName("type-of-use-residential"), "residentialAssetClass",
                values.includes('Residential'));
            this.updateAccordionVisibility(this.getAccordionByName("weitere"), "otherAssetClass",
                values.includes('Mixed-Use') || values.includes('Other') || values.includes('Nursing Home') || values.includes('Datacenter'));
        }
    }

    updateAccordionVisibility(filter, filterSectionClass, isVisible) {
        if (isVisible) {
            this.filterVisibility[filterSectionClass] = "filtering-element";
            filter["isActive"] = true;
        } else {
            this.filterVisibility[filterSectionClass] = "filtering-element slds-hide";
            filter["isActive"] = false;
            // accordion block is not visible -> set it's filters isActive = false
            let doResultTableRefresh = false;
            for (let i = 0; i < filter['filters'].length; i++) {
                if (filter['filters'][i]['isActive']) {
                    doResultTableRefresh = true;
                }
                filter['filters'][i]['isActive'] = false;
            }
            if (doResultTableRefresh) {
                this.handleRefreshTable();
            }
        }
    }

    getAccordionByName(name) {
        let result = null;
        for (const filter of this.filterList) {
            if (filter['accordionName'] === name) {
                result = filter;
                break;
            }
        }
        return result;
    }

    getFilterByFieldApiName(fieldApiName) {
        for (let i = 0; i < this.filterList.length; i++) {
            for (let j = 0; j < this.filterList[i]['filters'].length; j++) {
                if (this.filterList[i]['filters'][j]['apiName'] === fieldApiName){
                    return this.filterList[i]['filters'][j];
                }
            }
        }
        return {};
    }

    isFilterActive(filter) {
        for (const item of filter) {
            if (item["isActive"]) {
                return true;
            }
        }
        return false;
    }

    updateAccordionLabelState() {
        for (let i = 0; i < this.filterList.length; i++) {
            this.filterList[i]['isActive'] && this.isFilterActive(this.filterList[i]['filters']) ?
                this.accordionElementList[i].classList.add("accordion-active") :
                this.accordionElementList[i].classList.remove("accordion-active");
            if (this.filterList[i]['accordionName'] === "investorenselektion") {
                for (let j = 0; j < this.filterList[i]['filters'].length; j++) {
                    if (this.filterList[i]['filters'][j]['label'] === "Nutzungsart"){
                        this.updateTypeOfUseAccordionsVisibility(this.filterList[i]['filters'][j]['values']);
                    }
                }
            }
        }
    }

    handleRefreshTable()
    {
        console.log('>>> handleRefreshTable starts <<<');
        const object = {
            tableMetadata: {
                columns: this.columns,
                keyField: this.keyField,
            },
            formMetadata: {
                fields: {
                    account: this.accountFormFields,
                    contact: this.contactFormFields
                },
                columns: 2
            },
            filterList: JSON.stringify(this.filterList)
        }

        //Notify investorSelectionDatatable.js to search for records.
        fireEvent(this.pageRef, 'InvestorSelection__matchingRecords', object);


        console.log('>>> handleRefreshTablet ends <<<');
    }

    /**
     * @description
     */
    handleDataTableChange(payload) {
        console.log('InvestorSelectionObjectMatching:handleDataTableChange', JSON.parse(JSON.stringify(payload)));

        /**
         * @deprecated US 1179 Dynamic Matching-Parameters
         if (payload['matchingRecords'].length !== 0) {
            let service = new AcquisitionProfileService(payload['acquisitionProfiles']);
            // do not change filters dynamically if there is no matchingRecords in the search results
            for (const elem of this.checkboxFilterElementList) {
                const propertyApiName = elem.getAttribute('data-api-name');
                if (this.lastChangedFilterApiName !== propertyApiName) {
                    let matchingAcquisitionFiled = this.propertyToAcquisitionProfileMatchingMap[propertyApiName];
                    if (matchingAcquisitionFiled) {
                        const hasEmptyOrSameEntryValues = service.hasEmptyOrSameEntryValues(matchingAcquisitionFiled);
                        elem.setVisible(!hasEmptyOrSameEntryValues);

                        let filter = this.getFilterByFieldApiName(propertyApiName);
                        if (hasEmptyOrSameEntryValues) {
                            filter['isActive'] = false;

                            if (filter['apiName'] === 'TypeOfUse__c') {
                                this.updateAccordionVisibility(this.getAccordionByName("type-of-use-hotel"), "hotelAssetClass", false);
                                this.updateAccordionVisibility(this.getAccordionByName("type-of-use-office"), "officeAssetClass", false);
                                this.updateAccordionVisibility(this.getAccordionByName("type-of-use-land"), "landAssetClass", false);
                                this.updateAccordionVisibility(this.getAccordionByName("type-of-use-industrie"), "industryAssetClass", false);
                                this.updateAccordionVisibility(this.getAccordionByName("type-of-use-retail"), "retailAssetClass", false);
                                this.updateAccordionVisibility(this.getAccordionByName("type-of-use-residential"), "residentialAssetClass", false);
                                this.updateAccordionVisibility(this.getAccordionByName("weitere"), "otherAssetClass", false);
                            }
                        }
                    } else {
                        console.log('matchingAcquisitionFiled', propertyApiName, 'not mapped to AcquisitionProfile!!!');
                        elem.setVisible(false);
                    }
                }
            }
        }
         */

        if (!this.isTableLoaded) {
            this.isTableLoaded = true;
            this.template.querySelector('[data-id="filter-block"]').classList.remove("slds-hide");
        }
    }
}