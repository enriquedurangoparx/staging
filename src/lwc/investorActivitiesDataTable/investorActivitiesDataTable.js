import LightningDatatable from 'lightning/datatable';
import investorActivityContactsTemplate from './investorActivityContactTemplate.html';

export default class InvestorActivitiesDataTable extends LightningDatatable {
    static customTypes = {
        investorActivityContacts: {
            template: investorActivityContactsTemplate,
            typeAttributes: ['customValue'],
        }
    };
}