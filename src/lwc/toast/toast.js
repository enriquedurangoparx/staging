import { LightningElement, api, track } from 'lwc';
//import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import LANG from '@salesforce/i18n/lang';

export default class Toast extends LightningElement {
    //This component based on https://www.lightningdesignsystem.com/components/toast/

    @api state;// possible values: Success, Warning, Error
    @api mode;// possible values: Not Closable, Closable
    @api germanMessage;
    @api englishMessage;

    @track showMessage = true;// Indicate if component is visible or not.

    _userLanguage = LANG;// Determines the user's _userLanguage

    connectedCallback(){
        /*const evt = new ShowToastEvent({
            title: 'Title',
            message: 'Message',
            variant: 'success',
            mode: 'sticky'
        });
        this.dispatchEvent(evt);*/
    }

    /********************
     * HANDLE EVENTS
     ********************/
    handleCloseClickHandler(event){
        this.showMessage = false;
    }

    /********************
     * TEMPLATE
     ********************/
    get theme(){
        let returnValue = 'slds-notify slds-notify_toast ';
        switch (this.state) {
            case 'Success':
                returnValue += 'slds-theme_success';
                break;
            case 'Warning':
                returnValue += 'slds-theme_warning';
                break;
            case 'Error':
                returnValue += 'slds-theme_error';
                break;
            default:
                break;
        }

        return returnValue;
    }

    get iconName(){
        let returnValue = '';

        switch (this.state) {
            case 'Success':
                returnValue = 'utility:success';
                break;
            case 'Warning':
                returnValue = 'utility:warning';
                break;
            case 'Error':
                returnValue = 'utility:error';
                break;
            default:
                break;
        }

        return returnValue;
    }

    get iconVariant(){
        return ((typeof this.state !== 'undefined' && (this.state === 'Success' || this.state === 'Error')) ? 'inverse' : '');
    }

    get isClosable(){
        return ((typeof this.mode !== 'undefined' && this.mode === 'Not Closeable') ? false : true);
    }

    get message(){
        return (this._isGerman_userLanguage() ? this.germanMessage : this.englishMessage);
    }

    /********************
     * HELPER FUNCTIONS
     ********************/
    _isGerman_userLanguage(){
        return (this._userLanguage === 'de' ? true : false);
    }

}