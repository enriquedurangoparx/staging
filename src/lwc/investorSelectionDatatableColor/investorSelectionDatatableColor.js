import LightningDatatable from 'lightning/datatable';

import checkboxContainer from './checkboxContainer.html';
import actionButtonRow from "./actionButtonsContainer.html";

export default class InvestorSelectionDatatableColor extends LightningDatatable {
    static customTypes = {
        checkboxContainer: {
            template: checkboxContainer,
            typeAttributes: ['disabledandchecked', 'tableName'],
        },
        actionButtonRow: {
            template: actionButtonRow,
            typeAttributes: ['rowStatus']
        }
    };
}