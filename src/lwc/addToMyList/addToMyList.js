import { LightningElement, api, track } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { NavigationMixin } from 'lightning/navigation';
import { reduceErrors } from 'c/lwcUtil';

import recordsSearch from '@salesforce/apex/SearchController.search';
import createInvestorListItem from '@salesforce/apex/AddToMyListController.createInvestorListItemRecord';

// import custom labels:
import applicationNameLabel from '@salesforce/label/c.AddToMyListApplicationName';
import addActionLabel from '@salesforce/label/c.AddToMyListAddAction';
import searchLabel from '@salesforce/label/c.AddToMyListSearchLabel';
import searchPlaceholderLabel from '@salesforce/label/c.AddToMyListSearchPlaceholderLabel';
import contactIncludedNotificationLabel from '@salesforce/label/c.AddToMyListContactIncludedNotification';
import addToMyListHereLabel from '@salesforce/label/c.AddToMyListHere';


export default class addToMyList extends NavigationMixin(LightningElement)
{
    @api objectApiName;
    @api recordId;

    label = {
        applicationNameLabel,
        addActionLabel,
        searchLabel,
        searchPlaceholderLabel,
        contactIncludedNotificationLabel,
        addToMyListHereLabel
    }

    // configuration-objects for lookup components
    searchString = {
        objectApiName: 'InvestorsList__c', 
        fieldApiName: 'Name', 
        searchTerm: '', 
        isLikeSearch: true, 
        orderBy: 'Name ASC', 
        isMultiEntry: false,
        addNewRecordModeIsOn: false
    };

    @track
    selectedRecordId;

    // add new record functionality
    addRecordObjectApiName = 'InvestorsList__c';
    addRecordFields = ['Name', 'Description__c', 'AccessType__c'];


    get recordSelected()
    {
        return this.selectedRecordId ? false : true;
    }

    handleRecordsSearch(event)
    {
        this.searchString.searchTerm = event.detail.searchTerm;
        let searchParam = {
            searchJSON: JSON.stringify(this.searchString)
        };

        recordsSearch(searchParam)
            .then(results => {
                this.template
                    .querySelector('[data-id="nameSearchInput"]')
                    .setSearchResults(results);
            })
            .catch(error => {
                this.notifyUser(
                    'Lookup Error',
                    'An error occured while searching with the lookup field.\n' +
                        reduceErrors(error).join(', '),
                    'error'
                );
            });
    }

    handleRecordChange(event)
    {
        if(event.target.selection && event.target.selection.length > 0) {
            this.selectedRecordId = event.target.selection[0].id;
        } else {
            this.selectedRecordId = undefined;
        }
    }

    handleRecordSelectionChange(event)
    {
        if(event.target.selection && event.target.selection.length > 0) {
            for(let i=0; i<event.target.selection.length; i++) {
                this.selectedRecordId = event.target.selection[i].id;
            }
        } else {
            this.selectedRecordId = undefined;
        }
    }

    handleCreateRecord()
    {
        let params = {
            recordId: this.recordId,
            investorListId: this.selectedRecordId,
            objApiName: this.objectApiName
        }
        createInvestorListItem(params)
        .then(results => {
            let resp = JSON.parse(results);
            if (resp.isSuccess)
            {
                this.notifyUser(
                    resp.title,
                    resp.message,
                    resp.recordId,
                    resp.variant
                );
            }
            else
            {
                this.notifyUser(
                    resp.title,
                    resp.message,
                    resp.variant
                );
            }
            
        })
    }

    /**
     * notify user
     * @param {string} title
     * @param {string} message
     * @param {string} variant
     */
    notifyUser(title, message, variant) {
        const toastEvent = new ShowToastEvent({
            title,
            message,
            variant
        });
        this.dispatchEvent(toastEvent);
    }

     /**
     * notify user
     * @param {string} title
     * @param {string} message
     * @param {string} variant
     */
    notifyUser(title, message, recId, variant) {
        const event = new ShowToastEvent({
            "title": title,
            "message": message,
            "variant": variant,
            "messageData": [
                {
                    url: '/' + recId,
                    label: this.label.addToMyListHereLabel
                }
            ]
        });
        this.dispatchEvent(event);
    }
}