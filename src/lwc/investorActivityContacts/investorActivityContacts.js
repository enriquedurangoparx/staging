import { LightningElement, api } from 'lwc';

export default class InvestorActivityContacts extends LightningElement {
    @api contacts;
}