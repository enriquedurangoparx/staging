import { LightningElement, track, wire } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { registerListener, unregisterAllListeners } from 'c/pubsub';
import { CurrentPageReference } from 'lightning/navigation';
import loadInvestorsList from '@salesforce/apex/InvestorSelectionHelper.loadInvestorsList';

import pleaseChooseListLabel from '@salesforce/label/c.PleaseChooseList';
import generalLabelNoRecordsToQuery from '@salesforce/label/c.GeneralLabelNoRecordsToQuery';
import generalLabelShowMore from '@salesforce/label/c.GeneralLabelShowMore';

export default class investorSelectionMyLists extends LightningElement 
{
    @wire(CurrentPageReference) pageRef;
    @track investorsList;
    @track isInvestorListVisible;
    opportunityId = new URL(window.location.href).searchParams.get('c__recordId');

    label = {
        pleaseChooseListLabel,
        generalLabelNoRecordsToQuery,
        generalLabelShowMore
    };

    connectedCallback()
    {
        this.isInvestorListVisible = true;
        registerListener('investorSelectionInvestorList-getInvestors', this.loadInvestorsList, this);
        this.loadInvestorsList();
    }

    investorsListLimitSize = 25;
    investorsListLimitSizeStep = 25;
    investorsListLoadedSize = 0;

    @track
    isLoading = false;

    loadInvestorsList()
    {
        this.investorsListLimitSize = 25;
        this.isLoading = true;
        loadInvestorsList({limitSize: this.investorsListLimitSize})
        .then( result => {
            let tempResult = JSON.parse(result);
            this.investorsList = tempResult.investorsList;
            this.isLoading = false;

            this.investorsListLoadedSize = this.investorsList ? this.investorsList.length : 0;
        }).catch(error => {
            console.log('error: ', error);
            this.isLoading = false;
        });
    }

    loadMoreInvestorsList()
    {
        this.isLoading = true;
        this.investorsListLimitSize = this.investorsListLimitSize + this.investorsListLimitSizeStep;
        loadInvestorsList({limitSize: this.investorsListLimitSize})
        .then( result => {
            let tempResult = JSON.parse(result);
            this.investorsList = tempResult.investorsList;

            if (this.investorsListLoadedSize === this.investorsList.length)
            {
                this.notifyUser(undefined, this.label.generalLabelNoRecordsToQuery,'info');
            }

            this.investorsListLoadedSize = this.investorsList ? this.investorsList.length : 0;
            this.isLoading = false;
        }).catch(error => {
            console.log('error: ', error);
            this.isLoading = false;
        });
    }

    /**
     * notify user
     * @param {string} title
     * @param {string} message
     * @param {string} variant
     */
    notifyUser(title, message, variant) {
        const toastEvent = new ShowToastEvent({
            title,
            message,
            variant
        });
        this.dispatchEvent(toastEvent);
    }
}