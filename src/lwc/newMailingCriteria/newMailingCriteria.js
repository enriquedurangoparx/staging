/**
 * Created by npo for US2403.
 */

import { LightningElement, track, wire, api } from 'lwc';
import correspondenceNameLabel from '@salesforce/label/c.CorrespondenceName';
import investorStatusLabel from '@salesforce/label/c.InvestorStatus';
import senderLabel from '@salesforce/label/c.Sender';
import bccLabel from '@salesforce/label/c.Bcc';
import bccHelpMessageLabel from '@salesforce/label/c.NewMailingBccHelpMessage';
import invalidChosenUserLabel from '@salesforce/label/c.InvalidChosenUser';
import { getPicklistValues } from 'lightning/uiObjectInfoApi';
import { getObjectInfo } from 'lightning/uiObjectInfoApi';
import STATUS_FIELD from '@salesforce/schema/AccountToOpportunity__c.Status__c';
import INVESTOR_OBJECT from '@salesforce/schema/AccountToOpportunity__c';
import lookupSearch from '@salesforce/apex/LookupController.search';
import findPermissionsForCurrentUser from '@salesforce/apex/MailPermissionService.findPermissionsForCurrentUser';
import { reduceErrors } from 'c/lwcUtil';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import Id from '@salesforce/user/Id';

import { getRecord } from 'lightning/uiRecordApi';
import USER_NAME_FIELD from '@salesforce/schema/User.Name';
import OPPORTUNITY_NAME_FIELD from '@salesforce/schema/Opportunity.Name';
import OPPORTUNITY_OWNER_NAME_FIELD from '@salesforce/schema/Opportunity.Owner.Name';
import OPPORTUNITY_OWNER_ID_FIELD from '@salesforce/schema/Opportunity.OwnerId';

const INVESTOR_STATUS_AVAILABLE_OPTIONS = [
    'Initial Contact',
	'Teaser sent',
	'NDA sent',
	'IM sent',
	'Process Letter sent',
	'Site Visit',
	'Further Documents',
	'Pre-DD Dataroom',
	'Bidder Meeting',
	'Letter of Intent',
	'Data Room'
];

export default class NewMailingCriteria extends LightningElement {
	label = {
		correspondenceNameLabel,
		investorStatusLabel,
		senderLabel,
		bccLabel,
		invalidChosenUserLabel,
		bccHelpMessageLabel
	}

    userField = {objectApiName: "Campaign", apiName: "Sender__c", isMultiEntry: false};

	@track
	investorStatusOptions;
	@track
	error;
	@track
	chosenUserIsLoaded = false;

	@api
	correspondenceName;
	@api
	bcc;
	@api
	investorStatus;
	@api
	chosenUser;
	@api
	opportunityId;
	@api
	isFormEditingDisabled = false;


	@wire(getObjectInfo, { objectApiName: INVESTOR_OBJECT })
	handleResult({error, data}) {
		if(data) {
			this.investorDefaultRecordTypeId = data.defaultRecordTypeId;
		}
	}

	@wire(getPicklistValues, { recordTypeId: '$investorDefaultRecordTypeId', fieldApiName: STATUS_FIELD })
	handlePVResult({error, data}) {
	    let availableStatuses = new Set();
	    INVESTOR_STATUS_AVAILABLE_OPTIONS.forEach(item => availableStatuses.add(item))
		if(data) {
			this.investorStatusOptions = [];
			let self = this;
			data.values.forEach(function(item){
			    console.log('...item.value.toString(): ' + item.value.toString())
			    if (availableStatuses.has(item.value.toString())) {
			        self.investorStatusOptions.push(item);
				}
			})
		}
	}

   @wire(getRecord, { recordId: Id, fields: [USER_NAME_FIELD]})
   wireuser({error, data}) {
		if (data) {
			this.currentUserName = data.fields.Name.value;
		}
	}

	@wire(getRecord, {
		recordId: '$opportunityId',
		fields: [OPPORTUNITY_OWNER_ID_FIELD, OPPORTUNITY_OWNER_NAME_FIELD]
	})
	opportunity({error, data}) {
		if (data) {
		    console.log('...opportunity owner' + JSON.stringify(data.fields.Owner.displayValue));
			this.opportunityOwnerName = data.fields.Owner.displayValue;
			this.opportunityOwnerId = data.fields.OwnerId.value;
			this.setDefaultSender();
		}
	}

 	connectedCallback()
 	{
		findPermissionsForCurrentUser()
		.then(result => {
			this.mailPermissions = result;
		})
		.catch(error => {
			console.log('...findPermissionsForCurrentUser.error: ' + error);
		});
	}

	handleInvestorStatusChange(event){}

	handleUserLookupSearch(event) {
		var searchParam = {
			parentObjectApiName: this.userField.objectApiName,
			lookupApiName: this.userField.apiName,
			searchTerm: event.detail.searchTerm
		};
		lookupSearch(searchParam)
			.then(results => {
				this.template
					.querySelector('[data-id="nameSearchInput"]')
					.setSearchResults(results);
			})
			.catch(error => {
				this.notifyUser(
					'Lookup Error',
					'An error occured while searching with the lookup field.\n' +
						reduceErrors(error).join(', '),
					'error'
				);
			});
	}

	/**
	 * notify user
	 * @param {string} title
	 * @param {string} message
	 * @param {string} variant
	 */
	notifyUser(title, message, variant) {
		const toastEvent = new ShowToastEvent({
			title,
			message,
			variant
		});
		this.dispatchEvent(toastEvent);
	}

	handleUserLookupChange(event) {
		let selection = event.target.selection;
		let chosenUser = selection.length > 0 ? selection[0] : undefined;
		this.chosenUser = chosenUser;
		if (chosenUser !== undefined && !this.validateChosenUser())
		{
		    this.error = this.label.invalidChosenUserLabel;
		}
		else
		{
		    this.error = '';
		}
		this.sendDataChangeEvent();
	}

	validateChosenUser()
	{
	    let chosenUser = this.chosenUser;
	    let valid = false;
	    if (chosenUser === undefined)
	    {
			return valid;
		}
		if (this.mailPermissions !== undefined && this.mailPermissions.length > 0)
		{
			this.mailPermissions.forEach(function(permission){
			    let delegated = permission.DelegatedUser__c.toString();
			    let user = permission.User__c.toString();

				if (user === chosenUser.id.toString())
				{
					valid = true;
				}
			});
		}
		return valid || Id === chosenUser.id;
	}

	handleCorrespondenceNameChange(event)
	{
		this.correspondenceName = event.target.value;
		this.sendDataChangeEvent();
	}

	handleInvestorStatusChange(event)
	{
		this.investorStatus = event.target.value;
		this.sendDataChangeEvent();
	}

	handleBccChange(event)
	{
		this.bcc = event.target.value;
		this.sendDataChangeEvent();
	}

	sendDataChangeEvent()
	{
		const event = CustomEvent('datachange', {
			detail: {
				correspondenceName: this.correspondenceName,
				investorStatus: this.investorStatus,
				sender: this.chosenUser,
				bcc: this.bcc
			},
		});
		this.dispatchEvent(event);
	}

	setDefaultSender()
	{
	    console.log('...setDefaultSender:' + this.opportunityOwnerId + ' ' + this.opportunityOwnerName);
		if (!this.chosenUser)
		{
			this.chosenUser =  {
				id: this.opportunityOwnerId,//this.opportunity.OwnerId,
				sObjectType: 'User',
				icon: 'standard:user',
				title: this.opportunityOwnerName//this.opportunity.Owner ? this.opportunity.Owner.Name : 'tetet'
			}
			this.sendDataChangeEvent();
			console.log('...setDefaultSender.chosenUser:' + JSON.stringify(this.chosenUser));
		}
		this.senderId = this.chosenUser.id;
		this.chosenUserIsLoaded = true;
	}
}