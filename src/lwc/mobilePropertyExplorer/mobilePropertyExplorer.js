import {LightningElement, track, wire} from 'lwc';
import searchProperties from '@salesforce/apex/MobilePropertyExplorerController.getProperties';
import { CurrentPageReference } from 'lightning/navigation';
import {registerListener} from 'c/pubsub';

import { getPicklistValues } from 'lightning/uiObjectInfoApi';
import { getObjectInfo } from 'lightning/uiObjectInfoApi';
import PROPERTY_OBJECT from '@salesforce/schema/PropertyObject__c';
import TYPEOFUSE_FIELD from '@salesforce/schema/PropertyObject__c.TypeOfUse__c';

// Custom Labels
import filter from '@salesforce/label/c.mobilePropertyExplorerFilter';
import hideFilter from '@salesforce/label/c.mobilePropertyExplorerHideFilter';
import distance from '@salesforce/label/c.mobilePropertyExplorerDistance';
import rentedSpace from '@salesforce/label/c.mobilePropertyExplorerRentedSpace';
import available from '@salesforce/label/c.mobilePropertyExplorerAvailable';
import chosen from '@salesforce/label/c.mobilePropertyExplorerChosen';
import list from '@salesforce/label/c.mobilePropertyExplorerList';
import map from '@salesforce/label/c.mobilePropertyExplorerMap';

export default class MobilePropertyExplorer extends LightningElement {
    @wire(CurrentPageReference) pageRef;
    @track location = {};
    @track _selectedTypesOfUse = [];
    isLoading = false;
    properties;

    objectAndFieldApiNameTypeOfUse = TYPEOFUSE_FIELD;

    // Filter
    @track showFilters = false;
    @track minDistance = 0;
    @track maxDistance = 0.2;
    @track minRented = 0;
    @track maxRented = 100;
    @track typeOfUse = '';

    @track urlParams;

    label = {
      filter,
      hideFilter,
      distance,
      rentedSpace,
      available,
      chosen,
      list,
      map
     };


    connectedCallback() {
        this.getLocation();
        registerListener('distanceFilterEvent', this.handleDistanceChange, this);
        registerListener('rentedSpaceFilterEvent', this.handleRentedSpaceFilterChange, this);
    }

    getProperties(){
        this.isLoading = true;
       
        searchProperties({
          latitude: this.location.latitude,
          longitude: this.location.longitude,
          distanceMin: this.minDistance,
          distanceMax: this.maxDistance,
          rentedMin: this.minRented,
          rentedMax : this.maxRented,
          typeOfUse : this.typeOfUse
        })
        .then(result => {
            const tempResult = JSON.parse(JSON.stringify(result));
            let _properties = this._preprocessData(tempResult);
            this.properties = JSON.parse(JSON.stringify(_properties));

            const mobileList = this.template.querySelector('c-mobile-property-explorer-list');
            mobileList.setProperties(this.properties);

            this.handleChangeSearchProperties();

            this.isLoading = false;
        }).catch(error => {
            this.isLoading = false;
        })
        this.isLoading = false;
    }

    _preprocessData(result){
        let preprocessData = [];
        result.forEach(element => {
            if(typeof element.PropertyOwner__c !== 'undefined'){
                element.propertyOwner = element.PropertyOwner__r.Name;
            }
            preprocessData.push(element);
        });

       return preprocessData;
    }

    getLocation() {
        this.isLoading = true;
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition((position) => {
            this.location.latitude = position.coords.latitude;
            this.location.longitude = position.coords.longitude;

            this.getProperties();
          });
        }
    }

      handleToggleFilter(){
        this.showFilters = !this.showFilters;
      }

      handleDistanceChange(filters) {
        this.minDistance = filters.minValue / 1000;
        this.maxDistance = filters.maxValue / 1000;

        this.getProperties();
    }

      handleRentedSpaceFilterChange(filters){
        this.minRented = filters.minValue;
        this.maxRented = filters.maxValue;

        this.getProperties();
      }


      @wire(getObjectInfo, { objectApiName: PROPERTY_OBJECT })
      objectInfo;

      @wire(getPicklistValues, { recordTypeId: '$objectInfo.data.defaultRecordTypeId', fieldApiName: TYPEOFUSE_FIELD})
      typeOfUseValues;

      handleChange(event) {
          this._selectedTypesOfUse = event.detail.value;

          this.typeOfUse = '';
          for(let i = 0; i < this._selectedTypesOfUse.length; i++){
            this.typeOfUse += '\'' + this._selectedTypesOfUse[i] + '\',';
          }
          if(this.typeOfUse != ''){
            this.typeOfUse = this.typeOfUse.substring(0, this.typeOfUse.length - 1);
          }

          this.getProperties();
      }

    handleActiveTab(event)
    {
        if (event.target.label === this.label.map)
        {
            this.handleChangeSearchProperties();
        }
    }

    handleChangeSearchProperties()
    {
        this.urlParams = {
            latitude: this.location.latitude,
            longitude: this.location.longitude,
            distanceMin: this.minDistance,
            distanceMax: this.maxDistance,
            rentedMin: this.minRented,
            rentedMax : this.maxRented,
            typeOfUse : this.typeOfUse
        };
    }

    get typeOfUseFieldLabel(){
      return (typeof this.objectInfo !== 'undefined' ? this.objectInfo.data.fields[this.objectAndFieldApiNameTypeOfUse.fieldApiName].label : '');
    }
}