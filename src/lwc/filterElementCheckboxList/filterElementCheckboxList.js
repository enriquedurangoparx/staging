import { LightningElement, api, track, wire } from 'lwc';
import { CurrentPageReference } from 'lightning/navigation';
import { fireEvent } from 'c/pubsub';

export default class FilterElementCheckboxList extends LightningElement {
    @api picklistValues;
    @api sectionLabel;
    @api staticLabel;
    @api accordionLabel;
    @api activeValue;
    @api checkboxLabel = '';
    @api displayAccordion;
    @track value = [];
    @track isCheckboxGroupDisabled = false;
    @track elementActiveClass = "";
    @track layoutClass = "";
    @wire(CurrentPageReference) pageRef;
    hasRendered = false;
    checkboxElement = null;
    lastVisibilityState = true;
    allValues = [];

    renderedCallback() {
        if (!this.hasRendered) {
            this.hasRendered = true;
            this.checkboxElement = this.template.querySelector('lightning-input.cb1');

            if (this.activeValue) {
                this.value = this.activeValue;
                this.checkboxElement.checked = true;
                this.elementActiveClass = "accordion-active";
                this.isCheckboxGroupDisabled = false;
            } else {
                this.isCheckboxGroupDisabled = true;
            }
        }
    }

    handleElementCheckboxChange(event) {
        if (!this.allValues.length) {
            this.allValues = this.picklistValues.data.values.map(row => {
                return row.value;
            });
        }

        this.isCheckboxGroupDisabled = !event.target.checked;
        // this.value = event.target.checked ? this.picklistValues.data.values : []; // sets checkboxes unchecked
        this.value = event.target.checked ? this.allValues : []; // sets checkboxes unchecked
        this.elementActiveClass = this.isCheckboxGroupDisabled ? "" : "accordion-active";

        fireEvent(this.pageRef, 'filterValueChanged', {
            label: this.staticLabel,
            isActive: this.value.length > 0,
            values: JSON.stringify(this.value),
            minValue: '',
            maxValue: ''
        });
    }

    handleChange(event) {
        this.value = event.detail.value;
        console.log('FilterElementCheckboxList:handleChange', JSON.stringify(this.value));

        if (this.value && this.value.length > 0) {
            this.elementActiveClass = "accordion-active";
            this.checkboxElement.checked = true;
        } else {
            this.elementActiveClass = "";
            this.checkboxElement.checked = false;
        }

        fireEvent(this.pageRef, 'filterValueChanged', {
            label: this.staticLabel,
            isActive: this.value.length > 0,
            values: JSON.stringify(this.value),
            minValue: '',
            maxValue: ''
        });
    }

    @api setVisible(state) {
        this.layoutClass = state ? '' : 'slds-hide';
        if (state && this.lastVisibilityState !== state) {
            fireEvent(this.pageRef, 'filterValueChanged', {
                label: this.staticLabel,
                isActive: this.value.length > 0,
                values: JSON.stringify(this.value.split(';')),
                minValue: '',
                maxValue: ''
            });
        }
        this.lastVisibilityState = state;
    }

    get isAccordionVisible() {
        return this.displayAccordion ? (this.displayAccordion === 'true') : true;
    }
}