import { LightningElement, wire, api, track } from 'lwc';
import { getPicklistValues } from 'lightning/uiObjectInfoApi';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

import error from '@salesforce/label/c.GeneralLabelError';
import missingFLS from '@salesforce/label/c.AutocompleteComboboxMissingFLS';
import selectAll from '@salesforce/label/c.AutocompleteComboboxSelectAll';
import deselectAll from '@salesforce/label/c.AutocompleteComboboxDeselectAll';
import placeholder from '@salesforce/label/c.AutocompleteComboboxSelectanOption';

export default class AutocompleteCombobox extends LightningElement {
    @api fieldLabel; // Text label for the input.
    @api iconName; // Defines iconname for pill.

    @api objectAndFieldApiName; // Contains object and api in the following convention: <ObjectApiName>.<FieldApiName>
    @api recordTypeId; // i.e. master recordtype id.

    @track filteredPicklistValues; // Contains filtered picklist values.
    @track pillItems; // Contains list of pill.
    @track searchTerm; // Contains the value of input field, to search for.

    /**
     * Track internal states.
     */
    @track _inputHasFocus; // State about if cursor is in inputfield (true) or not (false).
    @track _placeholder; // Holds the internal status of placeholder.
    @track _activeSelectedValues = []; // Holds only the apiname for active selected picklist values
    
    _originalPicklistValues; // Contains list of all retrieved picklist values -- read only.
    _buttonIsClicked = false; // Holds the interal status of is button clicked.

    // Expose and wrap the labels.
    label = {
        error,
        missingFLS,
        selectAll,
        deselectAll,
        placeholder
    };

    /**
     * Load picklistvalues from $objectAndFieldApiName.
     */
    @wire(getPicklistValues, { recordTypeId: '$recordTypeId', fieldApiName: '$objectAndFieldApiName' })
    wiredPicklistValues({ error, data }) {
        if (data) {
            this._convertPicklistValuesToIterableForm(data);
        } else if (error) {
            const evt = new ShowToastEvent({
                title: this.label.error,
                message: `${this.label.missingFLS} ${this.objectAndFieldApiName}`,
                variant: 'error',
                mode: 'sticky'
            });
            this.dispatchEvent(evt);
        }
    }
 
    /********************
     * HANDLE EVENTS
     ********************/
    /**
     * Handle get focus on input field. Is triggered when user click in input-field.
     */
    handleOnClickSearchTerm(event){
        let tempFilteredList = [];

        //Show again all picklist value and show an checkmark on already selected values.
        if(typeof this._activeSelectedValues !== 'undefined' && this._activeSelectedValues.length > 0){
            this._originalPicklistValues.forEach(picklist => {
                if(this._activeSelectedValues.includes(picklist.item)){
                    picklist.isActive = true;
                }

                tempFilteredList.push(picklist);
            });
            this.filteredPicklistValues = tempFilteredList;
        } else {//No selected picklist values available => Show all.
            this.filteredPicklistValues = this._originalPicklistValues;
        }
        
        this.showDropDownList = true;//Show all values from picklist.
    }

    /**
     * Handle loss of focus on picklist value (div-container around ul-element).
     */
    handleOnMouseLeave(event){
        this._inputHasFocus = ! this._inputHasFocus;
    }

    /**
     * Handle typing on input field to search for picklist values.
     */
    handleOnKeyPress(event){
        const searchTerm = this.template.querySelector('[data-input="combobox-id-1"]').value;
        this.searchTerm = (typeof searchTerm === 'undefined' ? '' : searchTerm);

        // Filter only if there is a valid this.searchTerm.
        if(this.searchTerm !== 'undefined' || this.searchTerm !== ''){
            this.filteredPicklistValues = this._originalPicklistValues.filter(function(element){
                const searchTermLowercase = searchTerm.toLowerCase();
                const labelToLowerCase = element.label.toLowerCase();

                if(labelToLowerCase.includes(searchTermLowercase)){
                    return true;
                }
                
                return false;
            })
        } else {// Reset to initally state.
            this.filteredPicklistValues = [];
            this.filteredPicklistValues = this._originalPicklistValues;
        }
        
        // Notify ulListItem for the new searchTerm
        this.querySelectorAll('c-ul-list-item').forEach(component => {
            component.searchTerm = this.searchTerm;
        })
    }

    /**
     * One listitem on picklist is clicked.
     */
    handleOnListItemClicked(event){
        const item = event.detail.item;
        const label = event.detail.label;
        const isActive = event.detail.isActive;

        // Reset given searchTerm.
        this.template.querySelector('[data-input="combobox-id-1"]').value = '';

        let tempLlistOfPicklistValues = this.filteredPicklistValues;// Temporary copy of this.filteredPicklistValues.
        tempLlistOfPicklistValues.some(picklist => {
            if(item === picklist.item){// Found clicked picklistvalue.
                picklist.isActive = ! picklist.isActive;// Toogle it.

                return true;
            }
        });
        this.filteredPicklistValues = tempLlistOfPicklistValues;

        /**
         * BEGIN -- Add pill to pill container.
         * 
         * Create pill only if selected picklistvalue is active (true) AND is not previous selected.
         */
        if(isActive && !(item in this._activeSelectedValues)){
            this._add(label, item);
        } else {
            this._remove(item);
        }
        /**
         * END -- Add pill to pill container.
         */
    }

    /**
     * Handle click on close icon for pill and contains api-name for picklist value.
     */
    handlePillItemRemove(event) {
        const name = event.detail.item.name;

        this._remove(name);
    }

    /**
     * Is triggered when de/select-button is clicked.
     * 
     * @param {event} event 
     */
    handleSelectedAll(event){
        // Click wants to select all values.
        if(! this._buttonIsClicked){
            let tempPillItems = [];

            // BEGIN -- Add pills to pillcontainer.
            this._originalPicklistValues.forEach(picklist => {
                tempPillItems.push(
                    {
                        type: 'icon',
                        label: picklist.label,
                        name: picklist.item,
                        iconName: this.iconName,
                        variant: 'squaire',
                        alternativeText: picklist.label
                    }
                );
            })
            this.pillItems = tempPillItems;
            // END -- Add pills to pillcontainer.

            // BEGIN -- Select all picklist values.
            let tempLlistOfPicklistValues = this._originalPicklistValues;// Load all picklist values.
            tempLlistOfPicklistValues.forEach(picklist => {
                picklist.isActive = true;// Activate picklist value.
                this._activeSelectedValues.push(picklist.item);
            });
            this.filteredPicklistValues = tempLlistOfPicklistValues;
            // END -- Select all picklist values.

        } else {// Deselect all previous selected values.
            this._activeSelectedValues = [];
            this.pillItems = [];

            // BEGIN -- Select all picklist values.
            let tempLlistOfPicklistValues = this._originalPicklistValues;// Load all picklist values.
            tempLlistOfPicklistValues.forEach(picklist => {
                picklist.isActive = false;// Deactivate picklist value.
            });
            this.filteredPicklistValues = tempLlistOfPicklistValues;
            // END -- Select all picklist values.
        }

        this._buttonIsClicked = ! this._buttonIsClicked;
        this._notifyParent();
    }

    /********************
     * TEMPLATE
     ********************/
    /**
     * Holds the value of placeholder.
     */
    @api
    get placeholder(){
        return (typeof this._placeholder === 'undefined' ? this.label.placeholder : this._placeholder);
    }

    set placeholder(value){
        this._placeholder = value;
        return (typeof this._placeholder !== 'undefined' ? this._placeholder : this.label.placeholder);
    }

    /**
     * Reset all values to initial state.
     */
    @api
    reset(){
        this._activeSelectedValues = [];
        this.filteredPicklistValues = this._originalPicklistValues;
        this.pillItems = [];
        // Reset given searchTerm.
        this.template.querySelector('[data-input="combobox-id-1"]').value = '';
        this._buttonIsClicked = false;

        let tempFilteredPicklistValues = [];
        this.filteredPicklistValues.forEach(picklist => {
            picklist.isActive = false;
        })
        this.filteredPicklistValues = tempFilteredPicklistValues;

        this._notifyParent();
    }

    get buttonLabel(){
        return (this._buttonIsClicked ? this.label.deselectAll : this.label.selectAll);
    }

    get showDropDownList(){
        return this._inputHasFocus;
    }

    set showDropDownList(value){
        this._inputHasFocus = value;
    }

    get dynamicDropDownClasses(){
        let classs = 'slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click';
        classs += (this._inputHasFocus ? ' slds-is-open' : '');

        return classs;
    }

    get ariaExpanded(){
        return (this._inputHasFocus ? true : false );
    }

    /**
     * Indicates if pillcontainer is visible or not. Initially it is not visible.
     * Is only visible if one or more picklistvalue(s) are selected and input has no focus.
     */
    get showPillContainer(){
        return (typeof this.pillItems !== 'undefined' && this.pillItems.length > 0 && ! this._inputHasFocus ? true : false);
    }

    /**
     * Show button if there is a picklist value select
     */
    get showSelectButton(){
        return (typeof this._activeSelectedValues !== 'undefined' ? true : false);
    }
    
    get showLabel(){
        return (typeof this.fieldLabel !== 'undefined' ? true : false);
    }

    /********************
     * HELPER FUNCTIONS
     ********************/
    /**
     * Convert picklistvalues into an array.
     */
    _convertPicklistValuesToIterableForm(data){
        let tempLlistOfPicklistValues = [];// Temporary copy of this.filteredPicklistValues
        
        data.values.forEach(picklist => {
            tempLlistOfPicklistValues.push(
                {
                    label: picklist.label,
                    item: picklist.value,
                    isActive: false
                }
            )
        });

        this.filteredPicklistValues = tempLlistOfPicklistValues;
        this._originalPicklistValues = tempLlistOfPicklistValues;
    }

    /**
     * Add pill to container and recalculate total number of selected picklist value(s).
     * 
     * @param {String} label 
     * @param {String} item  -- apiname of picklist value.
     */
    _add(label, item){
        // Push item to pill container only if item is not already in it.
        if(! this._activeSelectedValues.includes(item)){
            // Temporary copy of this.pillItems.
            let tempPillItems = (typeof this.pillItems !== 'undefined' ? this.pillItems : []);
            this._activeSelectedValues.push(item);

            tempPillItems.push(
                {
                    type: 'icon',
                    label: label,
                    name: item,
                    iconName: this.iconName,
                    variant: 'squaire',
                    alternativeText: label,
                }
            );

            this.pillItems = tempPillItems;
        }

        this._notifyParent();
    }

    /**
     * Remove pill from container, deselect picklist value and recalculate total number of still selected picklist value(s).
     * 
     * @param {String} item  -- apiname of picklist value.
     */
    _remove(item){
        this._activeSelectedValues = this._activeSelectedValues.filter((apiname) => apiname !== item);

        /**
         * Necessary to call it this way: https:// salesforce.stackexchange.com/questions/274338/lwc-lightning-pill-container-with-href-attribute-fires-remove-event-twice
         * Remove the pill when "X" is clicked.
         */
        // eslint-disable-next-line @lwc/lwc/no-async-operation

        // Deselect picklist value
        let tempLlistOfPicklistValues = this.filteredPicklistValues;// Temporary copy of this._originalPicklistValues.
        tempLlistOfPicklistValues.forEach(picklist => {
            if(item === picklist.item){
                picklist.isActive = false;
            }
        });
        this.filteredPicklistValues = tempLlistOfPicklistValues;

        // BEGIN -- Remove item from pill container.
        let tempPillItems = [];
        this.pillItems.forEach(pill => {
            if(pill.name !== item){
                tempPillItems.push(pill);
            }
        })
        this.pillItems = tempPillItems;
        // END -- Remove item from pill container.
        
        this._notifyParent();
    }

    /**
     * Dispatch the event to parent component.
     */
    _notifyParent(){
        // Creates the event with the contact ID data.
        const changeEvent = new CustomEvent('change', { detail: this._activeSelectedValues });

        // Dispatches the event.
        this.dispatchEvent(changeEvent);
    }
}