import { LightningElement, api, track } from 'lwc';

// import custom labels
import close from '@salesforce/label/c.InvestorsListCloseLabel';
import cancel from '@salesforce/label/c.investorsListCancelLabel';
import header from '@salesforce/label/c.InvestorsListEditModalHeader'
import call from '@salesforce/label/c.InvestorsListEditModalCall';
import task from '@salesforce/label/c.InvestorsListEditModalTask';
import event from '@salesforce/label/c.InvestorsListEditModalEvent';
import offer from '@salesforce/label/c.InvestorsListEditModalOffer';
import save from '@salesforce/label/c.InvestorsListEditModalSave';

export default class InvestorsListActivityModal extends LightningElement {
    @track showModal = false;
    @track saveButtonDisabled = false;
    @api recordId;
    @api contactId;
    @api listOfContacts;

    @track isLoading = false;

    record;


    label = {
        close,
        cancel,
        header,
        call,
        task,
        event,
        offer,
        save
    }

    createActivity(){
        let form;
        form = this.template.querySelector('[data-id="task-form"]');
        form.submit();
        this.isLoading = true;
        this.saveButtonDisabled = true;
    }

    /********************
     * EXPOSED FUNCTIONS
     ********************/
    /**
     * open objective selection modal
     */
    @api
    openModal(record) {
        console.log('...openModal:');
        this.recordId = record.rowId;
        this.contactId = record.contactId;
        console.log('...listOfContacts: ' + this.listOfContacts);
        this.record = record;
        this.showModal = true;
    }

    /**
     * close objective selection modal
     */
    @api
    closeModal() {
        this.showModal = false;
        this.isLoading = false;
        this.saveButtonDisabled = false;
    }

    get rowRecord()
    {
        return this.record;
    }

    handledisablebutton(event){
        this.saveButtonDisabled = event.detail;
    }

}