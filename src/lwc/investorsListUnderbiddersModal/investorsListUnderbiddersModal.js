import { LightningElement, api, track, wire } from 'lwc';
import { showToast, showErrorToast } from 'c/utilsSObject';
import { CurrentPageReference } from 'lightning/navigation';
import { fireEvent } from 'c/pubsub';
import saveUnderbiddersFunc from '@salesforce/apex/InvestorsListController.saveUnderbidders';

import close from '@salesforce/label/c.InvestorsListCloseLabel';
import cancel from '@salesforce/label/c.investorsListCancelLabel';
import save from '@salesforce/label/c.MyListSaveListEdit';
import underbidders from '@salesforce/label/c.InvestorsListUnderbiddersLabel';
import updatableUnderbiddersLabel from '@salesforce/label/c.InvestorsListUpdatableUnderbidders';
import notUpdatableUnderbiddersLabel from '@salesforce/label/c.InvestorsListNotUpdatableUnderbidders';
import successLabel from '@salesforce/label/c.GeneralLabelSuccess';
import updateSuccessLabel from '@salesforce/label/c.GeneralLabelSuccessUpdateMessage';

export default class InvestorsListUnderbiddersModal extends LightningElement {
    @api investorPreviousStatus;
    @track showModal = false;
    @track recordsToUpdate = [];
    @track notUpdatableRecords = [];
    recordsWithClosingStatus = [];
    investorWhichIsGettingClosed = null;

    @wire(CurrentPageReference) pageRef;

    @api openModal(selectedRows, investorWhichIsGettingClosed) {
        this.investorWhichIsGettingClosed = investorWhichIsGettingClosed;
        this.recordsToUpdate = [];
        this.notUpdatableRecords = [];
        this.recordsWithClosingStatus = [];

        for (let i = 0; i < selectedRows.length; i++) {
            let record = {
                'Id' : selectedRows[i].Id,
                'Underbidder__c' : selectedRows[i].underbidder,
                'Account__r' : {
                    'Name' : selectedRows[i].Name
                }
            };
            if (selectedRows[i].status !== "Closing") {
                if (selectedRows[i]['lastBidDate'] && selectedRows[i]['lastBidAmount']) {
                    this.recordsToUpdate.push(record);
                } else {
                    this.notUpdatableRecords.push(record);
                }
            } else {
                record.Underbidder__c = 'No';
                this.recordsWithClosingStatus.push({'Id' : selectedRows[i].Id, 'Underbidder__c': 'No'});
            }
        }

        this.showModal = true;
    }

    label = {
        close,
        cancel,
        save,
        underbidders,
        updatableUnderbiddersLabel,
        notUpdatableUnderbiddersLabel
    };

    closeModal() {
        fireEvent(this.pageRef, 'investorsListUnderbiddersModal_updateHasBeenCancelled',
        {investorWhichIsGettingClosed:this.investorWhichIsGettingClosed, investorPreviousStatus: this.investorPreviousStatus} );
        this.showModal = false;
    }

    handleCancel() {
        fireEvent(this.pageRef, 'investorsListUnderbiddersModal_updateHasBeenCancelled',
        		{investorWhichIsGettingClosed:this.investorWhichIsGettingClosed, investorPreviousStatus: this.investorPreviousStatus});
        this.closeModal();
    }

    updateUnderbidders() {
        let underbidders = this.recordsToUpdate.map((row) => {
            return { 'Id' : row.Id, 'Underbidder__c': 'Yes' };
        });
        underbidders.push(...this.recordsWithClosingStatus);
        
        saveUnderbiddersFunc({jsonUnderbidders: JSON.stringify(underbidders)})
        .then(() => {
            fireEvent(this.pageRef, 'investorsListUnderbiddersModal_recordsUpdated', {underbidders: underbidders});

            showToast(successLabel, updateSuccessLabel, 'success');
            this.showModal = false;
        })
        .catch((error) => {
            showErrorToast(error, 'investorsListUnderbiddersModal::updateUnderbidders');
        });
    }
}