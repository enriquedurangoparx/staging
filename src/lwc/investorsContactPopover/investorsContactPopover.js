import { LightningElement, api } from 'lwc';
import closeModal from '@salesforce/label/c.InvestorsListTableClose';
import modalHeadline from '@salesforce/label/c.InvestorsListContactDetailHeadline';

export default class InvestorsContactPopover extends LightningElement {
    @api contact;
    @api top = 100;
    @api left = 100;
    @api popover = false;

    label = {
        close: closeModal,
        headline: modalHeadline
    };

    renderedCallback() {
        const popover = this.template.querySelector('.slds-box');

        if (popover) {
            popover.style.top = this.top + 'px';
            popover.style.left = this.left + 'px';
        }
    }

    closeModal() {
        this.dispatchEvent(new CustomEvent('closemodal'));
    }
}