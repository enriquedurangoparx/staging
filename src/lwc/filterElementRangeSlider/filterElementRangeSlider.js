import { LightningElement, api, track, wire } from 'lwc';
import { registerListener, unregisterAllListeners, fireEvent } from 'c/pubsub';
import { CurrentPageReference } from 'lightning/navigation';

export default class FilterElementRangeSlider extends LightningElement {
    @api sectionLabel;
    @api staticLabel;
    @api min;
    @api max;
    @api diapasonStart;
    @api diapasonEnd;
    @api step;
    @track isCheckboxGroupDisabled = false;
    @wire(CurrentPageReference) pageRef;
    hasRendered = false;
    currentMinValue;
    currentMaxValue;
    returnRangeElement = {};

    get range() {
        this.currentMinValue = this.min ? this.min : 0;
        this.currentMaxValue = this.max
            ? this.max
            : this.diapasonEnd ? this.diapasonEnd : 200;
        return {min: this.currentMinValue, max: this.currentMaxValue};
    }

    get isChecked() {
        return Boolean(this.min && this.max);
    }

    get isDisabled() {
        return !this.isChecked;
    }

    get eventName() {
        return this.staticLabel + "_rangeChanged";
    }

    renderedCallback() {
        if (!this.hasRendered) {
            this.hasRendered = true;

            this.returnRangeElement = this.template.querySelector("c-return-range");
        }
    }

    connectedCallback()
    {
        registerListener(this.eventName, this.handleFilterElementRangeChanged, this);
    }

    disconnectedCallback() {
        unregisterAllListeners(this);
    }

    handleElementCheckboxChange(event) {
        this.isCheckboxGroupDisabled = !event.target.checked;

        let checkbox = this.template.querySelector('lightning-input');
        if (checkbox) {
            this.isCheckboxGroupDisabled ? checkbox.classList.remove('label-active') : checkbox.classList.add('label-active');
        }

        this.returnRangeElement.setDisabled(this.isCheckboxGroupDisabled);
        fireEvent(this.pageRef, 'filterValueChanged', {
            label: this.staticLabel,
            isActive: !this.isCheckboxGroupDisabled,
            values: '',
            minValue: this.isCheckboxGroupDisabled ? 0 : this.currentMinValue,
            maxValue: this.isCheckboxGroupDisabled ? 0 : this.currentMaxValue
        });
    }

    handleFilterElementRangeChanged(payload) {
        this.currentMinValue = payload["minValue"];
        this.currentMaxValue = payload["maxValue"];
        fireEvent(this.pageRef, 'filterValueChanged', {
            label: this.staticLabel,
            isActive: !this.isCheckboxGroupDisabled,
            values: '',
            minValue: payload["minValue"],
            maxValue: payload["maxValue"]
        });
    }
}