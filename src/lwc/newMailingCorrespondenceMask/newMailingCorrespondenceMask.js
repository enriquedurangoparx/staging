/**
 * Created by napo on 3/11/20.
 */

import { LightningElement, api, track, wire } from 'lwc';
import loadInvestorsWrappersJsonByIdsAndCampaign from '@salesforce/apex/NewMailingController.loadInvestorsWrappersJsonByIdsAndCampaign';
import removeInvestorFromCampaign from '@salesforce/apex/NewMailingController.removeInvestorFromCampaign';

import investorContactLabel from '@salesforce/label/c.NewMailingContactTableInvestorContact';
import languageLabel from '@salesforce/label/c.NewMailingContactTableLanguage';
import emailLabel from '@salesforce/label/c.NewMailingContactTableEmail';
import shippingLabel from '@salesforce/label/c.NewMailingContactTableShipping';
import mailingStatusLabel from '@salesforce/label/c.NewMailingMailingStatus';
import contactStatusLabel from '@salesforce/label/c.NewMailingContactTableStatus';

import salutationDELabel from '@salesforce/label/c.SalutationDE';
import salutationENLabel from '@salesforce/label/c.SalutationEN';
import noInvestorsLabel from '@salesforce/label/c.NoInvestors';

import generalLabelSuccess from '@salesforce/label/c.GeneralLabelSuccess';
import generalLabelError from '@salesforce/label/c.GeneralLabelError';
import newMailingInvestorRemoveSuccessLabel from '@salesforce/label/c.NewMailingInvestorRemoveSuccess';

import SENDING_TYPE_FIELD from '@salesforce/schema/CampaignMember.SendingType__c';
import CAMPAIGN_MEMBER_OBJECT from '@salesforce/schema/CampaignMember';
import CONTACT_OBJECT from '@salesforce/schema/Contact';
import LANGUAGE_FIELD from '@salesforce/schema/Contact.ContactLanguage__c';


import { getPicklistValues } from 'lightning/uiObjectInfoApi';
import { getObjectInfo } from 'lightning/uiObjectInfoApi';

import { ShowToastEvent } from 'lightning/platformShowToastEvent';


export default class NewMailingCorrespondenceMask extends LightningElement {

	label = {
		investorContactLabel,
		languageLabel,
		emailLabel,
		shippingLabel,
		contactStatusLabel,
		salutationDELabel,
		salutationENLabel,
		mailingStatusLabel,
		generalLabelSuccess,
		generalLabelError,
		newMailingInvestorRemoveSuccessLabel,
		noInvestorsLabel
	}
	@track
	isLoading = false;

	@api
	investorsIds; //newly or additionally selected

	@track
	sendingTypeOptions;

	@api
	investorsList;


	get isInvestorsListEmpty()
	{
	    return this.investorsList ? this.investorsList.length === 0 : true;
	}

	@api
	campaignId;
	@api
	isFormEditingDisabled;

	@track
	noInvestors = false;


	@wire(getObjectInfo, { objectApiName: CAMPAIGN_MEMBER_OBJECT })
	handleResult({error, data}) {
		if(data) {
			this.campaignMemberRecordTypeId = data.defaultRecordTypeId;
		}
	}

	@wire(getObjectInfo, { objectApiName: CONTACT_OBJECT })
	handleContactObjectResult({error, data}) {
		if(data) {
			this.contactRecordTypeId = data.defaultRecordTypeId;
		}
	}

	@wire(getPicklistValues, { recordTypeId: '$campaignMemberRecordTypeId', fieldApiName: SENDING_TYPE_FIELD })
	handlePVResult({error, data}) {
		if(data) {
			this.sendingTypeOptions = [];
			let self = this;
			let sendingTypeOptions = this.sendingTypeOptions;
			data.values.forEach(function(item){
				sendingTypeOptions.push({label: item.label, value: item.value})
			});
			this.defineTableColumns();
		}
	}

	@wire(getPicklistValues, { recordTypeId: '$contactRecordTypeId', fieldApiName: LANGUAGE_FIELD })
	handleLanguagePVResult({error, data}) {
		if(data) {
			this.languageOptions = [{label: '-', value: ''}];
			let self = this;
			let languageOptions = this.languageOptions;
			data.values.forEach(function(item){
				languageOptions.push({label: item.label, value: item.value})
			});
			this.defineTableColumns();
		}
	}

	@track investorContactsColumns;

	connectedCallback()
	{
		this.template.addEventListener('picklistchange', this.handlePicklistUpdate.bind(this));

		if (((this.investorsList === undefined || this.investorsList === null || this.investorsList.length === 0)
				&& this.investorsIds !== undefined && this.investorsIds !== null && this.investorsIds.length > 0)
				|| this.investorsIds !== undefined && this.investorsIds.length > 0 && this.campaignId !== undefined)
		{
		    this.isLoading = true;
		    let self = this;
			let investors = [];
			let existingInvestorsSet = new Set();
			investors.forEach(function(investor){
				existingInvestorsSet.add(investor.investorId);
			});
			let investorsIds = this.investorsIds + '';
			loadInvestorsWrappersJsonByIdsAndCampaign({investorsIds : investorsIds.split(','), campaignId: this.campaignId})
			.then(resultJson => {
			    let result = JSON.parse(resultJson);
				result.forEach(function(investor){
					let investorContactsList = investor.investorContactsList;
//					// todo: REMOVE NEXT THREE LINES - needed for bulk testing
//					investorContactsList.forEach(function(contact){
//						contact.investorContactSendingType = 'To';
//					});

					let currentInvestor = investor;
//					newInvestors.push(currentInvestor);
					if (!existingInvestorsSet.has(currentInvestor.Id + '')) {
					    investors.push(currentInvestor);
					    existingInvestorsSet.add(currentInvestor.investorId + '');
					}
				});

				this.investorsList = investors;
//				// todo: REMOVE NEXT 4 LINES - needed for bulk testing
//				let self = this;
//				this.investorsList.forEach(function(investor){
//				    self.generateSalutations(investor.investorId);
//				});
				this.noInvestors = this.investorsList === undefined || this.investorsList.length > 0;
				this.defineTableColumns();
				this.sendInvestorsListChangeEvent();
				self.isLoading = false;

			})
			.catch(error => {
				console.log('...getInvestorsWithContacts init: fail' + JSON.toString(error));
				self.isLoading = false;
			});
		}
		else
		{
        	this.defineTableColumns();
        	this.noInvestors = this.investorsList === undefined || this.investorsList.length === 0;
        }

	}

	defineTableColumns()
	{
		let self = this;
		this.investorContactsColumns =
		[
			 {label: self.label.investorContactLabel, type: 'url', fieldName: 'contactUrl', typeAttributes: { label: { fieldName: 'contactName' }, target: '_blank' }},
			 {label: self.label.languageLabel, type: 'sendingTypePicklist', fieldName: 'language', wrapText: true,
						typeAttributes: {
								customValue: self.languageOptions,
								fieldName: 'language',
								isDisabled: this.isFormEditingDisabled,
								recordId: { fieldName: 'investorContactId' }
							}
						},
			 {label: self.label.emailLabel, type: 'text', fieldName: 'contactEmail'},
			 {label: self.label.shippingLabel, type: 'sendingTypePicklist', wrapText: true,
						fieldName: 'investorContactSendingType',
						typeAttributes: {
								customValue: self.sendingTypeOptions,
								fieldName: 'investorContactSendingType',
								isDisabled: this.isFormEditingDisabled,
								recordId: { fieldName: 'investorContactId' }
							}
						},
			 {label: self.label.mailingStatusLabel, type: 'text', fieldName: 'mailingStatus'},
			 {label: self.label.contactStatusLabel, type: 'text', fieldName: 'contactStatus'}
		 ];
	}

	@api
	handleFormDeactivation()
	{
	    this.defineTableColumns();
	}

	handlePicklistUpdate(event)
	{
	    console.log('...NewMailingCorrespondenceMask.handlePicklistUpdate(event) start');
		let recordId = event.detail.recordId + '';
		let fieldName = event.detail.fieldName;
		let value = event.detail.value;
		let contactWrapper;
		let self = this;
		let investorIdForSalutation;
		this.investorsList = JSON.parse(JSON.stringify(this.investorsList));
		this.investorsList.forEach(function(investor){
			investor.investorContactsList.forEach(function(contact){
				if(contact.investorContactId.toString() === recordId)
				{
					contact[fieldName] = value;
					investorIdForSalutation = investor.investorId;
				}
			});
		});
		this.sendInvestorsListChangeEvent();
		Promise.resolve().then(this.generateSalutations(investorIdForSalutation));
//		this.generateSalutations(investorIdForSalutation);
		console.log('...NewMailingCorrespondenceMask.handlePicklistUpdate(event) end');
	}

	sendInvestorsListChangeEvent()
	{
		const e = new CustomEvent('investorslistchange', { detail: { investorsList: this.investorsList} });
        this.dispatchEvent(e);
 	}

	handleSalutationChange(event)
	{
		let value = event.target.value;
		let recordId = event.target.dataset.targetId + '';
		let fieldName = event.target.dataset.targetField + '';
		this.investorsList = JSON.parse(JSON.stringify(this.investorsList));
		this.investorsList.forEach(function(investor){
			if(investor.investorId.toString() === recordId)
			{
				investor[fieldName] = value;
			}
		});

		this.sendInvestorsListChangeEvent();
	}

	handleSalutationENChange(event)
	{
	    this.sendInvestorsListChangeEvent();
	}

	handleInvestorDelete(event)
	{
		let recordId = event.detail.investorId + '';
		let self = this;
		if (this.campaignId !== undefined)
		{
			removeInvestorFromCampaign({campaignId: this.campaignId, investorId: recordId})
			.then(resultJson => {
					let result = JSON.parse(resultJson);
					if(result.isSuccess) {
						 this.dispatchToast(this.label.generalLabelSuccess, this.label.newMailingInvestorRemoveSuccessLabel, 'success');

					}
					else {
						this.dispatchToast(this.label.generalLabelError, 'delete error', 'error');
						if (result.hasErrorDetails)
						{
							this.hasErrorDetails = true;
							this.errorDetails = result.additionalErrors;
						}
					}
			})
			.catch(error => {
				console.log('...error: ' + JSON.stringify(error));
			});
		}
		let newList = [];
		this.investorsList.forEach(function(record){
			if (record.investorId.toString() !== recordId)
			{
				newList.push(record);
			}
		});
		this.investorsList = newList;
		this.noInvestors = this.investorsList === undefined || this.investorsList.length === 0;
		this.sendInvestorsListChangeEvent();
	}

	handleRemoveInvestorModalOpening(event)
	{
		let recordId = event.target.dataset.targetId;
		const modal = this.template.querySelector('c-new-mailing-investor-remove-modal');
		modal.investorId = recordId;
		modal.show();
	}

	dispatchToast(title, message, variant) {
		this.dispatchEvent(new ShowToastEvent({title: title,message: message,variant: variant}));
	}

	generateSalutations(investorIdForSalutation)
	{
	    let foundInvestor;
		this.investorsList.forEach(function(element){
			if(element.investorId === investorIdForSalutation)
			{
				foundInvestor = element;
			}
		});

	    if (foundInvestor !== undefined)
	    {
	        let enSalutation = '';
	        let deSalutation = '';
	        foundInvestor.investorContactsList.forEach(function(contact){
	            let sendingType = contact.investorContactSendingType + '';
				if (sendingType === 'To')
				{
				    let contactLanguage = contact.language + '';
					if (contactLanguage === 'English')
					{
						enSalutation += contact.contactSalutationEn + ', ';
     				}
					else if (contactLanguage === 'German')
					{
						deSalutation += contact.contactSalutationDe + ', ';
					}
    			}
            });
            foundInvestor.mailingConfigSalutationEn = enSalutation;
            foundInvestor.mailingConfigSalutationDe = deSalutation;
	    }
	}

}