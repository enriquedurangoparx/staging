import { LightningElement, api, wire, track} from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { CurrentPageReference } from 'lightning/navigation';
import { fireEvent } from 'c/pubsub';

import deleteInvestorsCall from '@salesforce/apex/InvestorSelectionHelper.deleteInvestorContact';

// Import custom labels
import close from '@salesforce/label/c.InvestorsListCloseLabel';
import cancel from '@salesforce/label/c.investorsListCancelLabel';
import deleteButtonLabel from '@salesforce/label/c.InvestorsListDeleteContact';
import deleteHeading from '@salesforce/label/c.InvestorsListDeleteSelectedContactHeading';
import deleteConfirmation from '@salesforce/label/c.InvestorsListDeleteSelectedContactConfirmation';

import generalLabelError from '@salesforce/label/c.GeneralLabelError';


export default class investorSelectionDeleteSelectedContactModal extends LightningElement {
    @wire(CurrentPageReference) pageRef;
    @api recordId;
    @track showModal = false;

    label = {
        close,
        cancel,
        deleteButtonLabel,
        deleteHeading,
        deleteConfirmation
    }

    @api
    openModal(recordId) {
        this.showModal = true;
        this.recordId = recordId;
    }

    closeModal() {
        this.showModal = false;
    }

    deleteMethod(){
        fireEvent(this.pageRef, 'InvestorSelectionDatatable__isLoading', {isLoading : true});
        deleteInvestorsCall({recordId: this.recordId})
        .then(result => {
            fireEvent(this.pageRef, 'InvestorSelectionDatatable__isLoading', {isLoading : false});
            let res = JSON.parse(result);

            if (res.isSuccess)
            {
                this.closeModal();
                this.handleDeletion();
            }
            else
            {
                this.notifyUser(res.title, res.message, 'error');
            }
            
            
        })
        .catch(error => {
            fireEvent(this.pageRef, 'InvestorSelectionDatatable__isLoading', {isLoading : false});
            this.notifyUser(generalLabelError, JSON.stringify(error), 'error');
        });
    }

    handleDeletion(){
        fireEvent(this.pageRef, 'InvestorSelection__resetPage', {tabName: '', isSelectedRecordDeleted: true});
    }

    /**
     * notify user
     * @param {string} title
     * @param {string} message
     * @param {string} variant
     */
    notifyUser(title, message, variant) {
        const toastEvent = new ShowToastEvent({
            title,
            message,
            variant
        });
        this.dispatchEvent(toastEvent);
    }
}