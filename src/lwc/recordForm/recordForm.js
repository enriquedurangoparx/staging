/* eslint-disable no-console */
/**
 * A generic component to show editable fields for a object. Use lightning-record-form.
 *
 * @author Christian Schwabe (Christian.Schwabe@colliers.com)
 * @var {string}    recordTypeId            The ID of the record type, which is required if you created multiple record types but don't have a default.
 * @var {string}    objectApiName           Contains API-Name for standard- or custom object.
 * @var {string}    columns                 Specifies the number of columns for the form. (standard: 2)
 * @var {string}    recordId                Contains recordId to show specific record.
 * @var {string}    mode                    Specifies the interaction and display style for the form. Possible values: view, edit, readonly.
 *                                          If a record ID is not provided, the default mode is edit, which displays a form to create new records.
 *                                          If a record ID is provided, the default mode is view, which displays field values with edit icons on updateable fields.
 * --@var {string}    appBuilderLayoutType    The type of layout to use to display the form fields. Possible values: Compact, Full, None. When creating a new record, only the full layout is supported. If you choose 'None' this attribute will be ignored.
 *                                          NOTICE: Use this attribute ONLY on lightning app builder.
 * --@var {string}    layoutType              The type of layout to use to display the form fields. Possible values: Compact, Full. When creating a new record, only the full layout is supported.
 *                                          NOTICE: If get layoutType() evaluates to 'None' only fields will be displayed.
 * --@var {string}    layoutType
 * @var {array}     fields             Array that contains field-apinames.
 * --@var {array}     publicFields            If you use this component in another component fill in an array to this variable.
 * --@var {string}    appBuilderFields        Commaseparated list of apinames for fields.
 *                                          NOTICE: Use this attribute ONLY on lightning app builder.
 * @var {string}    eventName               The event name is the name of the triggering component and describes what has changed. For example: ReturnRange__yearOfConstructionRangeChange or Tile_tileSelected
 *
 */
import { LightningElement, api, track, wire } from 'lwc';
import { CurrentPageReference, NavigationMixin } from 'lightning/navigation';
import { registerListener, unregisterAllListeners,fireEvent } from 'c/pubsub';


export default class RecordForm extends NavigationMixin(LightningElement) {
    @track objectApiName;
    @track recordTypeId;

    @track mode = 'view';
    //@api columns = 2;
    @track columns;
    //@api recordId;
    @track recordId;
    @api eventName;
    @api cardName;
    @api iconName;
    @track fields;
    @track dataReceived = false;

    @wire(CurrentPageReference) pageRef;


    connectedCallback(){
        registerListener(
            this.eventName,
            this.handleChange,
            this
        );
    }

    disconnectedCallback() {
        unregisterAllListeners(this);
    }

    @api
    handleReset() {
        this.recordId = undefined;
    }

    get isComponentDetailsShown()
    {
        return (this.recordId ? true : false);
    }

    // Is fired when recordId is published through event (this.eventName)
    handleChange(event){
        this.dataReceived = false;
        setTimeout(() => {
            this.dataReceived = true;
        }, 50);
        this.recordId = event.detail.recordId;
        this.objectApiName = event.detail.objectApiName;
        this.recordTypeId = event.detail.recordTypeId;
        this.fields = event.detail.fields;
        this.mode = event.detail.mode;
        this.columns = event.detail.columns || 2;

        const recordFormChangeEvent = new CustomEvent(
            'recordformchange',
            {
                detail: {
                    objectApiName: this.objectApiName,
                    recordId: this.recordId
                }
            }
        );
        this.dispatchEvent(recordFormChangeEvent);
    }

    handleNavigateToRecord() {
        this[NavigationMixin.GenerateUrl]({
            type: 'standard__recordPage',
            attributes: {
                recordId: this.recordId,
                objectApiName: this.objectApiName,
                actionName: 'view'
            }
        }).then(url => {
            window.open(url);
        });
    }
}