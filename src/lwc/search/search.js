import { LightningElement, track, api } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

const MINIMAL_SEARCH_TERM_LENGTH = 2; // Min number of chars required to search
const SEARCH_DELAY = 300; // Wait 300 ms after user stops typing then, peform search

export default class search extends LightningElement {
    @api objectApiName;
    @api fieldApiName;
    @api lookupId;
    @api label;
    @api selection;
    @api placeholder = '';
    @api isMultiEntry = false;
    @api errors = [];
    @api scrollAfterNItems;
    @api disabled;
    @api variant = 'standard';
    @api hideSubtitle = false;
    @api required = false;
    @api invalidMessage = 'Complete this field.';
   
    @track searchTerm = '';
    @track searchResults = [];
    @track hasFocus = false;

    cleanSearchTerm;
    blurTimeout;
    searchThrottlingTimeout;

    // variables for new records creation starts
    @api addRecordObjectApiName;
    @api addRecordFields = [];

    @track showAddRecordForm = false;
    addRecordLabels = {
        close: 'Close',
        cancel: 'Cancel',
        save: 'Save',
        modalHeader: 'Create New Record',
        successMessage: 'Record created! See it {0}!',
        success: 'Success',
        error: 'Error',
        here: 'here'
    };
    // variables for new records creation ends

    /********************
     * LIFECYCLE
     ********************/

    connectedCallback() {
        this.selection = [];
    }

    renderedCallback() {
        if (this.disabled) {
            let element = this.template.querySelector('input');

            if (element) {
                element.disabled = true;
            }
        }
    }

    /********************
     * EXPOSED FUNCTIONS
     ********************/

    @api
    setSearchResults(results) {
        this.searchResults = results.map(result => {
            if (typeof result.icon === 'undefined') {
                result.icon = 'standard:default';
            }
            return result;
        });
    }

    @api
    getSelection() {
        return this.selection;
    }

    @api
    setSelection(selection) {
        this.selection = selection;
    }

    @api
    get labelHidden() {
        return this.variant === 'label-hidden';
    }

    @api
    reportValidity() {
        let element = this.template.querySelector('input');

        if (element) {
            element.focus();
        }
    }

    @api
    checkValidity() {
        if (!this.required) {
            return true;
        }

        let valid = false;

        if (this.isMultiEntry) {
            valid = this.selection && this.selection.length > 0;
        } else {
            let element = this.template.querySelector('input');

            if (element) {
                valid = element.checkValidity();
            }
        }

        if (!valid) {
            if (
                this.errors.length === 0 ||
                this.errors.findIndex(err => err.id === 'invalid') === -1
            ) {
                this.errors.push({
                    id: 'invalid',
                    message: this.invalidMessage
                });
            }
        } else {
            const errorIndex = this.errors.findIndex(
                err => err.id === 'invalid'
            );

            this.errors = this.errors
                .slice(0, errorIndex)
                .concat(this.errors.slice(errorIndex + 1));
        }

        return valid;
    }

    /********************
     * INTERNAL FUNCTIONS
     ********************/

    updateSearchTerm(newSearchTerm) {
        this.searchTerm = newSearchTerm;

        // Compare clean new search term with current one and abort if identical
        const newCleanSearchTerm = newSearchTerm
            .trim()
            .replace(/\*/g, '')
            .toLowerCase();
        if (this.cleanSearchTerm === newCleanSearchTerm) {
            return;
        }

        // Save clean search term
        this.cleanSearchTerm = newCleanSearchTerm;

        // Ignore search terms that are too small
        if (newCleanSearchTerm.length < MINIMAL_SEARCH_TERM_LENGTH) {
            this.searchResults = [];
            return;
        }

        // Apply search throttling (prevents search if user is still typing)
        if (this.searchThrottlingTimeout) {
            clearTimeout(this.searchThrottlingTimeout);
        }
        // eslint-disable-next-line @lwc/lwc/no-async-operation
        this.searchThrottlingTimeout = setTimeout(() => {
            // Send search event if search term is long enougth
            if (this.cleanSearchTerm.length >= MINIMAL_SEARCH_TERM_LENGTH) {
                const searchEvent = new CustomEvent('search', {
                    detail: {
                        searchTerm: this.cleanSearchTerm,
                        selectedIds: this.selection.map(element => element.id)
                    }
                });
                this.dispatchEvent(searchEvent);
            }
            this.searchThrottlingTimeout = null;
        }, SEARCH_DELAY);
    }

    isSelectionAllowed() {
        if (this.isMultiEntry) {
            return true;
        }
        return !this.hasSelection();
    }

    hasResults() {
        return this.searchResults.length > 0;
    }

    hasSelection() {
        if (!this.selection) {
            return false;
        }

        return this.selection.length > 0;
    }

    /********************
     * EVENT HANDLING
     ********************/

    handleInput(event) {
        // Prevent action if selection is not allowed
        if (!this.isSelectionAllowed()) {
            return;
        }
        this.updateSearchTerm(event.target.value);
    }

    handleResultClick(event) {
        const recordId = event.currentTarget.dataset.recordid;


        // Save selection
        let selectedItem = this.searchResults.filter(
            result => result.id === recordId
        );
        if (selectedItem.length === 0) {
            return;
        }
        selectedItem = selectedItem[0];
        const newSelection = [...this.selection];
        newSelection.push(selectedItem);
        this.selection = newSelection;

        // Reset search
        this.searchTerm = '';
        this.searchResults = [];


        // Notify parent components that selection has changed
        this.dispatchEvent(
            new CustomEvent('selectionchange', {
                detail: {
                    newItem: selectedItem
                }
            })
        );
    }

    handleComboboxClick() {
        // Hide combobox immediatly
        if (this.blurTimeout) {
            window.clearTimeout(this.blurTimeout);
        }
        this.hasFocus = false;
    }

    handleFocus() {
        // Prevent action if selection is not allowed
        if (!this.isSelectionAllowed()) {
            return;
        }
        this.hasFocus = true;
    }

    handleBlur() {
        // Prevent action if selection is not allowed
        if (!this.isSelectionAllowed()) {
            return;
        }
        // Delay hiding combobox so that we can capture selected result
        // eslint-disable-next-line @lwc/lwc/no-async-operation
        this.blurTimeout = window.setTimeout(() => {
            this.hasFocus = false;
            this.blurTimeout = null;
        }, 300);
    }

    handleRemoveSelectedItem(event) {
        const recordId = event.currentTarget.name;
        this.selection = this.selection.filter(item => item.id !== recordId);
        // Notify parent components that selection has changed
        this.dispatchEvent(new CustomEvent('selectionchange'));
    }

    handleClearSelection() {
        this.selection = [];
        // Notify parent components that selection has changed
        this.dispatchEvent(new CustomEvent('selectionchange'));
    }

    /********************
     * STYLE EXPRESSIONS
     ********************/

    get getContainerClass() {
        // let css = 'slds-combobox_container slds-has-inline-listbox ';
        let css = 'slds-combobox_container ';
        if (this.hasFocus && this.hasResults()) {
            css += 'slds-has-input-focus ';
        }
        if (this.errors.length > 0) {
            css += 'has-custom-error';
        }
        return css;
    }

    get getDropdownClass() {
        let css =
            'slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click ';
        if (this.hasFocus && this.hasResults()) {
            css += 'slds-is-open';
        } else {
            css += 'slds-combobox-lookup';
        }
        return css;
    }

    get getInputClass() {
        let css =
            'slds-input slds-combobox__input has-custom-height ' +
            (this.errors.length === 0 ? '' : 'has-custom-error ');
        if (!this.isMultiEntry) {
            css +=
                'slds-combobox__input-value ' +
                (this.hasSelection() ? 'has-custom-border' : '');
        }
        return css;
    }

    get getComboboxClass() {
        let css = 'slds-combobox__form-element slds-input-has-icon ';
        if (this.isMultiEntry) {
            css += 'slds-input-has-icon_right';
        } else {
            css += this.hasSelection()
                ? 'slds-input-has-icon_left-right'
                : 'slds-input-has-icon_right';
        }
        return css;
    }

    get getSearchIconClass() {
        let css = 'slds-input__icon slds-input__icon_right ';
        if (!this.isMultiEntry) {
            css += this.hasSelection() ? 'slds-hide' : '';
        }
        return css;
    }

    get getClearSelectionButtonClass() {
        return (
            'slds-button slds-button_icon slds-input__icon slds-input__icon_right ' +
            (this.hasSelection() ? '' : 'slds-hide')
        );
    }

    get getSelectIconName() {
        return this.hasSelection()
            ? this.selection[0].icon
            : 'standard:default';
    }

    get getSelectIconClass() {
        return (
            'slds-combobox__input-entity-icon ' +
            (this.hasSelection() ? '' : 'slds-hide')
        );
    }

    get getInputValue() {
        if (this.isMultiEntry) {
            return this.searchTerm;
        }
        return this.hasSelection() ? this.selection[0].title : this.searchTerm;
    }

    get getListboxClass() {
        return (
            'slds-listbox slds-listbox_vertical slds-dropdown slds-dropdown_fluid ' +
            (this.scrollAfterNItems
                ? 'slds-dropdown_length-with-icon-' + this.scrollAfterNItems
                : '')
        );
    }

    get isInputReadonly() {
        if (this.isMultiEntry) {
            return false;
        }
        return this.hasSelection();
    }

    get isExpanded() {
        return this.hasResults();
    }

    addRecordShowModal()
    {
        this.showAddRecordForm = true;
    }

    addRecordCloseModal() {
        this.showAddRecordForm = false;
        // setTimeout(function(){ alert("After 5 seconds!"); }, 5000);
    }

    addRecordSuccess(event)
    {
        this.notifyUserWithUrl(this.addRecordCustomLabels.success, this.addRecordCustomLabels.successMessage, event.detail.id, 'success');
        this.addRecordCloseModal();
        const evnt = new CustomEvent('recordcreated', {
            detail: JSON.stringify(event.detail)
            // detail: {
            //     test: JSON.stringify(event.detail)
            //     // searchTerm: this.cleanSearchTerm,
            //     // selectedIds: this.selection.map(element => element.id)
            // }
        });
        this.dispatchEvent(evnt);
    }

    notifyUserWithUrl(title, message, recId, variant) {
        const event = new ShowToastEvent({
            "title": title,
            "message": message,
            "variant": variant,
            "messageData": [
                {
                    url: '/' + recId,
                    label: this.addRecordCustomLabels.here
                }
            ]
        });
        this.dispatchEvent(event);
    }

    @api
    set addRecordCustomLabels(labels)
    {
        let tempLabels = this.addRecordLabels;
        tempLabels.close = labels.close ? labels.close : tempLabels.close;
        tempLabels.cancel = labels.cancel ?  labels.cancel : tempLabels.cancel;
        tempLabels.save = labels.save ? labels.save : tempLabels.save;
        tempLabels.modalHeader = labels.modalHeader ? labels.modalHeader : tempLabels.modalHeader;
        tempLabels.successMessage = labels.successMessage ?  labels.successMessage : tempLabels.successMessage;
        tempLabels.success = labels.success ? labels.success : tempLabels.success;
        tempLabels.error = labels.error ? labels.error : tempLabels.error;
        tempLabels.here = labels.here ? labels.here : tempLabels.here;
        this.addRecordLabels = tempLabels;
    }

    get addRecordCustomLabels()
    {
        return this.addRecordLabels;
    }

    @api
    invokeHandleResultClick(recId)
    {
        let evnt = {
            currentTarget: {
                dataset: {
                    recordid: ''
                }
            }
        };
        evnt.currentTarget.dataset.recordid = recId;
        
        this.handleResultClick(evnt);
    }

    testrequired = true;
}