import { LightningElement, api, track, wire } from 'lwc';
import genericTemplate from './parentChildRelatedList.html';
import opportunityTemplate from './parentChildRelatedListOpportunity.html';
import error from './error.html';

import { getObjectInfo } from 'lightning/uiObjectInfoApi';
import CONTACT_OBJECT from '@salesforce/schema/Contact';

import { CurrentPageReference, NavigationMixin } from 'lightning/navigation';
import { sort } from 'c/utils';

import FORM_FACTOR from '@salesforce/client/formFactor';
import LANG from '@salesforce/i18n/lang';

import getFlatHierachy from '@salesforce/apex/ParentChildRelatedListController.getFlatHierachy';

import noRecordsFound from '@salesforce/label/c.GeneralLabelNoRecordsFound';
import viewAll from '@salesforce/label/c.RelatedListViewAll';

const RECORDS_TO_SHOW = 5;
const URL_DELIMITER = '/';

export default class ParentChildRelatedList extends NavigationMixin(LightningElement) {
    @api objectApiName;
    @api recordId;

    @api relatedListLabelGerman;// German translation of title for related list.
    @api relatedListLabelEnglish;// English translation of title for related list.
    @api relationshipName;// Name of relationship from child object-field.
    @api parentObjectApiName;// i.e.: AccountToArea__c.
    @api childObjectApiName;// i.e.: ContactToAccountToArea__c.
    @api germanColumnDefinition;// Column definition of datatable (german).
    @api englishColumnDefinition;// Column definition of datatable (english).
    @api whereCondition; // Describes the field-apiname on the parent object to the current record.

    @track data;// Holds list of data for datatable.
    @track error;

    /**
     * Holds internal status.
     */
    @track _parentFields;
    @track _childFields;
    @track _parentFieldMapping;
    @track _childFieldMapping;
    @track _showSpinner;// Indicates if spinner is shown.
    @track _isFullPage;// Indicates if full page is active.

    parentObjectInfo;
    childObjectInfo;
    _userLanguage = LANG;// Determines the current user language.

    // Sort direction of datatable.
    sortedBy;
    sortedDirection;

    label = {
        viewAll: viewAll,
        noRecordsFound: noRecordsFound,
    }

    /**
     * Use getObjectInfo to determine the label of parent column.
     */
    @wire(getObjectInfo, { objectApiName: '$parentObjectApiName' })
    parentObjectInfo;

    @wire(getObjectInfo, { objectApiName: '$childObjectApiName' })
    childObjectInfo;

    @wire(getObjectInfo, { objectApiName: CONTACT_OBJECT })
    contactObjectInfo;

    // Injects the page reference that describes the current page.
    @wire(CurrentPageReference)
    currentPageReference;
    
    @wire(getFlatHierachy, {
        recordId: '$recordId',
        relationshipName: '$relationshipName',
        whereApiName: '$whereCondition',
        parentFields: '$_parentFields',
        childFields: '$_childFields',
        parentObjectApiName: '$parentObjectApiName',
        parentFieldMapping: '$_parentFieldMapping',
        childFieldMapping: '$_childFieldMapping'
    })
    wiredRecords({ error, data})
    {
        console.log('>>>ParentChildRelatedList -- wiredRecords().');
        if (data) {
            this.data = data;
            this.data = this._isFullPage ? this.data : this.data.slice(0, RECORDS_TO_SHOW);// Slice records to max RECORDS_TO_SHOW when this._fullpage === false.
            this.error = 'undefined';
            this.preProcessData();
            this._showSpinner = false;
        } else if (error) {
            this.error = error;
            this.data = 'undefined';
        }
    }

    /**
     * Show different templates on various objects.
     */
    render()
    {
        console.log('>>>ParentChildRelatedList -- render().');
        console.log('>>>this.recordId: ' + JSON.stringify(this.recordId, null, '\t'));

        let returnValue;
        if(typeof this.objectApiName !== 'undefined'){// Decide based on this.objectApiName.
            switch (this.objectApiName) {
                case 'Area__c':
                    returnValue = genericTemplate;
                    break;
                case 'PropertyObject__c':
                    returnValue = genericTemplate;
                    break;
                case 'Portfolio__c':
                    returnValue = genericTemplate;
                    break;
                case 'Opportunity':
                    returnValue = opportunityTemplate;
                    break;
                default:
                    break;
            }
        } else {// Required parameter is missing
            returnValue = error;
        }
        

        return returnValue;
    }

    connectedCallback(){
        console.log('>>>ParentChildRelatedList -- connectedCallback.');
        this._showSpinner = true;// Start loading the spinner.

        // Retrieve url-parameter.
        this.recordId = (typeof this.currentPageReference.state.c__recordId !== 'undefined' ? this.currentPageReference.state.c__recordId : this.recordId);
        this.objectApiName = (typeof this.currentPageReference.state.c__objectApiName !== 'undefined' ? this.currentPageReference.state.c__objectApiName : this.objectApiName);
        this._isFullPage = (typeof this.currentPageReference.state.c__isFullPage !== 'undefined' ? (this.currentPageReference.state.c__isFullPage === 'true') : this._isFullPage);
        
        this.relatedListLabelGerman = (typeof this.currentPageReference.state.c__relatedListLabelGerman !== 'undefined' ? this.currentPageReference.state.c__relatedListLabelGerman : this.relatedListLabelGerman);
        this.relatedListLabelEnglish = (typeof this.currentPageReference.state.c__relatedListLabelEnglish !== 'undefined' ? this.currentPageReference.state.c__relatedListLabelEnglish : this.relatedListLabelEnglish);
        
        this.relationshipName = (typeof this.currentPageReference.state.c__relationshipName !== 'undefined' ? this.currentPageReference.state.c__relationshipName : this.relationshipName);
        
        this._parentFields = (typeof this.currentPageReference.state.c__parentFields !== 'undefined' ? this.currentPageReference.state.c__parentFields : this._parentFields);
        this._parentFieldMapping = (typeof this.currentPageReference.state.c__parentFieldMapping !== 'undefined' ? JSON.parse(this.currentPageReference.state.c__parentFieldMapping) : this._parentFieldMapping);
        this.parentObjectApiName = (typeof this.currentPageReference.state.c__parentObjectApiName !== 'undefined' ? this.currentPageReference.state.c__parentObjectApiName : this.parentObjectApiName);
        this.whereCondition = (typeof this.currentPageReference.state.c__whereCondition !== 'undefined' ? this.currentPageReference.state.c__whereCondition : this.whereCondition);

        this._childFields = (typeof this.currentPageReference.state.c__childFields !== 'undefined' ? this.currentPageReference.state.c__childFields : this._childFields);
        this.childObjectApiName = (typeof this.currentPageReference.state.c__childObjectApiName !== 'undefined' ? this.currentPageReference.state.c__childObjectApiName : this.childObjectApiName);
        this._childFieldMapping = (typeof this.currentPageReference.state.c__childFieldMapping !== 'undefined' ? JSON.parse(this.currentPageReference.state.c__childFieldMapping) : this._childFieldMapping);

        this.germanColumnDefinition = (typeof this.currentPageReference.state.c__germanColumnDefinition !== 'undefined' ? JSON.parse(this.currentPageReference.state.c__germanColumnDefinition) : this.germanColumnDefinition);
        this.englishColumnDefinition = (typeof this.currentPageReference.state.c__englishColumnDefinition !== 'undefined' ? JSON.parse(this.currentPageReference.state.c__englishColumnDefinition) : this.englishColumnDefinition);
    }

    /********************
     * EVENT HANDLER
     ********************/
    sortHandler(event) {
        console.log('>>>AccountContactRelatedList -- sortHandler().');
        const sortedBy = event.detail.fieldName;
        const sortedDirection = event.detail.sortDirection;

        this.sortedBy = sortedBy;
        this.sortedDirection = sortedDirection;

        let tempData = this.data;
        this.data = sort(tempData, sortedDirection, sortedBy);
    }

    /********************
     * TEMPLATE
     ********************/
    get iconName(){
        console.log('>>>AccountContactRelatedList -- get iconName().');

        let iconUrl = (this.parentObjectInfo.data ? this.parentObjectInfo.data.themeInfo.iconUrl : '');
        let string_substringBeforeLast = this.substringBeforeLast(iconUrl, URL_DELIMITER);
        let string_substringAfterLast = this.substringAfterLast(iconUrl, URL_DELIMITER);

        let string_iconCategory = string_substringBeforeLast;
        let string_iconName = string_substringAfterLast.split('_')[0];
        let string_returnValue = string_iconCategory + ':' + string_iconName;

        return string_returnValue;
    }

    /**
     * Define the title of related list, depending on user language.
     */
    get relatedListTitle()
    {
        return (this._isGermanUserLanguage() ? this.relatedListLabelGerman : this.relatedListLabelEnglish);
    }

    /**
     * Convert commaseparated values to list. Parent fields.
     */
    @api
    get parentFields()
    {
        return this._parentFields;
    }

    set parentFields(value)
    {
        this._parentFields = [];
        value.split(',').forEach(field => {
            this._parentFields.push(field.trim());
        });
    }

    /**
     * Convert commaseparated values to list. Child fields.
     * I.e.: Contact__c, Contact__r.LastName, Contact__r.FirstName, Contact__r.Phone, Contact__r.Email, toLabel(Role__c)
     */
    @api
    get childFields()
    {
        return this._childFields;
    }

    set childFields(value)
    {
        this._childFields = [];
        value.split(',').forEach(field => {
            this._childFields.push(field.trim());
        });
    }

    /**
     * Return map.
     * I.e.: [{"value": "accountName", "key": "Account__r.Name"}, {"value": "accountId", "key": "Account__c"}]
     */
    @api
    get parentFieldMapping()
    {
        return this._parentFieldMapping;
    }

    /**
     * Converts given json to a list of object, like a map.
     * I.e.: {value: 'accountName', key: 'Account__r.Name'}.
     * 
     * key: to know which field (key) should be related to wrapper class field.
     * value: Is target field of wrapper class.
     */
    set parentFieldMapping(value){
        let map = JSON.parse(value);
        this._parentFieldMapping = map;        
    }

    /**
     * Return map.
     */
    @api
    get childFieldMapping()
    {
        return this._childFieldMapping;
    }

    /**
     * Converts given json to a list of object, like a map.
     * I.e.: {value: 'accountName', key: 'Account__r.Name'}.
     * 
     * key: to know which field (key) should be related to wrapper class field.
     * value: Is target field of wrapper class.
     */
    set childFieldMapping(value)
    {
        let map = JSON.parse(value);
        this._childFieldMapping = map;
    }

    /**
     * Define if "View all" is shown at the end of table.
     */
    get showViewAll()
    {
        let returnValue = false;
        if(
            typeof this.data !== 'undefined' &&
            this.data.length >= RECORDS_TO_SHOW &&
            ! this._isFullPage
        ){
            returnValue = true;
        }

        return returnValue;
    }

    /**
     * Return information about column header for datatable.
     */
    get columns(){
        if(this._isGermanUserLanguage())
        {
            return JSON.parse(this.germanColumnDefinition);
        } else
        {
            return JSON.parse(this.englishColumnDefinition);
        }
    }

    /**
     * Return the current form factor.
     */
    get isDesktopDevice()
    {
        return FORM_FACTOR === 'Large';
    }

    get hasNoRecords()
    {
        return ((typeof this.data !== 'undefined' && this.data.length === 0) ? true : false);
    }

    get showSpinner()
    {
        return this._showSpinner;
    }

    /**
     * Indicate if records are available.
     */
    get hasRecords()
    {
        return (typeof this.data !== 'undefined' && this.data.length > 0 ? true : false);
    }

    /**
     * Label for mobile device.
     */
    get accountNameLabel()
    {
        return (typeof this.parentObjectInfo.data != 'undefined' ? this.parentObjectInfo.data.fields['Account__c'].label : '');
    }

    get contactLastNameLabel()
    {
        return (typeof this.contactObjectInfo.data != 'undefined' ? this.contactObjectInfo.data.fields['LastName'].label : '');
    }

    get contactFirstNameLabel()
    {
        return (typeof this.contactObjectInfo.data != 'undefined' ? this.contactObjectInfo.data.fields['FirstName'].label : '');
    }

    get contactPhoneLabel()
    {
        return (typeof this.contactObjectInfo.data != 'undefined' ? this.contactObjectInfo.data.fields['Phone'].label : '');
    }

    get contactEmailLabel()
    {
        return (typeof this.contactObjectInfo.data != 'undefined' ? this.contactObjectInfo.data.fields['Email'].label : '');
    }

    get contactToAccountToAreaRoleLabel()
    {
        return (typeof this.childObjectInfo.data != 'undefined' ? this.childObjectInfo.data.fields['Role__c'].label : '');
    }

    get contactToAccountToAreaNoticeLabel()
    {
        return (typeof this.childObjectInfo.data != 'undefined' ? this.childObjectInfo.data.fields['Notice__c'].label : '');
    }

    /********************
     * HELPER FUNCTIONS
     ********************/
    _isGermanUserLanguage(){
        return (this._userLanguage === 'de' ? true : false);
    }

    /**
     * @description Returns the substring that occurs before the last occurrence of the specified separator.
     * 
     * @param {string} value
     * @param {string} separator
     * 
     * @returns {string}
     */
    substringBeforeLast(value, separator){
        let splittedElements = value.split(separator);
        let numberOfSplittedElements = splittedElements.length;
        let extractedValue = splittedElements[numberOfSplittedElements-2];
    
        return extractedValue;
    }

    /**
     * Returns the substring that occurs after the last occurrence of the specified separator.
     * 
     * @param {string} value
     * @param {string} separator
     * 
     * @returns {string}
     */
    substringAfterLast(value, separator){
        let splittedElements = value.split(separator);
        let numberOfSplittedElements = splittedElements.length;
        let extractedValue = splittedElements[numberOfSplittedElements-1];
    
        return extractedValue;
    }

    preProcessData()
    {
        console.log('>>>AccountContactRelatedList -- preProcessData().');

        let preprocessData = [];
        JSON.parse(JSON.stringify(this.data)).forEach(element => {
            element.accountId = '/' + element.accountId;
            element.contactLastNameId = (typeof element.contactLastName === 'undefined' ? '' : '/' + element.contactId);
            element.contactFirstNameId = (typeof element.contactFirstName === 'undefined' ? '' : '/' + element.contactId);
            
            //console.log('>>>element.isInvestorRepresentative: ' + JSON.stringify(element.isInvestorRepresentative, null, '\t'));
            element.isInvestorRepresentative = ((typeof element.isInvestorRepresentative === 'undefined' || element.isInvestorRepresentative === false)? '' : 'X');

            element.accountUrl = element.accountId;
            element.contactUrl = '/' + element.contactId;
            preprocessData.push(element);

            console.log('>>>element: ' + JSON.stringify(element, null, '\t'));
        });

        this.data = preprocessData;
    }

    /**
     * Open lightning tab "Parent Child Related List" (api-name: ParentChildRelatedList).
     */
    openFullViewPage()
    {
        console.log('>>>AccountContactRelatedList -- openFullViewPage().');

        this[NavigationMixin.Navigate]({
            type: 'standard__navItemPage',
            attributes: {
                apiName: 'ParentChildRelatedList',
            },
            // query string parameters
            state: {
                c__recordId: this.recordId,
                c__objectApiName: this.objectApiName,
                c__isFullPage: 'true',
                c__relatedListLabelGerman: this.relatedListLabelGerman,
                c__relatedListLabelEnglish: this.relatedListLabelEnglish,
                c__relationshipName: this.relationshipName,
                c__parentFields: this.parentFields,
                c__childFields: this.childFields,
                c__parentObjectApiName: this.parentObjectApiName,
                c__childObjectApiName: this.childObjectApiName,
                c__parentFieldMapping: JSON.stringify(this._parentFieldMapping),
                c__childFieldMapping: JSON.stringify(this._childFieldMapping),
                c__germanColumnDefinition: JSON.stringify(this.germanColumnDefinition),
                c__englishColumnDefinition: JSON.stringify(this.englishColumnDefinition),
                c__whereCondition: this.whereCondition
            }
        });
    }
}