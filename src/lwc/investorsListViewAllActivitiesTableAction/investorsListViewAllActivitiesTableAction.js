import { LightningElement, api } from 'lwc';

export default class InvestorsListViewAllActivitiesTableAction extends LightningElement {

    @api activityId;
    @api action;

    handleDeleteActivity(){
        const deleteEvent = CustomEvent('deleteactivity', {
            composed: true,
            bubbles: true,
            cancelable: true,
            detail: {
                activityId: this.activityId
            }
        });
        this.dispatchEvent(deleteEvent);
    }

}