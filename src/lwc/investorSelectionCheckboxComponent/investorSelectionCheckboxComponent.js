import { LightningElement, api, wire, track } from 'lwc';
import { CurrentPageReference } from 'lightning/navigation';
import { registerListener, fireEvent } from 'c/pubsub';

export default class InvestorSelectionCheckboxComponent extends LightningElement {
    @api disabledandchecked;
    @api recordId;
    @api tableName;
    @track checkBoxClass;
    @wire(CurrentPageReference) pageRef;

	connectedCallback() {
        registerListener('investorsSelection_selectOrUnSelectAll', this.selectOrUnSelect, this);
        this.setCheckBoxClass();
    }

    handleChange(event){
        if (this.tableName && (this.tableName === 'listTabInvestorContact' || this.tableName === 'listTabInvestor'))
        {
            const obj = {
                isChecked: event.target.checked,
                recordId: this.recordId,
                tableName: this.tableName
            }
            fireEvent(this.pageRef, 'investor_selection_datatable__toggle_checkbox', obj);
        }
        else
        {
            fireEvent(this.pageRef, 'InvestorSelectionTab__checkboxChecked', event.target.checked + '-' + this.recordId);
        }
    }

    selectOrUnSelect(data)
    {
        const obj = {
            isChecked: data.isChecked,
            recordId: this.recordId,
            tableName: data.tableName
        }
        // fireEvent(this.pageRef, 'investor_selection_datatable__toggle_checkbox', obj);
        if(!this.disabledandchecked)
        {
            switch(data.tableName) 
            {
                case 'listTabInvestor':
                    this.template.querySelector('lightning-input.listTabInvestor').checked = data.isChecked;
                    fireEvent(this.pageRef, 'investor_selection_datatable__toggle_checkbox', obj);
                    break;
                case 'listTabInvestorContact':
                    this.template.querySelector('lightning-input.listTabInvestorContact').checked = data.isChecked;
                    fireEvent(this.pageRef, 'investor_selection_datatable__toggle_checkbox', obj);
                    break;
                default:
                    this.template.querySelector('lightning-input.cb').checked = data.isChecked;
                    fireEvent(this.pageRef, 'InvestorSelectionTab__checkboxChecked', data.isChecked + '-' + this.recordId);
            }
        }
    }

    // this is for split table and split logic and buttons if tables are on the same page
    setCheckBoxClass()
    {
        if (this.tableName)
        {
            this.checkBoxClass = this.tableName;
        }
        else
        {
            this.checkBoxClass = 'cb';
        }
    }

}