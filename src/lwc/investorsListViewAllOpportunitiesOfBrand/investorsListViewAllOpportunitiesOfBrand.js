import { LightningElement, wire, track } from 'lwc';
import { CurrentPageReference, NavigationMixin } from'lightning/navigation';
import search from '@salesforce/apex/GenericSearch.search';
import rowLabelName from '@salesforce/label/c.InvestorsListTableRowName';
import rowLabelAccount from '@salesforce/label/c.InvestorsListTableRowAccount';
import rowLabelStatusSince from '@salesforce/label/c.InvestorsListTableRowStatusSince';
import rowLabelStatus from '@salesforce/label/c.InvestorsListStatusLabel';
import rowlastBidAmount from '@salesforce/label/c.InvestorsListLastBidAmount';
import rowlastBid from '@salesforce/label/c.InvestorsListLastBidLabel';
import rowLastActivity from '@salesforce/label/c.InvestorsListLastActivity';


import sectionOpenBrandOpps from '@salesforce/label/c.InvestorsListSectionOpenBrandOpportunities';

export default class InvestorsListViewAllOpportunitiesOfBrand extends LightningElement {
    recordId;
    @track openBrandOppData;
    oppBrandColumns = [{label: rowLabelName, fieldName: 'opportunityId',  type: 'url', typeAttributes: { label: {fieldName: 'opportunityName'}, target:'_blank'}, sortable: true },
                                {label: rowLabelAccount, fieldName: 'accountName', type: 'text', sortable: 'false'},
                                {label: rowLabelStatus, fieldName: 'Status__c', type: 'text', sortable: 'false'},
                                {label: rowLabelStatusSince, fieldName: 'LastStatusDate__c', type: 'date', sortable: 'false' },
                                {label: rowLastActivity, fieldName: 'LastActivityDate__c', type: 'date', sortable: 'false' },
                                {label: rowlastBid, fieldName: 'LastBidDate__c', type: 'date', sortable: 'false' },
                                {label: rowlastBidAmount, fieldName: 'LastBidAmount__c', type: 'currency', sortable: 'false' }
                                ];

    @wire(CurrentPageReference) pageRef;

    // UI-Labels
    label = {
        sectionOpenBrandOpps : sectionOpenBrandOpps
    };

    connectedCallback() {
        this.recordId = this.pageRef.state.c__recordId;
        this.getOpenBrandOpportunities(this.recordId);
    }


    getOpenBrandOpportunities(selectedBrandRecordId){
        if(selectedBrandRecordId != null){
            search({
                list_fieldApiName: ['Opportunity__c', 'Opportunity__r.Name', 'Account__r.Name','LastBidDate__c','LastBidAmount__c', 'LastActivityDate__c','LastStatusDate__c', 'toLabel(Status__c)'],
                string_objectApiName: 'AccountToOpportunity__c',
                string_searchTerm: selectedBrandRecordId,
                string_whereApiName: 'Account__r.Brand__c',
                string_filter : 'Opportunity__r.IsClosed = false',
                string_soqlOperator: '=',
                string_orderBy: 'Opportunity__r.CloseDate desc',
                integer_limit: 1000,
                integer_offset: 0
            })
            .then(result => {
                const tempResult = JSON.parse(JSON.stringify(result));
                let _openOppData = this._preprocessBrandOpportunityData(tempResult);
                this.openBrandOppData = JSON.parse(JSON.stringify(_openOppData));

            }).catch(error => {
                console.error(JSON.stringify(error, null, '\t'));
            })
        }
    }

    _preprocessBrandOpportunityData(result){
        let preprocessData = [];
        result.forEach(element => {
            element.opportunityId = '/' + element.Opportunity__c;
            element.opportunityName = element.Opportunity__r.Name;
            element.accountName = element.Account__r.Name;

            preprocessData.push(element);
        });

       return preprocessData;
    }
}