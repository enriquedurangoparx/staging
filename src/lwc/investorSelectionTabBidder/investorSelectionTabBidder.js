import { LightningElement, track, wire, api } from 'lwc';
import { CurrentPageReference } from 'lightning/navigation';
import { registerListener, unregisterAllListeners, fireEvent } from 'c/pubsub';

// custom apex methods
import getBidderInvestors from '@salesforce/apex/InvestorSelectionHelper.getBidderInvestors';

// import custom labels
import investorsSelectionUnderbidder from '@salesforce/label/c.InvestorsSelectionUnderbidder';
import investorsSelectionArea from '@salesforce/label/c.InvestorsSelectionArea';
import investorsSelectionBidderTabMessage from '@salesforce/label/c.InvestorsSelectionBidderTabMessage';
import investorsSelectionStartDate from '@salesforce/label/c.InvestorsSelectionStartDate';
import investorsSelectionEndDate from '@salesforce/label/c.InvestorsSelectionEndDate';
import investorsSelectionCurrentMonthPicklistLabel from '@salesforce/label/c.InvestorsSelectionCurrentMonthPicklistLabel';
import investorsSelectionCurrentQuarterPicklistLabel from '@salesforce/label/c.InvestorsSelectionCurrentQuarterPicklistLabel';
import investorsSelectionCurrentSemesterPicklistLabel from '@salesforce/label/c.InvestorsSelectionCurrentSemesterPicklistLabel';
import investorsSelectionCurrentYearPicklistLabel from '@salesforce/label/c.InvestorsSelectionCurrentYearPicklistLabel';
import investorsSelectionCustomPicklistLabel from '@salesforce/label/c.InvestorsSelectionCustomPicklistLabel';

import investorsSelectionBidderAccount from '@salesforce/label/c.InvestorsSelectionBidderAccount';
import investorsSelectionBidderLastBidDate from '@salesforce/label/c.InvestorsSelectionBidderLastBidDate';
import investorsSelectionBidderLastBidAmount from '@salesforce/label/c.InvestorsSelectionBidderLastBidAmount';
import investorsSelectionBidderVolume from '@salesforce/label/c.InvestorsSelectionBidderVolume';
import investorsSelectionBidderDealCaptain from '@salesforce/label/c.InvestorsSelectionBidderDealCaptain';
import investorsSelectionBidderTypeOfUse from '@salesforce/label/c.InvestorsSelectionBidderTypeOfUse';
import investorsSelectionBidderLocation from '@salesforce/label/c.InvestorsSelectionBidderLocation';
import investorsSelectionBidderRiskCategory from '@salesforce/label/c.InvestorsSelectionBidderRiskCategory';
import investorsSelectionBidderAssetShareDeal from '@salesforce/label/c.InvestorsSelectionBidderAssetShareDeal';
import investorsSelectionStartDateValidity from '@salesforce/label/c.InvestorsSelectionStartDateValidity';
import investorsSelectionEndDateValidity from '@salesforce/label/c.InvestorsSelectionEndDateValidity';


import generalLabelNone from '@salesforce/label/c.GeneralLabelNone';

const KEY_FIELD = 'Id';
const SORTED_BY = 'investorLastBidDate';

const BIDDER_TABLE_COLUMNS = [
    { label: ' ', fieldName: 'accountId', type: 'checkboxContainer', cellAttributes: { class: { fieldName: 'bgClass' }},typeAttributes: { disabledandchecked: {fieldName : 'disabledandchecked'}}, fixedWidth: 45, sortable: true },
    { label: investorsSelectionBidderAccount, fieldName: 'accountLink',  type: 'url', typeAttributes: { label: {fieldName: 'accountName'}, target:'_blank'}, sortable: true },
    { label: investorsSelectionBidderLastBidDate, fieldName: 'investorLastBidDate',  type: 'date-local', typeAttributes:{ month: "2-digit", day: "2-digit"}, cellAttributes: { class: { fieldName: 'bgClass' }}, sortable: true },
    { label: investorsSelectionBidderLastBidAmount, fieldName: 'investorLastBidAmount',  type: 'currency', cellAttributes: { class: { fieldName: 'bgClass' }}, sortable: true },
    { label: investorsSelectionBidderVolume, fieldName: 'opportunityTransactionVolume', type: 'number', cellAttributes: { class: { fieldName: 'bgClass' }}, sortable: true },
    { label: investorsSelectionBidderDealCaptain, fieldName: 'opportunityOwnerLink',  type: 'url', typeAttributes: { label: {fieldName: 'opportunityOwnerName'}, target:'_blank'}, sortable: true },
    { label: investorsSelectionBidderTypeOfUse, fieldName: 'opportunityPropertyOrPortfolioTypeOfUse', type: 'text',  cellAttributes: { class: { fieldName: 'bgClass' }}, sortable: true },
    { label: investorsSelectionBidderLocation, fieldName: 'opportunityPropertyOrPortfolioCity',  type: 'text', cellAttributes: { class: { fieldName: 'bgClass' }}, sortable: true },
    { label: investorsSelectionBidderRiskCategory, fieldName: 'opportunityRiskCategory',  type: 'text', cellAttributes: { class: { fieldName: 'bgClass' }}, sortable: true },
    { label: investorsSelectionBidderAssetShareDeal, fieldName: 'opportunityAssetShareDeal',  type: 'text', cellAttributes: { class: { fieldName: 'bgClass' }}, sortable: true },
    { label: '', type: 'actionButtonRow', fieldName: 'accountId', fixedWidth: 46, cellAttributes: { class: { fieldName: 'bgClass' }}}
];

const ACCOUNT_FORM_FIELDS = ['Name', 'Brand__c', 'BillingStreet', 'BillingCity'];
const CONTACT_FORM_FIELDS = ['AccountId', 'Title', 'FirstName', 'LastName', 'Position__c', 'Email'];

export default class InvestorSelectionTabBidder extends LightningElement
{
    @wire(CurrentPageReference) pageRef;
    opportunityId = new URL(window.location.href).searchParams.get('c__recordId');

    // variables that will pass to event to render datatable
    accountFormFields = ACCOUNT_FORM_FIELDS;
    contactFormFields = CONTACT_FORM_FIELDS;
    columns = BIDDER_TABLE_COLUMNS;
    keyField = KEY_FIELD;
    sortedBy = SORTED_BY;


    label = {   
        investorsSelectionUnderbidder,
        investorsSelectionArea,
        investorsSelectionBidderTabMessage,
        investorsSelectionStartDate,
        investorsSelectionEndDate,
        investorsSelectionCurrentMonthPicklistLabel,
        investorsSelectionCurrentQuarterPicklistLabel,
        investorsSelectionCurrentSemesterPicklistLabel,
        investorsSelectionCurrentYearPicklistLabel,
        investorsSelectionCustomPicklistLabel,
        investorsSelectionBidderAccount,
        investorsSelectionBidderLastBidDate,
        investorsSelectionBidderLastBidAmount,
        investorsSelectionBidderVolume,
        investorsSelectionBidderDealCaptain,
        investorsSelectionBidderTypeOfUse,
        investorsSelectionBidderLocation,
        investorsSelectionBidderRiskCategory,
        investorsSelectionBidderAssetShareDeal,
        investorsSelectionStartDateValidity,
        investorsSelectionEndDateValidity,
        generalLabelNone
    };

    @track
    picklistOptions = [
        {value: generalLabelNone, label: generalLabelNone},
        {value: investorsSelectionCurrentMonthPicklistLabel, label: investorsSelectionCurrentMonthPicklistLabel},
        {value: investorsSelectionCurrentQuarterPicklistLabel, label: investorsSelectionCurrentQuarterPicklistLabel},
        {value: investorsSelectionCurrentSemesterPicklistLabel, label: investorsSelectionCurrentSemesterPicklistLabel},
        {value: investorsSelectionCurrentYearPicklistLabel, label: investorsSelectionCurrentYearPicklistLabel},
        {value: investorsSelectionCustomPicklistLabel, label: investorsSelectionCustomPicklistLabel}
    ];

    @track
    picklistValue = generalLabelNone;

    @track
    startDateValue;

    @track
    endDateValue;

    @track
    dateInputsDisabled = true;

    @track
    showDateInputs = false;

    @track
    bidderRecords;

    INVESTOR_SELECTION_BIDDER_CONTROLL_EVENT_NAME = 'InvestorSelection__bidderSectionControll';

    bidderRecordsLimitSize;
    bidderRecordsLimitSizeStep;
    loadedDataSize;
    dataLoadingFinished = false;

    connectedCallback()
    {
        registerListener('InvestorSelectionTabBidder__bidderTabInitData', this.initData, this);
        registerListener('InvestorSelectionTabBidder__bidderTabLoadMoreRecords', this.loadMoreBidderRecords, this);
        this.initData();
    }

    initData()
    {
        this.picklistValue = this.label.generalLabelNone;
        this.showDateInputs = false;
        let object = {
            showSelectedRecords : true,
            hasRecordsAfterSearch : false,
            hasSelectedRecords : false
        }
        fireEvent(this.pageRef, this.INVESTOR_SELECTION_BIDDER_CONTROLL_EVENT_NAME, object);
    }

    getBidderInvestors()
    {
        this.initLazyLoadState();
        getBidderInvestors(
            { 
                opportunityId: this.opportunityId,
                startDate: this.startDateValue,
                endDate: this.endDateValue,
                limitSize: this.bidderRecordsLimitSize
            }
        ).then( result => {
            this.bidderRecords = JSON.parse(result);
            this.loadedDataSize = this.bidderRecords.length;
            this.showBidderRecords();
        });
    }

    initLazyLoadState()
    {
        this.bidderRecordsLimitSize = 25;
        this.bidderRecordsLimitSizeStep = 10;
        this.loadedDataSize = 0;
        this.dataLoadingFinished = false;
    }

    loadMoreBidderRecords()
    {
        this.bidderRecordsLimitSize = this.bidderRecordsLimitSize + this.bidderRecordsLimitSizeStep;
        getBidderInvestors(
            { 
                opportunityId: this.opportunityId,
                startDate: this.startDateValue,
                endDate: this.endDateValue,
                limitSize: this.bidderRecordsLimitSize
            }
        ).then( result => {
            this.bidderRecords = JSON.parse(result);

            if (this.loadedDataSize < this.bidderRecords.length)
            {
                this.loadedDataSize = this.bidderRecords.length;
                this.showBidderRecords();
            }
            else
            {
                // prevent lazy load if we don't have records anymore
                fireEvent(this.pageRef, 'InvestorSelection__bidderStopLoading', null);
            }
        });
    }

    handlePicklistChange(event) {
        this.picklistValue = event.detail.value;
        if (this.picklistValue === this.label.generalLabelNone)
        {
            this.showDateInputs = false;
            fireEvent(this.pageRef, 'InvestorSelection__resetPage', null);
        } 
        else if (this.picklistValue === this.label.investorsSelectionCustomPicklistLabel)
        {
            this.showDateInputs = true;
            this.dateInputsDisabled = false;

            this.calculateDateInputs();
            this.getBidderInvestors();
        }
        else
        {
            this.showDateInputs = true;
            this.dateInputsDisabled = true;

            this.calculateDateInputs();
            this.getBidderInvestors();
        }
    }

    calculateDateInputs()
    {
        let newStartDate = new Date();
        let newEndDate = new Date();
        let localDate = new Date();
        let currentDay = localDate.getDate();
        let currentMonth = localDate.getMonth();
        let currentYear = localDate.getFullYear();

        switch(this.picklistValue) 
        {
            case this.label.investorsSelectionCurrentMonthPicklistLabel:
                newStartDate = new Date(currentYear, currentMonth, 2);
                newEndDate = new Date(currentYear, currentMonth + 1, 1);
                break;
            case this.label.investorsSelectionCurrentQuarterPicklistLabel:
                let currentQuarter = parseInt(currentMonth / 3 ) + 1 ;
                switch(currentQuarter) 
                {
                    case 1:
                        newStartDate = new Date(currentYear, 0, 2);
                        newEndDate = new Date(currentYear, 3, 1);
                        break;
                    case 2:
                        newStartDate = new Date(currentYear, 3, 2);
                        newEndDate = new Date(currentYear, 6, 1);
                        break;
                    case 3:
                        newStartDate = new Date(currentYear, 6, 2);
                        newEndDate = new Date(currentYear, 9, 1);
                        break;
                    case 4:
                        newStartDate = new Date(currentYear, 9, 2);
                        newEndDate = new Date(currentYear, 12, 1);
                        break;
                }
                break;
            case this.label.investorsSelectionCurrentSemesterPicklistLabel:
                let currentSemester = parseInt(currentMonth / 6 ) + 1 ;
                switch(currentSemester) 
                {
                    case 1:
                        newStartDate = new Date(currentYear, 0, 2);
                        newEndDate = new Date(currentYear, 6, 1);
                        break;
                    case 2:
                        newStartDate = new Date(currentYear, 6, 2);
                        newEndDate = new Date(currentYear, 12, 1);
                        break;
                }
                break;
            case this.label.investorsSelectionCurrentYearPicklistLabel:
                newStartDate = new Date(currentYear, 0, 2);
                newEndDate = new Date(currentYear, 12, 1);

                break;
            default:
                newEndDate.setDate(currentDay + 1);
        }

        this.startDateValue = newStartDate.toISOString().substring(0,10);
        this.endDateValue = newEndDate.toISOString().substring(0,10);
    }

    showBidderRecords()
    {
        const object = {
            tableMetadata: {
                columns: this.columns,
                keyField: this.keyField,
                sortedBy: this.sortedBy
            },
            formMetadata: {
                fields: {
                    account: this.accountFormFields,
                    contact: this.contactFormFields
                },
                columns: 2
            },
            bidderRecords: JSON.stringify(this.bidderRecords)
        }

        //Notify investorSelectionDatatable.js to search for records.
        fireEvent(this.pageRef, 'InvestorSelection__bidderRecords', object);
    }

    handleStartDateChange(event)
    {
        this.startDateValue = event.detail.value;

        this.checkDateInputsValidity();
    }

    handleEndDateChange(event)
    {
        this.endDateValue = event.detail.value;

        this.checkDateInputsValidity();
    }

    checkDateInputsValidity()
    {
        let hasErrors = false;
        
        if (this.picklistValue !== this.label.generalLabelNone)
        {
            let startDateInput = this.template.querySelector('[data-id="dateInputStartDate"]');
            let endDateInput = this.template.querySelector('[data-id="dateInputEndDate"]');
            
            if (this.startDateValue > this.endDateValue)
            {
                startDateInput.setCustomValidity(this.label.investorsSelectionStartDateValidity);
                endDateInput.setCustomValidity(this.label.investorsSelectionEndDateValidity);
                hasErrors = true;
            } 
            else {
                startDateInput.setCustomValidity('');
                endDateInput.setCustomValidity('');
                hasErrors = false;
            }

            startDateInput.reportValidity();
            endDateInput.reportValidity();
        }
        
        if (!hasErrors)
        {
            this.getBidderInvestors();
        }
    }
}