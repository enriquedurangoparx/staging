({
    setPageref : function(component, event, helper) {
        var navLink = component.find("navLink");

        var pageRef = {
            type: 'standard__objectPage',
            attributes: {
                actionName: 'list',
                objectApiName: 'Embargo__c',

            },
            state: {
                filterName: "All"
            }
        };

        navLink.navigate(pageRef, true);
    },
})