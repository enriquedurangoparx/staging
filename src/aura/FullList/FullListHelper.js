/**
 * Created by dvor on 7/2/18.
 */
({
    showSpinner: function(component){
            var spinner = component.find("spinner");
            $A.util.removeClass(spinner, "slds-hide");
        }
})