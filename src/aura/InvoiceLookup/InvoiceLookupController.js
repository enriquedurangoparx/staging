({
    handleConfirm: function(component, event, helper) {
        if (component.get("v.selectedInvoiceId") === '') {
            helper.getInvoiceOptions(component);
        } else {
            helper.setCustomerInvoice(component);
        }
    },
    handleCancel: function(component, event, helper) {
        helper.clearForm(component);
    },
    selectInvoice: function(component, event, helper) {
        component.set("v.isDisabledConfirm", false);
    }
})