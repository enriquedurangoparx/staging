({
    getInvoiceOptions: function(component) {
        this.showSpinner(component, true);
        var invoiceNumber = component.get("v.invoiceNumber");
        invoiceNumber = invoiceNumber.trim();
        if (invoiceNumber !== '') {
            var action = component.get("c.getInvoiceOptionsByNumber");
            action.setParams({
                invoiceNumber: invoiceNumber
            });
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var invoices = response.getReturnValue();
                    if (invoices.length === 0) {
                        this.showMessage(component, 'Error', $A.get("$Label.c.No_records_found"));
                        component.set("v.invoices", []);
                    } else {
                        component.set("v.isDisabledConfirm", true);
                        component.set("v.invoices", invoices);
                    }
                } else if (state === "ERROR") {
                    if (response.getError()[0] && response.getError()[0].message) {
                        this.showMessage(component, 'Error', response.getError()[0].message);
                    }
                    component.set("v.invoices", []);
                    console.error('error: ', response.getError()[0].message);
                }
                this.showSpinner(component, false);
            });
            $A.enqueueAction(action);
        } else {
            this.showMessage(component, 'Error', $A.get("$Label.c.Please_enter_the_correct_value"));
            component.set("v.invoices", []);
            this.showSpinner(component, false);
        }
    },
    setCustomerInvoice: function(component) {
        this.showSpinner(component, true);
        var action = component.get("c.setCustomerInvoiceId");
        action.setParams({
            invoiceId: component.get("v.recordId"),
            customerInvoiceId: component.get("v.selectedInvoiceId")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var invoice = response.getReturnValue();
                var labelWithParams = $A.get("$Label.c.Invoice_0_from_Account_1_is_now_linked_to_this_invoice");
                labelWithParams = labelWithParams.replace('{0}', invoice.Name);
                labelWithParams = labelWithParams.replace('{1}', invoice.ONB2__Account__r.Name);
                this.showMessage(component, 'Success', labelWithParams);
                this.clearForm(component);
            } else if (state === "ERROR") {
                if (response.getError()[0] && response.getError()[0].message) {
                    this.showMessage(component, 'Error', response.getError()[0].message)
                }
                console.error('error: ', response.getError()[0].message);
            }
            this.showSpinner(component, false);
        });
        $A.enqueueAction(action);
    },
    clearForm: function(component) {
        component.set("v.invoices", []);
        component.set("v.invoiceNumber", "");
        component.set("v.selectedInvoiceId", "");
        component.set("v.isDisabledConfirm", false);
    },
    showSpinner: function(component, showSpinner) {
        component.set('v.isLoading', showSpinner);
    },
    showMessage: function(component, title, message) {
        var toast = $A.get("e.force:showToast");
        if (toast) {
            //fire the toast event in Salesforce app and Lightning Experience
            toast.setParams({
                "title": title,
                "message": message
            });
            toast.fire();
        }
    }
})