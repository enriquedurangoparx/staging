<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>View</actionName>
        <comment>Action override created by Lightning App Builder during activation.</comment>
        <content>OSClusterManagement</content>
        <formFactor>Large</formFactor>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Flexipage</type>
        <pageOrSobjectType>Opportunity</pageOrSobjectType>
    </actionOverrides>
    <brand>
        <headerColor>#D13EA7</headerColor>
        <logo>X1200pxFlurkarte</logo>
        <logoVersion>1</logoVersion>
        <shouldOverrideOrgTheme>false</shouldOverrideOrgTheme>
    </brand>
    <formFactors>Large</formFactors>
    <isNavAutoTempTabsDisabled>false</isNavAutoTempTabsDisabled>
    <isNavPersonalizationDisabled>false</isNavPersonalizationDisabled>
    <label>OS Cluster Management</label>
    <navType>Standard</navType>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>OSClusterManagement</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Opportunity</pageOrSobjectType>
        <recordType>Opportunity.OSClusterManagement</recordType>
        <type>Flexipage</type>
        <profile>OS Cluster Manager</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>OSClusterManagement</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Opportunity</pageOrSobjectType>
        <recordType>Opportunity.OSClusterManagement</recordType>
        <type>Flexipage</type>
        <profile>Admin</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>OSClusterManagement</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Opportunity</pageOrSobjectType>
        <recordType>Opportunity.OSClusterManagement</recordType>
        <type>Flexipage</type>
        <profile>OS Cluster Manager Platform</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>Tab</actionName>
        <content>HomepageColliersFinance</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>standard-home</pageOrSobjectType>
        <type>Flexipage</type>
        <profile>Colliers Finance</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>Tab</actionName>
        <content>KamHomePage</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>standard-home</pageOrSobjectType>
        <type>Flexipage</type>
        <profile>Admin</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>Tab</actionName>
        <content>KamHomePage</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>standard-home</pageOrSobjectType>
        <type>Flexipage</type>
        <profile>Key Account Manager (Analytics)</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>Tab</actionName>
        <content>KamHomePage</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>standard-home</pageOrSobjectType>
        <type>Flexipage</type>
        <profile>Key Account Manager</profile>
    </profileActionOverrides>
    <tabs>standard-Account</tabs>
    <tabs>standard-Contact</tabs>
    <tabs>standard-Opportunity</tabs>
    <tabs>Municipality__c</tabs>
    <tabs>Cluster__c</tabs>
    <tabs>Parcel__c</tabs>
    <tabs>standard-report</tabs>
    <tabs>AccountToOpportunity__c</tabs>
    <uiType>Lightning</uiType>
    <utilityBar>OS_Cluster_Management_UtilityBar</utilityBar>
</CustomApplication>
